import { createMuiTheme, colors } from '@material-ui/core';
import typography from './typography';


const theme = createMuiTheme({
    palette: {
        text: {
            primary: '#fff',
            success: '#5CC075',
            error: '#E72D16',
            warning: '#F7C230'
        }
    },
    shadow:{
        small: '0 .125rem .25rem rgba(0,0,0,.075)',
        medium: '0 4px 6px -1px rgba(0,0,0,0.1),0 2px 4px -1px rgba(0,0,0,0.06)',
        large: '0 10px 15px -3px rgba(0,0,0,0.1),0 4px 6px -2px rgba(0,0,0,0.05)',
        xlarge: '0 .5rem 1rem rgba(0,0,0,.15) !important'
    },
    header: {
        primary: {
            main: '#576bfe'
        },
        secondary: {

        },
        component: {
            primary: '#4f64eb',
            secondary: '#7b8af1',
            third: '#21BA45'
        }
    },
    group: {
        grPimpinan :{
            main: '#212121',
        },
        grEselon1: {
            main: '#B71C1C'
        },
        grEselon2: {
            main: '#E91E63'
        },
        grEselon3: {
            main: '#9C27B0'
        },
        grEselon4: {
            main: '#3F51B5'
        },
        grEselon5: {
            main: '#2196F3'
        },
        grUnitKearsipan: {
            main: '#009688'
        },
        grFungsionalUmum: {
            main: '#1DE9B6'
        },
        grPengelolaSurat: {
            main: '#4CAF50'
        },
        grSekertarisPimpinan: {
            main: '#CDDC39'
        },
        grPengelolaKeuangan: {
            main: '#FBC02D'
        },
        grFungsionalTertentu: {
            main: '#1DE9B6'
        },
        grPaniteraPengganti: {
            main: '#FF5722'
        },
        grPeneliti: {
            main: '#795548'
        },
        grAuditor: {
            main: '#9E9E9E'
        },
        grPranataKomputer: {
            main: '#607D8B'
        },

    },
    typography
})

export default theme;