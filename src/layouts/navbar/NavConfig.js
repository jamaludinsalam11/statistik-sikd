import SvgIconStyle from "../../component/sidebar/SvgIconStyle";
import { PATH_ADMIN } from "../../routes/paths";

const getIcon = (name) => <SvgIconStyle src={`/icons/${name}`} sx={{ width: 1, height: 1 }} />

const ICONS = {
    blog: getIcon('ic_blog'),
    cart: getIcon('ic_cart'),
    chat: getIcon('ic_chat'),
    mail: getIcon('ic_mail'),
    user: getIcon('ic_user'),
    kanban: getIcon('ic_kanban'),
    banking: getIcon('ic_banking'),
    booking: getIcon('ic_booking'),
    invoice: getIcon('ic_invoice'),
    calendar: getIcon('ic_calendar'),
    ecommerce: getIcon('ic_ecommerce'),
    analytics: getIcon('ic_analytics'),
    dashboard: getIcon('ic_dashboard'),
};


const navConfig = [
    // STATISTIK (OLD BY QUERY PARAMS)
    // --------------------------------------------------
    {
        subheader: 'statistik',
        items: []
    },

    // ADMIN 
    // --------------------------------------------------
    {
        subheader: 'admin',
        items: [
            { 
                title: 'child1', 
                path: PATH_ADMIN.child1, 
                icon: ICONS.dashboard,
                children: [
                    { title: 'child11', path: PATH_ADMIN.child1.child11 },
                    { title: 'child12', path: PATH_ADMIN.child1.child12 },
                    { title: 'child13', path: PATH_ADMIN.child1.child13 },
                ] 
            },
            { 
                title: 'child2', 
                path: PATH_ADMIN.child2,
                icon: ICONS.banking,
                children: [
                    { title: 'child21', path: PATH_ADMIN.child2.child21 },
                    { title: 'child22', path: PATH_ADMIN.child2.child22 },
                    { title: 'child23', path: PATH_ADMIN.child2.child23 },
                ]
                
            },
            { 
                title: 'child3', 
                path: PATH_ADMIN.child3,
                icon: ICONS.banking,
                children: [
                    { title: 'child31', path: PATH_ADMIN.child3.child31 },
                    { title: 'child32', path: PATH_ADMIN.child3.child32 },
                    { title: 'child33', path: PATH_ADMIN.child3.child33 },
                ]
            },
        ]
    }
]



export default navConfig;