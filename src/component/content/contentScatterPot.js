import React, { useState, useEffect, useCallback, useMemo, useRef} from 'react'
import { ScatterPlot ,ResponsiveScatterPlot } from '@nivo/scatterplot'
import { Grid , Paper, Typography, Divider, Chip} from '@material-ui/core'
import {useLocation} from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';
// import * as wjChart from '@grapecity/wijmo.react.chart';
import * as chart from '@grapecity/wijmo.chart';
// import * as wjChartAnnotation from '@grapecity/wijmo.react.chart.annotation';
// import '@grapecity/wijmo.styles/wijmo.css';
import moment from 'moment';
import ScatterPlotDialog from './contentScatterPotDialog';
import {Chart,  Scatter } from 'react-chartjs-2';
import annotationPlugin from "chartjs-plugin-annotation";
import datalabelsPlugin from "chartjs-plugin-datalabels";
import Avatar from '@material-ui/core/Avatar';
import FolderIcon from '@material-ui/icons/Folder';
import {
    Chart as ChartJS,
  defaults,
    LinearScale,
    PointElement,
    LineElement,
    Tooltip,
    Legend,
  } from 'chart.js';


  import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ContentScatterPotKuadran from './contentScatterPotKuadran';
import ModalAllNbvUnitkerja from './modalNinebox/modalNineboxAllNbvUnitkerja';
Chart.register([annotationPlugin,
  // datalabelsPlugin,
  LinearScale, PointElement, LineElement, Tooltip, Legend])
// Scatter.register([annotationPlugin,datalabelsPlugin])
const datadummy = [
    { people_name: 'Black Panther', kinerja: 56, potensial_talenta: 65 },
    { people_name: 'Avengers: Infinity War', kinerja: 8.5, potensial_talenta: 67 },
    { people_name: 'Incredibles 2', kinerja: 87, potensial_talenta: 89 },
    { people_name: 'Jurassic World: Fallen Kingdom', kinerja: 78, potensial_talenta: 97 },
    
];
const dataDummy2 = {
    datasets: [
        {
          label: 'Eselon I',
          data: [
            { x: 90, y: 80, name: 'M Guntur Hamzah', jabatan: 'Eselon I', kuadran: 'Kuadran IV', box: '8', nbv: 92 },
            { x: 85, y: 82, name: 'Muhidim', jabatan: 'Eselon I', kuadran: 'Kuadran IV', box: '8', nbv: 85 },
          ],
          backgroundColor: 'rgba(5, 99, 132, 1)',
        },
    ],
}

const ContentScatterPot = () => {
    const classes = useStyles()
    const chartRef = useRef();
    const [pallete, setPallete] = useState(chart.Palettes.light)
    const [data, setData] = useState(datadummy)
    const [eselon1, setEselon1] = useState(datadummy)
    const [eselon2, setEselon2] = useState(datadummy)
    const [eselon3, setEselon3] = useState(datadummy)
    const [eselon4, setEselon4] = useState(datadummy)
    const [eselon5, setEselon5] = useState(datadummy)
    const [other, setOther] = useState(datadummy)

    const [kuadran1, setKuadran1] = useState('')
    const [kuadran2, setKuadran2] = useState('')
    const [kuadran3a, setKuadran3a] = useState('')
    const [kuadran3b, setKuadran3b] = useState('')
    const [kuadran4, setKuadran4] = useState('')
    const [percentageKuadran, setPercentageKuadran] = useState({
      kuadran1: 0,
      kuadran2: 0,
      kuadran3a: 0,
      kuadran3b: 0,
      kuadran4: 0,
    })

    const [jumlahPegawai, setJumlahPegawai] = useState(0)
    const [blmMasukKuadran, setBlmMasukKuadran] = useState(0)

    const [unitkerja, setUnitkerja] = useState(null)
    const [options, setOptions] = useState(null)
    const [dataChartjs, setDataChartjs] = useState(null)

    const [dataModalAllNbvUnitkerja, setDataModalAllNbvUnitkerja] = useState([])
    const [dataModalAllNbvUnitkerjaCutOff, setDataModalAllNbvUnitkerjaCutOff] = useState([])
    const [dataScatterPotSeparate, setDataScatterPotSeparate] = useState([])
    const [openDialog, setOpenDialog] = useState(false)
    const [openModalKuadran, setOpenModalKuadran] = useState(false)
    const [dataModalKuadran, setDataModalKuadran] = useState([])
    const [paramsKuadran, setParamsKuadran] = useState(0)
    const [userTarget, setUserTarget] = useState(null)
    const [modalAllNbvUnitkerja, setModalAllNbvUnitkerja] = useState(false)
    let search  = useLocation().search
    const roleid = new URLSearchParams(search).get('select_id')
    const tgl1 = new URLSearchParams(search).get('tgl1')
    const tgl2 = new URLSearchParams(search).get('tgl2')

    const tahun = moment(tgl1).format('YYYY')
    console.log(openDialog)
    // componentWill
    useEffect(()=> {
        const fetchScat = async() => {
            try{
                // const api = process.env.REACT_APP_API_APPS_FASTIFY2+'/sikd/api/v1/ninebox/scatterplot/'+tahun+'/'+roleid
                /** not used on 2023-03-15 */
                // const apiProd = process.env.REACT_APP_API_APPS_FASTIFY2+'/sikd/api/v1/ninebox/scatterplot/v2/'+tahun+'/'+roleid
                // const apiDev = 'http://localhost:3001/sikd/api/v1/ninebox/scatterplot/v2/'+tahun+'/'+roleid
                const apiProd = process.env.REACT_APP_API_APPS_NEST_SERVICE_32_NEST_2+'/scatterplot/v3/'+tahun+'/'+roleid
                const apiDev = 'http://localhost:3025/scatterplot/v3/'+tahun+'/'+roleid
                const api = process.env.NODE_ENV === "development" ? apiDev : apiProd
                // const api2 = 'http://localhost:3001/sikd/api/v1/ninebox/scatterplot/v2/'+tahun+'/'+roleid
                // console.log('fetchScatterPlot',api)
                // const fetchScatterPlot  = await fetch(api)
                // const resScatterPlot = await fetchScatterPlot.json()

                const fetchScatterPlot2 = await fetch(api)
                const resScatterPlot2 = await fetchScatterPlot2.json()
                console.log('resScatterPlot2',resScatterPlot2)
                // setData(resScatterPlot.result)
                // setEselon1(resScatterPlot.eselon1)
                // setEselon2(resScatterPlot.eselon2)
                // setEselon3(resScatterPlot.eselon3)
                // setEselon4(resScatterPlot.eselon4)
                // setEselon5(resScatterPlot.eselon5)
                // setOther(resScatterPlot.other)
                // setPallete(chart.Palettes.light)

                setJumlahPegawai(resScatterPlot2.jumlah_pegawai.total)
                setUnitkerja(resScatterPlot2.unitkerja == null || resScatterPlot2.unitkerja === 'SEKJEN' ? null : resScatterPlot2.unitkerja_full) 
                setBlmMasukKuadran(resScatterPlot2.jumlah_pegawai_kuadran.belummasukkuadran)
                setDataChartjs(resScatterPlot2)

                setKuadran1(resScatterPlot2.list_pegawai_kuadran.kuadran1)
                setKuadran2(resScatterPlot2.list_pegawai_kuadran.kuadran2)
                setKuadran3a(resScatterPlot2.list_pegawai_kuadran.kuadran3a)
                setKuadran3b(resScatterPlot2.list_pegawai_kuadran.kuadran3b)
                setKuadran4(resScatterPlot2.list_pegawai_kuadran.kuadran4)
                setPercentageKuadran({
                  kuadran1: resScatterPlot2.percentage_pegawai_kuadran.kuadran1,
                  kuadran2: resScatterPlot2.percentage_pegawai_kuadran.kuadran2,
                  kuadran3a: resScatterPlot2.percentage_pegawai_kuadran.kuadran3a,
                  kuadran3b: resScatterPlot2.percentage_pegawai_kuadran.kuadran3b,
                  kuadran4: resScatterPlot2.percentage_pegawai_kuadran.kuadran4,
                })

                setDataScatterPotSeparate(resScatterPlot2.datasets_separate)
                setDataModalAllNbvUnitkerja(resScatterPlot2.all_unitkerja)
                setDataModalAllNbvUnitkerjaCutOff(resScatterPlot2.all_unitkerja_cutoff)
                // setDataModalKuadran({ 
                //   kuadran1: resScatterPlot2.list_pegawai_kuadran.kuadran1,
                //   kuadran2: resScatterPlot2.list_pegawai_kuadran.kuadran2,
                //   kuadran3a: resScatterPlot2.list_pegawai_kuadran.kuadran3a,
                //   kuadran3b: resScatterPlot2.list_pegawai_kuadran.kuadran3b,
                //   kuadran4: resScatterPlot2.list_pegawai_kuadran.kuadran4,
                // })
                // console.log(resScatterPlot2.list_pegawai_kuadran.kuadran1)
                // console.log(resScatterPlot2.list_pegawai_kuadran.kuadran2)
                const chartjs3Options = {
                    maintainAspectRatio: false,
                    // responsive: true,
                    layout: {
                        padding: {
                            left: 0
                        }
                    },
                    scales: {
                        y: {
                          min: 0,
                          max: 100,
                        //   beginAtZero: true, 
                          grid: {
                            display: false
                          },
                          title: {
                            display: true,
                            text: 'Kinerja',
                            color: '#911',
                            font: {
                              family: 'Poppins, sans-serif',
                              size: 14,
                              weight: 'bold',
                              lineHeight: 1.2
                            }
                          }
                        },
                        x: {
                          min: 0,
                          max: 100,
                          grid: {
                            display: false
                          },
                          title: {
                            display: true,
                            text: 'Potensi Talenta',
                            color: '#911',
                            font: {
                              family: 'Poppins, sans-serif',
                              size: 14,
                              weight: 'bold',
                              lineHeight: 0.5
                            }
                          }
                        }
                    },
                    elements: {
                        point: {
                            borderWidth: 3,
                            radius: 6
                        }
                    },
                    interaction:{
                        mode: 'nearest'
                    },
                    plugins:{
                        htmlLegend: {
                            // ID of the container to put the legend in
                            containerID: 'legend-container',
                        },
                        legend:{
                            position: 'right',
                            
                            labels: {
                                usePointStyle: true,
                                
                            },
                        },
                        tooltip:{
                            // position: 'cursor',
                            callbacks: {
                                title: function(tooltipItems, data) {
                                    const itterate = tooltipItems.map(function(x){ 
                                        const obj  = [
                                            x.raw.people_name,
                                            x.raw.people_desc
                                        ]
                                        return obj
                                    })
                                    // console.log('itterate',itterate)
                                    const result = [
                                        tooltipItems[0].raw.people_name,
                                        
                                        tooltipItems[0].raw.people_desc,
                                        // "==============================",
                                        // tooltipItems[0].raw.unitkerja_full,
                
                                    ]
                                   return itterate
                                }, 
                                afterTitle: function(tooltipItems, data) {
                                    // const itterate = tooltipItems.map((x,i )=> x.raw.people_name)
                                    // console.log(tooltipItems[0])
                                    // const result = [
                                    //     tooltipItems[0].raw.people_name,
                                    //     tooltipItems[0].raw.people_desc,
                                    //     ,
                
                                    // ]
                                //    return tooltipItems[0].raw.unitkerja_full
                                },
                                label: function(context) {
                                    let label = context.dataset.label || '';
                                    // console.log(context)
                                    const result = [
                                        'Unitkerja: ' + ' ' + context.raw.unitkerja_full,
                                        'Jabatan: ' + ' ' + context.raw.jabatan,
                                        'Kinerja: ' + ' ' + context.raw.kinerja,
                                        'Potensial Talenta: ' + ' ' + context.raw.potensial_talenta,
                                        'Kuadran: ' + ' ' + context.raw.kuadran,
                                        'Box: ' + ' ' + context.raw.box,
                                        'NBV: ' + ' ' + context.raw.nbv,
                                    ]
                                  
                                    return result;
                                }
                            }
                        },
                        annotation: {
                            annotations: {
                                
                                box1: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 0,
                                    xMax: 40,
                                    yMin: 0,
                                    yMax: 40,
                                    backgroundColor: "rgba(133,28,118,0.8)",
                                    borderColor: "rgba(133,28,118,0.8)",
                                    
                                },
                                kuadran21: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 40,
                                    xMax: 80,
                                    yMin: 40,
                                    yMax: 80,
                                    backgroundColor: "rgb(252,252,153)",
                                    borderColor: "rgb(252,252,153)"
                                },
                                kuadran22: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 0,
                                    xMax: 40,
                                    yMin: 40,
                                    yMax: 80,
                                    backgroundColor: "rgb(252,252,153)",
                                    borderColor: "rgb(252,252,153)"
                                },
                                kuadran23: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 40,
                                    xMax: 80,
                                    yMin: 0,
                                    yMax: 40,
                                    backgroundColor: "rgb(252,252,153)",
                                    borderColor: "rgb(252,252,153)"
                                },
                                kuadran31: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 80,
                                    xMax: 100,
                                    yMin: 0,
                                    yMax: 100,
                                    backgroundColor: "rgb(180,204,78, 1)",
                                    borderColor: "rgb(180,204,78, 1)"
                                },
                                kuadran32: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 0,
                                    xMax: 100,
                                    yMin: 80,
                                    yMax: 100,
                                    backgroundColor:"rgb(180,204,78, 1)",
                                    borderColor:"rgb(180,204,78, 1)"
                                },
                                kuadran4: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 80,
                                    xMax: 100,
                                    yMin: 80,
                                    yMax: 100,
                                    backgroundColor:"rgb(39,118,71, 1)",
                                    borderColor:"rgb(39,118,71, 1)",
                                    // click: function(context, event) {
                                    //   console.log('im kuadran 4')
                                    // },
                                    // enter: function(context, event) {
                                    //   console.log('im kuadran 4 with mouseover')
                                    // },
                                },
                                label1: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 20,
                                    yValue: 15,
                                    backgroundColor: 'rgb(239,255,253,0)',
                                    color: 'rgb(239,255,253)',
                                    content: ['Kuadran I'],
                                    font: {
                                      size: 14,
                                    },
                                    click: function(context, event) {
                                      // setParamsKuadran(1)
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                      setParamsKuadran(1)
                                      handleKuadran(1, datass)
                                      // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                      // setOpenModalKuadran(true)
                                    }
                                },
                                label12: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 20,
                                    yValue: 10,
                                    backgroundColor: 'rgb(239,255,253,0)',
                                    color: 'rgb(239,255,253)',
                                    content: [`(Pegawai: ${resScatterPlot2.jumlah_pegawai_kuadran.kuadran1})`],
                                    font: {
                                      size: 12,
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                      setParamsKuadran(1)
                                      handleKuadran(1, datass)
                                    }
                                },
                               
                                label31: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 35,
                                    yValue: 92,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: ['Kuadran III.a'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran3a
                                      setParamsKuadran(3)
                                      handleKuadran(3, datass)
                                      
                                      // setDataModalKuadran(kuadran3a == null || kuadran3a.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran3a : kuadran3a) 
                                      // setTimeout(function(){
                                      //   setOpenModalKuadran(true)
                                      // }, 1000)
                                      // handleKuadran(3)
                                    }
                                },
                                label32: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 35,
                                    yValue: 85,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: [`(Pegawai: ${resScatterPlot2.jumlah_pegawai_kuadran.kuadran3a})`],
                                    font: {
                                      size: 12
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran3a
                                      setParamsKuadran(3)
                                      handleKuadran(3, datass)
                                    }
                                },
                                label41: {
                                    type: 'label', 
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 95,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    color: 'rgb(239,255,253)',
                                    content: ['Kuadran'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran4
                                      setParamsKuadran(5)
                                      handleKuadran(5, datass)
                                    }
                                },
                                label42: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 90,
                                    backgroundColor: 'rgb(239,255,253,0)',
                                    color: 'rgb(239,255,253)',
                                    content: [' IV'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran4
                                      setParamsKuadran(5)
                                      handleKuadran(5, datass)
                                    }
                                },
                                label43: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 85,
                                    backgroundColor: 'rgb(239,255,253,0)',
                                    color: 'rgb(239,255,253)',
                                    content: [`(Pegawai: ${resScatterPlot2.jumlah_pegawai_kuadran.kuadran4})`],
                                    font: {
                                      size: 12
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran4
                                      setParamsKuadran(5)
                                      handleKuadran(5, datass)
                                    }
                                },
                                label321: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 65,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: ['K'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran3b
                                      setParamsKuadran(4)
                                      handleKuadran(4, datass)
                                    }
                                },
                                label322: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 60,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: ['u'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran3b
                                      setParamsKuadran(4)
                                      handleKuadran(4, datass)
                                    }
                                },
                                label323: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 55,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: ['a'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran3b
                                      setParamsKuadran(4)
                                      handleKuadran(4, datass)
                                    }
                                },
                                label324: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 50,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: ['d'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran3b
                                      setParamsKuadran(4)
                                      handleKuadran(4, datass)
                                    }
                                },
                                label325: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 45,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: ['r'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran3b
                                      setParamsKuadran(4)
                                      handleKuadran(4, datass)
                                    }
                                },
                                label326: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 40,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: ['a'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran3b
                                      setParamsKuadran(4)
                                      handleKuadran(4, datass)
                                    }
                                },
                                label327: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 35,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: ['n'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran3b
                                      setParamsKuadran(4)
                                      handleKuadran(4, datass)
                                    }
                                },
                                label328: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 25,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: ['III.b'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran3b
                                      setParamsKuadran(4)
                                      handleKuadran(4, datass)
                                    }
                                },
                                label329: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 90,
                                    yValue: 10,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: [`(Pegawai: ${resScatterPlot2.jumlah_pegawai_kuadran.kuadran3b})`],
                                    font: {
                                      size: 12
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran3b
                                      setParamsKuadran(4)
                                      handleKuadran(4, datass)
                                    }
                                },
                                label2: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 60,
                                    yValue: 15,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: ['Kuadran II'],
                                    font: {
                                      size: 14
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran2
                                      setParamsKuadran(2)
                                      handleKuadran(2, datass)
                                    }
                                },
                                label21: {
                                    type: 'label',
                                    drawTime: 'beforeDatasetsDraw',
                                    xValue: 60,
                                    yValue: 10,
                                    backgroundColor: 'rgba(245,245,245,0)',
                                    content: [`(Pegawai: ${resScatterPlot2.jumlah_pegawai_kuadran.kuadran2} )`],
                                    font: {
                                      size: 12
                                    },
                                    click: function(context, event) {
                                      const datass = resScatterPlot2.list_pegawai_kuadran.kuadran2
                                      setParamsKuadran(2)
                                      handleKuadran(2, datass)
                                    }
                                },
                            }
                        }
                    }
                }
                setOptions(chartjs3Options)
            } catch (err) {
                console.log(err)
            }
        }
        fetchScat()
    },[openDialog])
    const getElementsAtEvent = (event) => {
        
        if(event.length !== 0){
            if(roleid === 'uk.1.1' || roleid === 'uk.1.1.18.26'){
                const userTarget = event[0].element.$context.raw
                setOpenDialog(true)
                setUserTarget(userTarget)
            }
        } else {
        }
    }


    const handleCloseAllNbvUnitkerja = () => {
      document.body.style.overflow = 'auto';
      setModalAllNbvUnitkerja(false)
    } 
    const handleKuadran = async(params, data) => {
      if(params == 0){
        setDataModalKuadran([])
      } else if (params == 1) {
        setDataModalKuadran(data == null ? [] : data)
      } else if (params == 2) {
        setDataModalKuadran(data == null ? [] : data)
      } else if (params == 3) {
        setDataModalKuadran(data == null ? [] : data)
      } else if (params == 4) {
        setDataModalKuadran(data == null ? [] : data)
      } else if (params == 5) {
        setDataModalKuadran(data == null ? [] : data)
      }

      setOpenModalKuadran(true)
    }
    // console.log(kuadran1)
    // console.log(kuadran2)
    // console.log(kuadran3a)
    // console.log(kuadran3b)
    // console.log(kuadran4)
    // useEffect(() => {
    //   handleKuadran(paramsKuadran)
    // },[paramsKuadran])
 
    const handleCloseDialog = () => {
        document.body.style.overflow = 'auto';
        setOpenDialog(false);
      };
    const handleCloseModalKuadran = () => {
      document.body.style.overflow = 'auto';
      setDataModalKuadran([])
      setOpenModalKuadran(false);
    };
    const initChart = (sender) => {
        sender.dataLabel.content = customDataLabel;
        sender.tooltip.content = customTooltip;
    };

    const customTooltip = (ht) => {
        let item = ht.item;
        return `<b>Nama:</b> ${item.people_name} </br><b>
        Kinerja:</b> ${item.kinerja}</br><b>
        Potensial Talenta:</b> ${item.potensial_talenta}</br><b>
        NBV:</b> ${item.nbv}</br><b>
        Jabatan:</b> ${item.jabatan}</br><b>
        Kuadran:</b> ${item.kuadran}</br><b>
        Box:</b> ${item.box}</br><b>
        
        `;
    }
    const customDataLabel = (ht) => {
        return null
    };

    const customFormatter = (engine, ht, defaultRenderer) => {
        if (ht.jabatan === 'Eselon II' || ht.jabatan === "Eselon I") {
            engine.stroke = 'white';
            engine.fill = 'white';
        }
        defaultRenderer();
    }


    return (
        <>
        <Grid item xs={8} >
            <Paper className={classes.contentTTE} style={{textAlign: 'center',position: 'relative'}}>
                <Typography variant="h5" gutterBottom >Kuadran Umum Kinerja dan potensi talenta Kolektif</Typography>  
                <Typography variant="subtitle2" gutterBottom >{unitkerja}</Typography>  
                {/* <Divider/> */}
                {unitkerja == null ? '' : <Divider/> }
                
                <div style={{postition: 'inherit', marginTop: '.5em', marginRight: '2%', textAlign: 'right', zIndex: 1000}}>
                    <Chip
                        avatar={<Avatar>{jumlahPegawai}</Avatar>}
                        label="Total Pegawai"                    
                        onClick={() => setModalAllNbvUnitkerja(true)}
                        color="primary"
                        style={{ marginBottom: '-200px',postition: 'inherit',}}
                        // onDelete={handleDelete}
                        // deleteIcon={<DoneIcon />}
                    />
                </div>
                <div style={{position: 'static', height: '500px', paddingTop: '0em'}}>
                    {
                        dataChartjs ?
                            <Scatter
                            options={options}
                            data={dataChartjs}
                            getElementAtEvent={getElementsAtEvent}
                            // style={{maxHeight: '480px', maxWidth: '450px'}}
                            />
                            : ''
                    }
                    
                </div>
        

                <ModalAllNbvUnitkerja 
                    dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja}
                    dataModalAllNbvUnitkerjaCutOff={dataModalAllNbvUnitkerjaCutOff}
                    dataScatterPotSeparate={dataScatterPotSeparate}
                    modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                    handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                    tahun_now={tahun}
                    select_id1={roleid}
                />

            </Paper>
        </Grid>
        <Grid item xs={4}>
            <Paper className={classes.contentTTE} >
                <Typography variant="h5" gutterBottom style={{textAlign: 'center',}}>Keterangan </Typography>  
                <Divider/>
                <Grid style={{paddingTop: '2em'}}>
                    <Typography variant="h5" gutterBottomstyle={{textAlign: 'left',}} >Kuadran I</Typography>  
                    <Typography variant="caption" gutterBottom style={{fontSize: 12}} >Kinerja di bawah ekspektasi dan potensi talenta rendah</Typography>
                </Grid>
                <Grid style={{paddingTop: '1em'}}>
                    <Typography variant="h5" gutterBottomstyle={{textAlign: 'left',}} >Kuadran II</Typography>  
                    <Typography variant="caption" gutterBottom style={{fontSize: 12}} >Kinerja sesuai ekspektasi dan / atau potensi talenta sedang</Typography><br></br>
                    <ol type='a' style={{paddingLeft: '2em', fontSize: 12 }}>
                        <li><Typography variant="caption" gutterBottom style={{fontSize: 12}} >Kinerja sesuai ekspektasi dan potensi talenta rendah</Typography><br></br></li>
                        <li><Typography variant="caption" gutterBottom style={{fontSize: 12}} >Kinerja dibawah ekspektasi dan potensi talenta sedang</Typography><br></br></li>
                    </ol>
                    {/* <Typography variant="caption" gutterBottom style={{fontSize: 12}} >a. Kinerja sesuai ekspektasi dan potensi talenta rendah</Typography><br></br>
                    <Typography variant="caption" gutterBottom style={{fontSize: 12}} >b. Kinerja dibawah ekspektasi dan potensi talenta sedang</Typography><br></br> */}
                </Grid>
                <Grid style={{paddingTop: '1em'}}>
                    <Typography variant="h5" gutterBottomstyle={{textAlign: 'left',}} >Kuadran III</Typography>  
                    <Typography variant="caption" gutterBottom >Kinerja melampaui ekspektasi atau potensi talenta tinggi</Typography><br></br>
                    <ol type='a' style={{paddingLeft: '2em', fontSize: 12 }}>
                        <li><Typography variant="caption" gutterBottom style={{fontSize: 12}} >Kinerja melampaui ekspektasi dan potensi talenta sedang</Typography><br></br></li>
                        <li><Typography variant="caption" gutterBottom style={{fontSize: 12}} >Kinerja sesuai ekspektasi dan potensi talenta tinggi</Typography><br></br></li>
                    </ol>
                    {/* <Typography variant="caption" gutterBottom >a. Kinerja melampaui ekspektasi dan </Typography><br></br>
                    <Typography variant="caption" gutterBottom style={{marginLeft: '15px'}}> potensi talenta sedang</Typography><br></br>
                    <Typography variant="caption" gutterBottom >b. Kinerja sesuai ekspektasi dan potensi talenta tinggi</Typography><br></br> */}
                </Grid>
                <Grid style={{paddingTop: '1em'}}>
                    <Typography variant="h5" gutterBottomstyle={{textAlign: 'left',}} >Kuadran IV</Typography>  
                    <Typography variant="caption" gutterBottom >Kinerja dan potensi talenta excellent (<span style={{fontStyle: 'italic'}}>successor candidate</span>)</Typography>
                </Grid>

            </Paper>
        </Grid>

        {/* <Grid item xs={8}>
            <Scatter
                options={options}
                data={dataChartjs ? dataChartjs : dataDummy2}
                // onClick={onClickChartjs}
                getElementAtEvent={getElementsAtEvent}
                
            />
        </Grid> */}
        <ContentScatterPotKuadran 
          openModalKuadran={openModalKuadran} 
          data={dataModalKuadran} 
          paramsKuadran={paramsKuadran} 
          percentageKuadran={percentageKuadran} 
          handleCloseModalKuadran={() => handleCloseModalKuadran()} 
        />
        
        <ScatterPlotDialog openDialog={openDialog} userTarget={userTarget} handleCloseDialog={handleCloseDialog} tgl1={tgl1} tgl2={tgl2} roleid={roleid}/> 
        </>
    )
}

export default ContentScatterPot


const useStyles = makeStyles((theme) => ({

	content: {
		padding: theme.spacing(2),
		textAlign: 'center',
		borderRadius: 15,
		backgroundColor: 'white',
		color: theme.palette.text.secondary,
		marginTop: '-3%',
		zIndex: 1,
		position: 'relative',
	},
	contentDate: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		borderRadius: 25
	},
	contentDateSD: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		boxShadow: 'none'
	},
	contentDivider: {
		paddingTop: theme.spacing(6),
		paddingBottom: theme.spacing(6),
	},
	contentTTE: {
		padding: theme.spacing(2),
		textAlign: 'left',
		color: theme.palette.text.secondary,
		height: '650px',
		boxShadow: theme.shadow.large,
	},
	contentTTEvalue: {
		paddingTop: theme.spacing(7),
		paddingBottom: theme.spacing(1),
		fontWeight: 500,
	},

}));

const theme = {
    tooltip: {
      container: {
        background: 'rgba(51, 51, 51, 0.9)',
        color: '#fff',
        fontSize: '12px',
        borderRadius: '0',
        boxShadow: 'none',
        padding: '10px 14px',
      },
    },
};

// function ScatterPlotDialog({openDialog, userTarget, handleCloseDialog}) {
//     // const [open, setOpen] = React.useState(openDialog);
  

//     console.log('openDialog', openDialog)
//     console.log('userTarget', userTarget)
//     return (
//       <div>
//         <Dialog
//           open={openDialog}
//           onClose={handleCloseDialog}
//           aria-labelledby="alert-dialog-title"
//           aria-describedby="alert-dialog-description"
//         >
//           {/* <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle> */}
//           <Grid
//             container
//             direction="row"
//             justifyContent="center"
//             alignItems="center"
//             style={{textAlign: 'center'}}
//             >
//                 <div>
//                 <Grid item style={{paddingTop: '1em', paddingBottom: '1em', textAlign: 'center'}}>
//                     <Avatar>
//                         <FolderIcon />
//                     </Avatar>

//                     <Typography variant="h1" style={{color: 'black'}}>{userTarget? userTarget.name: ''}</Typography>
//                 </Grid></div>
           
//           </Grid>
//           <DialogContent>
//             <DialogContentText id="alert-dialog-description">
//               Anda ingin membuka profil {userTarget? userTarget.name: ''} ?
//             </DialogContentText>
//           </DialogContent>
//           <DialogActions>
//             <Button onClick={handleCloseDialog} color="primary">
//               Batalkan
//             </Button>
//             <Button onClick={handleCloseDialog} color="primary" autoFocus>
//               Ya
//             </Button>
//           </DialogActions>
//         </Dialog>
//       </div>
//     );
//   }


// <block:plugin:0>
const getOrCreateLegendList = (chart, id) => {
    const legendContainer = document.getElementById(id);
    let listContainer = legendContainer.querySelector('ul');
  
    if (!listContainer) {
      listContainer = document.createElement('ul');
      listContainer.style.display = 'flex';
      listContainer.style.flexDirection = 'row';
      listContainer.style.margin = 0;
      listContainer.style.padding = 0;
  
      legendContainer.appendChild(listContainer);
    }
  
    return listContainer;
  };
  
  const htmlLegendPlugin = {
    id: 'htmlLegend',
    afterUpdate(chart, args, options) {
      const ul = getOrCreateLegendList(chart, options.containerID);
  
      // Remove old legend items
      while (ul.firstChild) {
        ul.firstChild.remove();
      }
  
      // Reuse the built-in legendItems generator
      const items = chart.options.plugins.legend.labels.generateLabels(chart);
  
      items.forEach(item => {
        const li = document.createElement('li');
        li.style.alignItems = 'center';
        li.style.cursor = 'pointer';
        li.style.display = 'flex';
        li.style.flexDirection = 'row';
        li.style.marginLeft = '10px';
  
        li.onclick = () => {
          const {type} = chart.config;
          if (type === 'pie' || type === 'doughnut') {
            // Pie and doughnut charts only have a single dataset and visibility is per item
            chart.toggleDataVisibility(item.index);
          } else {
            chart.setDatasetVisibility(item.datasetIndex, !chart.isDatasetVisible(item.datasetIndex));
          }
          chart.update();
        };
  
        // Color box
        const boxSpan = document.createElement('span');
        boxSpan.style.background = item.fillStyle;
        boxSpan.style.borderColor = item.strokeStyle;
        boxSpan.style.borderWidth = item.lineWidth + 'px';
        boxSpan.style.display = 'inline-block';
        boxSpan.style.height = '20px';
        boxSpan.style.marginRight = '10px';
        boxSpan.style.width = '20px';
  
        // Text
        const textContainer = document.createElement('p');
        textContainer.style.color = item.fontColor;
        textContainer.style.margin = 0;
        textContainer.style.padding = 0;
        textContainer.style.textDecoration = item.hidden ? 'line-through' : '';
  
        const text = document.createTextNode(item.text);
        textContainer.appendChild(text);
  
        li.appendChild(boxSpan);
        li.appendChild(textContainer);
        ul.appendChild(li);
      });
    }
  };
  // </block:plugin>