import React, {useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import PropTypes from 'prop-types';
import { makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { Button, Tooltip } from '@material-ui/core';

import GaugeChart from 'react-gauge-chart';
import ReactSpeedometer from 'react-d3-speedometer'
import moment from 'moment';
import ModalAverageResponseMk from './modalNinebox/modalAverageResponseMk';

const useStyles = makeStyles((theme) => ({

	contentWaktuRespon: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '600px',
		boxShadow: theme.shadow.large,
		// borderRadius: 15
	},
	contentWaktuResponSuccess: {
		color: theme.palette.text.success
	},
	contentWaktuResponError: {
		color: theme.palette.text.error
	},
	contentWaktuResponPercentageSuccess: {
		color: theme.palette.text.success,
		display: 'flex',
		alignItems: 'center',
		flexWrap: 'wrap',
		fontSize: '2rem',
	},
	contentWaktuResponPercentageError: {
		color: theme.palette.text.error,
		display: 'flex',
		alignItems: 'center',
		flexWrap: 'wrap',
		fontSize: '3rem'
	},
	contentWaktuResponButton:{
		marginTop: theme.spacing(2),
		backgroundColor: theme.palette.text.warning,
		color: 'white',
		borderRadius: 20,
		'&:hover': {
			backgroundColor: '#EFAF00',
		 },
	},


}));



function scaleValue(value, from, to) {
  var scale = (to[1] - to[0]) / (from[1] - from[0]);
  var capped = Math.min(from[1], Math.max(from[0], value)) - from[0];
  return ~~(capped * scale + to[0]);
}

const ContentWaktuResponV2 = () => {
    const classes = useStyles();
    const [respon, setRespon] = useState({respon:[], average_all:[]})
    const [respon2, setRespon2] = useState(null)
    const [ min, setMin ]     = useState(null);
    const [ max, setMax ]     = useState(null);
    const [ avg, setAvg ]     = useState(null);
    const [ value, setValue ] = useState(null);
    const [ unread, setUnread ] = useState(null);
    const [ verySlow, setVerySlow] = useState(0)
    const [ slow, setSlow] = useState(0)
    const [ average, setAverage] = useState(0)
    const [ fast, setFast] = useState(0)
    const [ veryFast, setVeryFast] = useState(0)
    const [ modalAverageResponseMk, setModalAverageResponseMk] = useState(false)
    const [ dataAverageResponseMk, setAverageResponseMk] = useState([])

    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1 = new URLSearchParams(search).get('tgl1');
    const tgl2 = new URLSearchParams(search).get('tgl2');
    

    useEffect(() => {
      const Respon = async() => {
        try{
          const tahun = moment(tgl2).format('YYYY')
          const statistikDev = `http://localhost:3001/sikd/api/v1/ninebox/scatterplot/statistik-responsetime/${tahun}`
          const statistikProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/scatterplot/statistik-responsetime/${tahun}`
          const apiStatistik = process.env.NODE_ENV === 'development' ? statistikDev : statistikProd
          const fetchStatistik = await fetch(apiStatistik)
          const resStatistik = await fetchStatistik.json()
          const { very_slow, slow, average, fast, very_fast, ...rest } = resStatistik[0]
          setVerySlow(very_slow)
          setSlow(slow)
          setAverage(average)
          setFast(fast)
          setVeryFast(very_fast)
          console.log(resStatistik)


          const apiResponseProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/profil-pemakaian/waktu-response/response-pegawai/${select_id}/${tgl1}/${tgl2}`
          const apiResponseDev = `http://localhost:3001/sikd/api/v1/profil-pemakaian/waktu-response/response-pegawai/${select_id}/${tgl1}/${tgl2}`
          const apiResponse = process.env.NODE_ENV === 'development' ? apiResponseDev : apiResponseProd
          const fetchResponse = await fetch(apiResponse)
          const resResponse = await fetchResponse.json()
          console.log('resStatistik',resResponse)
          setRespon2(resResponse)




        } catch(err) {
          console.log('Error in contentWaktuResponV2s', err)
        }
      }
      Respon()
    }, [])
      
    const handleCloseAverageResponseMk = () => {
      document.body.style.overflow = 'auto';
      setModalAverageResponseMk(false)
    } 
    return (
        <Grid 
          item 
          lg={6}
          sm={6}
          xl={6}
          xs={6}>
            <Paper className={classes.contentWaktuRespon}>
                <Grid item md={12}>
                    <Typography variant="h5" gutterBottom>Waktu Response</Typography>
                    
                    <Divider/>
                </Grid>
                <Grid 
                  item 
                  lg={12}
                  sm={12}
                  xl={12}
                  xs={12}
                  style={{height: '480px',}}
                >
                  {respon2 == null ? '' 
                  :
                    <div style={{paddingTop: '5em', width:'100%', margin:'auto', height: '100%'}}>
                        <ReactSpeedometer 
                          forceRender={true}
                            width={500}
                            // height={450}
                            // minValue={0} 
                            fluidWidth={true}
                            minValue={respon2.FIXmax} 
                            // minValue={Number(min)} 
                            // maxValue={Number(max) }                    
                            maxValue={Number(respon2.FIXmin) === 0 ? 0 : Number(respon2.FIXmin)}                    
                            value={respon2&&respon2.belumdibaca > 30 ?  100 : Number(respon2.FIXval)  }    
                            // value={6}    
                            needleHeightRatio={0.8}                    
                            currentValueText={respon2&&respon2.belumdibaca > 30 ? 'Harap semua surat dibaca!' : respon2.responsetime_pegawai.responsetime_text ? respon2.responsetime_pegawai.responsetime_text : 'Belum ada' }
                            ringWidth={100}
                            valueTextFontWeight="bold"  
                            // customSegmentStops={min ? [1000,901,634,367,100,0] : ''}                 
                            customSegmentStops={respon2.FIXmax ? [100,90,63.4,36.6,10,0] : ''}                 
                            customSegmentLabels={[                    
                              {                    
                                text: 'Very Slow',                    
                                position: 'OUTSIDE',                    
                                color: '#555',                    
                              },                    
                              {                    
                                text: 'Slow',                    
                                position: 'OUTSIDE',                    
                                color: '#555',                    
                              },                    
                              {                    
                                text: 'Average',                    
                                position: 'OUTSIDE',                    
                                color: '#555',                    
                                // fontSize: '19px',                    
                              },                    
                              {                    
                                text: 'Fast',                    
                                position: 'OUTSIDE',                    
                                color: '#555',                    
                              },                    
                              {                    
                                text: 'Very Fast',                    
                                position: 'OUTSIDE',                    
                                color: '#555',                    
                              },                    
                            ]}         
                            segmentColors={['#FF471A', '#F6961E', '#ECDB23', '#AEE228', '#6AD72D']}    
                            needleTransitionDuration={3333}                    
                            needleTransition="easeElastic"                    
                            needleColor={'#00587A'}                    
                            textColor={'#626262'}
                            segments={5}
                        />
                    </div>
                    }
                </Grid>
                <Grid item md={12} style={{marginTop: '-4em'}}>

                
                  <Tooltip title="Presentase Very Slow Keseluruhan MK">
                    <Button variant="contained" style={{backgroundColor: '#FF471A', color: 'white'}}>
                      {verySlow} %
                    </Button>
                  </Tooltip>
                  <Tooltip title="Presentase Slow Keseluruhan MK">
                    <Button variant="contained" style={{backgroundColor: '#F6961E', color: 'white'}}>
                      {slow}%
                    </Button>
                  </Tooltip>
                  <Tooltip title="Presentase AVERAGE Keseluruhan MK">
                    <Button variant="contained" style={{backgroundColor: '#ECDB23', color: 'white'}}>
                      {average}%
                    </Button>
                  </Tooltip>
                  <Tooltip title="Presentase FAST Keseluruhan MK">
                    <Button variant="contained" style={{backgroundColor: '#AEE228', color: 'white'}}>
                      {fast}%
                    </Button>
                  </Tooltip>
                  <Tooltip title="Presentase VERY FAST Keseluruhan MK">
                    <Button variant="contained" style={{backgroundColor: '#6AD72D', color: 'white'}}>
                      {veryFast}%
                    </Button>
                  </Tooltip>
                </Grid>
                <Grid item md={12}>

                  {respon2 && respon2.belumdibaca > 30 
                    ?   <div>
                          <div>
                            <Typography 
                              className={classes.contentWaktuResponError} 
                              variant="caption"
                            >
                              Melebihi ambang batas surat yang tidak dibaca (30 surat)
                            </Typography>
                          </div>
                          <div>
                            <Typography 
                              className={classes.contentWaktuResponError} 
                              variant="caption"
                            >
                              maka waktu response menjadi Very Slow
                            </Typography>
                          </div>
                        </div>
                    : ''
                  }
                  <Typography 
                    className={classes.contentWaktuResponError} 
                    variant="subtitle2"
                  >
                    Rata-rata waktu response seluruh pegawai MK :
                  </Typography>
                  <Typography 
                    className={classes.contentWaktuResponError} 
                    variant="h5"
                    onClick={() => setModalAverageResponseMk(true)}
                  >
                    {respon2&&respon2.responsetime_average.responsetime_text}
                  </Typography>
                  <ModalAverageResponseMk 
                    modalAverageResponseMk={modalAverageResponseMk}
                    dataAverageResponseMk={dataAverageResponseMk}
                    handleCloseAverageResponseMk={handleCloseAverageResponseMk}
                  />
                </Grid>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={1}
                >

                  
                </Grid>
                </Paper>
            
            </Grid>
    )
}

ContentWaktuResponV2.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  wt:PropTypes.number,
  minValue:PropTypes.number,
  maxValue:PropTypes.number,
  value:PropTypes.number,
  
}

export default ContentWaktuResponV2;