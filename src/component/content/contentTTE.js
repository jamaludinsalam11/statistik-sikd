import React, { useEffect, useState } from 'react';
import moment from 'moment';
import {useLocation} from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Skeleton from '@material-ui/lab/Skeleton';
import ProgressBar from 'react-customizable-progressbar'
// import AssignmentIcon from '@mui/icons-material/Assignment';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import logoDs from './ds-logo.png'


const useStyles = makeStyles((theme) => ({

	content: {
		padding: theme.spacing(2),
		textAlign: 'center',
		borderRadius: 15,
		backgroundColor: 'white',
		color: theme.palette.text.secondary,
		marginTop: '-3%',
		zIndex: 1,
		position: 'relative',
	},
	contentDate: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		borderRadius: 25
	},
	contentDateSD: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		boxShadow: 'none'
	},
	contentDivider: {
		paddingTop: theme.spacing(6),
		paddingBottom: theme.spacing(6),
	},
	contentTTE: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '420px',
		boxShadow: theme.shadow.large,
	},
	contentTTEvalue: {
		// paddingTop: theme.spacing(7),
		paddingBottom: theme.spacing(1),
		fontWeight: 600,
		fontSize: 26

	},

}));

function scaleValue(value, from, to) {
	var scale = (to[1] - to[0]) / (from[1] - from[0]);
	var capped = Math.min(from[1], Math.max(from[0], value)) - from[0];
	return ~~(capped * scale + to[0]);
}

function scaleValue2(value, r1, r2){
	return ( value - r1[ 0 ] ) * ( r2[ 1 ] - r2[ 0 ] ) / ( r1[ 1 ] - r1[ 0 ] ) + r2[ 0 ];
}

const ContentTTE = () => {
    const classes = useStyles();
	const [tte, setTTE] = useState(null)
	const [tteAll, setTTEAll] = useState(null)
	const [percentageTTDE, setPercentageTTDE] = useState(null)

	let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1 = new URLSearchParams(search).get('tgl1');
    const tgl2 = new URLSearchParams(search).get('tgl2');

	useEffect(() => {
		const fetchTTE = async () => {
			try{
				await Promise.all([
					fetch(`${process.env.REACT_APP_API_TTE}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
					fetch(`${process.env.REACT_APP_API_TTE_ALL}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
				  ])
				  .then((responses) => {
					  return Promise.all(responses.map(function (response) {
						  return response.json();
					  }));
				  })
				  .then((data) => {
					  const dataTTEuser = data[0].data[0].total_ttde
					  const dataTTEall = data[1].data[0].all_ttde
					  console.log('iam in contentTTEuser', dataTTEuser)
					  console.log('iam in contentTTEAll', dataTTEall)

					  const percentage = scaleValue2(dataTTEuser, [0, dataTTEall], [0, 100]).toFixed(2)
					  console.log('percentage TTDE', percentage)
					  setPercentageTTDE(percentage)
					  setTTE(dataTTEuser)
					  setTTEAll(dataTTEall)
				  })



			} catch(err){
				console.log(err)
			}
		}
		fetchTTE();
	}, [])
	// console.log('tte', tte.data)
    return(
        <Grid item xs={6}>
            <Paper className={classes.contentTTE} style={{textAlign: 'center'}}>
                <Typography variant="h5" gutterBottom >Tanda Tangan Elektronik</Typography>
				<Divider/>
				<div style={{textAlign: 'center', scale: '1.3', display: 'flex', justifyContent: 'center', paddingTop: '3.4em'}}>
				<ProgressBar
				  progress={percentageTTDE && percentageTTDE}
				  cut={120}
				  rotate={-210}
				  strokeWidth={20}
				  strokeColor="#5d9cec"
				  strokeLinecap="round"
				  trackStrokeWidth={12}
				  trackStrokeColor="#e6e6e6"
				  trackStrokeLinecap="round"
				  pointerRadius={0}
				  initialAnimation={true}
				  transition="1.5s ease 0.5s"
				  trackTransition="0s ease"
				  style={{width: '500px'}}
				> 
					<div className="indicator-volume" style={{marginTop: '-12em'}}>
						<div className="inner">
							{/* <AccountBalanceIcon style={{fontSize: 42, color: percentageTTDE == 0 ? 'rgba(0, 0, 0, 0.54)' : '#5d9cec'}}/> */}
							<img
								src={logoDs}
								style={{height: 54.6, width: 44.6}}
							/>
							{/* <div className="percentage">{percentageTTDE}%</div> */}
							<Typography variant="h1" className={classes.contentTTEvalue} gutterbottom style={{color: percentageTTDE == 0 ? 'rgba(0, 0, 0, 0.54)' : '#5d9cec'}}>
								{percentageTTDE == null
									? <Skeleton animation="wave" height={35}  variant="text" style={{ width: '70px', margin: 'auto',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
									: `${percentageTTDE} %`
								}
								
							</Typography>
							<Typography variant="h4"  gutterbottom style={{paddingBottom: '1em', fontSize: '1em', color: percentageTTDE == 0 ? 'rgba(0, 0, 0, 0.54)' : '#5d9cec'}}>
								{tte == null
									? <Skeleton animation="wave" height={30}  variant="text" style={{ width: '90px', margin: 'auto',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
									: `(${tte} of ${tteAll})`
								}
							</Typography>
						</div>
					</div>
				</ProgressBar>
				</div>
                {/* <Typography variant="h1" className={classes.contentTTEvalue} gutterbottom>
					{percentageTTDE == null
						? <Skeleton animation="wave" height={55}  variant="text" style={{ width: '100px', margin: 'auto',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
						: `${percentageTTDE} %`
					}
					
				</Typography>
                <Typography variant="h4"  gutterbottom style={{paddingBottom: '1em'}}>
					{tte == null
						? <Skeleton animation="wave" height={40}  variant="text" style={{ width: '150px', margin: 'auto',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
						: `(${tte} of ${tteAll})`
					}
				</Typography> */}
				{/* <Typography variant="subtitle2" gutterBottom>Presentase Penggunaan Tanda Tangan Elektronik Mahkamah Konstitusi pada periode </Typography>
				<Typography variant="subtitle2" gutterBottom>{moment(tgl1).lang('id').format('Do MMMM YYYY ')}-  {moment(tgl2).lang('id').format('Do MMMM YYYY')}</Typography> */}
            </Paper>
        </Grid>
    )
}

export default ContentTTE;