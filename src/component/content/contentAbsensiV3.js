import React, {useEffect, useState} from 'react';
import moment from 'moment';
import Moment from 'moment'
import { extendMoment } from 'moment-range';
import {useLocation} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import ContentGrafikTerbaca from './contentGrafikSuratTerbaca';
import {
	Avatar,
	Box,
	Card,
	CardContent,
	Grid,
	Typography
  } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';
import { red } from '@material-ui/core/colors';
import JumlahJamKerja from '../absensi/JumlahJamKerja';
import JamKerjaPerMinggu from '../absensi/JamKerjaPerMinggu';
import JamKerjaPerHari from '../absensi/JamKerjaPerHari';
import Terlambat from '../absensi/Terlambat';
import PulangCepat from '../absensi/PulangCepat';
import TidakAbsenPulang from '../absensi/TidakAbsenPulang';
import PulangCepatV2 from '../absensi/PulangCepatV2';
import TerlambatV2 from '../absensi/TerlambatV2';

import JumlahJamKerjaV2 from '../absensi/JumlahJamKerjaV2';
import JamKerjaPerMingguV2 from '../absensi/JamKerjaPerMingguV2';
import JamKerjaPerHariV2 from '../absensi/JamKerjaPerHariV2';
import TidakAbsenPulangV2 from '../absensi/TidakAbsenPulangV2';


const useStyles = makeStyles((theme) => ({
	contentSuratTerbaca: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '200px',
		boxShadow: theme.shadow.large,
	},
	contentSuratTerbacaGrid: {
		paddingTop: theme.spacing(2)
	},
	contentSuratTerbacaGrid2: {
		paddingTop: theme.spacing(2),
		paddingBottom: theme.spacing(2),
	},
	contentTingkatTanggapSIKD: {
		color: theme.palette.text.success
	},
	contentSuratTerbacaDibaca: {
		color: theme.palette.text.success
	},
	contentSuratTerbacaBelumDibaca: {
		color: theme.palette.text.error
	},

}));

function scaleValue(value, from, to) {
	var scale = (to[1] - to[0]) / (from[1] - from[0]);
	var capped = Math.min(from[1], Math.max(from[0], value)) - from[0];
	return (capped * scale + to[0]);
}

function convertToBulan(value){
	const hasilBulan = 
		value == 1 
		? 'Januari' 
		: value == 2 
		? 'Februari' 
		: value == 3
		? 'Maret'
		: value == 4  
		? 'April'
		: value == 5
		? 'Mei'
		: value == 6
		? 'Juni'
		: value == 7
		? 'Juli'
		: value == 8
		? 'Agustus'
		: value == 9
		? 'September'
		: value == 10
		? 'Oktober'
		: value == 11
		? 'November'
		: 'Desember'
	return hasilBulan
}

const Sorting = (data) =>{
  
	// Standar Sort Javascript - stable
	const sort = data.sort((a,b) => a.id_calendar - b.id_calendar)
	return sort
}


const ContentAbsensiV2 = () => {
    const classes = useStyles();
    let search  							= useLocation().search;
    const select_id 						= new URLSearchParams(search).get('select_id');
    const tgl1 								= new URLSearchParams(search).get('tgl1');
    const tgl2 								= new URLSearchParams(search).get('tgl2');
	const [tgl, setTgl] 					= useState({tgl1: tgl1, tgl2:tgl2});
	const [loading, setLoading]				= useState(true)
	const [user, setUser] 					= useState(null)
	const [dataMentah, setDataMentah] 		= useState(null)
	const [dataSempurna, setDataSempurna] 	= useState(null)
	const [totalJam, setTotalJam]			= useState(null)
	const [perMinggu, setPerMinggu]			= useState(null)
	const [perHari, setPerHari]				= useState(null)
	const [ratarataJam, setRataRataJam]		= useState(null)
	const [data, setData]					= useState(null)
	const [seconds, setSeconds]				= useState(null)
	/**
	 * Declare Date
	 */
	const moment2 			= extendMoment(Moment)
	const bulanMulaiArray 	= moment(tgl1).format('YYYY-MM-DD')
	const bulanAkhirArray 	= moment(tgl2).format('YYYY-MM-DD')
	const range 			= moment2.range(bulanMulaiArray, bulanAkhirArray)
	const r2 				= range.snapTo('YYYY')
	const month_number 		= Array.from(r2.by('months')).map(m => Number(m.format('M')))
	const month_name 		= Array.from(r2.by('months')).map(m => convertToBulan(Number(m.format('MM'))))
	const tahun_now 		= moment(tgl1).format('YYYY')
	console.log('range : ',  month_name)
	console.log('range : ',  month_number)

	// useEffect(() => {
	// 	const timer = setInterval(() => {
	// 		setSeconds(seconds + 10000)
	// 	}, 100)
	// 	return () => clearInterval(timer)
	// },[seconds])
	console.log('auto update value : ', seconds)
	useEffect(() => {
		/** For Development, bulan - tahun - NIP */
		// const bulan = 'Juli'
		// const tahun = '2020'
		
		setLoading(true)
		const fetchUser = async() => {
			try{
				/**
				 * GET USER DATA FROM apps.mkri.id / API bang Alex
				 * ======================================================
				 * - @host REACT_APP_API_USER
				 * - bridge sikd to data pegawai apps melalui nomor NIP
				 * - yang ada pada table Role SIKD
				 * - data user akan digunakan untuk mengambil data 
				 * 	 dari API Abensi bulanan
				 */
				const res 			= await fetch(`${process.env.REACT_APP_API_USER}?select_id=${select_id}`)
				const res_getNip 	= await res.json()
				const no_nip		= res_getNip.data[0].no_nip

				/**
				 * Interact with API APPS.MKRI.ID
				 */
				const apiFetchUser 	= `${process.env.REACT_APP_API_APPS_PEGAWAI}/${no_nip}`
				const fetch_user 	= await fetch(apiFetchUser)
				const res_user 		= await fetch_user.json()
				const pgw_id		= res_user[0].pgw_id
				console.log('pgw_id', pgw_id)

				setUser(res_user[0])
				/**
				 * Olah Absensi
				 */
				const apiAbsensiNew 	= `${process.env.REACT_APP_API_APPS_ABSENSIV2}/${pgw_id}/${tahun_now}`
				const fetch_absensi 	= await fetch(apiAbsensiNew)
				const res_absensi 		= await fetch_absensi.json()
				const total_jam_kerja 	= Number(parseFloat(res_absensi.totaljamkerja).toFixed(1))
				const per_minggu 		= Number(parseFloat(res_absensi.rata2perMinggu).toFixed(1))
				const per_hari 			= Number(parseFloat(res_absensi.rata2perHari).toFixed(1))
				console.log('res_absensi v2', res_absensi)
				



				/**
				 * Get Rata-rata jam kerja perbulan
				 * dari Array bulan yang telah ditempuh
				 * Manually
				 */
				const apiJan	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[0]}/${tahun_now}`
				const apiFeb	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[1]}/${tahun_now}`
				const apiMar	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[2]}/${tahun_now}`
				const apiApr	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[3]}/${tahun_now}`
				const apiMei	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[4]}/${tahun_now}`
				const apiJun	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[5]}/${tahun_now}`
				const apiJul	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[6]}/${tahun_now}`
				const apiAgt	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[7]}/${tahun_now}`
				const apiSep	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[8]}/${tahun_now}`
				const apiOkt	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[9]}/${tahun_now}`
				const apiNov	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[10]}/${tahun_now}`
				const apiDes	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[11]}/${tahun_now}`
				
				const [fetchJanuari,fetchFebruari,fetchMaret,
					fetchApril,fetchMei,fetchJuni,fetchJuli,
					fetchAgustus,fetchSeptember,fetchOktober,
					fetchNovember,fetchDesember] = await Promise.all([
						fetch(apiJan),
						fetch(apiFeb),
						fetch(apiMar),
						fetch(apiApr),
						fetch(apiMei),
						fetch(apiJun),
						fetch(apiJul),
						fetch(apiAgt),
						fetch(apiSep),
						fetch(apiOkt),
						fetch(apiNov),
						fetch(apiDes)
				])

				const resJan = await fetchJanuari.json()
				const resFeb = await fetchFebruari.json()
				const resMar = await fetchMaret.json()
				const resApr = await fetchApril.json()
				const resMei = await fetchMei.json()
				const resJun = await fetchJuni.json()
				const resJul = await fetchJuli.json()
				const resAgt = await fetchAgustus.json()
				const resSep = await fetchSeptember.json()
				const resOkt = await fetchOktober.json()
				const resNov = await fetchNovember.json()
				const resDes = await fetchDesember.json()

				let combineArrayBulan		= []
				combineArrayBulan.push(
					...resJan, ...resFeb, ...resMar, ...resApr,
					...resMei, ...resJun, ...resJul, ...resAgt,
					...resSep, ...resOkt, ...resNov, ...resDes
				)
				const cobineArrayBulanSorted = Sorting(combineArrayBulan)
				console.log('rata2Januari: ', cobineArrayBulanSorted)

				let tgl_today = moment().format('YYYY-MM-DD') 

				let rata2Absen = []
				let jmlWfh = 0
				let jmlWfo = 0
				let sumJam = 0
				let jumlahHari = 0
				let arrJamKerja = []
				cobineArrayBulanSorted.forEach((el) => {
					if(!(el.bool_hari_libur || el.bool_weekend || el.id == null || tgl_today < el.tanggal)){
						rata2Absen.push(el)
						if(el.bool_wfh == true){
							jmlWfh = jmlWfh + 1
							arrJamKerja.push(7.5)
							sumJam = sumJam + 7.5
							
						} else {
							jmlWfo = jmlWfo + 1
							arrJamKerja.push(8)
							sumJam = sumJam + 8
						} 
						jumlahHari = jumlahHari + 1
						
					}
				})
				// const ratarataSeharusnya = sumJam / jumlahHari 
				const ratarataSeharusnya = 7.5
				const compareRatarata = Number(parseFloat(per_hari - ratarataSeharusnya).toFixed(2)) 
				const rataPercentage = scaleValue(compareRatarata, [0,ratarataSeharusnya], [0,100])

				/**
				 * Debugging
				 */
				console.log('jmlWfh: ', jmlWfh)
				console.log('jmlWfo: ', jmlWfo)
				console.log('jmlHari: ', jumlahHari)
				console.log('arrJamKerja: ', arrJamKerja)
				console.log('ratarataaa: ' , sumJam / jumlahHari )
				
				let objToState = {
					'total_jam_kerja' 	: total_jam_kerja,
					'per_minggu'		: per_minggu,
					'per_hari'			: per_hari,
					'ratarataActual' 	: per_hari,
					'ratarataSeharusnya': ratarataSeharusnya,
					'ratarataGap'		: compareRatarata,
					'percentageRatarata': parseFloat(rataPercentage).toFixed(2),
					'minusPositive'		: rataPercentage > 0 ? 'positive' : rataPercentage < 0 ? 'minus' : 'equals'
				}
				console.log('objRataRata: ', objToState)
				setRataRataJam(rataPercentage)
				setData(objToState)
				setTotalJam(total_jam_kerja)
				setPerMinggu(per_minggu)
				setPerHari(per_hari)
				setLoading(false)
			} catch (e) {
				console.log(e)
				setLoading(false)
			}
		}
		fetchUser()
	}, [])

	
    return(
		
		<>
		<Grid
			item
			lg={4}
			sm={4}
			xl={4}
			xs={4}
		><JumlahJamKerjaV2 data={data} loading={loading ? true : loading}/></Grid>
		<Grid
			item
			lg={4}
			sm={4}
			xl={4}
			xs={4}
		><JamKerjaPerMingguV2 data={data} /> </Grid>
		<Grid
			item
			lg={4}
			sm={4}
			xl={4}
			xs={4}
		><JamKerjaPerHariV2 data={data} ratarata={ratarataJam} /> </Grid>
		<Grid
			item
			lg={4}
			sm={4}
			xl={4}
			xs={4}
		><TerlambatV2 data={dataSempurna}/> </Grid>
		<Grid
			item
			lg={4}
			sm={4}
			xl={4}
			xs={4}
		><PulangCepatV2 data={dataSempurna}/> </Grid>
		<Grid
			item
			lg={4}
			sm={4}
			xl={4}
			xs={4}
		><TidakAbsenPulangV2 data={dataSempurna}/> </Grid>
		{/* <Grid
			item
			lg={4}
			sm={6}
			xl={4}
			xs={12}
		><JamKerjaPerMinggu data={dataSempurna} /> </Grid>
		<Grid
			item
			lg={4}
			sm={6}
			xl={4}
			xs={12}
		><JamKerjaPerHari data={dataSempurna} /> </Grid>
		
		<Grid
			item
			lg={4}
			sm={6}
			xl={4}
			xs={12}
		><Terlambat data={dataSempurna}/> </Grid>
		<Grid
			item
			lg={4}
			sm={6}
			xl={4}
			xs={12}
		><PulangCepat data={dataSempurna}/> </Grid>
		<Grid
			item
			lg={4}
			sm={6}
			xl={3}
			xs={12}
		><TidakAbsenPulang data={dataSempurna}/> </Grid> */}

		</>
    )
}

export default ContentAbsensiV2;