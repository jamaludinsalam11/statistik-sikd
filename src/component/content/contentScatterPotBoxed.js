import React, { useState, useEffect, useCallback, useMemo, useRef} from 'react'
import { ScatterPlot ,ResponsiveScatterPlot } from '@nivo/scatterplot'
import { Grid , Paper, Typography, Divider, Chip} from '@material-ui/core'
import {useLocation} from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';
// import * as wjChart from '@grapecity/wijmo.react.chart';
import * as chart from '@grapecity/wijmo.chart';
// import * as wjChartAnnotation from '@grapecity/wijmo.react.chart.annotation';
// import '@grapecity/wijmo.styles/wijmo.css';
import moment from 'moment';
import ScatterPlotDialog from './contentScatterPotDialog';
import {Chart,  Scatter } from 'react-chartjs-2';
import annotationPlugin from "chartjs-plugin-annotation";
import datalabelsPlugin from "chartjs-plugin-datalabels";
import Avatar from '@material-ui/core/Avatar';
import FolderIcon from '@material-ui/icons/Folder';
import {
    Chart as ChartJS,
  defaults,
    LinearScale,
    PointElement,
    LineElement,
    Tooltip,
    Legend,
  } from 'chart.js';


  import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ContentScatterPotKuadran from './contentScatterPotKuadran';
import Penerapan from '../footer/penerapan/Penerapan';
import RumpunJabatan from '../footer/rumpun-jabatan/RumpunJabatan';
import BaganSuksesi from '../footer/bagan-suksesi/BaganSuksesi';
import JabatanKritikal from '../footer/JabatanKritikal';
Chart.register([annotationPlugin,LinearScale, PointElement, LineElement, Tooltip, Legend])
// Scatter.register([annotationPlugin,datalabelsPlugin])
const datadummy = [
    { people_name: 'Black Panther', kinerja: 56, potensial_talenta: 65 },
    { people_name: 'Avengers: Infinity War', kinerja: 8.5, potensial_talenta: 67 },
    { people_name: 'Incredibles 2', kinerja: 87, potensial_talenta: 89 },
    { people_name: 'Jurassic World: Fallen Kingdom', kinerja: 78, potensial_talenta: 97 },
    
];
const dataDummy2 = {
    datasets: [
        {
          label: 'Eselon I',
          data: [
            { x: 90, y: 80, name: 'M Guntur Hamzah', jabatan: 'Eselon I', kuadran: 'Kuadran IV', box: '8', nbv: 92 },
            { x: 85, y: 82, name: 'Muhidim', jabatan: 'Eselon I', kuadran: 'Kuadran IV', box: '8', nbv: 85 },
          ],
          backgroundColor: 'rgba(5, 99, 132, 1)',
        },
    ],
}

const ContentScatterPotBoxed = () => {
    const classes = useStyles()
    const chartRef = useRef();
    const [pallete, setPallete] = useState(chart.Palettes.light)
    const [data, setData] = useState(datadummy)
    const [eselon1, setEselon1] = useState(datadummy)
    const [eselon2, setEselon2] = useState(datadummy)
    const [eselon3, setEselon3] = useState(datadummy)
    const [eselon4, setEselon4] = useState(datadummy)
    const [eselon5, setEselon5] = useState(datadummy)
    const [other, setOther] = useState(datadummy)

    const [kuadran1, setKuadran1] = useState('')
    const [kuadran2, setKuadran2] = useState('')
    const [kuadran3a, setKuadran3a] = useState('')
    const [kuadran3b, setKuadran3b] = useState('')
    const [kuadran4, setKuadran4] = useState('')
    const [percentageKuadran, setPercentageKuadran] = useState({
      kuadran1: 0,
      kuadran2: 0,
      kuadran3a: 0,
      kuadran3b: 0,
      kuadran4: 0,
    })

    const [jumlahPegawai, setJumlahPegawai] = useState(0)
    const [blmMasukKuadran, setBlmMasukKuadran] = useState(0)

    const [unitkerja, setUnitkerja] = useState(null)
    const [options, setOptions] = useState(null)
    const [options2, setOptions2] = useState(null)
    const [dataChartjs, setDataChartjs] = useState(null)
    const [dataChartjs2, setDataChartjs2] = useState(null)
    const [dataAnnotationsLine, setDataAnnotationsLine] = useState({
      xMax: 0,
      xMin: 0, 
      xScaleID: 'x',
      yMax: 0,
      yMin: 0,
      yScaleID: 'y',
      hovered: false,
      box_potensi_talenta: 0,
      box_kinerja: 0,
      kinerja: 0,
      potensial_talenta: 0,
      nbv: 0,
      people_name: '',
      people_desc: '',
      unitkerja: '',
      color: ''
    })

    const [openDialog, setOpenDialog] = useState(false)
    const [openModalKuadran, setOpenModalKuadran] = useState(false)
    const [dataModalKuadran, setDataModalKuadran] = useState([])
    const [paramsKuadran, setParamsKuadran] = useState(0)
    const [userTarget, setUserTarget] = useState(null)
    let search  = useLocation().search
    const roleid = new URLSearchParams(search).get('select_id')
    const tgl1 = new URLSearchParams(search).get('tgl1')
    const tgl2 = new URLSearchParams(search).get('tgl2')
    const select_id = new URLSearchParams(search).get('select_id')

    const tahun = moment(tgl1).format('YYYY')
    // console.log(openDialog)
    // componentWill
    useEffect(()=> {
        const fetchScat = async() => {
            try{
                // const api = process.env.REACT_APP_API_APPS_FASTIFY2+'/sikd/api/v1/ninebox/scatterplot/'+tahun+'/'+roleid
                const api2Prod = process.env.REACT_APP_API_APPS_FASTIFY2+'/sikd/api/v1/ninebox/scatterplot/v2-boxed/'+tahun+'/'+roleid
                const api2Dev = 'http://localhost:3001/sikd/api/v1/ninebox/scatterplot/v2-boxed/'+tahun+'/'+roleid
                const api2 = process.env.NODE_ENV ==='development' ? api2Dev : api2Prod
                // const api2 = 'http://localhost:3001/sikd/api/v1/ninebox/scatterplot/v2/'+tahun+'/'+roleid
                // console.log('fetchScatterPlot',api)
                // const fetchScatterPlot  = await fetch(api)
                // const resScatterPlot = await fetchScatterPlot.json()

                const fetchScatterPlot2 = await fetch(api2)
                const resScatterPlot2 = await fetchScatterPlot2.json()
                // console.log('resScatterPlot2',resScatterPlot2)
                // setData(resScatterPlot.result)
                // setEselon1(resScatterPlot.eselon1)
                // setEselon2(resScatterPlot.eselon2)
                // setEselon3(resScatterPlot.eselon3)
                // setEselon4(resScatterPlot.eselon4)
                // setEselon5(resScatterPlot.eselon5)
                // setOther(resScatterPlot.other)
                // setPallete(chart.Palettes.light)

                setJumlahPegawai(resScatterPlot2.jumlah_pegawai.total)
                setUnitkerja(resScatterPlot2.unitkerja == null || resScatterPlot2.unitkerja === 'SEKJEN' ? null : resScatterPlot2.unitkerja_full) 
                setBlmMasukKuadran(resScatterPlot2.jumlah_pegawai_kuadran.belummasukkuadran)
                const resScatterPlot2_1 = resScatterPlot2.dataset1
                setDataChartjs(resScatterPlot2.datasets1)
                setDataChartjs2(resScatterPlot2.datasets3)
                setDataAnnotationsLine({
                  xMax: resScatterPlot2.scatterPlotAnnotationsLine.xMax,
                  xMin: resScatterPlot2.scatterPlotAnnotationsLine.xMin,
                  xScaleID: resScatterPlot2.scatterPlotAnnotationsLine.xScaleID,
                  yMax: resScatterPlot2.scatterPlotAnnotationsLine.yMax,
                  yMin: resScatterPlot2.scatterPlotAnnotationsLine.yMin,
                  yScaleID: resScatterPlot2.scatterPlotAnnotationsLine.yScaleID,
                  hovered: false,
                  box_potensi_talenta: resScatterPlot2.scatterPlotAnnotationsLine.box_potensi_talenta,
                  box_kinerja: resScatterPlot2.scatterPlotAnnotationsLine.box_kinerja,
                  kinerja: resScatterPlot2.scatterPlotAnnotationsLine.kinerja,
                  potensial_talenta: resScatterPlot2.scatterPlotAnnotationsLine.potensial_talenta,
                  nbv: resScatterPlot2.scatterPlotAnnotationsLine.nbv,
                  people_name: resScatterPlot2.scatterPlotAnnotationsLine.people_name,
                  people_desc: resScatterPlot2.scatterPlotAnnotationsLine.people_desc,
                  unitkerja: resScatterPlot2.scatterPlotAnnotationsLine.unitkerja,
                  unitkerja_full: resScatterPlot2.scatterPlotAnnotationsLine.unitkerja_full,
                  color: resScatterPlot2.scatterPlotAnnotationsLine.color
                })

                setKuadran1(resScatterPlot2.list_pegawai_kuadran.kuadran1)
                setKuadran2(resScatterPlot2.list_pegawai_kuadran.kuadran2)
                setKuadran3a(resScatterPlot2.list_pegawai_kuadran.kuadran3a)
                setKuadran3b(resScatterPlot2.list_pegawai_kuadran.kuadran3b)
                setKuadran4(resScatterPlot2.list_pegawai_kuadran.kuadran4)
                setPercentageKuadran({
                  kuadran1: resScatterPlot2.percentage_pegawai_kuadran.kuadran1,
                  kuadran2: resScatterPlot2.percentage_pegawai_kuadran.kuadran2,
                  kuadran3a: resScatterPlot2.percentage_pegawai_kuadran.kuadran3a,
                  kuadran3b: resScatterPlot2.percentage_pegawai_kuadran.kuadran3b,
                  kuadran4: resScatterPlot2.percentage_pegawai_kuadran.kuadran4,
                })
                // setDataModalKuadran({ 
                //   kuadran1: resScatterPlot2.list_pegawai_kuadran.kuadran1,
                //   kuadran2: resScatterPlot2.list_pegawai_kuadran.kuadran2,
                //   kuadran3a: resScatterPlot2.list_pegawai_kuadran.kuadran3a,
                //   kuadran3b: resScatterPlot2.list_pegawai_kuadran.kuadran3b,
                //   kuadran4: resScatterPlot2.list_pegawai_kuadran.kuadran4,
                // })
                // console.log(resScatterPlot2.list_pegawai_kuadran.kuadran1)
                // console.log(resScatterPlot2.list_pegawai_kuadran.kuadran2)
                const chartjs3Options = {
                    maintainAspectRatio: false,
                    // responsive: true,
                    layout: {
                        padding: {
                            left: 0
                        }
                    },
                    scales: {
                        y: {
                          min: 0,
                          max: 100,
                        //   beginAtZero: true, 
                          grid: {
                            display: false
                          },
                          title: {
                            display: true,
                            text: 'Kinerja',
                            color: '#911',
                            font: {
                              family: 'Poppins, sans-serif',
                              size: 14,
                              weight: 'bold',
                              lineHeight: 1.2
                            }
                          }
                        },
                        x: {
                          min: 0,
                          max: 100,
                          grid: {
                            display: false
                          },
                          title: {
                            display: true,
                            text: 'Potensi Talenta',
                            color: '#911',
                            font: {
                              family: 'Poppins, sans-serif',
                              size: 14,
                              weight: 'bold',
                              lineHeight: 0.5
                            }
                          }
                        }
                    },
                    elements: {
                        point: {
                            borderWidth: 3,
                            radius: 6
                        }
                    },
                    interaction:{
                        mode: 'nearest'
                    },
                    plugins:{
                        htmlLegend: {
                            // ID of the container to put the legend in
                            containerID: 'legend-container',
                        },
                        legend:{
                            position: 'right',
                            display: false,
                            labels: {
                                usePointStyle: true,
                                
                            },
                        },
                        tooltip:{
                          enabled: false,
                          // display: false,
                            // position: 'cursor',
                            // callbacks: {
                            //     title: function(tooltipItems, data) {
                            //         const itterate = tooltipItems.map(function(x){ 
                            //             const obj  = [
                            //                 x.raw.people_name,
                            //                 x.raw.people_desc
                            //             ]
                            //             return obj
                            //         })
                            //         // console.log('itterate',itterate)
                            //         const result = [
                            //             tooltipItems[0].raw.people_name,
                                        
                            //             tooltipItems[0].raw.people_desc,
                            //             // "==============================",
                            //             // tooltipItems[0].raw.unitkerja_full,
                
                            //         ]
                            //        return itterate
                            //     }, 
                            //     afterTitle: function(tooltipItems, data) {
                            //         // const itterate = tooltipItems.map((x,i )=> x.raw.people_name)
                            //         // console.log(tooltipItems[0])
                            //         // const result = [
                            //         //     tooltipItems[0].raw.people_name,
                            //         //     tooltipItems[0].raw.people_desc,
                            //         //     ,
                
                            //         // ]
                            //     //    return tooltipItems[0].raw.unitkerja_full
                            //     },
                            //     label: function(context) {
                            //         let label = context.dataset.label || '';
                            //         // console.log(context)
                            //         const result = [
                            //             'Unitkerja: ' + ' ' + context.raw.unitkerja_full,
                            //             'Jabatan: ' + ' ' + context.raw.jabatan,
                            //             'Kinerja: ' + ' ' + context.raw.kinerja,
                            //             'Potensial Talenta: ' + ' ' + context.raw.potensial_talenta,
                            //             'Kuadran: ' + ' ' + context.raw.kuadran,
                            //             'Box: ' + ' ' + context.raw.box,
                            //             'NBV: ' + ' ' + context.raw.nbv,
                            //         ]
                                  
                            //         return result;
                            //     }
                            // }
                        },
                        annotation: {
                            common: {
                              drawTime: 'beforeDatasetsDraw'
                            },
                            annotations: {
                                // annotationLine1: {
                                //   type: 'line',
                                //   borderColor: 'rgba(7,59,76,1)',
                                //   borderDash: [3, 3],
                                //   borderWidth: 5,
                                //   xMax: 82,
                                //   xMin: 93.33,
                                //   xScaleID: 'x',
                                //   yMax: 48.66,
                                //   yMin: 59.99,
                                //   yScaleID: 'y',
                                //   label: {
                                //     display: true,
                                //     backgroundColor: 'green',
                                //     drawTime: 'beforeDatasetsDraw',
                                //     content: 'saassas'
                                //     // content: (ctx) => ['Average of dataset', 'is: ' + '50']
                                //   },
                                //   enter(ctx, event) {
                                //     console.log('ctx', ctx)
                                    
                                //     ctx.element.options.display = true;
                                //     return true;
                                //   },
                                //   leave(ctx, event) {
                                //     console.log('ctx leave', ctx)
                                //     ctx.element.options.display = true;
                                //     // element.label.options.display = false;
                                //     return true;
                                //   }
                                // },
                                Annotations_567: {
                                  type: 'line',
                                  arrowHeads: {
                                    sikdTest: 'jamalll'
                                  },
                                  borderColor: 'rgb(7,59,76,0.8)',
                                  borderWidth: 6,
                                  jabatan: 'asda',
                                  label: {
                                    enabled: (ctx) => ctx.hovered,
                                    backgroundColor: 'rgb(7,59,76,0.8)',
                                    drawTime: 'afterDatasetsDraw',
                                    sikdJamal: 'jamal',
                                    content: [
                                      `Nama:  {ctx.obj.test}, jabatan`, 
                                      `Unitkerja: {ctx.obj.tes}`,
                                      `Jabatan: `,
                                      `Box Kinerja: `,
                                      'Box Potensi Talenta: '
                                    ],
                                    textAlign: 'left',
                                    position: 'start'
                                    // position: (ctx) => ctx.hoverPosition
                                  },
                                  // scaleID: 'y',
                                  // value: 50,
                                  xMax: 82,
                                  xMin: 93.33,
                                  xScaleID: 'x',
                                  yMax: 48.66,
                                  yMin: 59.99,
                                  yScaleID: 'y',
                                  jamal: 'asdasdasd',
                                  // For more complex dynamic properties, you can store values on the persistent
                                  // context object then retrieve them via scriptable properties.  You'll have
                                  // to call chart.update() to reprocess the chart.
                                  enter(ctx, event) {
                                    
                                    ctx.obj = {
                                      test: 'Jamaludin Salam',
                                      tes: 'PPNPN'
                                    }
                                    ctx.hovered = true;
                                    ctx.hoverPosition = (event.x / ctx.chart.chartArea.width * 100) + '%';
                                    ctx.chart.update();
                                    console.log('ctx enter', ctx)
                                  },
                                  leave(ctx, event) {
                                    console.log('ctx leave', ctx)
                                    ctx.hovered = false;
                                    ctx.chart.update();
                                  }
                                },
                                /** Box 1 */
                                box1: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 0,
                                    xMax: 33.3,
                                    yMin: 0,
                                    yMax: 33.3,
                                    backgroundColor: "rgba(229,27,31,1)",
                                    borderColor: "rgba(229,27,31,1)",
                                    
                                },
                                label1: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 17,
                                  yValue: 16,
                                  backgroundColor: 'rgb(239,255,253,0)',
                                  color: 'rgb(239,255,253)',
                                  content: ['1'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 2 */
                                box2: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 33.34,
                                    xMax: 66.67,
                                    yMin: 33.34,
                                    yMax: 0,
                                    backgroundColor: "rgba(244,153,73,1)",
                                    borderColor: "rgba(244,153,73,1)"
                                },
                                label2: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 50,
                                  yValue: 16,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: 'rgb(43,43,43)',
                                  content: ['2'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 3 */
                                box3: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 0,
                                    xMax: 33.34,
                                    yMin: 33.34,
                                    yMax: 66.67,
                                    backgroundColor: "rgba(244,153,73,1)",
                                    borderColor: "rgba(244,153,73,1)"
                                },
                                label3: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 17,
                                  yValue: 50,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: 'rgb(43,43,43)',
                                  content: ['3'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 4 */
                                box4: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 0,
                                    xMax: 33.34,
                                    yMin: 66.67,
                                    yMax: 100,
                                    backgroundColor: "rgba(240,229,14,1)",
                                    borderColor: "rgba(240,229,14,1)"
                                },
                                label4: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 17,
                                  yValue: 84,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: 'rgb(43,43,43)',
                                  content: ['4'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 5 */
                                box5: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 33.34,
                                    xMax: 66.67,
                                    yMin: 33.34,
                                    yMax: 66.67,
                                    backgroundColor: "rgba(240,229,14,1)",
                                    borderColor: "rgba(240,229,14,1)"
                                },
                                label5: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 50,
                                  yValue: 50,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: 'rgb(43,43,43)',
                                  content: ['5'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 6 */
                                box6: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 66.67,
                                    xMax: 100,
                                    yMin: 0,
                                    yMax: 33.34,
                                    backgroundColor: "rgba(240,229,14,1)",
                                    borderColor: "rgba(240,229,14,1)"
                                },
                                label6: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 84,
                                  yValue: 16,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: 'rgb(43,43,43)',
                                  content: ['6'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 7 */
                                box7: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 33.34,
                                    xMax: 66.67,
                                    yMin: 66.67,
                                    yMax: 100,
                                    backgroundColor: "rgba(149,196,82,1)",
                                    borderColor: "rgba(149,196,82,1)"
                                },
                                label7: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 50,
                                  yValue: 84,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: 'rgb(43,43,43)',
                                  content: ['7'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 8 */
                                box8: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 66.67,
                                    xMax: 100,
                                    yMin: 33.34,
                                    yMax: 66.67,
                                    backgroundColor: "rgba(149,196,82,1)",
                                    borderColor: "rgba(149,196,82,1)"
                                },
                                label8: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 84,
                                  yValue: 50,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: 'rgb(43,43,43)',
                                  content: ['8'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 9 */
                                box9: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 66.67,
                                    xMax: 100,
                                    yMin: 66.67,
                                    yMax: 100,
                                    backgroundColor: "rgba(11,134,53,1)",
                                    borderColor: "rgba(11,134,53,1)"
                                },
                                label9: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 84,
                                  yValue: 84,
                                  backgroundColor: 'rgba(239,255,253,0)',
                                  color: 'rgb(239,255,253,1)',
                                  content: ['9'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },


                            }
                        }
                    }
                }
                const chartjs3Options2 = {
                    maintainAspectRatio: false,
                    // responsive: true,
                    layout: {
                        padding: {
                            left: 0
                        }
                    },
                    scales: {
                        y: {
                          min: 0,
                          max: 100,
                        //   beginAtZero: true, 
                          grid: {
                            display: false
                          },
                          title: {
                            display: true,
                            text: '',
                            color: '#911',
                            font: {
                              family: 'Poppins, sans-serif',
                              size: 14,
                              weight: 'bold',
                              lineHeight: 1.2
                            }
                          }
                        },
                        x: {
                          min: 0,
                          max: 100,
                          grid: {
                            display: false
                          },
                          title: {
                            display: true,
                            text: '',
                            color: '#911',
                            font: {
                              family: 'Poppins, sans-serif',
                              size: 14,
                              weight: 'bold',
                              lineHeight: 0.5
                            }
                          }
                        }
                    },
                    elements: {
                        point: {
                            borderWidth: 3,
                            radius: 10
                        }
                    },
                    interaction:{
                        mode: 'nearest'
                    },
                    plugins:{
                        htmlLegend: {
                            // ID of the container to put the legend in
                            containerID: 'legend-container',
                        },
                        legend:{
                            position: 'bottom',
                            display: true,
                            labels: {
                                usePointStyle: true,
                                
                            },
                        },
                        tooltip:{
                          enabled: false,
                          // display: false,
                            // position: 'cursor',
                            callbacks: {
                                title: function(tooltipItems, data) {
                                    const itterate = tooltipItems.map(function(x){ 
                                        const obj  = [
                                            x.raw.people_name,
                                            x.raw.people_desc
                                        ]
                                        return obj
                                    })
                                    // console.log('itterate',itterate)
                                    const result = [
                                        tooltipItems[0].raw.people_name,
                                        
                                        tooltipItems[0].raw.people_desc,
                                        // "==============================",
                                        // tooltipItems[0].raw.unitkerja_full,
                
                                    ]
                                   return itterate
                                }, 
                                afterTitle: function(tooltipItems, data) {
                                    // const itterate = tooltipItems.map((x,i )=> x.raw.people_name)
                                    // console.log(tooltipItems[0])
                                    // const result = [
                                    //     tooltipItems[0].raw.people_name,
                                    //     tooltipItems[0].raw.people_desc,
                                    //     ,
                
                                    // ]
                                //    return tooltipItems[0].raw.unitkerja_full
                                },
                                label: function(context) {
                                    let label = context.dataset.label || '';
                                    // console.log(context)
                                    let result = null
                                    if(context.raw.box_kinerja == null){
                                      result = [
                                        'Unitkerja: ' + ' ' + context.raw.unitkerja_full,
                                        'Jabatan: ' + ' ' + context.raw.jabatan,
                                        // 'Kinerja: ' + ' ' + context.raw.kinerja,
                                        'Box Potensi Talenta: ' + ' Box ' + context.raw.box_potensi_talenta + ' (' + context.raw.potensial_talenta + ')',
                                      ]
                                    }
                                    if(context.raw.box_potensi_talenta == null){
                                      result = [
                                        'Unitkerja: ' + ' ' + context.raw.unitkerja_full,
                                        'Jabatan: ' + ' ' + context.raw.jabatan,
                                        // 'Kinerja: ' + ' ' + context.raw.kinerja,
                                        'Box Kinerja: ' + ' Box ' + context.raw.box_kinerja + ' (' + context.raw.kinerja + ')',
                                      ]
                                    }
                                    
                                  
                                    return result;
                                }
                            }
                        },
                        annotation: {
                            common: {
                              drawTime: 'beforeDatasetsDraw'
                            },
                            annotations: {
                                // annotationLine1: {
                                //   type: 'line',
                                //   borderColor: 'rgba(7,59,76,1)',
                                //   borderDash: [3, 3],
                                //   borderWidth: 5,
                                //   xMax: 82,
                                //   xMin: 93.33,
                                //   xScaleID: 'x',
                                //   yMax: 48.66,
                                //   yMin: 59.99,
                                //   yScaleID: 'y',
                                //   label: {
                                //     display: true,
                                //     backgroundColor: 'green',
                                //     drawTime: 'beforeDatasetsDraw',
                                //     content: 'saassas'
                                //     // content: (ctx) => ['Average of dataset', 'is: ' + '50']
                                //   },
                                //   enter(ctx, event) {
                                //     console.log('ctx', ctx)
                                    
                                //     ctx.element.options.display = true;
                                //     return true;
                                //   },
                                //   leave(ctx, event) {
                                //     console.log('ctx leave', ctx)
                                //     ctx.element.options.display = true;
                                //     // element.label.options.display = false;
                                //     return true;
                                //   }
                                // },
                                Annotations_567: {
                                  type: 'line',
                                  // borderColor: resScatterPlot2.scatterPlotAnnotationsLine.color,
                                  borderColor: 'rgba(7,59,76,.5)',
                                  borderWidth: 8,
                                  borderRadius: 10,
                                  label: {
                                    // enabled: dataAnnotationsLine.hovered,
                                    enabled: (ctx) => ctx.hovered,
                                    backgroundColor: resScatterPlot2.scatterPlotAnnotationsLine.color,
                                    backgroundColor: 'rgba(7,59,76,.7)',
                                    // drawTime: 'afterDraw',
                                    drawTime: 'afterDatasetsDraw',
                                    sikdJamal: 'jamal',
                                    opacity: .5,
                                    content:  [
                                      `Nama:  ${resScatterPlot2.scatterPlotAnnotationsLine.people_name}`, 
                                      `Jabatan: ${resScatterPlot2.scatterPlotAnnotationsLine.people_desc}`,
                                      `Unitkerja: ${resScatterPlot2.scatterPlotAnnotationsLine.unitkerja_full}`,
                                      `Pangkat: ${resScatterPlot2.scatterPlotAnnotationsLine.jabatan}`,
                                      `Box Kinerja: ${resScatterPlot2.scatterPlotAnnotationsLine.box_kinerja}`,
                                      `Box Potensi Talenta: ${resScatterPlot2.scatterPlotAnnotationsLine.box_potensi_talenta}`
                                    ],
                                    textAlign: 'left',
                                    position: 'start' 
                                    // position: (ctx) => ctx.hoverPosition
                                  },
                                  // scaleID: 'y',
                                  // value: 50,
                                  xMax: resScatterPlot2.scatterPlotAnnotationsLine.xMax,
                                  xMin: resScatterPlot2.scatterPlotAnnotationsLine.xMin,
                                  xScaleID: resScatterPlot2.scatterPlotAnnotationsLine.xScaleID,
                                  yMax: resScatterPlot2.scatterPlotAnnotationsLine.yMax,
                                  yMin: resScatterPlot2.scatterPlotAnnotationsLine.yMin,
                                  yScaleID: resScatterPlot2.scatterPlotAnnotationsLine.yScaleID,
                                  // For more complex dynamic properties, you can store values on the persistent
                                  // context object then retrieve them via scriptable properties.  You'll have
                                  // to call chart.update() to reprocess the chart.
                                  enter(ctx, event) {
                                    
                                    ctx.obj = {
                                      people_name: 'Jamaludin Salam',
                                      people_desc: 'PPNPN'
                                    }
                                    setDataAnnotationsLine({hovered: true})
                                    ctx.hovered = true;
                                    ctx.borderColor = 'rgba(7,59,76, 1)'
                                    ctx.hoverPosition = (event.x / ctx.chart.chartArea.width * 100) + '%';
                                    ctx.chart.update();
                                    console.log('ctx enter', ctx)
                                  },
                                  leave(ctx, event) {
                                    setDataAnnotationsLine({hovered: false})
                                    // console.log('ctx leave', ctx)
                                    ctx.hovered = false;
                                    ctx.borderColor = 'rgba(7,59,76,.5)'
                                    ctx.chart.update();
                                  }
                                },
                                /** Box 1 */
                                box1: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 0,
                                    xMax: 33.3,
                                    yMin: 0,
                                    yMax: 33.3,
                                    backgroundColor: resScatterPlot2.boxActive.box1 == false ? 'rgb(211,211,211)' : "rgba(229,27,31,1)" ,
                                    borderColor: resScatterPlot2.boxActive.box1 == false ? 'rgb(128,128,128)' : "rgba(229,27,31,1)",
                                    
                                },
                                label1: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 17,
                                  yValue: 16,
                                  backgroundColor: 'rgb(239,255,253,0)',
                                  color: 'rgb(239,255,253)',
                                  content: ['1'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 2 */
                                box2: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 33.34,
                                    xMax: 66.67,
                                    yMin: 33.34,
                                    yMax: 0,
                                    backgroundColor: resScatterPlot2.boxActive.box2 == false ? 'rgb(211,211,211)' : "rgba(244,153,73,1)",
                                    borderColor: resScatterPlot2.boxActive.box2 == false ? 'rgb(128,128,128)' : "rgba(244,153,73,1)"
                                },
                                label2: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 50,
                                  yValue: 16,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: resScatterPlot2.boxActive.box2 == false ? 'rgb(239,255,253)' : 'rgb(43,43,43)',
                                  content: ['2'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 3 */
                                box3: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 0,
                                    xMax: 33.34,
                                    yMin: 33.34,
                                    yMax: 66.67,
                                    backgroundColor: resScatterPlot2.boxActive.box3 == false ? 'rgb(211,211,211)' : "rgba(244,153,73,1)",
                                    borderColor: resScatterPlot2.boxActive.box3 == false ? 'rgb(128,128,128)' : "rgba(244,153,73,1)"
                                },
                                label3: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 17,
                                  yValue: 50,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: resScatterPlot2.boxActive.box3 == false ? 'rgb(239,255,253)' : 'rgb(43,43,43)',
                                  content: ['3'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 4 */
                                box4: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 0,
                                    xMax: 33.34,
                                    yMin: 66.67,
                                    yMax: 100,
                                    backgroundColor: resScatterPlot2.boxActive.box4 == false ? 'rgb(211,211,211)' : "rgba(240,229,14,1)",
                                    borderColor: resScatterPlot2.boxActive.box4 == false ? 'rgb(128,128,128)' : "rgba(240,229,14,1)"
                                },
                                label4: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 17,
                                  yValue: 84,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: resScatterPlot2.boxActive.box4 == false ? 'rgb(239,255,253)' : 'rgb(43,43,43)',
                                  content: ['4'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 5 */
                                box5: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 33.34,
                                    xMax: 66.67,
                                    yMin: 33.34,
                                    yMax: 66.67,
                                    backgroundColor: resScatterPlot2.boxActive.box5 == false ? 'rgb(211,211,211)' : "rgba(240,229,14,1)",
                                    borderColor: resScatterPlot2.boxActive.box5 == false ? 'rgb(128,128,128)' : "rgba(240,229,14,1)"
                                },
                                label5: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 50,
                                  yValue: 50,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: resScatterPlot2.boxActive.box5 == false ? 'rgb(239,255,253)' :  'rgb(43,43,43)',
                                  content: ['5'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 6 */
                                box6: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 66.67,
                                    xMax: 100,
                                    yMin: 0,
                                    yMax: 33.34,
                                    backgroundColor: resScatterPlot2.boxActive.box6 == false ? 'rgb(211,211,211)' : "rgba(240,229,14,1)",
                                    borderColor: resScatterPlot2.boxActive.box6 == false ? 'rgb(128,128,128)' : "rgba(240,229,14,1)"
                                },
                                label6: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 84,
                                  yValue: 16,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: resScatterPlot2.boxActive.box6 == false ? 'rgb(239,255,253)' : 'rgb(43,43,43)',
                                  content: ['6'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 8 */
                                box8: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 33.34,
                                    xMax: 66.67,
                                    yMin: 66.67,
                                    yMax: 100,
                                    backgroundColor: resScatterPlot2.boxActive.box8 == false ? 'rgb(211,211,211)' : "rgba(149,196,82,1)",
                                    borderColor: resScatterPlot2.boxActive.box8 == false ? 'rgb(128,128,128)' : "rgba(149,196,82,1)"
                                },
                                label8: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 50,
                                  yValue: 84,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: resScatterPlot2.boxActive.box8 == false ? 'rgb(239,255,253)' : 'rgb(43,43,43)',
                                  content: ['8'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 7 */
                                box7: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 66.67,
                                    xMax: 100,
                                    yMin: 33.34,
                                    yMax: 66.67,
                                    backgroundColor: resScatterPlot2.boxActive.box7 == false ? 'rgb(211,211,211)' : "rgba(149,196,82,1)",
                                    borderColor: resScatterPlot2.boxActive.box7 == false ? 'rgb(128,128,128)' :  "rgba(149,196,82,1)"
                                },
                                label7: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 84,
                                  yValue: 50,
                                  backgroundColor: 'rgb(43,43,43,0)',
                                  color: resScatterPlot2.boxActive.box7 == false ? 'rgb(239,255,253)' :  'rgb(43,43,43)',
                                  content: ['7'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },

                                /** Box 9 */
                                box9: {
                                    drawTime: "beforeDatasetsDraw",
                                    display: true,
                                    type: "box",
                                    xMin: 66.67,
                                    xMax: 100,
                                    yMin: 66.67,
                                    yMax: 100,
                                    backgroundColor: resScatterPlot2.boxActive.box9 == false ? 'rgb(211,211,211)' : "rgba(11,134,53,1)",
                                    borderColor: resScatterPlot2.boxActive.box9 == false ? 'rgb(128,128,128)' :  "rgba(11,134,53,1)"
                                },
                                label9: {
                                  type: 'label',
                                  drawTime: 'beforeDatasetsDraw',
                                  xValue: 84,
                                  yValue: 84,
                                  backgroundColor: 'rgba(239,255,253,0)',
                                  color: resScatterPlot2.boxActive.box9 == false ? 'rgb(239,255,253)' :  'rgb(239,255,253,1)',
                                  content: ['9'],
                                  font: {
                                    size: 38,
                                  },
                                  // click: function(context, event) {
                                  //   // setParamsKuadran(1)
                                  //   const datass = resScatterPlot2.list_pegawai_kuadran.kuadran1
                                  //   setParamsKuadran(1)
                                  //   handleKuadran(1, datass)
                                  //   // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                  //   // setOpenModalKuadran(true)
                                  // }
                                },


                            }
                        }
                    }
                }
                setOptions(chartjs3Options)
                setOptions2(chartjs3Options2)
            } catch (err) {
                console.log(err)
            }
        }
        fetchScat()
    },[openDialog])
    const getElementsAtEvent = (event) => {
        
        // console.log(userTarget)
        // const url = `?select_id=${userTarget.role_id}&tgl1=2022-01-01 00:00:00&tgl2=2022-02-17 23:59:59`
        // const res = roleid === 'uk.1.1' ? window.open(url, '_blank').focus() : ''
        // return res
         
        if(event.length !== 0){
            if(roleid === 'uk.1.1' || roleid === 'uk.1.1.18.26'){
                const userTarget = event[0].element.$context.raw
                setOpenDialog(true)
                setUserTarget(userTarget)
            }
        } else {
            // const userTarget =  event[0].element.$context.raw
            // setUserTarget(userTarget)
            // console.log(event)
        }
    }



    const handleKuadran = async(params, data) => {
      if(params == 0){
        setDataModalKuadran([])
      } else if (params == 1) {
        setDataModalKuadran(data == null ? [] : data)
      } else if (params == 2) {
        setDataModalKuadran(data == null ? [] : data)
      } else if (params == 3) {
        setDataModalKuadran(data == null ? [] : data)
      } else if (params == 4) {
        setDataModalKuadran(data == null ? [] : data)
      } else if (params == 5) {
        setDataModalKuadran(data == null ? [] : data)
      }

      setOpenModalKuadran(true)
    }

 
    const handleCloseDialog = () => {
        document.body.style.overflow = 'auto';
        setOpenDialog(false);
      };
    const handleCloseModalKuadran = () => {
      document.body.style.overflow = 'auto';
      setDataModalKuadran([])
      setOpenModalKuadran(false);
    };
    const initChart = (sender) => {
        sender.dataLabel.content = customDataLabel;
        sender.tooltip.content = customTooltip;
    };

    const customTooltip = (ht) => {
        let item = ht.item;
        return `<b>Nama:</b> ${item.people_name} </br><b>
        Kinerja:</b> ${item.kinerja}</br><b>
        Potensial Talenta:</b> ${item.potensial_talenta}</br><b>
        NBV:</b> ${item.nbv}</br><b>
        Jabatan:</b> ${item.jabatan}</br><b>
        Kuadran:</b> ${item.kuadran}</br><b>
        Box:</b> ${item.box}</br><b>
        
        `;
    }
    const customDataLabel = (ht) => {
        return null
    };

    const customFormatter = (engine, ht, defaultRenderer) => {
        if (ht.jabatan === 'Eselon II' || ht.jabatan === "Eselon I") {
            engine.stroke = 'white';
            engine.fill = 'white';
        }
        defaultRenderer();
    }




    /** MODALS */
    const [openJabatanKritikal, setOpenJabatanKritikal] = useState(false)
    const [openBaganSuksesi, setOpenBaganSuksesi] 		= useState(false)
    const [openRumpunJabatan, setOpenRumpunJabatan] 	= useState(false)
    const [openPenerapan, setOpenPenerapan] 	= useState(false)
  


  
  /** Jabatan Kritikal */
  const handleOpenJabatanKritikal = () => {
    setOpenJabatanKritikal(true);
  };
      
  const handleCloseJabatanKritikal = () => {
    setOpenJabatanKritikal(false);
    document.body.removeAttribute('style')
  };
  /** Bagan Suksesi */
  const handleOpenBaganSuksesi = () => {
    setOpenBaganSuksesi(true);
  };
      
  const handleCloseBaganSuksesi = () => {
    setOpenBaganSuksesi(false);
  };
  /** Rumpun Jabatan */
  const handleOpenRumpunJabatan = () => {
    setOpenRumpunJabatan(true);
  };
    
  const handleCloseRumpunJabatan = () => {
    setOpenRumpunJabatan(false);
    document.body.removeAttribute('style')
  };
  
  /** PENERAPAN */
  const handleOpenPenerapan = () => {
    setOpenPenerapan(true);
  };
  const handleClosePenerapan = () => {
    setOpenPenerapan(false);
    document.body.removeAttribute('style')
  };

    return (
        <>
          <Grid item xs={8} >
              <Paper className={classes.contentTTE} style={{textAlign: 'center',position: 'relative'}}>
                  <Typography variant="h5" gutterBottom >Kuadran Khusus Kinerja dan Potensi Talenta Individual</Typography>  
                  {/* <Typography variant="subtitle2" gutterBottom >{unitkerja}</Typography>   */}
                  {/* <Divider/> */}
                  {unitkerja == null ? '' : <Divider/> }
                  
                  {/* <div style={{marginTop: '.5em', marginRight: '2%', textAlign: 'right'}}>
                      <Chip
                          avatar={<Avatar>{jumlahPegawai}</Avatar>}
                          label="Total Pegawai"
                          // clickable
                          color="primary"
                          style={{ marginBottom: '-300px',}}
                          // onDelete={handleDelete}
                          // deleteIcon={<DoneIcon />}
                      />
                  </div> */}
                  {/* <Grid container xs={8} > */}
                    {/* <Grid item xs={6} >
                      <div style={{position: 'relative', height: '500px', paddingTop: '0em'}}>
                          {
                              dataChartjs ?
                                  <Scatter
                                  options={options}
                                  data={dataChartjs}
                                  getElementAtEvent={getElementsAtEvent}
                                  // style={{maxHeight: '480px', maxWidth: '450px'}}
                                  />
                                  : ''
                          }
                          
                      </div>
                    </Grid> */}
                    <Grid item xs={12} >
                      <div style={{position: 'relative', height: '500px', paddingTop: '0em', paddingLeft: '0em', paddingRight: '2em'}}>
                          {
                              dataChartjs2 ?
                                  <Scatter
                                  options={options2}
                                  data={dataChartjs2}
                                  getElementAtEvent={getElementsAtEvent}
                                  // style={{maxHeight: '480px', maxWidth: '450px'}}
                                  />
                                  : ''
                          }
                          
                      </div>
                    </Grid>
                  {/* </Grid> */}
                

              </Paper>
          </Grid>
        

          <Grid item xs={4}>
              <Paper className={classes.contentTTE} >
                  <Typography variant="h5" gutterBottom style={{textAlign: 'center',}}>Keterangan </Typography>  
                  <Divider/>
                  <Grid style={{paddingTop: '2em'}}>
                      <Typography variant="caption" gutterBottomstyle={{textAlign: 'left',}} >
                      Grafik ini menggambarkan posisi pegawai dalam nine box dan kuadran. Setiap pegawai terdeteksi dalam dua box (kinerja dan potensi talenta). Box Biru adalah box kinerja, sedang Box Merah adalah potensi talenta. Grafik ini juga menunjukkan posisi pegawai dalam kuadran. Semakin ke kanan atas semakin baik, baik dari aspek kinerja maupun potensi talenta.   
                      </Typography>  
                      {/* <Typography variant="caption" gutterBottom style={{fontSize: 12}} >Bertujuan untuk menggambarkan dimana Box pada variabel Kinerja dan Potensi Talenta berada . Sebagai bahan pendukung pengambilan keputusan</Typography> */}
                  </Grid>
                  <Grid style={{paddingTop: '1em'}}>
                      {/* <Typography variant="h5" gutterBottomstyle={{textAlign: 'left',}} >Kuadran II</Typography>   */}
                      <Typography variant="caption" gutterBottom style={{fontSize: 12}} >
                      Manajemen dapat menjadikan grafik dan manajemen talenta ini sebagai bahan/data pertimbangan dalam pengambilan keputusan yang terukur, transparan, dan akuntabel.
                      </Typography><br></br>
                      
                  </Grid>
                  <Grid container spacing={3} style={{ marginTop: '2em'}}>
                    <Grid item lg={6} xs={6}>
                      <Button variant='contained' color="primary" onClick={handleOpenJabatanKritikal} style={{ height:'100px'}}>Jabatan Kritikal</Button>
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Button variant='contained' color="primary" onClick={handleOpenRumpunJabatan} style={{ height:'100px'}}>Rumpun Jabatan</Button>
                    </Grid>
                  </Grid>
                  <Grid container spacing={3} style={{ marginTop: '1em'}}>
                    <Grid item lg={6} xs={6}>
                      <div style={{ width: '100%'}}>
                        <Button variant='contained' color="primary" onClick={handleOpenPenerapan} style={{ height:'100px', minWidth: '100%'}}>Penerapan</Button>

                      </div>
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <div style={{ width: '100%'}}>
                        <Button variant='contained' color="primary"  onClick={handleOpenPenerapan} style={{ height:'100px', minWidth: '100%'}} disabled>Rencana Suksesi</Button>

                      </div>
                    </Grid>
                  </Grid>
                  {/* <Grid style={{paddingTop: '1em'}}>
                      <div style={{ display: 'flex', alignItems: 'center'}}>
                        <Button variant="contained" color="primary" style={{ borderRadius: 50, backgroundColor: '#89CFFD', color: '#89CFFD'}}>.</Button> 
                        <Typography variant="h5" gutterBottomstyle={{textAlign: 'left'}} style={{marginLeft: '1em'}}> : Kinerja</Typography>  
                      </div>
                  </Grid>
                  <Grid style={{paddingTop: '1em'}}>
                      <div style={{ display: 'flex', alignItems: 'center'}}>
                        <Button variant="contained" color="primary" style={{ borderRadius: 50, backgroundColor: '#EE6983', color: '#EE6983'}}>.</Button> 
                        <Typography variant="h5" gutterBottomstyle={{textAlign: 'left'}} style={{marginLeft: '1em'}}> : Potensi Talenta</Typography>  
                      </div>
                  </Grid> */}
                                            

              </Paper>
          </Grid>
          <ContentScatterPotKuadran 
            openModalKuadran={openModalKuadran} 
            data={dataModalKuadran} 
            paramsKuadran={paramsKuadran} 
            percentageKuadran={percentageKuadran} 
            handleCloseModalKuadran={() => handleCloseModalKuadran()} 
          />
          <ScatterPlotDialog openDialog={openDialog} userTarget={userTarget} handleCloseDialog={handleCloseDialog} tgl1={tgl1} tgl2={tgl2} roleid={roleid}/> 
          
          {
            openJabatanKritikal == true
            ? <JabatanKritikal
              openJabatanKritikal={openJabatanKritikal}
              handleOpenJabatanKritikal={() => handleOpenJabatanKritikal()}
              handleCloseJabatanKritikal={() => handleCloseJabatanKritikal()}
              tgl1={tgl1}
              tgl2={tgl2}
              select_id={select_id}
            />
            : ''
          }

          {
            openBaganSuksesi == true 
            ? 	<BaganSuksesi
                openBaganSuksesi={openBaganSuksesi}
                handleCloseBaganSuksesi={() => handleCloseBaganSuksesi()}
              />
            : ''
          }

          {
            openRumpunJabatan == true 
            ? 	<RumpunJabatan
                openRumpunJabatan={openRumpunJabatan}
                handleCloseRumpunJabatan={() => handleCloseRumpunJabatan()}
                handleOpenJabatanKritikal={() => handleOpenJabatanKritikal()}
                tgl1={tgl1}
                tgl2={tgl2}
                select_id={select_id}
              />
            : ''
          }
          {
            openPenerapan == true 
            ? 	<Penerapan
                openPenerapan={openPenerapan}
                handleClosePenerapan={() => handleClosePenerapan()}
                handleOpenPenerapan={() => handleOpenPenerapan()}
                tgl1={tgl1}
                tgl2={tgl2}
                select_id={select_id}
              />
            : ''
          }
        </>
    )
}

export default ContentScatterPotBoxed


const useStyles = makeStyles((theme) => ({

	content: {
		padding: theme.spacing(2),
		textAlign: 'center',
		borderRadius: 15,
		backgroundColor: 'white',
		color: theme.palette.text.secondary,
		marginTop: '-3%',
		zIndex: 1,
		position: 'relative',
	},
	contentDate: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		borderRadius: 25
	},
	contentDateSD: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		boxShadow: 'none'
	},
	contentDivider: {
		paddingTop: theme.spacing(6),
		paddingBottom: theme.spacing(6),
	},
	contentTTE: {
		padding: theme.spacing(2),
		textAlign: 'left',
		color: theme.palette.text.secondary,
		height: '650px',
		boxShadow: theme.shadow.large,
	},
	contentTTEvalue: {
		paddingTop: theme.spacing(7),
		paddingBottom: theme.spacing(1),
		fontWeight: 500,
	},

}));

const theme = {
    tooltip: {
      container: {
        background: 'rgba(51, 51, 51, 0.9)',
        color: '#fff',
        fontSize: '12px',
        borderRadius: '0',
        boxShadow: 'none',
        padding: '10px 14px',
      },
    },
};

// function ScatterPlotDialog({openDialog, userTarget, handleCloseDialog}) {
//     // const [open, setOpen] = React.useState(openDialog);
  

//     console.log('openDialog', openDialog)
//     console.log('userTarget', userTarget)
//     return (
//       <div>
//         <Dialog
//           open={openDialog}
//           onClose={handleCloseDialog}
//           aria-labelledby="alert-dialog-title"
//           aria-describedby="alert-dialog-description"
//         >
//           {/* <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle> */}
//           <Grid
//             container
//             direction="row"
//             justifyContent="center"
//             alignItems="center"
//             style={{textAlign: 'center'}}
//             >
//                 <div>
//                 <Grid item style={{paddingTop: '1em', paddingBottom: '1em', textAlign: 'center'}}>
//                     <Avatar>
//                         <FolderIcon />
//                     </Avatar>

//                     <Typography variant="h1" style={{color: 'black'}}>{userTarget? userTarget.name: ''}</Typography>
//                 </Grid></div>
           
//           </Grid>
//           <DialogContent>
//             <DialogContentText id="alert-dialog-description">
//               Anda ingin membuka profil {userTarget? userTarget.name: ''} ?
//             </DialogContentText>
//           </DialogContent>
//           <DialogActions>
//             <Button onClick={handleCloseDialog} color="primary">
//               Batalkan
//             </Button>
//             <Button onClick={handleCloseDialog} color="primary" autoFocus>
//               Ya
//             </Button>
//           </DialogActions>
//         </Dialog>
//       </div>
//     );
//   }


// <block:plugin:0>
const getOrCreateLegendList = (chart, id) => {
    const legendContainer = document.getElementById(id);
    let listContainer = legendContainer.querySelector('ul');
  
    if (!listContainer) {
      listContainer = document.createElement('ul');
      listContainer.style.display = 'flex';
      listContainer.style.flexDirection = 'row';
      listContainer.style.margin = 0;
      listContainer.style.padding = 0;
  
      legendContainer.appendChild(listContainer);
    }
  
    return listContainer;
  };
  
  const htmlLegendPlugin = {
    id: 'htmlLegend',
    afterUpdate(chart, args, options) {
      const ul = getOrCreateLegendList(chart, options.containerID);
  
      // Remove old legend items
      while (ul.firstChild) {
        ul.firstChild.remove();
      }
  
      // Reuse the built-in legendItems generator
      const items = chart.options.plugins.legend.labels.generateLabels(chart);
  
      items.forEach(item => {
        const li = document.createElement('li');
        li.style.alignItems = 'center';
        li.style.cursor = 'pointer';
        li.style.display = 'flex';
        li.style.flexDirection = 'row';
        li.style.marginLeft = '10px';
  
        li.onclick = () => {
          const {type} = chart.config;
          if (type === 'pie' || type === 'doughnut') {
            // Pie and doughnut charts only have a single dataset and visibility is per item
            chart.toggleDataVisibility(item.index);
          } else {
            chart.setDatasetVisibility(item.datasetIndex, !chart.isDatasetVisible(item.datasetIndex));
          }
          chart.update();
        };
  
        // Color box
        const boxSpan = document.createElement('span');
        boxSpan.style.background = item.fillStyle;
        boxSpan.style.borderColor = item.strokeStyle;
        boxSpan.style.borderWidth = item.lineWidth + 'px';
        boxSpan.style.display = 'inline-block';
        boxSpan.style.height = '20px';
        boxSpan.style.marginRight = '10px';
        boxSpan.style.width = '20px';
  
        // Text
        const textContainer = document.createElement('p');
        textContainer.style.color = item.fontColor;
        textContainer.style.margin = 0;
        textContainer.style.padding = 0;
        textContainer.style.textDecoration = item.hidden ? 'line-through' : '';
  
        const text = document.createTextNode(item.text);
        textContainer.appendChild(text);
  
        li.appendChild(boxSpan);
        li.appendChild(textContainer);
        ul.appendChild(li);
      });
    }
  };
  // </block:plugin>

  function average(ctx) {
    const values = ctx.chart.data.datasets[0].data;
    return values.reduce((a, b) => a + b, 0) / values.length;
  }
  function min(ctx) {
    const values = ctx.chart.data.datasets[0].data;
    return values.reduce((a, b) => Math.min(a, b), Infinity);
  }
  function toggleLabel(ctx, event) {
    const oneThirdWidth = ctx.element.width / 3;
    const chart = ctx.chart;
    const annotationOpts = chart.options.plugins.annotation.annotations.annotation;
    annotationOpts.label.enabled = !annotationOpts.label.enabled;
    annotationOpts.label.position = (event.x / ctx.chart.chartArea.width * 100) + '%';
    chart.update();
  }