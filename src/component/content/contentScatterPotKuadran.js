import React,{useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {
    Avatar,
    Button,
    Box,
    Card,
    CardContent,
    CardHeader ,
    CardMedia,
    Grid,
    Paper,
    Typography,
    Divider,
    Modal,
    Backdrop,
    Fade,
    List,
    ListItem,
    ListItemText,
    ListItemAvatar,
    Chip
  } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Chart from 'react-apexcharts'
import Skeleton from '@material-ui/lab/Skeleton';



ContentScatterPotKuadran.propTypes = {
    // data: PropTypes.array,
    data: PropTypes.arrayOf(
        PropTypes.shape({
          people_name: PropTypes.string,
          people_desc: PropTypes.string,
        })
    ),
    percentageKuadran: PropTypes.shape({
        kuadran1: PropTypes.number,
        kuadran2: PropTypes.number,
        kuadran3a: PropTypes.number,
        kuadran3b: PropTypes.number,
        kuadran4: PropTypes.number,
      })
};
export default function ContentScatterPotKuadran({openModalKuadran, data, paramsKuadran, percentageKuadran, handleCloseModalKuadran}) {
    // console.log(data)
    const classes = useStyles()
    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={openModalKuadran}
            onClose={handleCloseModalKuadran}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500, 
            }}
        >           
            <Fade in={openModalKuadran}>
                <div className={classes.paper} style={{height: '80vh'}}>
                    <div style={{textAlign: 'center'}}>
                        <Typography variant="h3">
                            {
                                paramsKuadran == 0 
                                ? 'Belum Memilih Kuadran'
                                : paramsKuadran == 1
                                ? 'Kuadran 1'
                                : paramsKuadran == 2
                                ? 'Kuadran 2'
                                : paramsKuadran == 3
                                ? 'Kuadran 3a'
                                : paramsKuadran == 4
                                ? 'Kuadran 3b'
                                : paramsKuadran == 5
                                ? 'Kuadran 4'
                                : ''
                            }
                        </Typography>
                        <Typography variant="subtitle2">Kumpulan pegawai yang masuk dalam kategori 
                            {
                                paramsKuadran == 0 
                                ? 'Belum Memilih Kuadran'
                                : paramsKuadran == 1
                                ? 'Kuadran 1'
                                : paramsKuadran == 2
                                ? 'Kuadran 2'
                                : paramsKuadran == 3
                                ? 'Kuadran 3a'
                                : paramsKuadran == 4
                                ? 'Kuadran 3b'
                                : paramsKuadran == 5
                                ? 'Kuadran 4'
                                : ''
                            }
                        </Typography>
                    </div>
 
                    <List className={classes.rootList}>
                        {
                            data.length == 0 || data == null 
                            ? 'Tidak ada data'
                            : data.map(function(el, index){
                                return (
                                    <ListItem key={index}> 
                                        <ListItemAvatar>
                                            <Avatar>
                                                <AccountCircleIcon />
                                            </Avatar>
                                            </ListItemAvatar>
                                        <ListItemText primary={el.people_name} secondary={`NBV: ${el.nbv} || ${el.people_desc}`} />
                                    </ListItem>
                                )
                            })
                        }
                        
                    </List>
                    <div style={{textAlign: 'center', marginTop: '2em'}}>
                        {/* {total ? <Typography variant="h5">{total} Pegawai</Typography> : ''} */}
                        {/* <Typography variant="h5">{data.length == 0 || data == null ? 0 : data.length} Pegawai</Typography> */}
                        <Chip
                            avatar={<Avatar>{data.length == 0 || data == null ? 0 : data.length}</Avatar>}
                            label="Total Pegawai"
                            color="primary"
                        />
                        <Typography variant="h5"> 
                            {
                                paramsKuadran == 0 
                                ? `0%`
                                : paramsKuadran == 1
                                ? `${percentageKuadran.kuadran1}% `
                                : paramsKuadran == 2
                                ? `${percentageKuadran.kuadran2}% `
                                : paramsKuadran == 3
                                ? `${percentageKuadran.kuadran3a}% `
                                : paramsKuadran == 4
                                ? `${percentageKuadran.kuadran3b}% `
                                : paramsKuadran == 5
                                ? `${percentageKuadran.kuadran4}% `
                                : ''
                            } dari seluruh Pegawai</Typography>
                    </div>
                </div>
            </Fade>
        </Modal>
    )
}


const useStyles = makeStyles((theme) => ({
	contentNineBox: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: 'auto',
		boxShadow: theme.shadow.large,
        width: '100%'
		// borderRadius: 15
	},
    active : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EE9524',color: '#fff',
        // height: '120px', borderRadius: '50px', backgroundColor: 'rgb(241, 225, 91)',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    activeAdmin : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EDDB2D', 
        // height: '120px', borderRadius: '50px', backgroundColor: '#CCF6C8',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    notActive : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px',  borderRadius: '50px', backgroundColor: '#e6e6e6', 
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    btn: {
        marginTop: '1.5em', borderRadius: 25 
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(8, 5, 8),
        borderRadius: 25
       
    },
    rootList: {
        marginTop: '2em',
        width: '100%',
        maxWidth: 600,
        backgroundColor: theme.palette.background.paper,
        overflow: 'auto',
        maxHeight: '80%'
    },
    card: {
        maxWidth: 600,
        margin: theme.spacing(2),
    },
    media: {
        height: 190,
    },
    fade_rule: {
        height: '5px',
        backgroundColor: '#E6E6E6',
        width: '120px',
        marginLeft:'auto', marginRight: 'auto',
        marginTop:'1em', marginBottom: '1em',
        backgroundImage: 'linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-o-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-moz-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-ms-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-gradient( linear, left bottom, right bottom, color-stop(0.02, white), color-stop(0.5, gray), color-stop(0.98, white) )'
}

}));