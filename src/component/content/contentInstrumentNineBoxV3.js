import React,{useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import moment from 'moment';
import Moment from 'moment'
import { extendMoment } from 'moment-range';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { Chart ,Bar, Radar } from 'react-chartjs-2';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ContentScatterPot from './contentScatterPot';
import ContentSpyderWeb from './contentSpyderWeb';


import annotationPlugin from "chartjs-plugin-annotation";
import datalabelsPlugin from "chartjs-plugin-datalabels";
import { Button, IconButton, Tooltip } from '@material-ui/core';
import ContentScatterPotBoxed from './contentScatterPotBoxed';

const useStyles = makeStyles((theme) => ({
	contentTotalAsalSurat: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: 'auto',
		boxShadow: theme.shadow.large,
		// borderRadius: 15
	},

}));

function scaleValue(value, from, to) {
    var scale = (to[1] - to[0]) / (from[1] - from[0]);
    var capped = Math.min(from[1], Math.max(from[0], value)) - from[0];
    return ~~(capped * scale + to[0]);
  }
function convertToBulan(value){
	const hasilBulan = 
		value == 1 
		? 'Januari' 
		: value == 2 
		? 'Februari' 
		: value == 3
		? 'Maret'
		: value == 4  
		? 'April'
		: value == 5
		? 'Mei'
		: value == 6
		? 'Juni'
		: value == 7
		? 'Juli'
		: value == 8
		? 'Agustus'
		: value == 9
		? 'September'
		: value == 10
		? 'Oktober'
		: value == 11
		? 'November'
		: 'Desember'
	return hasilBulan
}
const Sorting = (data) =>{
  
	// Standar Sort Javascript - stable
	const sort = data.sort((a,b) => a.id_calendar - b.id_calendar)
	return sort
}

Chart.register([annotationPlugin])
const ContentInstrumentNineBoxV3 = () => {
    const classes = useStyles();
    const [data, setData] = useState(null)
    const [dataRadar, setDataRadar] = useState(null)
    const [options, setOptions] = useState(null)
    const [optionsRadar, setOptionsRadar] = useState(null)
    const [kinerja, setKinerja] = useState(null)
    const [kompetensi, setKompetensi] = useState(null)
    const [potensial, setPotensial] = useState(null)
    const [potensi, setPotensi] = useState(null)
    const [rekamjejak, setRekamjejak] = useState(null)
    const [lainlain, setLainlain] = useState(null)
    let search  = useLocation().search
    const select_id1 = new URLSearchParams(search).get('select_id')
    const tgl1 = new URLSearchParams(search).get('tgl1')
    const tgl2 = new URLSearchParams(search).get('tgl2')
    const tgl1_rapat = moment(tgl1).format('YYYY-MM-DD')
    const tgl2_rapat = moment(tgl2).format('YYYY-MM-DD')

    const [averageKinerja, setAverageKinerja] = useState(0);
    const [averagePotensialTalenta, setAveragePotensialTalenta] = useState(0);

    /**
	 * Declare Date for absensi
	 */
	const moment2 			= extendMoment(Moment)
	const bulanMulaiArray 	= moment(tgl1).format('YYYY-MM-DD')
	const bulanAkhirArray 	= moment(tgl2).format('YYYY-MM-DD')
	const range 			= moment2.range(bulanMulaiArray, bulanAkhirArray)
	const r2 				= range.snapTo('YYYY')
	const month_number 		= Array.from(r2.by('months')).map(m => Number(m.format('M')))
	const month_name 		= Array.from(r2.by('months')).map(m => convertToBulan(Number(m.format('MM'))))
	const tahun_now 		= moment(tgl2).format('YYYY')

    useEffect(() => {
        const fetchAll = async () => {
            try{

                // const apiDev = `http://localhost:3001/sikd/api/v1/ninebox/user/profile-eselon2-april-2023/${select_id1}/${tahun_now}`;
                const apiDev = `http://localhost:3001/sikd/api/v1/ninebox/user/profile/${select_id1}/${tahun_now}`;
                const apiProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/user/profile/${select_id1}/${tahun_now}`
                const apis = process.env.NODE_ENV === 'development' ? apiDev : apiProd;
                const fetching = await fetch(apis)
                const fetched   = await fetching.json()
                const user      = fetched
                const no_nip	  = user.nip

                /**
                 * Tambahan initilaizz untuk absensi
                 */
                const pgw_id		    = user.pgw_id
                const select_unit   = user.RoleAtasan === 'uk.1.1' ? user.RoleId : user.RoleAtasan
                const select_id     = user.PeopleId

                // SIKD, Rapat, Surat, responseTime
                const api = process.env.NODE_ENV === 'development' 
                            ? 'http://localhost:3001'
                            : process.env.REACT_APP_API_APPS_FASTIFY2
                // const apiNineBox = `${api}/sikd/api/v1/ninebox/pegawai-eselon2-april-2023/${select_id1}/${tahun_now}`
                const apiNineBox = `${api}/sikd/api/v1/ninebox/pegawai/${select_id1}/${tahun_now}`
                const fetchNineBox = await fetch(apiNineBox)
                const resNineBox = await fetchNineBox.json()

                // Average Kinerja dan Potensial Talenta
                const kinerja_avg = resNineBox.avg_kinerja;
                const potensial_avg = resNineBox.avg_potensial;
                setAverageKinerja(kinerja_avg)
                setAveragePotensialTalenta(potensial_avg)
                const { 
                    one_skp, one_ekinerja, one_kehadiran_absensi, one_kehadiran_rapat, one_beban_kerja, one_kelebihan_jamkerja, one_keterlambatan, one_responsetime, 
                    two_potensi, level_jabatan,
                    three_kepemimpinan, three_integritas, three_kerjasama, three_komunikasi, three_orientasi_pada_hasil, three_pelayanan_publik, three_pengembangan_diri, three_pengembangan_oranglain, three_mengelola_perubahan, three_pengambilan_keputusan, three_perekat_bangsa, three_komitment, 
                    four_kualifikasi_pendidikan, four_pangkat_golongan, four_usia, four_pengalaman_dlm_jabatan, four_pelatihan_kepemimpinan, four_diklat_fungsional_teknis, four_pengembangan_kompetensi, four_hukuman_disiplin, four_penghargaan,
                    five_kemampuan_bhs_asing, five_perilaku_kerja , five_penghargaan_kolaboratif, 
                    nilai_kinerja, nilai_potensial_talenta, nilai_kompetensi_sub, nilai_potensi_sub, nilai_rekam_jejak_sub, nilai_lainlain_sub
                } = resNineBox.result[0];
                // console.log('resNineBox', people_id)

                setKinerja(Number(parseFloat(nilai_kinerja).toFixed(2)))
                setPotensial(nilai_potensial_talenta)
                setPotensi(nilai_potensi_sub)
                setKompetensi(nilai_kompetensi_sub)
                setRekamjejak(nilai_rekam_jejak_sub)
                setLainlain(nilai_lainlain_sub)

                const data_struktural = [
                    one_skp, one_ekinerja, one_kehadiran_absensi, Number(parseFloat(one_kelebihan_jamkerja).toFixed(2)), one_kehadiran_rapat, one_beban_kerja,  one_keterlambatan, one_responsetime,
                    two_potensi, 
                    three_kepemimpinan, three_integritas, three_kerjasama, three_komunikasi, three_orientasi_pada_hasil, three_pelayanan_publik, three_pengembangan_diri, three_pengembangan_oranglain,three_mengelola_perubahan, three_pengambilan_keputusan, three_perekat_bangsa, three_komitment,
                    four_kualifikasi_pendidikan, four_pangkat_golongan, four_usia, four_pengalaman_dlm_jabatan, four_pelatihan_kepemimpinan, 
                    // four_diklat_fungsional_teknis, 
                    four_pengembangan_kompetensi, four_hukuman_disiplin, four_penghargaan,
                    five_kemampuan_bhs_asing, five_perilaku_kerja, five_penghargaan_kolaboratif
                
                ]
                const data_fungsional = [
                    one_skp, one_ekinerja, one_kehadiran_absensi, Number(parseFloat(one_kelebihan_jamkerja).toFixed(2)), one_kehadiran_rapat, one_beban_kerja,  one_keterlambatan, one_responsetime,
                    two_potensi, 
                    three_kepemimpinan, three_integritas, three_kerjasama, three_komunikasi, three_orientasi_pada_hasil, three_pelayanan_publik, three_pengembangan_diri, three_pengembangan_oranglain,three_mengelola_perubahan, three_pengambilan_keputusan, three_perekat_bangsa, three_komitment,
                    four_kualifikasi_pendidikan, four_pangkat_golongan, four_usia, four_pengalaman_dlm_jabatan, 
                    // four_pelatihan_kepemimpinan, 
                    four_diklat_fungsional_teknis,four_pengembangan_kompetensi, four_hukuman_disiplin, four_penghargaan,
                    five_kemampuan_bhs_asing, five_perilaku_kerja, five_penghargaan_kolaboratif
                
                ]
                const data_pelaksana = [
                    one_skp, one_ekinerja, one_kehadiran_absensi, Number(parseFloat(one_kelebihan_jamkerja).toFixed(2)), one_kehadiran_rapat, one_beban_kerja,  one_keterlambatan, one_responsetime,
                    two_potensi, 
                    three_kepemimpinan, three_integritas, three_kerjasama, three_komunikasi, three_orientasi_pada_hasil, three_pelayanan_publik, three_pengembangan_diri, three_pengembangan_oranglain,three_mengelola_perubahan, three_pengambilan_keputusan, three_perekat_bangsa, three_komitment,
                    four_kualifikasi_pendidikan, four_pangkat_golongan, four_usia, four_pengalaman_dlm_jabatan, 
                    // four_pelatihan_kepemimpinan, four_diklat_fungsional_teknis,
                    four_pengembangan_kompetensi, four_hukuman_disiplin, four_penghargaan,
                    five_kemampuan_bhs_asing, five_perilaku_kerja, five_penghargaan_kolaboratif
                
                ]
                const data_original = [
                    one_skp, one_ekinerja, one_kehadiran_absensi, Number(parseFloat(one_kelebihan_jamkerja).toFixed(2)), one_kehadiran_rapat, one_beban_kerja,  one_keterlambatan, one_responsetime,
                    two_potensi, 
                    three_kepemimpinan, three_integritas, three_kerjasama, three_komunikasi, three_orientasi_pada_hasil, three_pelayanan_publik, three_pengembangan_diri, three_pengembangan_oranglain,three_mengelola_perubahan, three_pengambilan_keputusan, three_perekat_bangsa, three_komitment,
                    four_kualifikasi_pendidikan, four_pangkat_golongan, four_usia, four_pengalaman_dlm_jabatan, 
                    four_pelatihan_kepemimpinan, four_diklat_fungsional_teknis,
                    four_pengembangan_kompetensi, four_hukuman_disiplin, four_penghargaan,
                    five_kemampuan_bhs_asing, five_perilaku_kerja, five_penghargaan_kolaboratif
                
                ]
                const emptyBoxStruktural = {
                    drawTime: "beforeDatasetsDraw",
                    display: true,
                    type: "box",
                    xMin: 25.5,
                    xMax: 26.5,
                    yMin: 0,
                    yMax: 100,
                    backgroundColor: "rgba(141, 141, 170, 0.8)",
                    borderColor: "rgba(141, 141, 170, 0.8)"
                }
                const emptyBoxFungsional = {
                    drawTime: "beforeDatasetsDraw",
                    display: true,
                    type: "box",
                    xMin: 25.5,
                    xMax: 26.5,
                    yMin: 0,
                    yMax: 100,
                    backgroundColor: "rgba(141, 141, 170, 0.8)",
                    borderColor: "rgba(141, 141, 170, 0.8)"
                }
                const emptyBoxPelaksana = {
                    drawTime: "beforeDatasetsDraw",
                    display: true,
                    type: "box",
                    xMin: 25.5,
                    xMax: 26.5,
                    yMin: 0,
                    yMax: 100,
                    backgroundColor: "rgba(141, 141, 170, 0.8)",
                    borderColor: "rgba(141, 141, 170, 0.8)"
                }


                const annotationStruktural = {
                    emptyBoxStruktural: {
                        drawTime: "beforeDatasetsDraw",
                        display: true,
                        type: "box",
                        xMin: 25.5,
                        xMax: 26.5,
                        yMin: 0,
                        yMax: 100,
                        backgroundColor: "rgba(141, 141, 170, 0.5)",
                        borderColor: "rgba(141, 141, 170, 0.5)"
                    }
                }
                const annotationFungsional= {
                    emptyBoxFungsional: {
                        drawTime: "beforeDatasetsDraw",
                        display: true,
                        type: "box",
                        xMin: 24.5,
                        xMax: 25.5,
                        yMin: 0,
                        yMax: 100,
                        backgroundColor: "rgba(141, 141, 170, 0.5)",
                        borderColor: "rgba(141, 141, 170, 0.5)"
                    }
                }
                const annotationPelaksanaPPNPN= {
                    emptyBoxStruktural: {
                        drawTime: "beforeDatasetsDraw",
                        display: true,
                        type: "box",
                        xMin: 24.5,
                        xMax: 25.5,
                        yMin: 0,
                        yMax: 100,
                        backgroundColor: "rgba(141, 141, 170, 0.5)",
                        borderColor: "rgba(141, 141, 170, 0.5)"
                    },
                    emptyBoxFungsional: {
                        drawTime: "beforeDatasetsDraw",
                        display: true,
                        type: "box",
                        xMin: 25.5,
                        xMax: 26.5,
                        yMin: 0,
                        yMax: 100,
                        backgroundColor: "rgba(141, 141, 170, 0.5)",
                        borderColor: "rgba(141, 141, 170, 0.5)"
                    }
                }
                const dataa = {
                    labels: labels_original,
                    datasets: [
                      {
                        label: 'Value',
                        data: data_original,

                        backgroundColor: bgColorOriginal,
                        borderColor: borderColorOriginal,
                        borderWidth: 1,
                      },
                    ],
                }

                const optionss = {
                    scales: {
                      y: {
                        beginAtZero: true,
                        max: 100
                      },  
                     
                    },
                    plugins: {
                        legend: {
                          position: 'right',
                          display: false
                        },
                        title: {
                          display: true,
                          text: '(*) Disclaimer, data dalam tahap pengembangan dan belum sepenuhnya benar / lengkap',
                        },
                        annotation:{
                            annotations: level_jabatan == 1 ? annotationStruktural : 
                                        level_jabatan == 2 ? annotationFungsional :
                                        level_jabatan == 3 ? annotationPelaksanaPPNPN : annotationPelaksanaPPNPN
                        }
                      },
                    responsive: true,
                    maintainAspectRatio: false
                  };

                const RadarData = {
                    labels: [
                        'SKP (1.1)', 'E-Kinerja (1.2)', ['Kehadiran','Presensi (1.3)' ],['Kelebihan','Jam Kerja (1.4)'], 'Kehadiran Rapat (1.5)', ['Beban Kerja', 'SIKD (1.6)'], 'Keterlambatan (1.7)', 'Responsetime (1.8)',
                        'Potensi (2.1.1)', 
                        'Kepemimpinan (2.2.1)', 'Integritas (2.2.2)', 'Kerjasama (2.2.3)', 'Komunikasi (2.2.4)', ['Orientasi', 'pada hasil (2.2.5)'], ['Pelayanan', 'Publik (2.2.6)'], ['Pengembangan', 'diri (2.2.7)'], ['Pengembangan','orang lain (2.2.8)'], ['Mengelola','Perubahan (2.2.9)' ],  ['Pengambilan', 'Keputusan (2.2.10)'], 'Perekat Bangsa (2.2.11)',  'Komitmen (2.2.12)',
                        ['Kualifikasi / ', 'Pendidikan (2.3.1)'], ['Pangkat /', 'Golongan (2.3.2)'] , 'Usia (2.3.3)', ['Pengalaman','dalam jabatan (2.3.4)'], ['Pelatihan', 'Kepemimpinan (2.3.5)'] ,  ['Diklat Fungsional (2.3.6)'], ['Pengembangan', 'Kompetensi (2.3.7)' ], 'Hukuman Disiplin (2.3.8)', 'Penghargaan (2.3.9)', 
                        ['Kemampuan','Bahasa Asing (2.4)'] , ['Perilaku', 'Kerja (2.4.2)'], ['Penghargaan', 'Kolaboratif (2.4.3)']
                    ],
                    datasets: [{
                        label: "Value",
                        backgroundColor: "rgba(34, 202, 236, .2)",
                        borderColor: "rgba(34, 202, 236, 1)",
                        pointBackgroundColor: "rgba(34, 202, 236, 1)",
                        poingBorderColor: "#fff",
                        pointHoverBackgroundColor: "#fff",
                        pointHoverBorderColor: "rgba(34, 202, 236, 1)",
                        data: [
                            one_skp, one_ekinerja, one_kehadiran_absensi, one_kelebihan_jamkerja, one_kehadiran_rapat, one_beban_kerja,  one_keterlambatan, one_responsetime,
                            two_potensi, 
                            three_kepemimpinan, three_integritas, three_kerjasama, three_komunikasi, three_orientasi_pada_hasil, three_pelayanan_publik, three_pengembangan_diri, three_pengembangan_oranglain, three_mengelola_perubahan, three_pengambilan_keputusan, three_perekat_bangsa, three_komitment,
                            four_kualifikasi_pendidikan, four_pangkat_golongan, four_usia, four_pengalaman_dlm_jabatan, four_pelatihan_kepemimpinan, four_diklat_fungsional_teknis, four_pengembangan_kompetensi, four_hukuman_disiplin, four_penghargaan,
                            five_kemampuan_bhs_asing, five_perilaku_kerja, five_penghargaan_kolaboratif
                        ]
                    }]
                }

                const RadarOptions = {
                    scale: {
                        ticks: {
                          min: 0,
                          max: 100,
                          stepSize: 20,
                          showLabelBackdrop: false,
                          backdropColor: "rgba(203, 197, 11, 1)"
                        },
                        angleLines: {
                          color: "rgba(255, 255, 255, .3)",
                          lineWidth: 1
                        },
                        gridLines: {
                          color: "rgba(255, 255, 255, .3)",
                          circular: true
                        }
                    },
                    plugins: {
                        legend: {
                          position: 'right',
                          display: false
                        },
                        title: {
                          display: true,
                          text: '(*) Disclaimer, data dalam tahap pengembangan dan belum sepenuhnya benar / lengkap',
                        },
                      },
                    responsive: true,
                    maintainAspectRatio: false
                }
                setData(dataa)
                setOptions(optionss)

                setDataRadar(RadarData)
                setOptionsRadar(RadarOptions)
            } catch (err) {
                console.log('Error in contentGrafikTotal: ',err)
            }
        }
        fetchAll();
    }, [])


    return(
            <Grid item xs={12}>
                <Paper className={classes.contentTotalAsalSurat}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <Typography variant="h5" gutterBottom>Instrument Penilaian Nine Box</Typography>
                            <Divider/>
                        </Grid>
                        <Grid item xs={12}>
                            
                        </Grid>

                        {/* <ContentSpyderWeb dataRadar={dataRadar} optionsRadar={optionsRadar}/> */}
                        <ContentScatterPot/>
                        <ContentScatterPotBoxed/>
                        
                        <Grid item xs={12}>
                            
                            {data 
                            ? <div style={{height: '450px', paddingTop: '2em', paddingRight: '1.5em', paddingLeft: '1em'}}>
                                <Bar data={data} options={options} />
                                </div>
                            : ''
                        }
                        </Grid>
                        <Grid item xs={12} style={{paddingTop: '2em', paddingBottom: '2em'}}> 
                            <Divider/>
                        </Grid>

                        {/* <Grid item xs={12} > 
                        {dataRadar 
                            ? <ContentSpyderWeb dataRadar={dataRadar} optionsRadar={optionsRadar}/>
                            : ''
                        }
                        </Grid> */}

                        <Grid item xs={12} style={{paddingTop: '2em'}}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <Typography variant="h4">Skema Penilaian</Typography>
                                </Grid>
                                <Grid item xs={3}>
                                    
                                    <div style={{paddingTop: '4em', paddingBottom: '3em', 
                                    width: '100%', backgroundColor: 'rgba(255, 99, 132, 0.2)', 
                                    borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                    borderRadius: '10px', height: '100%'
                                    }}>
                                        

                                        <div style={{marginTop: '0em'}}>
                                            <Typography variant="h5" gutterBottom>Kinerja (100%)</Typography>
                                            <Typography variant="h2"gutterBottom style={{paddingTop: '.7em', fontWeight: 'bold'}}>{kinerja}</Typography>
                                            <Typography variant="subtitle2" style={{fontSize: 12}} >{ kinerja == null ? 0 : parseFloat((kinerja / 100) * 100).toFixed(2)} % dari Kinerja</Typography>
                                            {
                                                Number(kinerja) >= 0 && Number(kinerja) < 40 ? <Typography variant="subtitle2" gutterBottom style={{color: 'red', fontWeight: 'bold' , opacity: .8}}>Kinerja di bawah ekspektasi </Typography> :
                                                Number(kinerja) >= 40 && Number(kinerja) < 80 ? <Typography variant="subtitle2" gutterBottom style={{color: 'orange', fontWeight: 'bold'}}>Kinerja sesuai ekspektasi </Typography> :
                                                Number(kinerja) >= 80 && Number(kinerja) < 100 ? <Typography variant="subtitle2" gutterBottom style={{color: 'green', fontWeight: 'bold'}}>Kinerja melampaui ekspektasi </Typography> : ''
                                            }
                                        </div>

                                        <div style={{marginTop: '0.8em', textAlign:'center',}}>
                                            
                                            <div><Typography variant='caption'>Rata-rata seluruh pegawai</Typography></div>
                                            <div>
                                            <Button 
                                                    variant="contained" 
                                                    style={{
                                                        backgroundColor: 'rgba(255, 99, 132, 0.8)', color: 'white',
                                                        borderRadius: 25
                                                    }}>
                                                    {averageKinerja}
                                                </Button>
                                            </div>
                                        </div>
  
                                       
                                        
                                    </div>
                                </Grid>
                                <Grid item xs={9} style={{backgroundColor: 'rgba(153, 102, 255, 0.1)', borderRadius: '10px', }}>
                                    <Grid container spacing={1}>
                                        <Grid item xs={12}>
                                            <div style={{marginTop: '.3em', textAlign:'right', marginRight: '.5em'}}>
                                                {/* <Tooltip title="Potensial Talenta rata-rata keseluruhan pegawai di Mahkamah Konstitusi">
                                                    <Button 
                                                        variant="contained" 
                                                        style={{
                                                            backgroundColor: 'rgba(83, 184, 187, 1)', color: 'white',
                                                            borderRadius: 25
                                                        }}>
                                                        {averagePotensialTalenta}
                                                    </Button>
                                                </Tooltip> */}

                                                <div><Typography variant='caption'>Rata-rata seluruh pegawai</Typography></div>
                                                <div>
                                                <Button 
                                                        variant="contained" 
                                                        style={{
                                                            backgroundColor: 'rgba(83, 184, 187, 1)', color: 'white',
                                                            borderRadius: 25
                                                        }}>
                                                        {averagePotensialTalenta}
                                                    </Button>
                                                </div>
                                            </div>

                                            <div style={{marginTop: '-2.3em'}}>
                                                <Typography variant="h5" gutterBottom>Potensi Talenta (100%)</Typography>
                                                <Typography variant="h2" style={{fontWeight: 'bold'}}>{potensial}</Typography>
                                                {/* <Typography variant="subtitle2" >*Nilai saat ini</Typography> */}
                                                {
                                                    Number(potensial) >= 0 && Number(potensial) < 40 ? <Typography variant="subtitle2" gutterBottom style={{color: 'red', fontWeight: 'bold' , opacity: .8}}>Potensi Talenta di bawah ekspektasi </Typography> :
                                                    Number(potensial) >= 40 && Number(potensial) < 80 ? <Typography variant="subtitle2" gutterBottom style={{color: 'orange', fontWeight: 'bold'}}>Potensi Talenta sesuai ekspektasi </Typography> :
                                                    Number(potensial) >= 80 && Number(potensial) < 100 ? <Typography variant="subtitle2" gutterBottom style={{color: 'green', fontWeight: 'bold'}}>Potensi Talenta melampaui ekspektasi </Typography> : ''
                                                }
                                            </div>
                                            <div style={{marginTop: '0.8em', textAlign:'center',}}>
                                            
                                            {/* <div><Typography variant='caption'>Rata-rata seluruh pegawai</Typography></div>
                                            <div>
                                            <Button 
                                                    variant="contained" 
                                                    style={{
                                                        backgroundColor: 'rgba(255, 99, 132, 0.8)', color: 'white',
                                                        borderRadius: 25
                                                    }}>
                                                    {averageKinerja}
                                                </Button>
                                            </div> */}
                                        </div>
                                            
                                        </Grid>

                                                                             
                                        <Grid item xs={3}>
                                            <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                                width: '100%', backgroundColor: 'rgba(83, 184, 187, 0.6)', 
                                                borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                                borderRadius: '50px'
                                            }}>
                                                <Typography variant="subtitle1" gutterBottom>Potensi (30%)</Typography>
                                                <Typography variant="h3" gutterBottom style={{fontWeight: 'bold'}}>{potensi == null ? 0 : potensi}</Typography>
                                                <Typography variant="subtitle2" style={{fontSize: 12}} >{potensi == null ? 0 : parseFloat((potensi / potensial) * 100).toFixed(2)} % </Typography>
                                                <Typography variant="subtitle2" style={{fontSize: 12}} >dari Potensi Talenta</Typography>
                                            </div>
                                        </Grid>
                                        <Grid item xs={3}>
                                            <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                                width: '100%', backgroundColor: 'rgba(153, 102, 255, 0.2)', 
                                                borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                                borderRadius: '50px'
                                            }}>
                                                <Typography variant="subtitle1" gutterBottom>Kompetensi (30%)</Typography>
                                                <Typography variant="h3" gutterBottom style={{fontWeight: 'bold'}}>{kompetensi == null ? 0 : kompetensi}</Typography>
                                                <Typography variant="subtitle2" style={{fontSize: 12}} >{kompetensi == null ? 0 : parseFloat((kompetensi / potensial) * 100).toFixed(2)} %</Typography>
                                                <Typography variant="subtitle2" style={{fontSize: 12}} >dari Potensi Talenta</Typography>
                                            </div>
                                        </Grid>  
                                        <Grid item xs={3}>
                                        
                                            <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                            width: '100%', backgroundColor: 'rgba(75, 192, 192, 0.2)', 
                                            borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                            borderRadius: '50px'
                                            }}>
                                                <Typography variant="subtitle1" gutterBottom>Rekam Jejak (15%)</Typography>
                                                <Typography variant="h3" gutterBottom style={{fontWeight: 'bold'}}>{rekamjejak == null ? 0 : rekamjejak}</Typography>
                                                <Typography variant="subtitle2" style={{fontSize: 12}} >{rekamjejak == null ? 0 : parseFloat((rekamjejak / potensial) * 100).toFixed(2)} % </Typography>
                                                <Typography variant="subtitle2" style={{fontSize: 12}} >dari Potensi Talenta</Typography>
                                            </div>
                                        </Grid>
                                        <Grid item xs={3}>
                                            <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                            width: '100%', backgroundColor: 'rgba(255, 159, 64, 0.2)', 
                                            borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                            borderRadius: '50px'
                                            }}>
                                                <Typography variant="subtitle1" gutterBottom>Lain-lain (25%)</Typography>
                                                <Typography variant="h3" gutterBottom style={{fontWeight: 'bold'}}>{lainlain == null ? 0 : lainlain}</Typography>
                                                <Typography variant="subtitle2" style={{fontSize: 12}} >{lainlain == null ? 0 : parseFloat((lainlain / potensial) * 100).toFixed(2)} % </Typography>
                                                <Typography variant="subtitle2" style={{fontSize: 12}} >dari Potensi Talenta</Typography>
                                            </div>
                                        </Grid>
                                    </Grid>
                                </Grid>




                            </Grid>
                            
                        </Grid>
                    </Grid>
                    
                    
                    
                    
				</Paper>
			</Grid>
    )
}

export default ContentInstrumentNineBoxV3;


//** OPTIONS 1, Bagian Label Text Luar */
const labels_struktural = [
    'SKP (1.1)', 'E-Kinerja (1.2)', 'Kehadiran Presensi (1.3)','Kelebihan Jam Kerja (1.4)', 'Kehadiran Rapat (1.5)', 'Beban Kerja SIKD (1.6)', 'Keterlambatan (1.7)', 'Responsetime (1.8)',
    'Potensi (2.1.1)', 
    'Kepemimpinan (2.2.1)', 'Integritas (2.2.2)', 'Kerjasama (2.2.3)', 'Komunikasi (2.2.4)', 'Orientasi pada hasil (2.2.5)', 'Pelayanan Publik (2.2.6)', 'Pengembangan diri(2.2.7)', 'Pengembangan Orang Lain (2.2.8)' ,'Mengelola Perubahan (2.2.9)',  'Pengambilan Keputusan (2.2.10)', 'Perekat Bangsa (2.2.11)', 'Komitmen Kualitas (2.2.12)',
    'Kualifikasi / Pendidikan (2.3.1)', 'Pangkat / Golongan (2.3.2)', 'Usia (2.3.3)', 'Pengalaman dalam jabatan (2.3.4)',  'Pelatihan Kepemimpinan (2.3.5)',  
    // 'Diklat Fungsional (2.3.6)', 
    'Pengembangan kompetensi (2.3.7)', 'Hukuman Disiplin (2.3.8)', 'Penghargaan (2.3.9)', 
    'Kemampuan Bahasa Asing (2.4.1)', 'Perilaku Kerja (2.4.2)', 'Penghargaan Kolaboratif (2.4.3)'
]
const labels_fungsional = [
    'SKP (1.1)', 'E-Kinerja (1.2)', 'Kehadiran Presensi (1.3)','Kelebihan Jam Kerja (1.4)', 'Kehadiran Rapat (1.5)', 'Beban Kerja SIKD (1.6)', 'Keterlambatan (1.7)', 'Responsetime (1.8)',
    'Potensi (2.1.1)', 
    'Kepemimpinan (2.2.1)', 'Integritas (2.2.2)', 'Kerjasama (2.2.3)', 'Komunikasi (2.2.4)', 'Orientasi pada hasil (2.2.5)', 'Pelayanan Publik (2.2.6)', 'Pengembangan diri(2.2.7)', 'Pengembangan Orang Lain (2.2.8)' ,'Mengelola Perubahan (2.2.9)',  'Pengambilan Keputusan (2.2.10)', 'Perekat Bangsa (2.2.11)', 'Komitmen Kualitas (2.2.12)',
    'Kualifikasi / Pendidikan (2.3.1)', 'Pangkat / Golongan (2.3.2)', 'Usia (2.3.3)', 'Pengalaman dalam jabatan (2.3.4)',  
    // 'Pelatihan Kepemimpinan (2.3.5)',  
    'Diklat Fungsional (2.3.6)', 'Pengembangan kompetensi (2.3.7)', 'Hukuman Disiplin (2.3.8)', 'Penghargaan (2.3.9)', 
    'Kemampuan Bahasa Asing (2.4.1)', 'Perilaku Kerja (2.4.2)', 'Penghargaan Kolaboratif (2.4.3)'
]
const labels_pelaksana = [
    'SKP (1.1)', 'E-Kinerja (1.2)', 'Kehadiran Presensi (1.3)','Kelebihan Jam Kerja (1.4)', 'Kehadiran Rapat (1.5)', 'Beban Kerja SIKD (1.6)', 'Keterlambatan (1.7)', 'Responsetime (1.8)',
    'Potensi (2.1.1)', 
    'Kepemimpinan (2.2.1)', 'Integritas (2.2.2)', 'Kerjasama (2.2.3)', 'Komunikasi (2.2.4)', 'Orientasi pada hasil (2.2.5)', 'Pelayanan Publik (2.2.6)', 'Pengembangan diri(2.2.7)', 'Pengembangan Orang Lain (2.2.8)' ,'Mengelola Perubahan (2.2.9)',  'Pengambilan Keputusan (2.2.10)', 'Perekat Bangsa (2.2.11)', 'Komitmen Kualitas (2.2.12)',
    'Kualifikasi / Pendidikan (2.3.1)', 'Pangkat / Golongan (2.3.2)', 'Usia (2.3.3)', 'Pengalaman dalam jabatan (2.3.4)',  
    // 'Pelatihan Kepemimpinan (2.3.5)',  
    // 'Diklat Fungsional (2.3.6)', 
    'Pengembangan kompetensi (2.3.7)', 'Hukuman Disiplin (2.3.8)', 'Penghargaan (2.3.9)', 
    'Kemampuan Bahasa Asing (2.4.1)', 'Perilaku Kerja (2.4.2)', 'Penghargaan Kolaboratif (2.4.3)'
]
const labels_original = [
    'SKP (1.1)', 'E-Kinerja (1.2)', 'Kehadiran Presensi (1.3)','Kelebihan Jam Kerja (1.4)', 'Kehadiran Rapat (1.5)', 'Beban Kerja SIKD (1.6)', 'Keterlambatan (1.7)', 'Responsetime (1.8)',
    'Potensi (2.1.1)', 
    'Kepemimpinan (2.2.1)', 'Integritas (2.2.2)', 'Kerjasama (2.2.3)', 'Komunikasi (2.2.4)', 'Orientasi pada hasil (2.2.5)', 'Pelayanan Publik (2.2.6)', 'Pengembangan diri(2.2.7)', 'Pengembangan Orang Lain (2.2.8)' ,'Mengelola Perubahan (2.2.9)',  'Pengambilan Keputusan (2.2.10)', 'Perekat Bangsa (2.2.11)', 'Komitmen Kualitas (2.2.12)',
    'Kualifikasi / Pendidikan (2.3.1)', 'Pangkat / Golongan (2.3.2)', 'Usia (2.3.3)', 'Pengalaman dalam jabatan (2.3.4)',  
    'Pelatihan Kepemimpinan (2.3.5)',  
    'Diklat Fungsional (2.3.6)', 
    'Pengembangan kompetensi (2.3.7)', 'Hukuman Disiplin (2.3.8)', 'Penghargaan (2.3.9)', 
    'Kemampuan Bahasa Asing (2.4.1)', 'Perilaku Kerja (2.4.2)', 'Penghargaan Kolaboratif (2.4.3)'
]

//** OPTIONS 1, Bagian Warna  */
const bgColorOriginal = [
    'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',

          'rgba(83, 184, 187, 0.6)',

          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',

          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 159, 64, 0.2)',
]
const bgColorStruktural = [
    'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',

          'rgba(83, 184, 187, 0.6)',

          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',

          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
        //   'rgba(75, 192, 192, 0.2)',
          
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 159, 64, 0.2)',
]
const bgColorFungsional = [
    'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',

          'rgba(83, 184, 187, 0.6)',

          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',

          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
        //   'rgba(75, 192, 192, 0.2)',
          
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 159, 64, 0.2)',
]
const bgColorPelaksana = [
    'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 99, 132, 0.2)',

          'rgba(83, 184, 187, 0.6)',

          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(153, 102, 255, 0.2)',

          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(75, 192, 192, 0.2)',
        //   'rgba(75, 192, 192, 0.2)',
        //   'rgba(75, 192, 192, 0.2)',
          
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 159, 64, 0.2)',
]





const borderColorOriginal  = [
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',

    'rgba(0, 54, 56, 1)',

    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',

    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
   
    'rgba(255, 159, 64, 1)',
    'rgba(255, 159, 64, 1)',
    'rgba(255, 159, 64, 1)',
]
const borderColorStruktural  = [
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',

    'rgba(0, 54, 56, 1)',

    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',

    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    // 'rgba(75, 192, 192, 1)',
   
    'rgba(255, 159, 64, 1)',
    'rgba(255, 159, 64, 1)',
    'rgba(255, 159, 64, 1)',
]
const borderColorFungsional  = [
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',

    'rgba(0, 54, 56, 1)',

    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',

    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    // 'rgba(75, 192, 192, 1)',
   
    'rgba(255, 159, 64, 1)',
    'rgba(255, 159, 64, 1)',
    'rgba(255, 159, 64, 1)',
]
const borderColorPelaksana  = [
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(255, 99, 132, 1)',

    'rgba(0, 54, 56, 1)',

    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(153, 102, 255, 1)',

    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(75, 192, 192, 1)',
    // 'rgba(75, 192, 192, 1)',
    // 'rgba(75, 192, 192, 1)',
   
    'rgba(255, 159, 64, 1)',
    'rgba(255, 159, 64, 1)',
    'rgba(255, 159, 64, 1)',
]
