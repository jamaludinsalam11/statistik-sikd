import React, {useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import PropTypes from 'prop-types';
import { makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import GaugeChart from 'react-gauge-chart';
import ReactSpeedometer from 'react-d3-speedometer'

const useStyles = makeStyles((theme) => ({

	contentWaktuRespon: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: 'auto',
		boxShadow: theme.shadow.large,
		// borderRadius: 15
	},
	contentWaktuResponSuccess: {
		color: theme.palette.text.success
	},
	contentWaktuResponError: {
		color: theme.palette.text.error
	},
	contentWaktuResponPercentageSuccess: {
		color: theme.palette.text.success,
		display: 'flex',
		alignItems: 'center',
		flexWrap: 'wrap',
		fontSize: '2rem',
	},
	contentWaktuResponPercentageError: {
		color: theme.palette.text.error,
		display: 'flex',
		alignItems: 'center',
		flexWrap: 'wrap',
		fontSize: '3rem'
	},
	contentWaktuResponButton:{
		marginTop: theme.spacing(2),
		backgroundColor: theme.palette.text.warning,
		color: 'white',
		borderRadius: 20,
		'&:hover': {
			backgroundColor: '#EFAF00',
		 },
	},


}));



function scaleValue(value, from, to) {
  var scale = (to[1] - to[0]) / (from[1] - from[0]);
  var capped = Math.min(from[1], Math.max(from[0], value)) - from[0];
  return ~~(capped * scale + to[0]);
}

const ContentWaktuRespon = () => {
    const classes = useStyles();
    const [respon, setRespon] = useState({respon:[], average_all:[]})
    const [ min, setMin ]     = useState(null);
    const [ max, setMax ]     = useState(null);
    const [ avg, setAvg ]     = useState(null);
    const [ value, setValue ] = useState(null);
    const [ unread, setUnread ] = useState(null);

    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1 = new URLSearchParams(search).get('tgl1');
    const tgl2 = new URLSearchParams(search).get('tgl2');
    
 	useEffect(() => {
		const fetchRespon = async () => {
			try{
				await Promise.all([
                  fetch(`${process.env.REACT_APP_API_WAKTURESPONSE}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                  fetch(`${process.env.REACT_APP_API_AVERAGERESPONSE}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                  fetch(`${process.env.REACT_APP_API_BELUMDIBACA}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`)
                ])
                .then((responses) => {
                    return Promise.all(responses.map(function (response) {
                        return response.json();
                    }));
                })
                .then((data) => {
                    // debug API TTDE
                    // 1. Initial Asal Sumber
                    const waktu_response = data[0].data[0].waktu_response;
                    const respon_menit = data[0].data[0].wt;
                    const average_response = data[1].data[0].average_response;
                    const average_menit = data[1].data[0].wt;
                    const checknull = Number(data[0].data[0].wt2);

                    // console.log('waktu response' , data[0].data[0].waktu_response)
                    // console.log('average response' , data[1].data[0].average_response)

                    // Remove commas
                    const rm_average_wt = Number(data[1].data[0].wt2);
                    const rm_respon_wt2 = Number(data[0].data[0].wt2);
                    // console.log('DEGUBBBB:', Number(data[0].data[0].wt2));

                    const after_average_wt = Math.trunc(rm_average_wt);
                    const after_respon_wt2 = Math.trunc(rm_respon_wt2);
                    const fix_average_wt = Number(after_average_wt);
                    const fix_respon_wt2 = Number(after_respon_wt2);
                    
                    const pMax = fix_average_wt * 3;
                    const pMin = fix_average_wt * (-3);
                    const avg = fix_average_wt;
                    const max = fix_average_wt + pMax;
                    const min = fix_average_wt + pMin;
                    
                    const max2 = fix_average_wt * 7;
                    const avg2 = fix_average_wt;
                    const wt2  = fix_respon_wt2
                    // console.log('fix_average_wt', fix_average_wt)
                    // Choose Deviation
                    const oneday          = 86400;
                    const setengahjam     = 1800;
                    const satujam         = 3600;
                    const duajam          = 7200;
                    const tigajam         = 10800;
                    const empatjam        = 14400;
                    const limajam         = 18000;
                    const enamjam         = 21600;
                    const tujuhjam        = 25200;
                    const delapanjam      = 28800;
                    const sembilanjam     = 32400;
                    const sepuluhjam      = 36000;
                    const sebelasjam      = 39600;
                    const duabelasjam     = 43200;
                    const tigabelasjam    = 46800;
                    const empatbelasjam   = 50400;
                    const limabelasjam    = 54000;
                    const enambelasjam    = 57600;
                    const tujuhbelasjam   = 61200;
                    const delapanbelasjam = 64800;
                    const sembilasjam     = 68400;
                    const duapuluhjam     = 72000;
                    const duapuluhsatujam = 75560;
                    const duapuluhduajam  = 79200;
                    const duapuluhtigajam = 82800;
                    const halfday         = oneday * 1 + 1;
                    const twoday          = oneday * 2 + 1;
                    const thirdday        = oneday * 3 + 1;
                    const fouthday        = oneday * 4 + 1;
                    const fifthday        = oneday * 5 + 1;
                    const sixday          = oneday * 6 + 1;
                    const sevenday        = oneday * 7 + 1;
                    const eightday        = oneday * 8 + 1;
                    const nineday         = oneday * 9 + 1;
                    const tenday          = oneday * 10 + 1;
                    const elevenday       = oneday * 11 + 1;
                    const twelveday       = oneday * 12 + 1;
                    const thirdteenday    = oneday * 13 + 1;
                    const fourteenday     = oneday * 14 + 1;
                    const fiftheenday     = oneday * 15 + 1;
                    const sixteenday      = oneday * 16 + 1;
                    const seventeenday    = oneday * 17 + 1;
                    const eightteenday    = oneday * 18 + 1;
                    const nineteenday     = oneday * 19 + 1;
                    const twentyday       = oneday * 20 + 1;
                    const twentyoneday    = oneday * 21 + 1;
                    const twentytwoday    = oneday * 22 + 1;
                    const twentythreeday  = oneday * 23 + 1;
                    const twentyfourday   = oneday * 24 + 1;
                    const twentyfiveday   = oneday * 25 + 1;
                    const twentysixday    = oneday * 26 + 1;
                    const twentysevenday  = oneday * 27 + 1;
                    const twentyeightday  = oneday * 28 + 1;
                    const twentynineday   = oneday * 29 + 1;
                    const thirdtyday      = oneday * 30 + 1;
                    const thirdtoneyday   = oneday * 31 + 1;
                    const thirdttwoyday   = oneday * 32 + 1;
                    const thirdtythreeday = oneday * 33 + 1;
                    const thirdtyfourday  = oneday * 34 + 1;
                    const thirdtyfiveday  = oneday * 35 + 1;
                    const constantaDay = 
                                        fix_average_wt < setengahjam ? setengahjam * 1:
                                        fix_average_wt < satujam ? satujam * 1:
                                        fix_average_wt < duajam ? duajam * 1:
                                        fix_average_wt < tigajam ? tigajam  * 1:
                                        fix_average_wt < empatjam ? empatjam * 1 :
                                        fix_average_wt < limajam ? limajam *1 :
                                        fix_average_wt < enamjam ? enamjam * 1 :
                                        fix_average_wt < tujuhjam ? tujuhjam * 1  :
                                        fix_average_wt < delapanjam ? delapanjam * 1 :
                                        fix_average_wt < sembilanjam ? sembilanjam * 1 :
                                        fix_average_wt < sepuluhjam ? sepuluhjam * 1 :
                                        fix_average_wt < sebelasjam ? sebelasjam * 1:
                                        fix_average_wt < duabelasjam ? duabelasjam * 1:
                                        fix_average_wt < tigabelasjam ? tigabelasjam * 1:
                                        fix_average_wt < empatbelasjam ? empatbelasjam * 1:
                                        fix_average_wt < limabelasjam ? limabelasjam * 1:
                                        fix_average_wt < enambelasjam ? enambelasjam * 1:
                                        fix_average_wt < tujuhbelasjam ? tujuhbelasjam * 1:
                                        fix_average_wt < delapanbelasjam ? delapanbelasjam * 1:
                                        fix_average_wt < sembilasjam ? sembilasjam * 1:
                                        fix_average_wt < duapuluhjam ? duapuluhjam * 1:
                                        fix_average_wt < duapuluhsatujam ? duapuluhsatujam * 1:
                                        fix_average_wt < duapuluhduajam ? duapuluhduajam * 1:
                                        fix_average_wt < duapuluhtigajam ? duapuluhtigajam :
                                        fix_average_wt < halfday ? halfday :
                                        fix_average_wt < oneday ? oneday :
                                        fix_average_wt < twoday ? twoday * 1 :
                                        fix_average_wt < thirdday ? thirdday * 1 :
                                        fix_average_wt < fouthday ? fouthday * 1:
                                        fix_average_wt < fifthday ? fifthday * 1:
                                        fix_average_wt < sixday ? sixday :
                                        fix_average_wt < sevenday ? sevenday :
                                        fix_average_wt < eightday ? eightday :
                                        fix_average_wt < nineday ? nineday :
                                        fix_average_wt < tenday ? tenday :
                                        fix_average_wt < elevenday ? elevenday :
                                        fix_average_wt < twelveday ? twelveday :
                                        fix_average_wt < thirdteenday ? thirdteenday :
                                        fix_average_wt < fourteenday ? fourteenday :
                                        fix_average_wt < fiftheenday ? fiftheenday :
                                        fix_average_wt < sixteenday ? sixteenday :
                                        fix_average_wt < seventeenday ? seventeenday :
                                        fix_average_wt < eightteenday ? eightteenday :
                                        fix_average_wt < nineteenday ? nineteenday :
                                        fix_average_wt < twentyday ? twentyday :
                                        fix_average_wt < twentyoneday ? twentyoneday:
                                        fix_average_wt < twentytwoday ? twentytwoday :
                                        fix_average_wt < twentythreeday ? twentythreeday:
                                        fix_average_wt < twentyfourday ? twentyfourday :
                                        fix_average_wt < twentyfiveday ? twentyfiveday :
                                        fix_average_wt < twentysixday ? twentysixday :
                                        fix_average_wt < twentysevenday ? twentysevenday :
                                        fix_average_wt < twentyeightday ? twentyeightday :
                                        fix_average_wt < twentynineday ? twentynineday :
                                        fix_average_wt < thirdtyday ? thirdtyday :
                                        fix_average_wt < thirdtoneyday ? thirdtoneyday :
                                        fix_average_wt < thirdttwoyday ? thirdttwoyday :
                                        fix_average_wt < thirdtythreeday ? thirdtythreeday :
                                        fix_average_wt < thirdtyfourday ? thirdtyfourday :
                                        thirdtyfiveday
                     ;
                    
                    // Generate Value from User Respon & Average Respon
                   
                    // const vMax    = vMin > 0 ? (fix_average_wt + thirdday - vMin) : ;
                    const vAvg    = fix_average_wt
                    const vRespon = fix_respon_wt2 

                    // Rumus 
                    const maxOld  = fix_average_wt + constantaDay ;
                    const minOld  = fix_average_wt - constantaDay  ;
                    
                    // console.log('minOld', minOld)
                    const checkMinPositiveNegative = minOld > 0 ? minOld - minOld : minOld - minOld;
                    const checkMaxPositiveNegative = minOld > 0 ? maxOld - minOld : maxOld - minOld;
                    const checlValPositive = minOld > 0 ? vRespon - minOld : vRespon - minOld;
                    const FIXmin = scaleValue(checkMinPositiveNegative, [checkMinPositiveNegative,checkMaxPositiveNegative] , [0,100])
                    const FIXmax = scaleValue(checkMaxPositiveNegative, [checkMinPositiveNegative,checkMaxPositiveNegative] , [0,100])
                    const FIXval = scaleValue(checlValPositive, [checkMinPositiveNegative,checkMaxPositiveNegative] , [0,100])
                    // Kalau minOld negative maka harus  maxOld (-) -minOld ditambah agar menjadi positif
                    // Jika minOld positive maka harus maxOld (+) -minOld agar menjadi 0
                    // console.log('checkMinPositiveNegative', checkMinPositiveNegative)
                    // console.log('checkMaxPositiveNegative', checkMaxPositiveNegative)
                    // console.log('vRespon',vRespon)
                    // console.log('minOld',minOld)
                    // console.log('maxOld',maxOld)
                    // console.log('FIXmin',FIXmin)
                    // console.log('FIXmax',FIXmax)
                    // console.log('FIXval',FIXval)

                    /**
                     * Update baru dari Rapat Internal SIKD 22 Juli 2021
                     * - Jika belum baca pesan melebihi 30 surat, maka akan diset very slow
                     */
                    const unread_pre = data[2].data === "Not Found" ? 0 : Number(data[2].total)
                    // console.log('unread', unread)
                    const unread = unread_pre
                    // console.log('constantaDay', constantaDay)
                    // console.log('fix_average_wt', vAvg)
                    // console.log('Number(data[0].data[0].wt2)', Number(data[0].data[0].wt2))
                    setUnread(unread)
                    setMin(FIXmax)
                    setMax(FIXmin)
                    // console.log('',)
                    // setAvg(constanta_avg)
                    setValue(unread > 30 ? FIXmax : Number(data[0].data[0].wt2) === 0 ? 0 : FIXval)
                    // setValue(unread > 30 ? FIXmax : Number(data[0].data[0].wt2))
                    const coba2 = unread > 30 ? FIXmax : Number(data[0].data[0].wt2) === 0 ? 0 : FIXval
                    console.log('coba2', coba2)
                    console.log('FIXmax', FIXmax)
                    console.log('FIXval', FIXval)
                    console.log('FIXmin', FIXmin)
                    const valll = 100 - FIXval
                    const coba = scaleValue(valll, [FIXmin, FIXmax], [0,100])
                    // console.log('coba', coba)
                    // 2. Create Object
                    const objRespon =  {
                            "waktu_response"    : waktu_response,
                            "response_menit"    : respon_menit,
                            "wt"                : fix_respon_wt2 
                        }

                    const objAverage = {
                        "average_response"  : average_response,
                        "average_menit"     : average_menit,
                        "avg"               : avg,
                    }
                    

                    // 2. setState 
                    setRespon({respon: objRespon, average_all: objAverage})
                    
                })
			} catch(err){
				console.log(err)
			}
		}
		fetchRespon();
	}, [])
    
  // console.log('setMin:', min )
  // console.log('setMax:', max )
  // console.log('setValue:', value )
    return (
        <Grid 
          item 
          lg={6}
          sm={6}
          xl={6}
          xs={12}>
            <Paper className={classes.contentWaktuRespon}>
                <Grid item md={12}>
                    <Typography variant="h5" gutterBottom>Waktu Response</Typography>
                    
                    <Divider/>
                </Grid>
                <Grid 
                  item 
                  lg={12}
                  sm={12}
                  xl={12}
                  xs={12}
                  style={{height: '380px',}}
                >
                  {/* <h1>{min? `min: ${min}`: ''}</h1>
                  <h1>{max? `min: ${Number(max)}`: ''}</h1>
                  <h1>{value? `value: ${Number(value) == 0 ? Number(min) : Number(value)}`: ''}</h1> */}
                    <div style={{paddingTop: '3em', width:'100%', margin:'auto', height: '100%'}}>
                        <ReactSpeedometer 
                          forceRender={true}
                            width={500}
                            // height={450}
                            // minValue={0} 
                            fluidWidth={true}
                            minValue={Number(min)} 
                            // minValue={Number(min)} 
                            // maxValue={Number(max) }                    
                            maxValue={Number(max) === 0 ? 0 : Number(max)}                    
                            value={Number(value) ? Number(value) : Number(value)  }    
                            // value={6}    
                            needleHeightRatio={0.8}                    
                            currentValueText={unread > 30 ? 'Harap semua surat dibaca!' : respon.respon.waktu_response ? respon.respon.waktu_response : 'Belum ada' }
                            ringWidth={100}
                            valueTextFontWeight="bold"  
                            // customSegmentStops={min ? [1000,901,634,367,100,0] : ''}                 
                            customSegmentStops={min ? [100,90,63.4,36.6,10,0] : ''}                 
                            customSegmentLabels={[                    
                              {                    
                                text: 'Very Slow',                    
                                position: 'OUTSIDE',                    
                                color: '#555',                    
                              },                    
                              {                    
                                text: 'Slow',                    
                                position: 'OUTSIDE',                    
                                color: '#555',                    
                              },                    
                              {                    
                                text: 'Average',                    
                                position: 'OUTSIDE',                    
                                color: '#555',                    
                                // fontSize: '19px',                    
                              },                    
                              {                    
                                text: 'Fast',                    
                                position: 'OUTSIDE',                    
                                color: '#555',                    
                              },                    
                              {                    
                                text: 'Very Fast',                    
                                position: 'OUTSIDE',                    
                                color: '#555',                    
                              },                    
                            ]}         
                            segmentColors={['#FF471A', '#F6961E', '#ECDB23', '#AEE228', '#6AD72D']}    
                            needleTransitionDuration={3333}                    
                            needleTransition="easeElastic"                    
                            needleColor={'#00587A'}                    
                            textColor={'#626262'}
                            segments={5}
                        />
                    </div>
                </Grid>
                
                <Grid item md={12}>
                  {unread > 30 
                    ?   <div>
                          <div>
                            <Typography 
                              className={classes.contentWaktuResponError} 
                              variant="caption"
                            >
                              Melebihi ambang batas surat yang tidak dibaca (30 surat)
                            </Typography>
                          </div>
                          <div>
                            <Typography 
                              className={classes.contentWaktuResponError} 
                              variant="caption"
                            >
                              maka waktu response menjadi Very Slow
                            </Typography>
                          </div>
                        </div>
                    : ''
                  }
                  <Typography 
                    className={classes.contentWaktuResponError} 
                    variant="subtitle2"
                  >
                    Rata-rata waktu response seluruh pegawai MK :
                  </Typography>
                  <Typography 
                    className={classes.contentWaktuResponError} 
                    variant="h5"
                  >
                    {respon.average_all.average_response}
                  </Typography>
                </Grid>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={1}
                >

                  
                </Grid>
                </Paper>
            
            </Grid>
    )
}

ContentWaktuRespon.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  wt:PropTypes.number,
  minValue:PropTypes.number,
  maxValue:PropTypes.number,
  value:PropTypes.number,
  
}

export default ContentWaktuRespon;