import React, {useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import moment from 'moment';
import 'moment/locale/id.js'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ContentGrafikTerbaca from './contentGrafikSuratTerbaca';
import Skeleton from '@material-ui/lab/Skeleton';

import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';

import DoneIcon from '@material-ui/icons/Done';
import { Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
	contentSuratTerbaca: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '400px',
		boxShadow: theme.shadow.large,
	},
	contentSuratTerbacaGrid: {
		paddingTop: theme.spacing(2)
	},
	contentSuratTerbacaGrid2: {
		paddingTop: theme.spacing(2),
		paddingBottom: theme.spacing(2),
		textAlign: 'center'
	},
	contentTingkatTanggapSIKD: {
		color: theme.palette.text.success
	},
	contentSuratTerbacaDibaca: {
		color: theme.palette.text.success,
		textAlign: 'center'
	},
	contentSuratTerbacaBelumDibaca: {
		color: theme.palette.text.error
	},
	buttonLink: {
		borderRadius: '50px',
		fontSize: '0.8rem',
		'&:hover': {
			backgroundColor: '#3f51b5',
			boxShadow: 'none',
			color: 'white',
			borderRadius: '50px'
		},
		'&:active': {
			boxShadow: 'none',
			backgroundColor: 'black',
			color: '',
			borderRadius: '50px'
		},
	},
	ratarataAllMK: {
		background: '#5D95FF',
		padding: '10px 20px 10px 20px',
		borderRadius: '25px',
		marginRight: '-4em',
		color: 'white'
	}

}));


const ContentSuratTerbacaV2Hakim = () => {
    const classes = useStyles();

    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1_pre 	= new URLSearchParams(search).get('tgl1');
    const tgl2_pre 	= new URLSearchParams(search).get('tgl2');
	const tgl1		= moment(tgl1_pre).format('YYYY-MM-DD')
	const tgl2		= moment(tgl2_pre).format('YYYY-MM-DD')
	const tahun_now = moment().format('YYYY');
	const tahun_input = moment(tgl1).format('YYYY');
	//Arahan Sekjen 26 Oktober 2022 by WA
	const tgl3		= tahun_now === tahun_input ? moment().format('YYYY-MM-DD') : moment(tgl1).endOf('years').format('YYYY-MM-DD')
	const tahun     = moment(tgl1).format('YYYY')
	const [tgl, setTgl] = useState({tgl1: tgl1, tgl2:tgl2})
	const [read, setRead] = useState(null)
	const [unread, setUnread] = useState(null)
	const [text, setText] = useState(null)
	const [percentage, setPercentage] = useState(null)
	const [suratkeluar, setSuratkeluar] = useState(null)
	const [totalSuratMasuk, setTotalSuratMasuk] = useState(null)
	const [avgSuratMK, setAvgSuratMK] = useState(null)




	useEffect(() => {
		const fetchSuratV2 = async() => {
			try{
				const apiTotalSuratMasuk = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/profil-pemakaian/tingkat-tanggap-sikd/totalsuratmasuk/${select_id}/${tgl1}/${tgl2}`
				const fetchTotalSurat = await fetch(apiTotalSuratMasuk)
				const resTotalSurat = await fetchTotalSurat.json()
				const { 
					percentage, disposisi_unread, memo_unread, notadinas_unread ,
					tembusan_unread, text, total, ttd_unread, surat_read, surat_unread, surat_keluar
				} = resTotalSurat

				const apiAvgDev = `http://localhost:3001/sikd/api/v1/profil-pemakaian/tingkat-tanggap-sikd/last-avg-surat-mk/${tahun}`
				const apiAvgProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/profil-pemakaian/tingkat-tanggap-sikd/last-avg-surat-mk/${tahun}`
				const apiAvg = process.env.NODE_ENV === 'development' ? apiAvgDev : apiAvgProd
				const fetchAvgSuratMK = await fetch(apiAvg)
				const resAvgSuratMK  = await fetchAvgSuratMK.json()
				const suratmasuk_total = surat_read + surat_unread + surat_keluar


				setPercentage(percentage);
				setTgl({tgl1: tgl1, tgl2: tgl2})
				setRead(surat_read.toLocaleString('en-US'))
				setUnread(surat_unread.toLocaleString('en-US'))
				setText(text)
				setSuratkeluar(surat_keluar.toLocaleString('en-US'))
				setTotalSuratMasuk(suratmasuk_total.toLocaleString('en-US'))

				/** resAvgSuratMK */
				setAvgSuratMK(resAvgSuratMK[0])
			} catch(err) {
				console.log('error suratTerbacaV2',err)
			}
		}
		fetchSuratV2()

	}, [])
    return(
		<Grid item style={{width: '100%'}}
			// xl={12}
			xs={12}
			
		>
			<Paper className={classes.contentSuratTerbaca} >
				<Typography variant="h5" gutterBottom>Dokumen Elektronik</Typography>
				<Divider />
				<Grid
					container
					direction="row"
					justify="center"
					alignItems="center"
					spacing={1}
					
				>
					<Grid item xs={12} lg={12} >
						
					</Grid>
					<Grid item xs={12} lg={12} >
						<Grid
							container
							direction="row"
							justify="center"
							alignItems="center"
							spacing={5}
							
						>
							
							{/* <Grid item md={2} xs={2} sm={2} lg={2} style={{paddingTop: '3em'}}>
								<div style={{marginLeft: '2em', marginRight: '-2em'}}>
									<Typography variant="subtitle2" gutterBottom style={{paddingBottom: '.5em'}}>Tingkat Tanggap SIKD</Typography>
							
									{percentage 
										? <ContentGrafikTerbaca percentage={percentage}/> 
										: <Skeleton animation="wave" variant="circle" width={110} height={110} style={{ margin: 'auto', backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
									}
									
								</div>
							

							</Grid> */}
							<Grid item md={12} xs={12} sm={12} lg={12} >
								<Grid
									container
									direction="row"
									justifyContent="center"
									alignItems="flex-start"
									spacing={5}
									
								>
										<Grid item md={2} xs={2} sm={2} lg={2} className={classes.contentSuratTerbacaDibaca} style={{textAlign: 'center'}}>
											<Typography variant="subtitle2">Surat Masuk</Typography>
											<Typography variant="h4">{read&&read}</Typography> 
											<Typography variant="subtitle2"> </Typography>
										</Grid>
										<Grid item md={2} xs={2} sm={2} lg={2}  className={classes.contentSuratTerbacaBelumDibaca} style={{textAlign: 'center'}}>
											<Typography variant="subtitle2">Belum Dibaca</Typography>
											<Typography variant="h4">{unread && unread} </Typography>
											<Typography variant="caption"> {text && text} </Typography>
											
										</Grid>
										<Grid item md={2} xs={2} sm={2} lg={2} className={classes.contentSuratTerbacaBelumDibaca} style={{textAlign: 'center', color: '#2A0CBD'}}>
											<Typography variant="subtitle2" >Surat Keluar</Typography>
											<Typography variant="h4">{suratkeluar && suratkeluar} </Typography>
											{/* <Typography variant="caption"> {text && text} </Typography> */}
											
										</Grid>
										<Grid item md={2} xs={2} sm={2} lg={2} className={classes.contentSuratTerbacaBelumDibaca} style={{textAlign: 'center', color: '#2A0CBD'}}>
											<Typography variant="subtitle2" >Total Surat</Typography>
											<Typography variant="h4">{totalSuratMasuk} </Typography>
											{/* <Typography variant="caption"> {text && text} </Typography> */}
											
										</Grid>
										<Grid item md={2} xs={2} sm={2} lg={2}  className={classes.contentSuratTerbacaBelumDibaca} style={{textAlign: 'center', color: 'rgb(210, 128, 21)',marginTop: '0em'}}>
											<div className={classes.ratarataAllMK}>
											<Typography variant="subtitle2" >Rata-rata total surat seluruh pegawai MK</Typography>
											<Typography variant="h4" style={{fontWeight: 'bold'}}>
												{
													avgSuratMK == null ? '' : parseFloat(Number(avgSuratMK.avg_surat_masuk)+Number(avgSuratMK.avg_surat_keluar)).toFixed(0).toLocaleString('en-US')
													
												} 
											</Typography>
											</div>
											
											
										</Grid>
								</Grid>

								<Typography variant="subtitle2" align="center" >
									Pesan Telah Dibaca dan Belum Dibaca Pada periode bulan
										</Typography>
								<div></div>
								<Paper style={{marginLeft: '3em', marginRight: '3em', paddingTop: '1em', paddingBottom: '1em', borderRadius: 25, backgroundColor: '#2A0CBD', opacity: .8, color: '#fff'}}>
									<Typography variant="h4" align="center" style={{color: '#fff'}} >
										{moment(tgl1).lang('id').format('Do MMMM YYYY ')}-  {moment(tgl3).lang('id').format('Do MMMM YYYY')}
									</Typography>

								</Paper>
								<div style={{marginTop: '.4em'}}>
								<Typography variant="subtitle2">Sumber data:</Typography>
								<div>
									<Button href="https://sikd.mkri.id" color='primary'  className={classes.buttonLink}>SIKD</Button>
									<Button href="https://apps.mkri.id" color='primary'  className={classes.buttonLink}>Absensi Online</Button>
									<Button href="https://apps.mkri.id" color='primary'  className={classes.buttonLink}>SIMPEG</Button>
									<Button href="https://sivika.mkri.id/" color='primary'  className={classes.buttonLink}>SIVIKA</Button>
									<Button href="https://e-minutasi.mkri.id/" color='primary'  className={classes.buttonLink}>E-Minutasi</Button>
									<Button href="http://192.168.30.31/penilaian/personallogin" color='primary'  className={classes.buttonLink}>Penilaian 360</Button>
									{/* <Button href="https://sikd.mkri.id" style={{color: 'rgba(0, 0, 0, 0.54)'}}>SIKD</Button>
									<Button href="https://sikd.mkri.id" style={{color: 'rgba(0, 0, 0, 0.54)'}}>SIKD</Button> */}
									{/* <Typography variant="caption">SIKD, Absensi Online, SIMPEG, SIVIKA, Penilaian 360, </Typography> */}
								</div>
								<div>
									<Button href="https://checklist.mkri.id/"  color='primary'  className={classes.buttonLink}>Checklist Online</Button>
									<Button href="https://sikd.mkri.id/cat_baru/cat/ujian/index.php"  color='primary'  className={classes.buttonLink}>Assesment Center</Button>
									<Button  color='primary'  className={classes.buttonLink}>Data Sekunder</Button>
									<Button href="#"  color='primary'  className={classes.buttonLink}>Data Digital Lainnya</Button>
									{/* <Typography variant="caption">CheckList Online, Assessment Center, Data Sekunder, dan data digital lainnya</Typography> */}
								</div>
								{/* <Chip
									size="medium"
									// avatar={<Avatar>S</Avatar>}
									label="SIKD"
									// clickable
									color="primary"
									// onDelete={handleDelete}
									deleteIcon={<DoneIcon />}
									style={{marginRight: '2px'}}
								/>
								<Chip
									size="medium"
									// avatar={<Avatar>S</Avatar>}
									label="SIMPEG"
									// clickable
									color="primary"
									// onDelete={handleDelete}
									deleteIcon={<DoneIcon />}
									style={{marginRight: '2px', marginLeft: '2px'}}
								/>
								<Chip
									size="medium"
									// avatar={<Avatar>S</Avatar>}
									label="SIVIKA"
									// clickable
									color="primary"
									// onDelete={handleDelete}
									deleteIcon={<DoneIcon />}
									style={{marginRight: '2px', marginLeft: '2px'}}
								/>
								<Chip
									size="medium"
									// avatar={<Avatar>A</Avatar>}
									label="Absensi Online"
									// clickable
									color="primary"
									// onDelete={handleDelete}
									deleteIcon={<DoneIcon />}
									style={{marginRight: '2px', marginLeft: '2px'}}
								/>
								<Chip
									size="medium"
									// avatar={<Avatar>A</Avatar>}
									label="Data Sekunder"
									// clickable
									color="primary"
									// onDelete={handleDelete}
									deleteIcon={<DoneIcon />}
									style={{ marginLeft: '2px'}}
								/>
								<Chip
									size="medium"
									// avatar={<Avatar>A</Avatar>}
									label="Penilaian 360"
									clickable
									color="primary"
									// onDelete={handleDelete}
									deleteIcon={<DoneIcon />}
									style={{ marginLeft: '2px'}}
								/>
								<Chip
									size="medium"
									// avatar={<Avatar>A</Avatar>}
									label="Assessment Center"
									clickable
									color="primary"
									// onDelete={handleDelete}
									deleteIcon={<DoneIcon />}
									style={{ marginLeft: '2px'}}
								/> */}
								</div>
							</Grid>

						</Grid>
					</Grid>


				</Grid>
			</Paper>
		</Grid>
    )
}

export default ContentSuratTerbacaV2Hakim;