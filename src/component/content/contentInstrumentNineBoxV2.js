import React,{useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import moment from 'moment';
import Moment from 'moment'
import { extendMoment } from 'moment-range';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { Bar } from 'react-chartjs-2';



const useStyles = makeStyles((theme) => ({
	contentTotalAsalSurat: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '530px',
		boxShadow: theme.shadow.large,
		// borderRadius: 15
	},

}));

function scaleValue(value, from, to) {
    var scale = (to[1] - to[0]) / (from[1] - from[0]);
    var capped = Math.min(from[1], Math.max(from[0], value)) - from[0];
    return ~~(capped * scale + to[0]);
  }
function convertToBulan(value){
	const hasilBulan = 
		value == 1 
		? 'Januari' 
		: value == 2 
		? 'Februari' 
		: value == 3
		? 'Maret'
		: value == 4  
		? 'April'
		: value == 5
		? 'Mei'
		: value == 6
		? 'Juni'
		: value == 7
		? 'Juli'
		: value == 8
		? 'Agustus'
		: value == 9
		? 'September'
		: value == 10
		? 'Oktober'
		: value == 11
		? 'November'
		: 'Desember'
	return hasilBulan
}
const Sorting = (data) =>{
  
	// Standar Sort Javascript - stable
	const sort = data.sort((a,b) => a.id_calendar - b.id_calendar)
	return sort
}


const ContentInstrumentNineBoxV2 = () => {
    const classes = useStyles();
    const [data, setData] = useState(null)
    const [options, setOptions] = useState(null)
    let search  = useLocation().search
    const select_id1 = new URLSearchParams(search).get('select_id')
    const tgl1 = new URLSearchParams(search).get('tgl1')
    const tgl2 = new URLSearchParams(search).get('tgl2')
    const tgl1_rapat = moment(tgl1).format('YYYY-MM-DD')
    const tgl2_rapat = moment(tgl2).format('YYYY-MM-DD')

    /**
	 * Declare Date for absensi
	 */
	const moment2 			= extendMoment(Moment)
	const bulanMulaiArray 	= moment(tgl1).format('YYYY-MM-DD')
	const bulanAkhirArray 	= moment(tgl2).format('YYYY-MM-DD')
	const range 			= moment2.range(bulanMulaiArray, bulanAkhirArray)
	const r2 				= range.snapTo('YYYY')
	const month_number 		= Array.from(r2.by('months')).map(m => Number(m.format('M')))
	const month_name 		= Array.from(r2.by('months')).map(m => convertToBulan(Number(m.format('MM'))))
	const tahun_now 		= moment().format('YYYY')

    useEffect(() => {
        const fetchAll = async () => {
            try{
                const apiUser   = `${process.env.REACT_APP_API_USER}?select_id=${select_id1}`
                const fetchUser = await fetch(apiUser)
                const resUser   = await fetchUser.json()
                const user      = resUser.data[0]
                const no_nip	= resUser.data[0].no_nip

                /**
                 * Tambahan initilaizz untuk absensi
                 */
                 const apiFetchUserApps = `${process.env.REACT_APP_API_APPS_PEGAWAI}/${no_nip}`
                 const fetchUserApps 	= await fetch(apiFetchUserApps)
                 const resUserApps 	    = await fetchUserApps.json()
                 const pgw_id		    = resUserApps[0].pgw_id

                const role_id       = user.RoleId
                const select_unit   = user.RoleAtasan === 'uk.1.1' ? user.RoleId : user.RoleAtasan
                const select_id     = user.PeopleId
                console.log('pgw_id', pgw_id)
                console.log('select_unit', select_unit)
                console.log('select_id', select_id)
                
                // SIKD, Rapat, Surat, responseTime
                const apiStatistikUnit      = `${process.env.REACT_APP_API_STATISTIK_UNIT}?select_id=${select_unit}&&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiStatistikPerson    = `${process.env.REACT_APP_API_STATISTIK_PERSON}?select_roleid=${role_id}&select_id=${select_unit}&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiRapat              = `${process.env.REACT_APP_API_RAPAT}?select_id=${select_id}&tgl1=${tgl1_rapat}&tgl2=${tgl2_rapat}`
                const apiWaktuResponse      = `${process.env.REACT_APP_API_WAKTURESPONSE}?select_id=${select_id1}&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiAverageResponse    = `${process.env.REACT_APP_API_AVERAGERESPONSE}?select_id=${select_id1}&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiBelumDibaca        = `${process.env.REACT_APP_API_BELUMDIBACA}?select_id=${select_id1}&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiSudahDibaca        = `${process.env.REACT_APP_API_SUDAHDIBACA}?select_id=${select_id1}&tgl1=${tgl1}&tgl2=${tgl2}`
				const apiUnreadTTD          = `${process.env.REACT_APP_API_UNREAD_TTD}?select_id=${select_id1}&tgl1=${tgl1}&tgl2=${tgl2}`
				const apiUnreadDisposisi    = `${process.env.REACT_APP_API_UNREAD_DISPOSISI}?select_id=${select_id1}&tgl1=${tgl1}&tgl2=${tgl2}`
				const apiUnreadNotadinas    = `${process.env.REACT_APP_API_UNREAD_NOTADINAS}?select_id=${select_id1}&tgl1=${tgl1}&tgl2=${tgl2}`
				const apiUnreadTembusan     = `${process.env.REACT_APP_API_UNREAD_TEMBUSAN}?select_id=${select_id1}&tgl1=${tgl1}&tgl2=${tgl2}`
                
                /**
                 * Fetch
                 */
                const [fetchStatistikUnit, fetchStatistikPerson, fetchRapat, 
                    fetchWaktuResponse, fetchAverageResponse, fetchBelumDibaca,
                    fetchSudahDibaca, fetchUnreadDisposisi,
                    fetchUnreadNotadinas, fetchUnreadTembusan
                   
                ] = await Promise.all([
                    fetch(apiStatistikUnit),
                    fetch(apiStatistikPerson),
                    fetch(apiRapat),
                    fetch(apiWaktuResponse),
                    fetch(apiAverageResponse),
                    fetch(apiBelumDibaca),
                    fetch(apiSudahDibaca),
                    fetch(apiUnreadDisposisi),
                    fetch(apiUnreadNotadinas),
                    fetch(apiUnreadTembusan),
                ])

                /**
                 * Response json
                 */
                const resStatistikUnit      = await fetchStatistikUnit.json()
                const resStatistikPerson    = await fetchStatistikPerson.json()
                const resRapat              = await fetchRapat.json()
                const resWaktuResponse      = await fetchWaktuResponse.json()
                const resAverageResponse    = await fetchAverageResponse.json()
                const resBelumDibaca        = await fetchBelumDibaca.json()
                const resSudahDibaca 	    = await fetchSudahDibaca.json()
				const resUnreadDisposisi    = await fetchUnreadDisposisi.json()
				const resUnreadNotadinas    = await fetchUnreadNotadinas.json()
				const resUnreadTembusan	    = await fetchUnreadTembusan.json()


                console.log('resWaktuResponse', resWaktuResponse)
                console.log('resAverageResponse', resAverageResponse)
                console.log('resBelumDibaca', resBelumDibaca)

                /**
                 * Processing
                 * ----------------
                 * SIKD, Rapat, Response Time, Absensi, SKP
                 */

                //Rapat
                const rapat_percentage = resRapat.data[0].percentage

                //SIKD
                const total_pesan_person    = resStatistikPerson.data[0].jmlSurat
                const total_pesan_unit      = resStatistikPerson.data[0].RoleId_To === 'uk.1.1' ? resStatistikPerson.data[0].jmlSurat : resStatistikUnit.total_surat
                const rumus     = (total_pesan_person / total_pesan_unit ) * 95 / 100
                const hasil     = Math.ceil(rumus * 100)

                    // Set For Hakim
                    const hakimSKPtoPPK = role_id === 'uk.1' || role_id === 'uk.2' || role_id === 'uk.3' || role_id === 'uk.5' || role_id === 'uk.6' || role_id === 'uk.7' || role_id === 'uk.8' || role_id === 'uk.9' || role_id === 'uk.12'   ? 'PPK' : 'SKP'
                    const valSKPorPPK   = role_id === 'uk.1' || role_id === 'uk.2' || role_id === 'uk.3' || role_id === 'uk.5' || role_id === 'uk.6' || role_id === 'uk.7' || role_id === 'uk.8' || role_id === 'uk.9' || role_id === 'uk.12'   ? 100 : 0
                    const valRapatHakim = role_id === 'uk.1' || role_id === 'uk.2' || role_id === 'uk.3' || role_id === 'uk.5' || role_id === 'uk.6' || role_id === 'uk.7' || role_id === 'uk.8' || role_id === 'uk.9' || role_id === 'uk.12'   ? 100 : rapat_percentage

                // Tingkat Tanggap
                    const sudah_dibaca 	= Number(resSudahDibaca.data[0].sudah_dibaca)
                    const un_disposisi 	= resUnreadDisposisi.data[0].unread_disposisi
                    const un_notadinas 	= resUnreadNotadinas.data[0].unread_notadinas
                    const un_tembusan 	= resUnreadTembusan.data[0].unread_tembusan
                    const belum_dibaca	= un_disposisi + un_notadinas + un_tembusan

                    const total             = sudah_dibaca + un_disposisi + un_notadinas + un_tembusan
                    const calculate_dibaca 	= scaleValue(sudah_dibaca, [0,total] , [0,100]);
                    const percentage 		= belum_dibaca == 0 ? 100 : calculate_dibaca
                    console.log('belum_dibaca',belum_dibaca)
                    console.log('total',total)
                    console.log('un_notadinas',un_notadinas)
                    console.log('un_tembusan',un_tembusan)

                //ResponseTime
                    // 1. Initial Asal Sumber
                    const waktu_response    = resWaktuResponse.data[0].waktu_response;
                    const respon_menit      = resWaktuResponse.data[0].wt;
                    const average_response  = resAverageResponse.data[0].average_response;
                    const average_menit     = resAverageResponse.data[0].wt;
                    // Remove commas
                    const rm_average_wt = Number(resAverageResponse.data[0].wt2);
                    const rm_respon_wt2 = Number(resWaktuResponse.data[0].wt2);

                    const after_average_wt = Math.trunc(rm_average_wt);
                    const after_respon_wt2 = Math.trunc(rm_respon_wt2);
                    const fix_average_wt = Number(after_average_wt);
                    const fix_respon_wt2 = Number(after_respon_wt2);
                    const pMax = fix_average_wt * 3;
                    const pMin = fix_average_wt * (-3);
                    const avg = fix_average_wt;

                    // Choose Deviation
                    const oneday          = 86400;
                    const satujam         = 3600;
                    const duajam          = 7200;
                    const tigajam         = 10800;
                    const empatjam        = 14400;
                    const limajam         = 18000;
                    const enamjam         = 21600;
                    const tujuhjam        = 25200;
                    const delapanjam      = 28800;
                    const sembilanjam     = 32400;
                    const sepuluhjam      = 36000;
                    const sebelasjam      = 39600;
                    const duabelasjam     = 43200;
                    const tigabelasjam    = 46800;
                    const empatbelasjam   = 50400;
                    const limabelasjam    = 54000;
                    const enambelasjam    = 57600;
                    const tujuhbelasjam   = 61200;
                    const delapanbelasjam = 64800;
                    const sembilasjam     = 68400;
                    const duapuluhjam     = 72000;
                    const duapuluhsatujam = 75560;
                    const duapuluhduajam  = 79200;
                    const duapuluhtigajam = 82800;
                    const halfday         = oneday * 1 + 1;
                    const twoday          = oneday * 2 + 1;
                    const thirdday        = oneday * 3 + 1;
                    const fouthday        = oneday * 4 + 1;
                    const fifthday        = oneday * 5 + 1;
                    const sixday          = oneday * 6 + 1;
                    const sevenday        = oneday * 7 + 1;
                    const eightday        = oneday * 8 + 1;
                    const nineday         = oneday * 9 + 1;
                    const tenday          = oneday * 10 + 1;
                    const elevenday       = oneday * 11 + 1;
                    const twelveday       = oneday * 12 + 1;
                    const thirdteenday    = oneday * 13 + 1;
                    const fourteenday     = oneday * 14 + 1;
                    const fiftheenday     = oneday * 15 + 1;
                    const sixteenday      = oneday * 16 + 1;
                    const seventeenday    = oneday * 17 + 1;
                    const eightteenday    = oneday * 18 + 1;
                    const nineteenday     = oneday * 19 + 1;
                    const twentyday       = oneday * 20 + 1;
                    const twentyoneday    = oneday * 21 + 1;
                    const twentytwoday    = oneday * 22 + 1;
                    const twentythreeday  = oneday * 23 + 1;
                    const twentyfourday   = oneday * 24 + 1;
                    const twentyfiveday   = oneday * 25 + 1;
                    const twentysixday    = oneday * 26 + 1;
                    const twentysevenday  = oneday * 27 + 1;
                    const twentyeightday  = oneday * 28 + 1;
                    const twentynineday   = oneday * 29 + 1;
                    const thirdtyday      = oneday * 30 + 1;
                    const thirdtoneyday   = oneday * 31 + 1;
                    const thirdttwoyday   = oneday * 32 + 1;
                    const thirdtythreeday = oneday * 33 + 1;
                    const thirdtyfourday  = oneday * 34 + 1;
                    const thirdtyfiveday  = oneday * 35 + 1;
                    const constantaDay = 
                                        fix_average_wt < satujam ? satujam * 0.78:
                                        fix_average_wt < duajam ? duajam * 0.78:
                                        fix_average_wt < tigajam ? tigajam  * 0.78:
                                        fix_average_wt < empatjam ? empatjam * 0.78 :
                                        fix_average_wt < limajam ? limajam * 0.78 :
                                        fix_average_wt < enamjam ? enamjam * 0.78 :
                                        fix_average_wt < tujuhjam ? tujuhjam * 0.78  :
                                        fix_average_wt < delapanjam ? delapanjam * 0.78 :
                                        fix_average_wt < sembilanjam ? sembilanjam * 0.78 :
                                        fix_average_wt < sepuluhjam ? sepuluhjam * 0.89 :
                                        fix_average_wt < sebelasjam ? sebelasjam * 0.89:
                                        fix_average_wt < duabelasjam ? duabelasjam * 0.89:
                                        fix_average_wt < tigabelasjam ? tigabelasjam * 0.78:
                                        fix_average_wt < empatbelasjam ? empatbelasjam * 0.78:
                                        fix_average_wt < limabelasjam ? limabelasjam * 0.78:
                                        fix_average_wt < enambelasjam ? enambelasjam * 0.78:
                                        fix_average_wt < tujuhbelasjam ? tujuhbelasjam * 0.78:
                                        fix_average_wt < delapanbelasjam ? delapanbelasjam * 0.78:
                                        fix_average_wt < sembilasjam ? sembilasjam * 0.78:
                                        fix_average_wt < duapuluhjam ? duapuluhjam * 0.78:
                                        fix_average_wt < duapuluhsatujam ? duapuluhsatujam * 0.78:
                                        fix_average_wt < duapuluhduajam ? duapuluhduajam * 0.78:
                                        fix_average_wt < duapuluhtigajam ? duapuluhtigajam * 0.78:
                                        fix_average_wt < halfday ? halfday :
                                        fix_average_wt < oneday ? oneday :
                                        fix_average_wt < twoday ? twoday :
                                        fix_average_wt < thirdday ? thirdday :
                                        fix_average_wt < fouthday ? fouthday :
                                        fix_average_wt < fifthday ? fifthday :
                                        fix_average_wt < sixday ? sixday :
                                        fix_average_wt < sevenday ? sevenday :
                                        fix_average_wt < eightday ? eightday :
                                        fix_average_wt < nineday ? nineday :
                                        fix_average_wt < tenday ? tenday :
                                        fix_average_wt < elevenday ? elevenday :
                                        fix_average_wt < twelveday ? twelveday :
                                        fix_average_wt < thirdteenday ? thirdteenday :
                                        fix_average_wt < fourteenday ? fourteenday :
                                        fix_average_wt < fiftheenday ? fiftheenday :
                                        fix_average_wt < sixteenday ? sixteenday :
                                        fix_average_wt < seventeenday ? seventeenday :
                                        fix_average_wt < eightteenday ? eightteenday :
                                        fix_average_wt < nineteenday ? nineteenday :
                                        fix_average_wt < twentyday ? twentyday :
                                        fix_average_wt < twentyoneday ? twentyoneday:
                                        fix_average_wt < twentytwoday ? twentytwoday :
                                        fix_average_wt < twentythreeday ? twentythreeday:
                                        fix_average_wt < twentyfourday ? twentyfourday :
                                        fix_average_wt < twentyfiveday ? twentyfiveday :
                                        fix_average_wt < twentysixday ? twentysixday :
                                        fix_average_wt < twentysevenday ? twentysevenday :
                                        fix_average_wt < twentyeightday ? twentyeightday :
                                        fix_average_wt < twentynineday ? twentynineday :
                                        fix_average_wt < thirdtyday ? thirdtyday :
                                        fix_average_wt < thirdtoneyday ? thirdtoneyday :
                                        fix_average_wt < thirdttwoyday ? thirdttwoyday :
                                        fix_average_wt < thirdtythreeday ? thirdtythreeday :
                                        fix_average_wt < thirdtyfourday ? thirdtyfourday :
                                        thirdtyfiveday
                     ;
                    
                    const vRespon = fix_respon_wt2 
                    // Rumus 
                    const maxOld  = fix_average_wt + constantaDay ;
                    const minOld  = fix_average_wt - constantaDay  ;
                    const checkMinPositiveNegative = minOld > 0 ? minOld - minOld : minOld - minOld;
                    const checkMaxPositiveNegative = minOld > 0 ? maxOld - minOld : maxOld - minOld;
                    const checlValPositive = minOld > 0 ? vRespon - minOld : vRespon - minOld;
                    const FIXmin = scaleValue(checkMinPositiveNegative, [checkMinPositiveNegative,checkMaxPositiveNegative] , [0,1000])
                    const FIXmax = scaleValue(checkMaxPositiveNegative, [checkMinPositiveNegative,checkMaxPositiveNegative] , [0,1000])
                    const FIXval = scaleValue(checlValPositive, [checkMinPositiveNegative,checkMaxPositiveNegative] , [0,1000])
                    const unread_pre = resBelumDibaca.data === "Not Found" ? 0 : Number(resBelumDibaca.total)
                    const unread = unread_pre

                    const valResponTemp = unread > 30 ? FIXmax : Number(resWaktuResponse.data[0].wt2) == 0 ? 0 : FIXval
                    const valll = 1000 - FIXval
                    const percentage_responsetime = unread > 30 ? 0 : scaleValue(valll, [FIXmin, FIXmax], [0,100])
                    // console.log('percentage_responsetime', percentage_responsetime)
                    // console.log('percentage', percentage)
                    // console.log('FIXval', FIXval)





                
                const dataa = {
                    labels: ['Tingkat Tanggap', 'Waktu Respon', 'Beban Dokumen', 'other','other', 
                        'other', 'other', 'other', 'other', 'other', 'other', 'other', 
                        'other', 'other', 'other', 'other', 'other', 'other', 'other', 'other'
                    ],
                    datasets: [
                      {
                        label: 'Instrument Penilaian',
                        data: [percentage, percentage_responsetime, 0],
                        backgroundColor: [
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(54, 162, 235, 0.2)',
                          'rgba(255, 206, 86, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(255, 159, 64, 0.2)',
                        ],
                        borderColor: [
                          'rgba(255, 99, 132, 1)',
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 206, 86, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(255, 159, 64, 1)',
                        ],
                        borderWidth: 1,
                      },
                    ],
                }

                const optionss = {
                    scales: {
                      y: {
                        beginAtZero: true,
                        max: 100
                      },  
                     
                    },
                    responsive: true,
                    maintainAspectRatio: false
                  };
                setData(dataa)
                setOptions(optionss)
            } catch (err) {
                console.log('Error in contentGrafikTotal: ',err)
            }
        }
        fetchAll();
    }, [])

    // useEffect(() => {
    //     const initilize = async()=> {

    //     }
    //     return initilize
    // }, [data])
    // console.log('total',total)
    return(
            <Grid item xs={12}>
                <Paper className={classes.contentTotalAsalSurat}>
                    <Grid item xs={12}>
                        <Typography variant="h5" gutterBottom>Instrument Penilaian Nine Box</Typography>
                        <Divider/>
                        {data 
                        ? <div style={{height: '450px', paddingTop: '2em', paddingRight: '1.5em', paddingLeft: '1em'}}>
                            <Bar data={data} options={options} />
                            </div>
                        : ''
                    }
                    </Grid>
                    
                    
                    
				</Paper>
			</Grid>
    )
}

export default ContentInstrumentNineBoxV2;