import React,{useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import ContenGrafikTotal from './contentGrafikTotal';
import ContenGrafikDetail from './contentGrafikDetail';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
  } from "chart.js";
import { Bar } from "react-chartjs-2";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import moment from 'moment';

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

const GrafikV2 = () => {
    const classes = useStyles();
    const [data, setData] = useState(null);
    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1 = new URLSearchParams(search).get('tgl1');
    const tgl2 = new URLSearchParams(search).get('tgl2');

    const tgl1Formated = moment(tgl1).format('YYYY-MM-DD')
    const tgl2Formated = moment(tgl2).format('YYYY-MM-DD')
    useEffect(() => {
        const fetchAll = async () => {
            try{
               const res = await fetch(`${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/scatterplot/suratkeluarmasuk/${select_id}/${tgl1Formated}/${tgl2Formated}`)
               const result = await res.json()
                setData(result)
               console.log('GRAFIKV2',result)
            } catch (err) {
                console.log(err)
            }
        }
        fetchAll();
    }, [])
    // console.log('state:',state)
    return(
        <React.Fragment>
            {/* <ContenGrafikTotal total={state.total}/> */}
            <Grid item xs={6}>
                <Paper className={classes.contentGrafikDetail}>
                    <Grid item xs={12} style={{marginBottom: '3em'}}>
                        <Typography variant="h5" gutterBottom>Detail Surat Keluar/Masuk</Typography>
                        <Divider/>
                    </Grid>
                   {data == null 
                    ? ''
                    :    <Bar options={options} data={data} />
                    }
                </Paper>
            </Grid>
            {/* <ContenGrafikDetail detail={state.detail} totalSurat={state.totalSurat}/> */}
        </React.Fragment>
    )
}

export default GrafikV2;

const useStyles = makeStyles((theme) => ({
	contentGrafikDetail: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '420px',
		boxShadow: theme.shadow.large,
        paddingBottom: '3em'
		// borderRadius: 15
	},

}));

const options = {
    plugins: {
      title: {
        display: false,
        text: "Detail Surat Keluar/Masuk"
      },
      legend: {
        position: "bottom",
        align: 'center',
        labels: {
            usePointStyle: true,
            pointStyle: 'circle'
        }
      },
      tooltip: {
        callbacks: {
            label: function(context) {
                let label = context.dataset.label || '';
                // console.log(context)
                // console.log(data)
                const result = [
                    `${context.dataset.label2}: ${context.raw}`
                    // 'Unitkerja: ' + ' ' + context.raw.unitkerja_full
                ]
              
                return result;
            }
        }
      }
    },
    responsive: true,
    scales: {
      x: {
        stacked: true
      },
      y: {
        stacked: true
      }
    }
  };