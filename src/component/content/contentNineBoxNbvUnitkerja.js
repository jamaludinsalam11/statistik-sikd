import React,{useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import moment from 'moment';
import Moment from 'moment'
import { extendMoment } from 'moment-range';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { Chart ,Bar, Radar } from 'react-chartjs-2';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ContentScatterPot from './contentScatterPot';
import ContentSpyderWeb from './contentSpyderWeb';


import annotationPlugin from "chartjs-plugin-annotation";
import datalabelsPlugin from "chartjs-plugin-datalabels";
import { Button, IconButton, Tooltip } from '@material-ui/core';


Chart.register([annotationPlugin])

const ContentNineBoxNbvUnitkerja = () => {

    let search  = useLocation().search
    const select_id1 = new URLSearchParams(search).get('select_id')
    const tgl2 = new URLSearchParams(search).get('tgl2')
    const tahun_now 		= moment(tgl2).format('YYYY')

    const [ dataAvg, setDataAvg ] = useState(0)

    useEffect(() => {
        const fetchAll = async () => {
            try{

                const apiDev = `http://localhost:3001/sikd/api/v1/ninebox/scatterplot/rata-rata-nbv-unitkerja/${tahun_now}`;
                const apiProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/scatterplot/rata-rata-nbv-unitkerja/${tahun_now}`
                const apis = process.env.NODE_ENV === 'development' ? apiDev : apiProd;
                const fetching = await fetch(apis)
                const fetched   = await fetching.json()
                setDataAvg(fetched.data)
            } catch(e) {
                console.log('error contentNineBoxNbvUnitkerja:', e)
            }
        }
        fetchAll();
    }, [])

    const options = {
        responsive: true,
        plugins: {
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: 'Rata - Rata NBV tiap Unitkerja',
          },
        },
        scales: {
            x: {
              grid: {
                display: false
              }
            },
            y: {
              grid: {
                display: true
              },
            //   display: false
              beginAtZero: true,
                max: 100,
            //   ticks: {
            //     display: false
            //   }
            }
          }
      };

    const labels = ['Biro Renkeu', 'Biro SDMO', 'Biro HAK', 'Biro HP', 'Biro Umum', 'Puslitka', 'Pustik', 'Pusdik', 'Inspektorat'];
    const data = {
        labels,
        datasets: [
          {
            label: 'Rata - Rata NBV',
            data: dataAvg,
            backgroundColor: [
              "rgba(197,225,247,.6)",
              "rgba(246,209,96,.6)",
              "rgba(173,205,77,.6)",
              "rgba(255,180,131,.6)",
              "rgba(84,179,179,.6)",
              "rgba(227,128,127,.6)",
              "rgba(177,128,177,.6)",
              "rgba(141,171,108,.6)",
              "rgba(200,195,76,.6)"
            ], 
          }
        ],
      };
    return(
        <>
            <div style={{height: '450px', paddingTop: '', paddingRight: '1.5em', paddingLeft: '1em'}}>
                <Bar data={data} options={options} />
            </div>
        </>
    )
}


export default ContentNineBoxNbvUnitkerja;