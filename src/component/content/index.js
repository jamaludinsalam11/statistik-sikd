import React, {useState, useEffect} from 'react';
import {useLocation} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ContentTTE from './contentTTE';
import ContentSuratTerbaca from './contentSuratTerbaca';
import ContentAsalSurat from './contentAsalSurat';
import ContentAsalSuratMasuk from './contentAsalSuratMasuk'
import ContentAsalSuratKeluar from './contentAsalSuratKeluar'
import ContentWaktuRespon from './contentWaktuRespon';
import ContentBebanRespon from './contentBebanRespon';
import ContentAbsensi from './contentAbsensi';
import ContentAbsensiV2 from './contentAbsensiV2';
// import ContentGrafikTotal from './contentGrafikTotal';
// import ContentGrafikDetail from './contentGrafikDetail';
import Grafik from './contentGrafik';
import JumlahJamKerja from '../absensi/JumlahJamKerja';
import JamKerjaPerHari from '../absensi/JamKerjaPerHari';
import JamKerjaPerMinggu from '../absensi/JamKerjaPerMinggu';
import Terlambat from '../absensi/Terlambat';
import PulangCepat from '../absensi/PulangCepat';
import TidakAbsenPulang from '../absensi/TidakAbsenPulang';
import { ContentNineBox } from './contentNineBox';
import ContentGrafikTotal from './contentGrafikTotal';
import ContentInstrumentNineBox from './contentInstrumentNineBox';
import ContentInstrumentNineBoxV2 from './contentInstrumentNineBoxV2';
import ContentWaktuResponV2 from './contentWaktuResponV2';
import ContentSuratTerbacaV2 from './contentSuratTerbacaV2';
import ContentInstrumentNineBoxV3 from './contentInstrumentNineBoxV3';
import ContentScatterPot from './contentScatterPot';
import ContentSpiderweb from './contentSpiderweb';
import GrafikV2 from './contentGrafikV2';
import ContentRadialBar from './contentRadialBar';
import ContentRadialBarFusionchart from './contentRadialBarFusionchart';
import ContentSuratTerbacaV2Hakim from './contentSuratTerbacaV2Hakim';
import PegawaiTeladan from './contentTabsPegawaiTeladan';
import Recharging from './contentTabsRecharging';
import ContentInstrumentNineBoxV4 from './contentInstrumentNineBoxV4';
import { ContentNineBoxV2 } from './contentNineBoxV2';

const { io } = require("socket.io-client");


const useStyles = makeStyles((theme) => ({

	content: {
		padding: theme.spacing(2),
		textAlign: 'center',
		borderRadius: 15,
		backgroundColor: 'white',
		color: theme.palette.text.secondary,
		marginTop: '3%',
		zIndex: 1,
		position: 'relative',
	},
	contentDate: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		borderRadius: 25
	},
	contentDateSD: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		boxShadow: 'none'
		// borderRadius: 25
	},
	contentDivider: {
		paddingTop: theme.spacing(6),
		paddingBottom: theme.spacing(6),
	},



	contentTotalAsalSurat: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '400px',
		boxShadow: '0 .125rem .25rem rgba(0,0,0,.075) '
		// borderRadius: 15
	},

}));


const Content = () => {
	const classes = useStyles();
	const [user, setUser] = useState(null)
	let search  = useLocation().search;
	const select_id = new URLSearchParams(search).get('select_id');
	// const socket = io('http://localhost:3025')
	// useEffect(() => {
	



		
	// 	// setInterval(() => {
	// 	// 	/*
	// 	// 		Run a function or set any state here
	// 	// 	*/
			
	// 	// 	// console.log('from index content')
	// 	//   }, 10000);
	// 	window.addEventListener('focus', function (event) {
	// 		console.log('has focus');
	// 	});
	// 	// window.addEventListener('blur', function (event) {
	// 	// 	console.log('lost focus');
	// 	// });
	// }, [])
	// socket.emit('online', { roleId: select_id }, () => {

	// })
	// socket.on('disconnected', () => {
	// 	console.log(socket.id)
	// })

	// window.addEventListener("beforeunload", (ev) => 
	// {  
	// 	ev.preventDefault();
	// 	return ev.returnValue = 'Are you sure you want to close?';
	// });

	return (
		<div className={classes.content}>
			<Grid container spacing={3}>
				{/* <ContentTitle/> */}

				<Grid item xs={12} lg={12}>
					<Grid
						container
						spacing={3}
					>
						{/* <ContentScatterPot/> */}
						{/* <ContentSuratTerbaca/> */}
						{
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' ||
								select_id === 'uk.1.1'
							? <ContentSuratTerbacaV2Hakim/>
							: <ContentSuratTerbacaV2/>
						}
						
						{/* <ContentWaktuRespon/> */}
						<ContentWaktuResponV2/>
						
						{
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' 
								|| select_id === 'uk.1.1'
							? <ContentRadialBar/>
							: <ContentSpiderweb/>
							// :''
						}
						{
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' 
								// || select_id === 'uk.1.1'
							? <ContentRadialBarFusionchart/>
							: ''
						}
						{/* <ContentGrafikTotal/> */}
						{/* <ContentInstrumentNineBox/> */}
						{/* <ContentInstrumentNineBoxV2/> */}
						{
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' 
								// || select_id === 'uk.1.1'
							? ''
							: <ContentInstrumentNineBoxV3/>
							// : <ContentInstrumentNineBoxV4/>
						}
						
						{/* <ContentBebanRespon/> */}
						{/* - Revisi Sekjen 19 Juli 2021
							Jika RoleId is Hakim maka Component ContentAbsensi 
							tidak dimunculkan , dikarenakan Hakim tidak terikat Absensi,
						*/}

						{
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' 
								// || select_id === 'uk.1.1'
							? ''
							: <ContentAbsensiV2 />
						}
						
						{/* {user.RoleId} */}

						{/* {
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' 
							? ''
							: <ContentAsalSuratMasuk/>
						} */}
						{
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' 
								// || select_id === 'uk.1.1'
							? ''
							// : <ContentTTE/><ContentGrafikTotal/>
							: <ContentGrafikTotal/>
						}
						{/* {
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' 
							? ''
							: <ContentAsalSuratKeluar/>
						} */}
						{
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' 
								// || select_id === 'uk.1.1'
							? ''
							: <ContentAsalSurat/>
						}
						{
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' 
								// || select_id === 'uk.1.1'
							? ''
							: <GrafikV2/>
						}
						{
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' 
								// || select_id === 'uk.1.1'
							? ''
							: <ContentTTE/>
						}
						{
							select_id == null
							?  ''
							:  	select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
								select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
								select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
								select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' 
								// || select_id === 'uk.1.1'
							? ''
							: <ContentNineBoxV2/>
							// : <ContentNineBox/>
						}
						{/* <PegawaiTeladan/>
						<Recharging/> */}
						{/* <ContentAsalSuratMasuk/> */}
						{/* <ContentTTE/> */}
						{/* <ContentAsalSuratKeluar/> */}
						{/* <ContentAsalSurat/> */}
						{/* <Grafik/> */}
						{/* <ContentNineBox/> */}


					</Grid>
				</Grid>

			</Grid>
		</div>
	)
}

export default Content;