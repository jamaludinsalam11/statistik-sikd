import React, { useEffect, useState } from 'react'
import {useLocation} from "react-router-dom";
import { makeStyles} from '@material-ui/core/styles';
import moment from 'moment';
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import ReactFC from 'react-fusioncharts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import Powercharts from 'fusioncharts/fusioncharts.powercharts';
import { Divider, Grid, Paper, Typography } from '@material-ui/core';
// Resolves charts dependancy
Powercharts(FusionCharts);
ReactFC.fcRoot(FusionCharts, Charts, FusionTheme);


const useStyles = makeStyles((theme) => ({
	contentWaktuRespon: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: 'auto',
		boxShadow: theme.shadow.large,
		// borderRadius: 15
	},
}))

// const dataSource = {
//   chart: {
//     caption: "Total Dan Rata-rata Waktu Penyelesaian Perkara Pengujian Undang-undang Tahun 2022",
//     subcaption: "( Dalam Hari Kerja ) <br>2022",
//     xaxisname: "<b>Bulan</b>",
//     // yaxisname: "Total number of apps in store",
//     formatnumberscale: "1",
//     // plottooltext:
//     //   "<b>$dataValue</b> apps were available on <b>$seriesName</b> in $label",
//     theme: "fusion",
//     drawcrossline: "1"
//   },
//   categories: [
//     {
//       category: [
//         {
//           label: "Januari"
//         },
//         {
//           label: "Februari"
//         },
//         {
//           label: "Maret"
//         },
//         {
//           label: "April"
//         },
//         {
//           label: "Mei"
//         },
//         {
//           label: "Juni"
//         },
//         {
//           label: "Juli"
//         },
//         {
//           label: "Agustus"
//         },
//         {
//           label: "September"
//         },
//         {
//           label: "Oktober"
//         },
//         {
//           label: "November"
//         },
//         {
//           label: "Desember"
//         }
//       ]
//     }
//   ],
//   dataset: [
//     {
//       seriesname: "Total / 100",
//       data: [
//         {
//           value: "125000"
//         },
//         {
//           value: "300000"
//         },
//         {
//           value: "480000"
//         },
//         {
//           value: "800000"
//         },
//         {
//           value: "1100000"
//         }
//       ]
//     },
//     {
//       seriesname: "Rata-rata",
//       data: [
//         {
//           value: "70000"
//         },
//         {
//           value: "150000"
//         },
//         {
//           value: "350000"
//         },
//         {
//           value: "600000"
//         },
//         {
//           value: "1400000"
//         }
//       ]
//     },
//     {
//       seriesname: "Putusan",
//       data: [
//         {
//           value: "10000"
//         },
//         {
//           value: "100000"
//         },
//         {
//           value: "300000"
//         },
//         {
//           value: "600000"
//         },
//         {
//           value: "900000"
//         }
//       ]
//     },
//     {
//       seriesname: "Rata - rata waktu penanganan seluruh perkara",
//       data: [
//         {
//           value: "10000"
//         },
//         {
//           value: "100000"
//         },
//         {
//           value: "300000"
//         },
//         {
//           value: "600000"
//         },
//         {
//           value: "900000"
//         }
//       ]
//     }
//   ]
// };

const ContentRadialBarFusionchart = () => {

  let search  = useLocation().search;
  const select_id = new URLSearchParams(search).get('select_id');
  const pre_tgl1 = new URLSearchParams(search).get('tgl1');
  const tahun     = moment(pre_tgl1).format('YYYY');

  const [dataSource, setDataSource] = useState(null)

  const fetchS = async() => {
    try{
      const apiDev  = `http://localhost:3001/sikd/api/v1/ninebox/e-minutasi/v1/${tahun}`;
      const apiProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/e-minutasi/v1/${tahun}`
      
      const api       = process.env.NODE_ENV === 'development' ? apiDev : apiProd;
      const fetching  = await fetch(api)
      const fetched   = await fetching.json()
      setDataSource(fetched.dataSource)
      console.log('s',fetched.dataSource)
    } catch(error) {
      console.log(error)
    }

  }
  useEffect(() => {
    fetchS()
  },[])
  const classes = useStyles()
  return (
    <Grid 
    item 
    lg={12}
    sm={12}
    xl={12}
    xs={12}>
      <Paper className={classes.contentWaktuRespon}>
        <Grid item md={12}>
            {/* <Typography variant="h5" gutterBottom>Waktu Response</Typography> */}
            
            <Divider/>

            <ReactFC
              type="mscolumn2d"
              width="100%"
              height="50%"
              dataFormat="JSON"
              dataSource={dataSource}
            />
        </Grid>
      </Paper>
    </Grid>

  )
}

export default ContentRadialBarFusionchart;
