import React, {useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import moment from 'moment';
import 'moment/locale/id.js'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ContentGrafikTerbaca from './contentGrafikSuratTerbaca';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles((theme) => ({
	contentSuratTerbaca: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '300px',
		boxShadow: theme.shadow.large,
	},
	contentSuratTerbacaGrid: {
		paddingTop: theme.spacing(2)
	},
	contentSuratTerbacaGrid2: {
		paddingTop: theme.spacing(2),
		paddingBottom: theme.spacing(2),
		textAlign: 'center'
	},
	contentTingkatTanggapSIKD: {
		color: theme.palette.text.success
	},
	contentSuratTerbacaDibaca: {
		color: theme.palette.text.success,
		textAlign: 'center'
	},
	contentSuratTerbacaBelumDibaca: {
		color: theme.palette.text.error
	},

}));

function scaleValue(value, from, to) {
	var scale = (to[1] - to[0]) / (from[1] - from[0]);
	var capped = Math.min(from[1], Math.max(from[0], value)) - from[0];
	return ~~(capped * scale + to[0]);
}

const ContentSuratTerbaca = () => {
    const classes = useStyles();
	const [terbaca, setTerbaca] = useState({sudah_dibaca:null, belum_dibaca:null})
	const [percentage, setPercentage] = useState(null);
	const [ttd, setTTD] = useState(null)
	const [disposisi, setDisposisi] = useState(null)
	const [notadinas, setNotadinas] = useState(null)
	const [tembusan, setTembusan] = useState(null)
    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1_pre 	= new URLSearchParams(search).get('tgl1');
    const tgl2_pre 	= new URLSearchParams(search).get('tgl2');
	const tgl1		= moment(tgl1_pre).format('YYYY-MM-DD')
	const tgl2		= moment(tgl2_pre).format('YYYY-MM-DD')
	console.log('tgl1',tgl1)
	console.log('tgl2',tgl2)
	const [tgl, setTgl] = useState({tgl1: tgl1, tgl2:tgl2})

	useEffect(() => {
		const fetchTerbaca = async () => {
			try{
				const apiSudahDibaca = `${process.env.REACT_APP_API_SUDAHDIBACA}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`
				const apiUnreadTTD = `${process.env.REACT_APP_API_UNREAD_TTD}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`
				const apiUnreadDisposisi = `${process.env.REACT_APP_API_UNREAD_DISPOSISI}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`
				const apiUnreadNotadinas = `${process.env.REACT_APP_API_UNREAD_NOTADINAS}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`
				const apiUnreadTembusan = `${process.env.REACT_APP_API_UNREAD_TEMBUSAN}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`

				const [valSudahDibaca, valUnreadTTD, valUnreadDisposisi, valUnreadNotadinas, valUnreadTembusan] = await Promise.all([
					fetch(apiSudahDibaca),
					fetch(apiUnreadTTD),
					fetch(apiUnreadDisposisi),
					fetch(apiUnreadNotadinas),
					fetch(apiUnreadTembusan),
				])

				const resSudahDibaca 	=	await valSudahDibaca.json()
				const resUnreadTTD		=	await valUnreadTTD.json()
				const resUnreadDisposisi=	await valUnreadDisposisi.json()
				const resUnreadNotadinas= 	await valUnreadNotadinas.json()
				const resUnreadTembusan	= 	await valUnreadTembusan.json()

				const sudah_dibaca 	= Number(resSudahDibaca.data[0].sudah_dibaca)
				const un_ttd 		= resUnreadTTD.data[0].unread_ttd
				const un_disposisi 	= resUnreadDisposisi.data[0].unread_disposisi
				const un_notadinas 	= resUnreadNotadinas.data[0].unread_notadinas
				const un_tembusan 	= resUnreadTembusan.data[0].unread_tembusan
				const belum_dibaca	= un_ttd + un_disposisi + un_notadinas + un_tembusan

				const total = sudah_dibaca + un_ttd + un_disposisi + un_notadinas + un_tembusan
				const calculate_dibaca 	= scaleValue(sudah_dibaca, [0,total] , [0,100]);
				const percentage 		= belum_dibaca == 0 ? 100 : calculate_dibaca

				setPercentage(percentage);
				setTgl({tgl1: tgl1, tgl2: tgl2})
				setTTD(un_ttd)
				setDisposisi(un_disposisi)
				setNotadinas(un_notadinas)
				setTembusan(un_tembusan)
				setTerbaca({sudah_dibaca: sudah_dibaca, belum_dibaca: belum_dibaca})
				console.log('resSudahDibaca',sudah_dibaca)
				console.log('resUnreadTTD',un_ttd)
				console.log('resUnreadDisposisi',un_disposisi)
				console.log('resUnreadNotadinas',un_notadinas)
				console.log('resUnreadTembusan',un_tembusan)
     			console.log(sudah_dibaca)
                console.log(belum_dibaca)
                console.log('total',total)


				// await Promise.all([
                //     fetch(`${process.env.REACT_APP_API_SUDAHDIBACA}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                //     fetch(`${process.env.REACT_APP_API_BELUMDIBACA}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                // ])
                // .then((responses) => {
                //     return Promise.all(responses.map(function (response) {
                //         return response.json();
                //     }));
                // })
                // .then((data) => {
				// 	// console.log('im in Surat Terbaca', data)
                //     // debug API TTDE
                //     // 1. Initial Asal Sumber
                //     const sudah_dibaca = Number(data[0].data[0].sudah_dibaca);
                //     const belum_dibaca = data[1].data === "Not Found" ? 0 : Number(data[1].total);
				// 	const readed = 0
				// 	const disposisi = data[1].disposisi;
				// 	const notadinas = data[1].notadinas;
				// 	const tembusan = data[1].tembusan;
				// 	// console.log('disposisi', disposisi)
				// 	// 2. Calculate Percentage
				// 	const total = sudah_dibaca + belum_dibaca;
				// 	const calculate_dibaca = scaleValue(sudah_dibaca, [0,total] , [0,100]);
					
				// 	const percentage = belum_dibaca == 0 ? 100 : calculate_dibaca

                //     // console.log(sudah_dibaca)
                //     // console.log(belum_dibaca)

                //     // 2. setState 
                //     setTerbaca({sudah_dibaca: sudah_dibaca, belum_dibaca: belum_dibaca})
				// 	setPercentage(percentage);
				// 	setTgl({tgl1: tgl1, tgl2: tgl2})
				// 	setDisposisi(disposisi)
				// 	setNotadinas(notadinas)
				// 	setTembusan(tembusan)
                // })
			} catch(err){
				console.log(err)
			}
		}
		fetchTerbaca();
	}, [])
	// console.log('Sumber', terbaca)
    return(
		<Grid item 
			lg={12}
			sm={12}
			xl={12}
			xs={12}
			
		>
			<Paper className={classes.contentSuratTerbaca} >
				<Typography variant="h5" gutterBottom>Surat Terbaca</Typography>
				<Divider />
				<Grid
					container
					direction="row"
					justify="center"
					alignItems="center"
					spacing={1}
					
				>
					<Grid item md={12} >
						<Grid
							container
							direction="row"
							justify="center"
							alignItems="center"
							spacing={5}
							
						>
							
							<Grid item md={3} xs={3} sm={3} lg={3} style={{paddingTop: '3em'}}>
								<Typography variant="subtitle2" gutterBottom style={{paddingBottom: '.5em'}}>Tingkat Tanggap SIKD</Typography>
								{/* <Typography variant="h2" className={classes.contentTingkatTanggapSIKD}>100%</Typography> */}
								{percentage 
									? <ContentGrafikTerbaca percentage={percentage}/> 
									: <Skeleton animation="wave" variant="circle" width={110} height={110} style={{ margin: 'auto', backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
								}
								
							

							</Grid>
							<Grid item md={9} xs={9} sm={9} lg={9} >
								<Grid
									container
									direction="row"
									justify="center"
									alignItems="flex-start"
									// spacing={1}
									className={classes.contentSuratTerbacaGrid2}
									textAlign="center"
								>
									<Grid item md={4} xs={4} sm={4} lg={4} className={classes.contentSuratTerbacaDibaca} style={{textAlign: 'center'}}>
										<Typography variant="subtitle2">Dibaca</Typography>
										<Typography variant="h4">{terbaca.sudah_dibaca}</Typography> 
										<Typography variant="subtitle2"> </Typography>
										{/* {disposisi != null  
											? <Typography variant="h4">{terbaca.sudah_dibaca}</Typography> 
											: <Skeleton animation="wave"  variant="text" width="10" style={{height: 40, width: '60px', margin: 'auto', backgroundColor: 'rgba(221, 221, 221, 0.5)', textAlign: 'center'}}/>
										} */}
										
									</Grid>
									<Grid item md={5} xs={5} sm={5} lg={5} className={classes.contentSuratTerbacaBelumDibaca} style={{textAlign: 'center'}}>
										<Typography variant="subtitle2">Belum Dibaca</Typography>
										<Typography variant="h4">{terbaca.belum_dibaca} </Typography>
										{ disposisi == null || notadinas == null || tembusan == null || ttd == null
											? '' 
											: disposisi != null || notadinas != null || tembusan != null || ttd != null
											? <Typography variant="caption"> 
												{disposisi != 0 && notadinas != 0 && tembusan != 0 && ttd != 0 ? '(' : ''}
												{disposisi != 0 ? `${disposisi} Disposisi` : ''}  
												{notadinas != 0 && disposisi != 0 ? ', ' : notadinas == 0 && disposisi != 0 ? '' : '' } 
												{notadinas != 0 ? `${notadinas} Nota Dinas` : ''} 
												{tembusan != 0 &&  notadinas != 0 ? ', ': tembusan != 0 &&  disposisi != 0 ? ', ' : ' '} 
												{tembusan != 0 ? `${tembusan} Tembusan`: ''} 
												{ttd != 0 &&  tembusan != 0  ? ', ' : ttd != 0 &&  notadinas != 0 ? ', ' : ttd != 0 && disposisi != 0 ? ', ' : '' }
												{ttd != 0 ? `${ttd} Permohonan TTD` : ''}
												{disposisi != 0 && notadinas != 0 && tembusan != 0 && ttd != 0 ? ')' : ''}
												</Typography>
											: terbaca.belum_dibaca == null 
											? ''
											: ''
										}
										
										{/* {terbaca.belum_dibaca && terbaca.belum_dibaca 
											? <Typography variant="h4">{terbaca.belum_dibaca}</Typography>
											: <Skeleton animation="wave"  variant="text" width="10" style={{height: 40, width: '60px', margin: 'auto', backgroundColor: 'rgba(221, 221, 221, 0.5)', textAlign: 'center'}}/>
										} */}
									
									</Grid>
								</Grid>
								<Typography variant="subtitle2" align="center" >
									Pesan Telah Dibaca dan Belum Dibaca Pada periode bulan
										</Typography>
								<div></div>
								<Paper style={{marginLeft: '3em', marginRight: '3em', paddingTop: '1em', paddingBottom: '1em', borderRadius: 25, backgroundColor: '#2A0CBD', opacity: .8, color: '#fff'}}>
									<Typography variant="h4" align="center" style={{color: '#fff'}} >
										{moment(tgl1).lang('id').format('Do MMMM YYYY ')}-  {moment(tgl2).lang('id').format('Do MMMM YYYY')}
									</Typography>

								</Paper>
							</Grid>

						</Grid>
					</Grid>


				</Grid>
			</Paper>
		</Grid>
    )
}

export default ContentSuratTerbaca;