import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles((theme) => ({

	content: {
		padding: theme.spacing(2),
		textAlign: 'center',
		borderRadius: 15,
		backgroundColor: 'white',
		color: theme.palette.text.secondary,
	},
    contentTitle:{
        marginTop: theme.spacing(7)
    },
	contentDate: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		borderRadius: 25
	},
	contentDateSD: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		boxShadow: 'none'
		// borderRadius: 25
	},
	contentDivider: {
		paddingTop: theme.spacing(6),
		paddingBottom: theme.spacing(6),
	},
}));


const ContentTitle = () => {
    const classes = useStyles();
    return (
        <>
        <Grid item md={12} className={classes.contentTitle}>
            <Typography variant="h2">Data Pemakaian</Typography>
        </Grid>

        <Grid item md={12}>
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={2}
            >
                <Grid item md={3}>
                    <Paper className={classes.contentDate}>
                        <Typography variant="h4">2021/01/20</Typography>
                    </Paper>
                </Grid>
                <Grid item md={1}>
                    <Paper className={classes.contentDateSD}>s/d</Paper>
                </Grid>
                <Grid item md={3}>
                    <Paper className={classes.contentDate}>
                        <Typography variant="h4">2021/01/20</Typography>
                    </Paper>
                </Grid>
                <Grid item md={12} >
                    <div className={classes.contentDivider}>

                        <Divider />
                    </div>

                </Grid>
            </Grid>
        </Grid>
        </>
    )
}

export default ContentTitle;