import React, { useState, useEffect, useCallback, useMemo} from 'react'
import { Radar } from 'react-chartjs-2';
import { Grid , Paper, Typography, Divider} from '@material-ui/core'
import {useLocation} from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';

const ContentSpyderWeb = ({ dataRadar, optionsRadar }) => {
    const classes = useStyles()

    return (
        <Grid item xs={12}>
			{dataRadar 
                    ? <Radar data={dataRadar} options={optionsRadar} />
                    : ''
                }
            {/* <Paper className={classes.contentTTE} style={{textAlign: 'center'}}>
                
            </Paper> */}
        </Grid>
    )
}

export default ContentSpyderWeb

const useStyles = makeStyles((theme) => ({

	content: {
		// padding: theme.spacing(2),
		textAlign: 'center',
		borderRadius: 15,
		backgroundColor: 'white',
		color: theme.palette.text.secondary,
		marginTop: '-3%',
		zIndex: 1,
		position: 'relative',
	},
	contentDate: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		borderRadius: 25
	},
	contentDateSD: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		boxShadow: 'none'
	},
	contentDivider: {
		// paddingTop: theme.spacing(6),
		// paddingBottom: theme.spacing(6),
	},
	contentTTE: {
		// padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '550px',
		boxShadow: theme.shadow.large,
	},
	contentTTEvalue: {
		// paddingTop: theme.spacing(7),
		paddingBottom: theme.spacing(1),
		fontWeight: 600,
	},

}));
