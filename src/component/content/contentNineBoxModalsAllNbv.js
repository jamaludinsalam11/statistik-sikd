import React,{useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import './contentScatterPot.css'
const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const ModalAllNBV = (props) => {
  const classes = useStyles()
  const [open, setOpen] = useState(true)
  useEffect(() => {
      const nbv = async() => {
          try{
              const apiListOfTalent   = `${process.env.REACT_APP_API_APPS_LISTOFTALENT2}/${tahun_now}/90/100/${select_id1}/${roleAtasan}`
              const fetchListOfTalent = await fetch(apiListOfTalent)
              const resListOfTalent   = await fetchListOfTalent.json()
              const { total, percentage, result } = resListOfTalent
              const sort = await sortDESCandNullishValue(result)
              setBox9(sort)
              setTotal9(total)
              setPercentage9(percentage)
          } catch{

          }
      }
      nbv()
  },[])
  useEffect(() => {
      const nbv1 = async() => {
          try{
              setOpen(props.open)
          } catch{

          }
      }
      nbv1()
  }, [props])

  const handleClose = () => {
      setAllNbv(false)
  }
  return (
      <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <div className={classes.paper} style={{height: '80vh'}}>
          <div style={{textAlign: 'center'}}>
              <Typography variant="h3">List of Box 9</Typography>
              <Typography variant="subtitle2">Kumpulan pegawai yang masuk dalam kategori Box 9</Typography>
          </div>
          
          <List className={classes.rootList}>
              {
                  box9  ? box9.map((box) => (
                      <ListItem>
                          <ListItemAvatar>
                          <Avatar>
                              <AccountCircleIcon />
                          </Avatar>
                          </ListItemAvatar>
                          <ListItemText primary={box.people_name} secondary={box.people_desc} />
                      </ListItem>
                     
                  ))
                  : <div>
                      { [0,1,2,3,4,5].map(() => (
                          <Card className={classes.card}>
                              <CardHeader
                                  avatar={ <Skeleton animation="wave" variant="circle" width={40} height={40} />}
                                  title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
                                  subheader={<Skeleton animation="wave" height={10} width="40%" />}
                              />
                          </Card>
                      ))
                      }
                  </div>
              }
          </List>
          <div style={{textAlign: 'center', marginTop: '2em'}}>
              {total9 ? <Typography variant="h5">{total9} Pegawai</Typography> : ''}
              {percentage9 ? <Typography variant="caption">{`${percentage9}% dari seluruh pegawai`}</Typography> : ''}
          </div>
        </div>
      </Fade>
    </Modal>
  )
}