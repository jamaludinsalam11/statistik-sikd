import React,{useEffect,useState} from 'react';
import {useLocation} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Chip } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Skeleton from '@material-ui/lab/Skeleton';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles((theme) => ({
	contentAsalSurat: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '350px',
		boxShadow: theme.shadow.large
		// borderRadius: 15
	},
	contentAsalSuratGrid: {
		paddingTop: theme.spacing(2),
	},


}));


const ContentAsalSurat = () => {
    const classes = useStyles();
    // const [sumber, setSumber] = useState({external:[], internal:[]})
    const [suratKeluar, setSuratKeluar] = useState(null)
    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1 = new URLSearchParams(search).get('tgl1');
    const tgl2 = new URLSearchParams(search).get('tgl2');

	useEffect(() => {
		const fetchAsalSurat = async () => {
			try{
				await Promise.all([
                    fetch(`${process.env.REACT_APP_API_NODIN_KELUAR_ALL_UNIT}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_NODIN_KELUAR_ALL_MK}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_DISPOSISI_KELUAR_ALL_UNIT}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_DISPOSISI_KELUAR_ALL_MK}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_TEMBUSAN_KELUAR_ALL_UNIT}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_TEMBUSAN_KELUAR_ALL_MK}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    
                    fetch(`${process.env.REACT_APP_API_NODIN_KELUAR}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_DISPOSISI_KELUAR}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_TEMBUSAN_KELUAR}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    
                  
                ])
                .then((responses) => {
                    return Promise.all(responses.map(function (response) {
                        return response.json();
                    }));
                })
                .then((data) => {
                    console.log('data in Surat KELUAR', data)
                    /**
                     * Initial Data Mentah ASAL SURAT MASUK
                     * ====================================
                     * Guna untuk mencari berapa persen surat masuk yang di ampuh
                     */ 
                    const pre_nodin_keluar_all_unit      = data[0].data[0].notadinas_keluar_all_unit
                    const pre_nodin_keluar_all_mk        = data[1].data[0].notadinas_keluar_all_mk
                    const pre_disposisi_keluar_all_unit  = data[2].data[0].disposisi_keluar_all_unit
                    const pre_disposisi_keluar_all_mk    = data[3].data[0].disposisi_keluar_all_mk
                    const pre_tembusan_keluar_all_unit   = data[4].data[0].tembusan_keluar_all_unit
                    const pre_tembusan_keluar_all_mk     = data[5].data[0].tembusan_keluar_all_mk

                    const pre_nodin_keluar               = data[6].data[0].notadinas_keluar 
                    const pre_disposisi_keluar           = data[7].data[0].disposisi_keluar 
                    const pre_tembusan_keluar            = data[8].data[0].tembusan_keluar 

                    /**
                     * Processing Value Percentage 
                     * 
                     */
                    const nodin_keluar_unit          = Number(((pre_nodin_keluar / pre_nodin_keluar_all_unit) * 100).toFixed(2))
                    const nodin_keluar_mk            = Number(((pre_nodin_keluar / pre_nodin_keluar_all_mk  ) * 100).toFixed(2))
                    const disposisi_keluar_unit      = Number(((pre_disposisi_keluar / pre_disposisi_keluar_all_unit  ) * 100).toFixed(2))
                    const disposisi_keluar_mk        = Number(((pre_disposisi_keluar / pre_disposisi_keluar_all_mk  ) * 100).toFixed(2))
                    const tembusan_keluar_unit       = Number(((pre_tembusan_keluar / pre_tembusan_keluar_all_unit  ) * 100).toFixed(2))
                    const tembusan_keluar_mk         = Number(((pre_tembusan_keluar / pre_tembusan_keluar_all_mk  ) * 100).toFixed(2))

                    const dataReady = {
                        'nodin_keluar_unit'      : nodin_keluar_unit ,
                        'nodin_keluar_mk'       : nodin_keluar_mk ,
                        'disposisi_keluar_unit'  : disposisi_keluar_unit,
                        'disposisi_keluar_mk'    : disposisi_keluar_mk,
                        'tembusan_keluar_unit'   : tembusan_keluar_unit,
                        'tembusan_keluar_mk'     : tembusan_keluar_mk,
                    }

                    setSuratKeluar(dataReady)
                    // const pre_nodin_keluar_all_unit     = data[0].data[0].notadinas_keluar_all_unit
                    // const pre_nodin_keluar_all_mk       = data[1].data[0].notadinas_keluar_all_mk
                    // const pre_disposisi_keluar_all_unit = data[2].data[0].disposisi_keluar_all_unit
                    // const pre_disposisi_keluar_all_mk   = data[3].data[0].disposisi_keluar_all_mk
                    // const pre_tembusan_keluar_all_unit  = data[4].data[0].tembusan_keluar_all_unit
                    // const pre_tembusan_keluar_all_mk    = data[5].data[0].tembusan_keluar_all_mk




                    // console.log('pre_nodin_masuk_all_unit',disposisi_masuk_unit )

                    var preDataSuratMasuk = []
                    var preObjectSuratMasuk = {}

                    // for(var i = 0 ; )

                    // debug API TTDE
                    // 1. Initial Asal Sumber
                    // const sumber_external = Number(data[0].data[0].sumber_external);
                    // const sumber_internal = Number(data[1].data[0].sumber_internal);
                    // console.log(sumber_external)
                    // console.log(sumber_internal)

                    // 2. setState 
                    // setSumber({external: sumber_external, internal: sumber_internal})

                })
			} catch(err){
				console.log(err)
			}
		}
		fetchAsalSurat();
	}, [])
	console.log('Surat Masuk State', suratKeluar)
    return (
        <Grid item xs={8}>
            <Paper className={classes.contentAsalSurat}>
                <Typography variant="h5" style={{fontWeight: 'bold'}} gutterBottom>Presentase Surat Keluar</Typography>
                {/* <Divider/> */}
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={1}
                    className={classes.contentAsalSuratGrid}
                >
                    <Grid item md={12} >
                        <Typography variant="h5" gutterBottom align="center">Berdasarkan Unit Kerja</Typography>
                       
                    </Grid>
                    <Grid item md={3} >
                        <Typography variant="h5" gutterBottom><Chip label="Nota Dinas" color="primary"/></Typography>
                        <Typography variant="h4">
                            {suratKeluar == null
                                ? <Skeleton animation="wave" height={40}  variant="text" style={{ width: '100px', margin: 'auto',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                                : `${suratKeluar.nodin_keluar_unit} %` 
                            }
                        </Typography>
                    </Grid>
                    <Divider orientation="vertical"  />
                    <Grid item md={3} >
                        <Typography variant="h5" gutterBottom><Chip label="Disposisi" color="secondary"/></Typography>
                        <Typography variant="h4">
                            {suratKeluar == null
                                ? <Skeleton animation="wave" height={40}  variant="text" style={{ width: '100px', margin: 'auto',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                                : `${suratKeluar.disposisi_keluar_unit} %` 
                            }
                        </Typography>
                    </Grid>
                    <Grid item md={3} >
                        <Typography variant="h5" gutterBottom><Chip label="Tembusan"  style={{backgroundColor: '#f1e15b' , color: 'rgba(0, 0, 0, 0.54)'}}/></Typography>
                        <Typography variant="h4">
                            {suratKeluar == null
                                ? <Skeleton animation="wave" height={40}  variant="text" style={{ width: '100px', margin: 'auto',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                                : `${suratKeluar.tembusan_keluar_unit} %` 
                            }
                        </Typography>
                    </Grid>
                </Grid>
                <Divider style={{marginTop: '1em'}}/>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={1}
                    className={classes.contentAsalSuratGrid}
                >
                    <Grid item md={12} >
                        <Typography variant="h5" gutterBottom align="center">Berdasarkan Keseluruhan MK</Typography>
                       
                    </Grid>
                    <Grid item md={3} >
                        <Typography variant="h5" gutterBottom><Chip label="Nota Dinas" color="primary"/></Typography>
                        <Typography variant="h4">
                            {suratKeluar == null
                                ? <Skeleton animation="wave" height={40}  variant="text" style={{ width: '100px', margin: 'auto',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                                : `${suratKeluar.nodin_keluar_mk} %` 
                            }
                        </Typography>
                    </Grid>
                    <Divider orientation="vertical"  />
                    <Grid item md={3} >
                        <Typography variant="h5" gutterBottom><Chip label="Disposisi" color="secondary"/></Typography>
                        <Typography variant="h4">
                            {suratKeluar == null
                                ? <Skeleton animation="wave" height={40}  variant="text" style={{ width: '100px', margin: 'auto',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                                : `${suratKeluar.disposisi_keluar_mk} %` 
                            }
                        </Typography>
                    </Grid>
                    <Grid item md={3} >
                        <Typography variant="h5" gutterBottom><Chip label="Tembusan"  style={{backgroundColor: '#f1e15b' , color: 'rgba(0, 0, 0, 0.54)'}}/></Typography>
                        <Typography variant="h4">
                            {suratKeluar == null
                                ? <Skeleton animation="wave" height={40}  variant="text" style={{ width: '100px', margin: 'auto',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                                : `${suratKeluar.tembusan_keluar_mk} %` 
                            }
                        </Typography>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    )
}

export default ContentAsalSurat;