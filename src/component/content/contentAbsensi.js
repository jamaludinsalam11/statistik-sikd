import React, {useEffect, useState} from 'react';
import moment from 'moment';
import Moment from 'moment'
import { extendMoment } from 'moment-range';
import {useLocation} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import ContentGrafikTerbaca from './contentGrafikSuratTerbaca';
import {
	Avatar,
	Box,
	Card,
	CardContent,
	Grid,
	Typography
  } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';
import { red } from '@material-ui/core/colors';
import JumlahJamKerja from '../absensi/JumlahJamKerja';
import JamKerjaPerMinggu from '../absensi/JamKerjaPerMinggu';
import JamKerjaPerHari from '../absensi/JamKerjaPerHari';
import Terlambat from '../absensi/Terlambat';
import PulangCepat from '../absensi/PulangCepat';
import TidakAbsenPulang from '../absensi/TidakAbsenPulang';


const useStyles = makeStyles((theme) => ({
	contentSuratTerbaca: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '200px',
		boxShadow: theme.shadow.large,
	},
	contentSuratTerbacaGrid: {
		paddingTop: theme.spacing(2)
	},
	contentSuratTerbacaGrid2: {
		paddingTop: theme.spacing(2),
		paddingBottom: theme.spacing(2),
	},
	contentTingkatTanggapSIKD: {
		color: theme.palette.text.success
	},
	contentSuratTerbacaDibaca: {
		color: theme.palette.text.success
	},
	contentSuratTerbacaBelumDibaca: {
		color: theme.palette.text.error
	},

}));

function scaleValue(value, from, to) {
	var scale = (to[1] - to[0]) / (from[1] - from[0]);
	var capped = Math.min(from[1], Math.max(from[0], value)) - from[0];
	return ~~(capped * scale + to[0]);
}

function convertToBulan(value){
	const hasilBulan = 
		value == 1 
		? 'Januari' 
		: value == 2 
		? 'Februari' 
		: value == 3
		? 'Maret'
		: value == 4  
		? 'April'
		: value == 5
		? 'Mei'
		: value == 6
		? 'Juni'
		: value == 7
		? 'Juli'
		: value == 8
		? 'Agustus'
		: value == 9
		? 'September'
		: value == 10
		? 'Oktober'
		: value == 11
		? 'November'
		: 'Desember'
	return hasilBulan
}

const ContentAbsensi = () => {
    const classes = useStyles();
    let search  							= useLocation().search;
    const select_id 						= new URLSearchParams(search).get('select_id');
    const tgl1 								= new URLSearchParams(search).get('tgl1');
    const tgl2 								= new URLSearchParams(search).get('tgl2');
	const [tgl, setTgl] 					= useState({tgl1: tgl1, tgl2:tgl2});
	const [loading, setLoading]				= useState(true)
	const [user, setUser] 					= useState(null)
	const [dataMentah, setDataMentah] 		= useState(null)
	const [dataSempurna, setDataSempurna] 	= useState(null)

	useEffect(() => {
		/** For Development, bulan - tahun - NIP */
		// const bulan = 'Juli'
		// const tahun = '2020'
		const jamal = '199609072021033375'
		const adam 	= '199104082018011001'
		// const sekjen= '196501081990021001'
		// const sigit	= '196805181992031002'
		const bulanMulaiArray = moment(tgl1).format('YYYY-MM-DD')
		const bulanAkhirArray = moment(tgl2).format('YYYY-MM-DD')
		console.log('bulanArray',bulanMulaiArray)
		console.log('bulanAkhirArray',bulanAkhirArray)
		const moment2 = extendMoment(Moment)
		const range = moment2.range(bulanMulaiArray, bulanAkhirArray)
		const r1 = range.snapTo('MM')
		const r2 = range.snapTo('YYYY')
		const monthly = Array.from(r2.by('months')).map(m => m.format('MM'))
		const yearly = Array.from(r2.by('years')).map(m => m.format('YYYY'))
		console.log('range', monthly)
		console.log('range', yearly)

		/** For Production, select_id - bulan - tahun */
		const bulan = convertToBulan(moment(tgl1).format('M'))
		const tahun = moment(tgl1).format('YYYY')
		// select_id => from urlSearchParams
		
		console.log('tgl1moment', bulan)
		const fetchUser = async() => {
			
			try{
				/**
				 * GET USER DATA FROM apps.mkri.id / API bang Alex
				 * ======================================================
				 * - @host REACT_APP_API_USER
				 * - bridge sikd to data pegawai apps melalui nomor NIP
				 * - yang ada pada table Role SIKD
				 * - data user akan digunakan untuk mengambil data 
				 * 	 dari API Abensi bulanan
				 */
				setLoading(true)
				const res = await fetch(`${process.env.REACT_APP_API_USER}?select_id=${select_id}`)
				const results = await res.json()

				/**
				 * GET DATA ABSENSI from apps.mkri.id
				 * ======================================================
				 * - @host REACT_APP_API_APPS_PEGAWAI
				 * - data nya masih banyak yg perlu diolah
				 */
				
				
				// const res2 = await fetch(`${process.env.REACT_APP_API_APPS_PEGAWAI}/${adam}`)
				const res2 = await fetch(`${process.env.REACT_APP_API_APPS_PEGAWAI}/${results.data[0].no_nip}`)
				const results2 = await res2.json()
				const objUser = {
					"pgw_id"		: 	results2[0].pgw_id,
					"RoleId"        :   results.data[0].RoleId,
					"PeopleName"    :   results.data[0].PeopleName,
					"RoleName"      :   results.data[0].RoleName,
					"gjabatanName"  :   results.data[0].gjabatanName,
					"no_nip"        :   results.data[0].no_nip,
				}

				const res3 = await fetch(`${process.env.REACT_APP_API_APPS_ABSENSI}/${results2[0].pgw_id}/${bulan}/${tahun}`)
				const results3 = await res3.json()

				/**
				 * Create Array & Object Temporary , 
				 * hasil dari api data absensi
				 */
				let mentah1 = [];
				let objMentah1 = {}
				let total_jam_kerja_jadwal = 0 
				
				for(let i = 0 ; i < results3.length ; i++){
					let obj1 = results3[i]
					let tgl_today = moment().format('YYYY-MM-DD') 
					
					if(!(obj1.bool_hari_libur || obj1.bool_weekend || tgl_today < obj1.tanggal )){
						/**
						 * Get Waktu Jam Kerja harian
						 * --------------------------
						 * - Sesuai jadwal yang diinput
						 * - yang digunakan antara lain: 
						 * 		* a_jadwal, b_jadwal, (karna jadwal hanya menyantumkan Jam saja, maka dibuatkan 
						 * 			variable sementara agar digabungkan dengan tanggal dari api)
						 * 		* jadwal_duration (menentukan berapa lama jam kerja yang seharusnya, 
						 * 		 	data ini masih format moment js , jadi harus di translate lagi ke variabel selanjutnya),
						 * 		* jadwal_hours (mentranslate format moment js, output berupa Decimal JAM
						 * 			memfilter datetime dengan object property dari api :
						 * 			- tanggaljam_mulai !== null || tanggaljam_selesai !== null
						 * 			- && bool_wfh !== null	
						 * 			- jika WFH maka set maximal = 7.5 JAM
						 * 		* jadwal_minutes (convert Hours to Minutes)
						 */
							const a_jadwal_pimpinan_wfh		= moment('2021-07-15 07:00:59+07')
							const b_jadwal_pimpinan_wfh		= moment('2021-07-15 14:30:00+07')
							const a_jadwal_pimpinan_wfo		= moment('2021-07-15 07:00:59+07')
							const b_jadwal_pimpinan_wfo		= moment('2021-07-15 15:00:00+07')
							const a_jadwal_not 				= `${obj1.jadwal.tanggal} ${moment(obj1.jam_masuk, 'HH:mm:ss').format('HH:mm:ss')}`
							const b_jadwal_not 				= `${obj1.jadwal.tanggal} ${moment(obj1.jam_pulang, 'HH:mm:ss').format('HH:mm:ss')}`
							const a_jadwal 					= moment(a_jadwal_not)
							const b_jadwal					= moment(b_jadwal_not)
							const a 						= `${obj1.jadwal.tanggal} ${a_jadwal}`
							const jadwal_duration 			=  obj1.pgw_id == 569 || obj1.pgw_id == 579 && obj1.bool_wfh == true && obj1.jadwal_masuk == null || obj1.jadwal_keluar == null
																? moment.duration(b_jadwal_pimpinan_wfh.diff(a_jadwal_pimpinan_wfh))
																: obj1.pgw_id == 569 || obj1.pgw_id == 579 && obj1.bool_wfh == false
																? moment.duration(b_jadwal_pimpinan_wfo.diff(a_jadwal_pimpinan_wfo))
																: moment.duration(b_jadwal.diff(a_jadwal))
							// console.log('jadwal_duration',jadwal_duration 	)	
							/**
							 * Update dari pak sekjen 14 Juli 2021
							 * ----------------------------------
							 * Jika NIP = Sekjen dan Panitera maka set 7.5 Jam wfh 8 Jam WFO mset Manually
							 * via pgw_id
							 */
							// Jika NIP = Sekjen dan Panitera maka set 7.5 Jam wfh 8 Jam WFO mset Manually
							// Set By pgw_id = 569
							// const jadwal_hours 				= obj1.tanggaljam_mulai == null || obj1.tanggaljam_selesai == null 
							// 									? null 
							// 									: obj1.bool_wfh == true 
							// 									? 7.5 
							// 									: obj1.pgw_id == 569 || obj1.pgw_id == 579 && obj1.bool_wfh == true || obj1.tanggaljam_mulai == null || obj1.tanggaljam_selesai == null
							// 									? 7.5
							// 									: obj1.pgw_id == 569 || obj1.pgw_id == 579 && obj1.bool_wfh == false || obj1.tanggaljam_mulai == null || obj1.tanggaljam_selesai == null
							// 									? 8
							// 									: jadwal_duration.asHours()
							const jadwal_minutes 			= jadwal_duration.asMinutes()
							const total_jam_kerja_jadwal 	= 0 
							const  jadwal_hours = obj1.tanggaljam_mulai == null || obj1.tanggaljam_selesai == null 
													? null 
													: obj1.bool_wfh == true 
													? 7.5 
													: jadwal_duration.asHours()
							// if(obj1.pgw_id == 569 || obj1.pgw_id == 579 ){
							// 	const jadwal_hourss 	= obj1.tanggaljam_mulai == null || obj1.tanggaljam_selesai == null || obj1.jadwal_masuk == null || obj1.jadwal_pulang == null && obj1.bool_wfh == true  || obj1.bool_wfh == null || obj1.bool_hari_libur == false
							// 							? 7.5
							// 							: 8
							// 		jadwal_hours += jadwal_hourss
							// } else {
							// 	const jadwal_hourss 	= obj1.tanggaljam_mulai == null || obj1.tanggaljam_selesai == null 
							// 							? null 
							// 							: obj1.bool_wfh == true 
							// 							? 7.5 
							// 							: jadwal_duration.asHours()
							// 		jadwal_hours += jadwal_hourss
							// }
							// let jadwal_hours = 0
							// if(obj1.pgw_id == 569 || obj1.pgw_id == 579 ){
							// 	const jadwal_hourss 	= obj1.tanggaljam_mulai == null || obj1.tanggaljam_selesai == null || obj1.jadwal_masuk == null || obj1.jadwal_pulang == null && obj1.bool_wfh == true  || obj1.bool_wfh == null || obj1.bool_hari_libur == false
							// 							? 7.5
							// 							: 8
							// 		jadwal_hours += jadwal_hourss
							// } else {
							// 	const jadwal_hourss 	= obj1.tanggaljam_mulai == null || obj1.tanggaljam_selesai == null 
							// 							? null 
							// 							: obj1.bool_wfh == true 
							// 							? 7.5 
							// 							: jadwal_duration.asHours()
							// 		jadwal_hours += jadwal_hourss
							// }
							

						/**
						 * Get Waktu Jam Kerja harian
						 * --------------------------
						 * Actual 
						 * (Jam Kerja sesuai ketika user klik absen masuk dan absen keluar)
						 */
							const a_actual_not		= obj1.tanggaljam_mulai == null ? null : moment(obj1.tanggaljam_mulai).format( "YYYY-MM-DD HH:mm:ss")
							const b_actual_not		= obj1.tanggaljam_selesai == null ? null : moment(obj1.tanggaljam_selesai).format( "YYYY-MM-DD HH:mm:ss")
							const a_actual			= moment(obj1.tanggaljam_mulai, 'YYYY-MM-DD HH:mm:ss')
							const b_actual 			= moment(obj1.tanggaljam_selesai, 'YYYY-MM-DD HH:mm:ss')
							const actual_durations	= moment.duration(b_actual.diff(a_actual))
							/**
							 * Jika WFH maka tidak ada overtime, total jam kerja di set maksimal 7.5 jam 
							 * 
							 * @actual_hours || @actual_minutes
							 */
							const actual_minutes	= obj1.tanggaljam_mulai == null || obj1.tanggaljam_selesai == null ? null : actual_durations.asMinutes()
							const actual_hours		= obj1.tanggaljam_mulai == null || obj1.tanggaljam_selesai == null ? null : obj1.bool_wfh == true ? 7.5 : actual_durations.asHours()
							const tojam 			= moment(actual_hours).format('hh')

						/**
						 * Get Waktu Terlambat
						 * -------------------
						 * cek apakah a_actual itu melebihi dari a_jadwal value berupa boolean true or false
						 */
							const terlambat			= moment(a_actual).isAfter(a_jadwal)
							const terlambat_waktu	= Math.ceil(moment.duration(a_actual.diff(a_jadwal)).asMinutes())

						/**
						 * Get Pulang Cepat
						 * ------------------
						 * cek apakah b_actual itu belum menyentuh b_jadwal yang sudah ditetapkan
						 * compare				: ini untuk kasus user yang lupa absen, dan absen dihari setelah nya
						 * compared_tgl			: tanggal yg dinilai melebihi dr yang seharusnya dan dikurangkan -1 'days'
						 * pulangcepat			: true or false
						 * pulangcepat_waktu	: calculate how much user late, satuan minutes 
						 */
							const compare_tanggal	= moment(b_actual).add(-1, 'days')
							const compare 			= moment(compare_tanggal, 'days').isSame(b_jadwal, 'days')
							const compared_tgl		= moment(compare_tanggal, 'YYYY-MM-DD HH:mm:ss')
							const pulangcepat		= compare ? true : moment(b_actual).isBefore(b_jadwal) ? true : false
							const pulangcepat_waktu	= compare ? Math.ceil(moment.duration(b_jadwal.diff(compared_tgl)).asMinutes()) : Math.ceil(moment.duration(b_jadwal.diff(b_actual)).asMinutes())
						 	
						/* Object Temporary for prepare push to STATE dataMentah */
						objMentah1 = {
							'user'					: {
								"pgw_id"		: 	results2[0].pgw_id,
								"RoleId"        :   results.data[0].RoleId,
								"PeopleName"    :   results.data[0].PeopleName,
								"RoleName"      :   results.data[0].RoleName,
								"gjabatanName"  :   results.data[0].gjabatanName,
								"no_nip"        :   results.data[0].no_nip,
							},
							'bool_wfh'				: obj1.bool_wfh ,
							'pgw_id'				: obj1.jadwal.pgw_id,
							'haritanggal'			: `${obj1.hari}, ${obj1.jadwal.tanggal}`,
							'jadwal_masuk'			: a_jadwal_not,
							'jadwal_keluar'			: b_jadwal_not,
							'jadwal_hours'			: jadwal_hours,
							'jadwal_minutes'		: jadwal_minutes,
							'actual_checkin'		: a_actual_not,
							'actual_checkout'		: b_actual_not,
							'actual_hours'			: actual_hours,
							'actual_minutes'		: actual_minutes,
							'overtime_hours'		: actual_hours - jadwal_hours,
							'overtime_minutes'		: actual_minutes - jadwal_minutes,
							'label_hari_libur'		: obj1.label_hari_libur,
							'terlambat'				: terlambat,
							'terlambat_waktu'		: terlambat ? terlambat_waktu : null,
							'pulangcepat'			: pulangcepat,
							'pulangcepat_waktu'		: pulangcepat ? pulangcepat_waktu : null,
							'tidakabsen_pulang'		: obj1.tanggaljam_selesai == null && obj1.tanggaljam_mulai != null ? true : false 
	
						}
						/** Push to mentah1 Array variable */
						mentah1[i] = objMentah1
						 
					}
					
					
				}

				/** For Debugging
				 * console.log('debug dataMentah1 :',mentah1)
				 * filter empty_array dengan arr 
				 * console.log(result3) : untuk cek response api absen
				 */
				console.log(results3)
				console.log('debug dataMentah1 :',mentah1)
				// const arr = mentah1.filter(item=>item)
				// console.log('mentah1 lenght', arr.length)

				/** Push to dataMentah State */
				setDataMentah(mentah1)
				setLoading(true)
			} catch (e) {
				console.log(e)
				setLoading(false)
			}
		}
		fetchUser()
	}, [])


	/**
	 * PENGOLAHAN DATA ABSENSI
	 * ====================================================
	 * - akan berjalan setelah STATE dataMentah sudah ready
	 * - triggered by dataMentah
	 */
	useEffect(() => {
		const olahData = async () => {
			setLoading(true)
			try{
				/** Use filter method from pure javascript 
				 * for remove empty array hasil pemrosesan filter data mentah
				*/
				const res = dataMentah.filter(item=>item)
				console.log('res', res)

				/**
				 * Constanta Variable
				 * @var total_jamkerja 			: total jam kerja satu bulan berdasarkan hari yang tidak libur / weekend,
				 * @var total_jamkerja_jadwal	: total jam kerja sesuai absen yang diinputkan
				 * @var telat_waktu				: total akumulasi waktu telat, satuan minutes
				 * @var pulangcepat_waktu		: total akumulasi waktu pulang cepat, satuan minutes
				 */
				const total_jamkerja 		= res.reduce((accumulator, current) => accumulator + current.actual_hours, 0)
				const total_jamkerja_jadwal = res.reduce((accumulator, current) => accumulator + current.jadwal_hours, 0)
				const telat_waktu 			= res.reduce((accumulator, current) => accumulator + current.terlambat_waktu, 0)
				const pulangcepat_waktu 	= res.reduce((accumulator, current) => accumulator + current.pulangcepat_waktu, 0)
				
				/**
				 * Constanta Variable
				 * @var telat			: jumlah hari telat dalam satu bulan
				 * @var pulangcepat		: jumlah hari pulangcepat dalam satu bulan
				 * @var tidakabsenpulang: jumlah hari tidak absen pulang 
				 */
				let telat = 0
				let pulangcepat = 0
				let tidakabsenpulang = 0
				for(let i = 0; i < res.length; i++){
					if(res[i].terlambat == true) telat++		
					if(res[i].pulangcepat == true) pulangcepat++
					if(res[i].tidakabsen_pulang == true) tidakabsenpulang++
				}

				/**
				 * Constanta Variable Rata-Rata Mingguan dan Harian
				 */
				let counting = 0
				let jkperhari = 0
				let jkperhariJadwal = 0
				let tmpTotal = null
				let tmpTotalJadwal = null
				let tmpTotalMenit = 0;
				let tmpTotalMenitJadwal = 0;
				let pengurang = 0
				let jumlah_minggu = Math.floor(res.length / 5);
				let totalJamKerjaWfh = 0
				let totalJamKerjaWfhJadwal = 0
				let total_jamkerja_perminggu = 0
				let total_jamkerja_perminggu_jadwal = 0

				if(jumlah_minggu == 0) jumlah_minggu = 1;
				res.forEach((data, i) => {
					// console.log('data i', data)
					if(data.actual_checkout == null) pengurang = 1;
					if(data.user.pgw_id == 569 || data.user.pgw_id == 579 ){
						tmpTotal 				= data.bool_wfh ? 7.5 :  data.actual_hours
						tmpTotalJadwal			= data.bool_wfh ? 7.5 :  8
						totalJamKerjaWfh 		+= tmpTotal
						totalJamKerjaWfhJadwal	+= tmpTotalJadwal	
						if(jumlah_minggu == 1 && i > 4 + counting){

						} else {
							total_jamkerja_perminggu 		+= tmpTotal
							total_jamkerja_perminggu_jadwal	+= tmpTotalJadwal
						}
					} else if (data.actual_checkout !== null){
						tmpTotal 				= data.bool_wfh ? 7.5 :  data.actual_hours
						tmpTotalJadwal			= data.bool_wfh ? 7.5 :  data.jadwal_hours
						totalJamKerjaWfh 		+= tmpTotal
						totalJamKerjaWfhJadwal	+= tmpTotalJadwal	
						if(jumlah_minggu == 1 && i > 4 + counting){

						} else {
							total_jamkerja_perminggu 		+= tmpTotal
							total_jamkerja_perminggu_jadwal	+= tmpTotalJadwal
						}
					} else {
						counting++
					}
					// if(data.actual_checkout !== null){
						
					// 	tmpTotal 				= data.bool_wfh ? 7.5 :  data.actual_hours
					// 	tmpTotalJadwal			= data.bool_wfh ? 7.5 :   data.jadwal_hours
					// 	totalJamKerjaWfh 		+= tmpTotal
					// 	totalJamKerjaWfhJadwal	+= tmpTotalJadwal	
					// 	if(jumlah_minggu == 1 && i > 4 + counting){

					// 	} else {
					// 		total_jamkerja_perminggu 		+= tmpTotal
					// 		total_jamkerja_perminggu_jadwal	+= tmpTotalJadwal
					// 	}
					// } else {
					// 	counting++
					// }

				})

				let jml_hari_kerja = res.length - pengurang;
				if(jumlah_minggu > 1){
					tmpTotalMenit 		= total_jamkerja_perminggu * 60
					tmpTotalMenitJadwal	= total_jamkerja_perminggu_jadwal * 60
					tmpTotalMenit 		= tmpTotalMenit / jumlah_minggu
					tmpTotalMenitJadwal = tmpTotalMenitJadwal / jumlah_minggu

					total_jamkerja_perminggu 		= tmpTotalMenit / 60
					total_jamkerja_perminggu_jadwal = tmpTotalMenitJadwal / 60
					jkperhari 						= total_jamkerja_perminggu / 5
					jkperhariJadwal 				= total_jamkerja_perminggu_jadwal / 5
				} else {
					tmpTotalMenit 		= total_jamkerja_perminggu * 60
					tmpTotalMenitJadwal = total_jamkerja_perminggu_jadwal * 60
					tmpTotalMenit 		= tmpTotalMenit / jumlah_minggu
					tmpTotalMenitJadwal = tmpTotalMenitJadwal / jumlah_minggu

					total_jamkerja_perminggu 		= tmpTotalMenit / 60
					total_jamkerja_perminggu_jadwal = tmpTotalMenitJadwal / 60
					jkperhari  						= total_jamkerja_perminggu / jml_hari_kerja
					jkperhariJadwal					= total_jamkerja_perminggu_jadwal / jml_hari_kerja
				}

				jkperhari 		= totalJamKerjaWfh / jml_hari_kerja
				jkperhariJadwal = totalJamKerjaWfhJadwal / jml_hari_kerja
				

				/**
				 * FOR Debugging 
				 * TOTAL JAM KERJA, RATA-RATA JAM KERJA MINGGUAN HARIAN (ACTUAL)
				 * TOTAL JAM KERJA, RATA-RATA JAM KERJA MINGGUAN HARIAN (JADWAL)
				 */
				// console.log('useeffect datajadi : total_jamkerja', parseFloat(total_jamkerja).toFixed(1))
				// console.log('useeffect datajadi : total_jamkerja_jadwal', parseFloat(total_jamkerja_jadwal).toFixed(1))
				// console.log('useeffect datajadi : rata_jamkerja_minggu', parseFloat(total_jamkerja_perminggu))
				// console.log('useeffect datajadi : rata_jamkerja_minggu_jadwal', parseFloat(total_jamkerja_perminggu_jadwal))
				// console.log('useeffect datajadi : rata_jamkerja_hari', parseFloat(jkperhari).toFixed(1))
				// console.log('useeffect datajadi : rata_jamkerja_hari_jadwal', parseFloat(jkperhariJadwal).toFixed(1))
				// console.log('useeffect datajadi : telat', `${telat} kali`)
				// console.log('useeffect datajadi : telat_waktu_jam', Math.floor(telat_waktu / 60))
				// console.log('useeffect datajadi : telat_waktu_menit', telat_waktu % 60)
				// console.log('useeffect datajadi : pulangcepat', `${pulangcepat} kali`)
				// console.log('useeffect datajadi : pulangcepat_waktu_jam', Math.floor(pulangcepat_waktu / 60))
				// console.log('useeffect datajadi : pulangcepat_waktu_menit', pulangcepat_waktu % 60)
				// console.log('useeffect datajadi : tidakabsenpulang', tidakabsenpulang)


				/**
				 * SET Percentage from 
				 * ====================
				 * - jamkerja_total and jamkerja_total_jadwal	= a1, per_tahun, per_total_jadwal, percentage_total
				 * - jamkerja_minggu and kamkerja_minggu_jadwal = a2, per_mingguan, per_mingguan_jadwal, percentage_mingguan, per_mingguan_min_positive
				 * - jamkerja_hari and jamkerja_hari_jadwal		= a3, per_hari, per_hari_jadwal, percentage_hari, per_hari_min_positive	
				 * ---------------------------------------------------------------------------------------------------------
				 *   per_total_min_positive || per_mingguan_min_positive || per_hari_min_positive	 => (USE THIS for state)
				 */
				// const a1						= Number(parseFloat(total_jamkerja).toFixed(1)) - Number(parseFloat(total_jamkerja_jadwal).toFixed(1))
				const a1						= Number(parseFloat(total_jamkerja).toFixed(1)) - Number(parseFloat(totalJamKerjaWfhJadwal).toFixed(1))
				const per_total					= a1 < 0 ? Number(parseFloat(a1).toFixed(1)) * (-1) : Number(parseFloat(a1).toFixed(1))
				// const per_total_jadwal			= Number(parseFloat(total_jamkerja_jadwal).toFixed(1))
				const per_total_jadwal			= Number(parseFloat(totalJamKerjaWfhJadwal).toFixed(1))
				const percentage_total 			= scaleValue(per_total, [0, per_total_jadwal], [0,100])
				const per_total_min_positive	= a1 < 0 ? `- ${percentage_total} %` : `${percentage_total} %`
				const total_min_positive 		= a1 < 0 ? 'minus' : a1 > 0 ? 'positive' : 'equals'

				const a2 						= Number(parseFloat(total_jamkerja_perminggu).toFixed(1)) - Number(parseFloat(total_jamkerja_perminggu_jadwal).toFixed(1))
				const per_mingguan				= a2 < 0 ? Number(parseFloat(a2).toFixed(1)) * (-1) : Number(parseFloat(a2).toFixed(1))
				const per_mingguan_jadwal		= Number(parseFloat(total_jamkerja_perminggu_jadwal).toFixed(1))
				const percentage_mingguan		= scaleValue(per_mingguan, [0, per_mingguan_jadwal], [0,100])
				const per_mingguan_min_positive	= a2 < 0 ? `- ${percentage_mingguan} %` : `${percentage_mingguan} %`
				const mingguan_min_positive 	= a2 < 0 ? 'minus' : a2 > 0 ? 'positive' : 'equals'
				
				const a3						= Number(parseFloat(jkperhari).toFixed(1)) - Number(parseFloat(jkperhariJadwal).toFixed(1))
				const per_hari					= a3 < 0 ? Number(parseFloat(a3).toFixed(1)) * (-1) : Number(parseFloat(a3).toFixed(1))
				const per_hari_jadwal			= Number(parseFloat(jkperhariJadwal).toFixed(1))
				const percentage_hari			= scaleValue(per_hari, [0, per_hari_jadwal], [0,100])
				const per_hari_min_positive		= a3 < 0 ? `- ${percentage_hari} %` : `${percentage_hari} %`
				const hari_min_positive 		= a3 < 0 ? 'minus' : a3 > 0 ? 'positive' : 'equals'
				
				// console.log('Debug Percentage',per_hari_min_positive)


				/**
				 * KIRIM Data Sempurna to STATE dataSempurna
				 * Holaaaa SELESAI
				 */
				setDataSempurna({
					jamkerja_total 			: Number(parseFloat(total_jamkerja).toFixed(1)),
					jamkerja_total_jadwal	: Number(parseFloat(totalJamKerjaWfhJadwal ).toFixed(1)),
					// jamkerja_total_jadwal	: Number(parseFloat(total_jamkerja_jadwal).toFixed(1)),
					jamkerja_minggu			: Number(parseFloat(total_jamkerja_perminggu).toFixed(1)),
					jamkerja_minggu_jadwal	: Number(parseFloat(total_jamkerja_perminggu_jadwal).toFixed(1)),
					jamkerja_hari			: Number(parseFloat(jkperhari).toFixed(1)),
					jamkerja_hari_jadwal	: Number(parseFloat(jkperhariJadwal).toFixed(1)),
					percentage_total		: per_total_min_positive ,
					percentage_mingguan		: per_mingguan_min_positive,
					percentage_hari			: per_hari_min_positive ,
					min_positive_total		: total_min_positive ,
					min_positive_mingguan	: mingguan_min_positive ,
					min_positive_hari		: hari_min_positive ,
					telat					: telat,
					telat_jam				: Math.floor(telat_waktu / 60),
					telat_minute			: telat_waktu % 60,
					pulangcepat				: pulangcepat,
					pulangcepat_jam			: Math.floor(pulangcepat_waktu / 60),
					pulangcepat_minute		: pulangcepat_waktu % 60,
					tidakabsen_pulang		: tidakabsenpulang,
					user					: {
												"pgw_id"		: 	res[0].user.pgw_id,
												"RoleId"        :   res[0].user.RoleId,
												"PeopleName"    :   res[0].user.PeopleName,
												"RoleName"      :   res[0].user.RoleName,
												"gjabatanName"  :   res[0].user.gjabatanName,
												"no_nip"        :   res[0].user.no_nip,
											}
				})
				setLoading(false)
			} catch (e) {
				console.log(e)
				setLoading(false)
			}
		}
		olahData()
	},[dataMentah])
	// console.log('dataMentah', dataMentah)
	console.log('dataSempurna', dataSempurna)
	// console.log('loading', loading)
    return(
		<>
		<Grid
			item
			lg={4}
			sm={6}
			xl={4}
			xs={12}
		><JumlahJamKerja data={dataSempurna} loading={loading ? true : loading}/></Grid>
		<Grid
			item
			lg={4}
			sm={6}
			xl={4}
			xs={12}
		><JamKerjaPerMinggu data={dataSempurna} /> </Grid>
		<Grid
			item
			lg={4}
			sm={6}
			xl={4}
			xs={12}
		><JamKerjaPerHari data={dataSempurna} /> </Grid>
		
		<Grid
			item
			lg={4}
			sm={6}
			xl={4}
			xs={12}
		><Terlambat data={dataSempurna}/> </Grid>
		<Grid
			item
			lg={4}
			sm={6}
			xl={4}
			xs={12}
		><PulangCepat data={dataSempurna}/> </Grid>
		<Grid
			item
			lg={4}
			sm={6}
			xl={3}
			xs={12}
		><TidakAbsenPulang data={dataSempurna}/> </Grid>

		</>
    )
}

export default ContentAbsensi;