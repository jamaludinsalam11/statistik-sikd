import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { ResponsiveBar } from '@nivo/bar'
import { keys } from '@material-ui/core/styles/createBreakpoints';
import { Avatar, Chip } from '@material-ui/core';

// const data = [
//     {
//       "jenissurat": "Nota Dinas",
//       "surat masuk": 50,
//       "surat masukColor": "hsl(48, 70%, 50%)",
//       "surat keluar": 55,
//       "surat keluarColor": "hsl(188, 70%, 50%)",
      
//     },
//     {
//       "jenissurat": "Disposisi",
//       "surat masuk": 39,
//       "surat masukColor": "hsl(345, 70%, 50%)",
//       "surat keluar": 20,
//       "surat keluarColor": "hsl(253, 70%, 50%)",
      
//     },
//     {
//       "jenissurat": "Tembusan",
//       "surat masuk": 40,
//       "surat masukColor": "hsl(214, 70%, 50%)",
//       "surat keluar": 11,
//       "surat keluarColor": "hsl(284, 70%, 50%)",
      
//     },
    
//   ]

const useStyles = makeStyles((theme) => ({
	contentGrafikDetail: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '420px',
		boxShadow: theme.shadow.large,
        paddingBottom: '3em'
		// borderRadius: 15
	},

}));

const ContenGrafikDetail = ({detail, totalSurat}) => {
    const classes = useStyles();
    console.log('detail', detail)
    
    return(
            <Grid item xs={6}>
                <Paper className={classes.contentGrafikDetail}>
                    <Grid item xs={12}>
                        <Typography variant="h5" gutterBottom>Detail Surat Keluar/Masuk</Typography>
                        <Divider/>
                    </Grid>
                    <Grid item xs={12} style={{marginTop: '1em', marginBottom: '-1.5em'}}>
                        
                        <Chip
                            avatar={<Avatar style={{backgroundColor: 'white', color: 'rgb(244, 117, 96)'}}>{totalSurat.suratkeluar}</Avatar>}
                            label="Surat Keluar"
                            style={{backgroundColor: 'rgb(244, 117, 96)', color: 'white', marginRight: '1em'}}
                        />
                        <Chip
                            avatar={<Avatar style={{backgroundColor: 'white', color: 'rgb(232, 193, 160)'}}>{totalSurat.suratmasuk}</Avatar>}
                            label="Surat Masuk"
                            style={{backgroundColor: 'rgb(232, 193, 160)', color: 'white'}}
                        />
                    </Grid>
                    <ResponsiveBar
                        data={detail}
                        keys={[ 'surat masuk', 'surat keluar']}
                        indexBy="jenissurat"
                        margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
                        padding={0.3}
                        valueScale={{ type: 'linear' }}
                        indexScale={{ type: 'band', round: true }}
                        colors={{ scheme: 'nivo' }}
                        defs={[
                            {
                                id: 'dots',
                                type: 'patternDots',
                                background: 'inherit',
                                color: '#38bcb2',
                                size: 4,
                                padding: 1,
                                stagger: true
                            },
                            {
                                id: 'lines',
                                type: 'patternLines',
                                background: 'inherit',
                                color: '#eed312',
                                rotation: -45,
                                lineWidth: 6,
                                spacing: 10
                            }
                        ]}
                        fill={[
                            {
                                match: {
                                    id: 'fries'
                                },
                                id: 'dots'
                            },
                            {
                                match: {
                                    id: 'sandwich'
                                },
                                id: 'lines'
                            }
                        ]}
                        borderColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                        axisTop={null}
                        axisRight={null}
                        axisBottom={{
                            tickSize: 5,
                            tickPadding: 5,
                            tickRotation: 0,
                            legend: 'Jenis Surat',
                            legendPosition: 'middle',
                            legendOffset: 32
                        }}
                        axisLeft={{
                            tickSize: 5,
                            tickPadding: 5,
                            tickRotation: 0,
                            legend: 'Jumlah',
                            legendPosition: 'middle',
                            legendOffset: -40
                        }}
                        labelSkipWidth={12}
                        labelSkipHeight={12}
                        labelTextColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                        legends={[
                            {
                                dataFrom: 'keyss',
                                // data: detail.map((id, index) => ({
                                //     id: 'surat_masuk_count',
                                //     // color: "black",
                                //     // value: id === "surat masuk" ? 1 : 2,
                                //     label: "hi"
                                // })),
                                anchor: 'bottom-right',
                                direction: 'column',
                                justify: false,
                                translateX: 120,
                                translateY: 0,
                                itemsSpacing: 2,
                                itemWidth: 100,
                                itemHeight: 20,
                                itemDirection: 'left-to-right',
                                itemOpacity: 0.85,
                                symbolSize: 20,
                                effects: [
                                    {
                                        on: 'hover',
                                        style: {
                                            itemOpacity: 1
                                        }
                                    }
                                ]
                            }
                        ]}
                        animate={true}
                        motionStiffness={90}
                        motionDamping={15}
                    />
				</Paper>
			</Grid>
    )
}

export default ContenGrafikDetail;