import React, {useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import moment from 'moment'
import PropTypes from 'prop-types';
import { makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Skeleton from '@material-ui/lab/Skeleton';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import SemiCircleProgressBar from "react-progressbar-semicircle";
import GaugeChart from 'react-gauge-chart';
import ReactSpeedometer from 'react-d3-speedometer'

import Chart from 'react-apexcharts'
// import RadialBarChart from 'react-radial-bar-chart';

// Import react-circular-progressbar module and styles
import {
  CircularProgressbar,
  CircularProgressbarWithChildren,
  buildStyles
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";


const useStyles = makeStyles((theme) => ({

	contentWaktuRespon: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: 'auto',
		boxShadow: theme.shadow.large,
		// borderRadius: 15
	},
	contentWaktuResponSuccess: {
		color: theme.palette.text.success
	},
	contentWaktuResponError: {
		color: theme.palette.text.error
	},
	contentWaktuResponPercentageSuccess: {
		color: theme.palette.text.success,
		display: 'flex',
		alignItems: 'center',
		flexWrap: 'wrap',
		fontSize: '2rem',
	},
	contentWaktuResponPercentageError: {
		color: theme.palette.text.error,
		display: 'flex',
		alignItems: 'center',
		flexWrap: 'wrap',
		fontSize: '3rem'
	},
	contentWaktuResponButton:{
		marginTop: theme.spacing(2),
		backgroundColor: theme.palette.text.warning,
		color: 'white',
		borderRadius: 20,
		'&:hover': {
			backgroundColor: '#EFAF00',
		 },
	},
    CircularProgressbar : {
        height: 350
    }


}));



function scaleValue(value, from, to) {
  var scale = (to[1] - to[0]) / (from[1] - from[0]);
  var capped = Math.min(from[1], Math.max(from[0], value)) - from[0];
  return ~~(capped * scale + to[0]);
}



var options1 = {
    series: [76, 67],
            options: {
              chart: {
                height: 390,
                type: 'radialBar',
              },
              plotOptions: {
                radialBar: {
                  offsetY: 0,
                  startAngle: 0,
                  endAngle: 270,
                  hollow: {
                    margin: 5,
                    size: '30%',
                    background: 'transparent',
                    image: undefined,
                  },
                  dataLabels: {
                    name: {
                      show: false,
                    },
                    value: {
                      show: false,
                    }
                  }
                }
              },
              colors: ['#1ab7ea', '#39539E'],
              labels: ['Sikd', 'SKP'],
              legend: {
                show: true,
                floating: true,
                fontSize: '16px',
                position: 'left',
                offsetX: 60,
                offsetY: 25,
                labels: {
                  useSeriesColors: true,
                },
                markers: {
                  size: 0
                },
                formatter: function(seriesName, opts) {
                  return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex] + "%"
                },
                itemMargin: {
                  vertical: 6
                }
              },
              responsive: [{
                breakpoint: 380,
                options: {
                  legend: {
                      show: false
                  }
                }
              }]
            },
          
  };
  


const ContentBebanRespon = () => {
    const classes = useStyles();
    const [respon, setRespon] = useState({respon:[], average_all:[]})
    const [ min, setMin ]     = useState(null);
    const [ max, setMax ]     = useState(null);
    const [ avg, setAvg ]     = useState(null);
    const [ value, setValue ] = useState(null);

    const [ users, setUsers ] = useState(null);
    const [ unitKerja, setUnitKerja ] = useState(null);
    const [ unitPeson, setUnitPerson ] = useState(null);
    const [ hasil, setHasil ]  = useState(null);
    const [ hasilText, setHasilText ] = useState(null);
    const [ role, setRole ] = useState(null)

    const [apexChart, setApexChart] = useState(null)

    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1 = new URLSearchParams(search).get('tgl1');
    const tgl2 = new URLSearchParams(search).get('tgl2');

    const tgl1_rapat = moment(tgl1).format('YYYY-MM-DD')
    const tgl2_rapat = moment(tgl2).format('YYYY-MM-DD')
    
 	useEffect(() => {
		const fetchData = async () => {
			try{
				await fetch(`${process.env.REACT_APP_API_USER}?select_id=${select_id}`)
                    .then((responses) => {
                        return responses.json()
                    })
                    .then((res) => {
                        setUsers(res.data[0]);
                        return res.data[0];
                    })
                    .then((res2) => {
                        const role_atasan = res2.RoleAtasan ;
                        const role_id = res2.RoleId;
                        const select_unit = res2.RoleAtasan === 'uk.1.1' ? res2.RoleId :res2.RoleAtasan;
                        // const select_unit = res2.RoleAtasan === 'uk.1.1' ? res2.RoleId :res2.RoleAtasan;
                        const select_id = res2.PeopleId
                        console.log('res2 beban respon',select_id )
                        console.log(select_unit )
                        setRole(role_id)
                        // console.log('role_idasdasdasdasdasdasdasdasdasdasdasdasdads', role_id)
                        /**
                         * Select_id : Role Atasan,
                         * select_roleid : Role Person
                         */
                        Promise.all([
                            fetch(`${process.env.REACT_APP_API_STATISTIK_UNIT}?select_id=${select_unit}&&tgl1=${tgl1}&tgl2=${tgl2}`),
                            fetch(`${process.env.REACT_APP_API_STATISTIK_PERSON}?select_roleid=${role_id}&select_id=${select_unit}&tgl1=${tgl1}&tgl2=${tgl2}`),
                            fetch(`${process.env.REACT_APP_API_RAPAT}?select_id=${select_id}&tgl1=${tgl1_rapat}&tgl2=${tgl2_rapat}`)
                        ])
                            .then((responses2) => {
                                return Promise.all(responses2.map(function(response) {
                                    return response.json();
                                }));
                            })
                            .then((data) => {
                                setUnitKerja(data[0])
                                setUnitPerson(data[1])
                                // console.log('idul adha',data[2].data[0].percentage)

                                /**
                                 * Rapat 
                                 * */
                                const rapat_percentage = data[2].data[0].percentage

                                const total_pesan_person = data[1].data[0].jmlSurat;
                                const total_pesan_unit = data[1].data[0].RoleId_To === 'uk.1.1' ? data[1].data[0].jmlSurat : data[0].total_surat;

                                const rumus = (total_pesan_person / total_pesan_unit) * 95 / 100;
                                const hasil = Math.ceil(rumus * 100);
                                const hasil_text = `${hasil}%`
                                
                                setHasil(hasil);
                                setHasilText(hasil_text)

                                const hakim = role_id === 'uk.1' || role_id === 'uk.2' || role_id === 'uk.3' || role_id === 'uk.5' || role_id === 'uk.6' || role_id === 'uk.7' || role_id === 'uk.8' || role_id === 'uk.9' || role_id === 'uk.12'   ? 100 : 0
                                const hakimtoPPK = role_id === 'uk.1' || role_id === 'uk.2' || role_id === 'uk.3' || role_id === 'uk.5' || role_id === 'uk.6' || role_id === 'uk.7' || role_id === 'uk.8' || role_id === 'uk.9' || role_id === 'uk.12'   ? 'PPK' : 'SKP'
                                const hakimtoPPKVal = role_id === 'uk.1' || role_id === 'uk.2' || role_id === 'uk.3' || role_id === 'uk.5' || role_id === 'uk.6' || role_id === 'uk.7' || role_id === 'uk.8' || role_id === 'uk.9' || role_id === 'uk.12'   ? 100 : 0
                                const hakimRapatVal = role_id === 'uk.1' || role_id === 'uk.2' || role_id === 'uk.3' || role_id === 'uk.5' || role_id === 'uk.6' || role_id === 'uk.7' || role_id === 'uk.8' || role_id === 'uk.9' || role_id === 'uk.12'   ? 100 : rapat_percentage
                                const skp = role_id
                                // console.log('role_idasdasdasdasdasdasdasdasdasdasdasdasdads', role_id)

                                const options = {
                                    series: [hasil, hakimtoPPKVal, hakimRapatVal, 0],
                                    options: {
                                        chart: {
                                        height: 400,
                                        type: 'radialBar',
                                        },
                                        plotOptions: {
                                        radialBar: {
                                            offsetY: 0,
                                            startAngle: 0,
                                            endAngle: 270,
                                            hollow: {
                                            margin: 0,
                                            size: '35%',
                                            background: 'transparent',
                                            image: undefined,
                                            },
                                            dataLabels: {
                                                name: {
                                                    show: true,
                                                },
                                                value: {
                                                    show: true,
                                                }
                                            }
                                        }
                                    },
                                        colors: ['#1ab7ea', '#5AA897', '#F14668', '#3f51b5'],
                                        labels: ['SIKD', hakimtoPPK, 'Rapat', 'E-Kinerja'],
                                        legend: {
                                            show: true,
                                            floating: true,
                                            fontSize: '12px',
                                            position: 'left',
                                            offsetX: 28,
                                            offsetY: -5,
                                            labels: {
                                                useSeriesColors: true,
                                            },
                                            markers: {
                                                size: 0
                                            },
                                            formatter: function(seriesName, opts) {
                                                return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex] + "%"
                                            },
                                            itemMargin: {
                                                vertical: 0,
                                                position: 'left'
                                            }
                                        },
                                        responsive: [{
                                            breakpoint: 480,
                                            options: {
                                                legend: {
                                                    show: false
                                                }
                                            }
                                        }]
                                    },
                                };


                                setApexChart(options)


                                
                            })
                    })
                    .then((res3) =>{
                        // console.log(res3)
                    })
			} catch(err){
				console.log(err)
			}
		}
		fetchData();
	}, [])
    // console.log('state user', unitKerja)
    return (
        <Grid item 
        lg={6}
        sm={6}
        xl={6}
        xs={12}>
            <Paper className={classes.contentWaktuRespon}>
                <Grid item md={12}>
                    <Typography variant="h5" gutterBottom>Presentase Beban Kerja</Typography>
                    <Divider/>
                </Grid>
                <Grid item  
                  lg={12}
                  sm={12}
                  xl={12}
                  xs={12}
                 style={{height: 380, paddingTop: '2em', paddingBottom: '2em', paddingLeft:'3em',paddingRight:'3em',width: 'auto', margin: 'auto'}}>
                    <div style={{padding: 'auto auto', height: 380}}>
                    {/* <CircularProgressbar
                        value={hasil}
                        text={hasilText}
                        circleRatio={0.75}
                        styles={buildStyles({
                          rotation: 1 / 2 + 1 / 8,
                          strokeLinecap: "butt",
                          trailColor: "#eee",
                          height: '350px !important'
                          
                        })}
                        className={classes.CircularProgressbar}
                      /> */}

                   <div className="radial-chart">
                    {apexChart 
                        ?   <Chart
                                options={apexChart.options}
                                series={apexChart.series}
                                type="radialBar"
                                width="400"
                            />
                        : <Skeleton animation="wave" variant="circle" width={290} height={290} style={{ margin: 'auto', backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                    
                    }
                   {/* <Chart
                        options={apexChart.options}
                        series={apexChart.series}
                        type="radialBar"
                        width="420"
                    /> */}
                   </div>


                   
                      </div>
                </Grid>
                
                <Grid item md={12}>
                  <Typography 
                    className={classes.contentWaktuResponError} 
                    variant="subtitle2"
                  >
                    
                    {
                      role == null 
                      ? ''
                      :role === 'uk.1' || role === 'uk.2' || role === 'uk.3' || role === 'uk.5' || role === 'uk.6' || role === 'uk.7' || role === 'uk.8' || role === 'uk.9' || role === 'uk.12'   
                      ? 'Keterangan : PPK (Penanganan Perkara Konstitusi)'
                      : '(*) Disclaimer, sedang dalam tahap pengembangan untuk menuju kearah talent pool, informasi ini mungkin belum 100% akurat & lengkap '
                    }
                    
                  </Typography>
                 
                </Grid>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={1}
                >

                    {/* <Grid item md={12}>
                        <ZingChart data={myConfig} height={400}/>         
                    </Grid> */}
                    {/* <Grid item md={6} align="left" style={{paddingTop: '.8em'}}>
                        <ZingChart data={myConfig}/>                        
                        <div>
                            <Typography 
                                className={classes.contentWaktuResponSuccess}
                                variant="h5"
                            >
                                {respon.respon.waktu_response}
                            </Typography>
                        </div>
                        <div className={classes.contentWaktuResponPercentageSuccess}>
                            <ArrowDropUpIcon style={{fontSize: '3rem'}} />
                            <span>200%</span>
                        </div>  
                        <Typography 
                            className={classes.contentWaktuResponSuccess} 
                            variant="subtitle2"
                        >
                            Lebih cepat dari rata-rata waktu response seluruh pegawai MK 
                        </Typography>
                    </Grid>
                    <Grid item md={6} align="left">
                        <Typography variant="h6">Keterangan: </Typography>
                        <Typography variant="caption" 
                            style={{fontStyle: 'italic'}}>Rata - rata waktu respon seluruh pegawai </Typography>
                        <Typography variant="h6">{respon.average_all.average_response}</Typography>
                        <Button className={classes.contentWaktuResponButton} variant="contained">Show Details</Button>
                    </Grid> */}
                </Grid>
                </Paper>
            
            </Grid>
    )
}

ContentBebanRespon.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  wt:PropTypes.number,
  minValue:PropTypes.number,
  maxValue:PropTypes.number,
  value:PropTypes.number,
  
}

export default ContentBebanRespon;