import React, { useEffect, useState } from 'react';
import {useLocation} from "react-router-dom";
import { Grid, Typography, Paper, Divider } from '@material-ui/core';
import { makeStyles} from '@material-ui/core/styles';
import { SettingsOverscanOutlined } from '@material-ui/icons';
import ReactApexChart from 'react-apexcharts';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
    contentWaktuRespon: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '600px',
		boxShadow: theme.shadow.large,
        // background: 'rgb(38, 17, 142)'
		// borderRadius: 15
	},
}))

export default function ContentRadialBar() {
    const classes = useStyles()
    const [series, setSeries]   = useState(null)
    const [options, setOptions] = useState(null)
    const [title, setTitle]     = useState('')
    const [value, setValue]     = useState(null)
    const [nbvText, setNbvText] = useState('')
    const [kuadranText, setKuadranText] = useState('')
    const [boxText, setboxText] = useState('')
    const [nbvValue, setNbvValue] = useState(0)
    const [putusanValue, setPutusanValue] = useState(0)
    const [responsetimeValue, setResponsetimeValue] = useState(0)
    const [tingkatTanggapSikd, setTingkatTanggapSikd] = useState(0)

    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1_pre 	= new URLSearchParams(search).get('tgl1');
    const tgl2_pre 	= new URLSearchParams(search).get('tgl2');
	const tgl1		= moment(tgl1_pre).format('YYYY-MM-DD')
	const tgl2		= moment(tgl2_pre).format('YYYY-MM-DD')
    const tahun     = moment(tgl1_pre).format('YYYY');

    const fetchData = async() => {
        try {
            const apiDev = `http://localhost:3001/sikd/api/v1/ninebox/user/profile/${select_id}/${tahun}`;
            const apiProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/user/profile/${select_id}/${tahun}`
            
            const api = process.env.NODE_ENV === 'development' ? apiDev : apiProd;
            const fetching = await fetch(api)
            const fetched   = await fetching.json()

            const nbv_value         = fetched.value.nbv
            const kuadran_value     = fetched.value.kuadran
            const box_value         = fetched.value.box
        
            const nbv_text          = fetched.ninebox.nbv;
            const kuadran_text      = fetched.ninebox.kuadran2;
            const box_text          = fetched.ninebox.box2;

            // Presentase Putusan
            const apiDevPutusan     = `http://localhost:3001/sikd/api/v1/ninebox/e-minutasi/v1/${tahun}`;
            const apiProdPutusan    = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/e-minutasi/v1/${tahun}`
            const apiPutusan        = process.env.NODE_ENV === 'development' ? apiDevPutusan : apiProdPutusan;
            const fetchingPutusan   = await fetch(apiPutusan)
            const fetchedPutusan    = await fetchingPutusan.json()
            const nilai_putusan     = fetchedPutusan.presentasePutusan > 100 ? 100 : fetchedPutusan.presentasePutusan
            setPutusanValue(nilai_putusan)

            // Responsetime 
            // const apiDevResponsetime     = `http://localhost:3001/sikd/api/v1/profil-pemakaian/waktu-response/response-pegawai/${select_id}/${tgl1}/${tgl2}`;
            // const apiProdResponsetime    = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/profil-pemakaian/waktu-response/response-pegawai/${select_id}/${tgl1}/${tgl2}`
            // const apiResponsetime        = process.env.NODE_ENV === 'development' ? apiDevResponsetime : apiProdResponsetime;
            // const fetchingResponsetime   = await fetch(apiResponsetime)
            // const fetchedResponsetime    = await fetchingResponsetime.json()
            // const nilai_responsetime     = fetchedResponsetime.percentage_responsetime;
            // console.log('nilai_responsetime',nilai_responsetime)
            // setResponsetimeValue(nilai_responsetime)
            
            // Tingkat Tanggap SIKD
            const apiDevTingkatTanggapSikd      = `http://localhost:3001/sikd/api/v1/profil-pemakaian/tingkat-tanggap-sikd/totalsuratmasuk/${select_id}/${tgl1}/${tgl2}`;
            const apiProdTingkatTanggapSikd     = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/profil-pemakaian/tingkat-tanggap-sikd/totalsuratmasuk/${select_id}/${tgl1}/${tgl2}`
            const apiTingkatTanggapSikd         = process.env.NODE_ENV === 'development' ? apiDevTingkatTanggapSikd : apiProdTingkatTanggapSikd;
            const fetchingTingkatTanggapSikd    = await fetch(apiTingkatTanggapSikd)
            const fetchedTingkatTanggapSikd     = await fetchingTingkatTanggapSikd.json()
            const nilai_tingkattanggapsikd      = fetchedTingkatTanggapSikd.percentage
            setTingkatTanggapSikd(nilai_tingkattanggapsikd)

            const data = {
                // [nbv, box, kuadran]
                series: [nilai_tingkattanggapsikd, nilai_putusan],
                options: {
                    
                    states:{
                        hover: {
                            filter: {
                                type: 'lighten',
                                value: 0.1,
                            }
                        },
                    },
                    stroke: {
                        curve: "smooth",
                        lineCap: "round"
                    },
                    chart: {
                        height: 50,
                        type: 'radialBar',
                        events: {
                            click: function(event, chartContext, config) {
                               console.log(';clidker') // The last parameter config contains additional information like `seriesIndex` and `dataPointIndex` for cartesian charts
                            },
                            dataPointMouseEnter: function(event, chartContext, config) {
                                console.log(';dataPointMouseEnter', config.dataPointIndex) // ...
                                console.log(';dataPointMouseEnter', event) // ...
                                const kode = config.dataPointIndex;
                                if(kode == 0) {
                                    setTitle('Tingkat Tanggap SIKD')
                                    setValue(kode)
                                } else if (kode == 1) {
                                    setTitle('Putusan')
                                    setValue(kode)
                                } else {
                                    setTitle('')
                                    setValue(null)
                                }
                            },
                            dataPointSelection: function(event, chartContext, config) {
                                console.log(event)
                            },
                            dataPointMouseLeave: function(event, chartContext, config) {
                                setTitle('')  // ...
                                setValue(null)
                            }
                        },
                    },
                    plotOptions: {
                        radialBar: {
                            hollow: {
                                margin: 10,
                                size: "50%"
                            },
                            dataLabels: {
                                show: false,
                                name: {
                                    fontSize: '16px',
                                },
                                value: {
                                    fontSize: '12px',
                                    color: 'white',
                                    marginTop: '-5em'
                                },
                                total: {
                                    show: false,
                                    label: 'Total',
                                    formatter: function (w) {
                                        // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                                        return 249
                                    }
                                }
                            },
                            tooltip: {
                                enabled: false,
                                enabledOnSeries: undefined
                            }
                        }
                    },
                    fill: {
                        type: 'solid',
                        colors: ['#F14F87', '#F2DF3A', '#40CAE5']
                    },
                  labels: ['Responsetime', 'Putusan'],
                }
            }
    
    
            setSeries(data.series)
            setOptions(data.options)
            setNbvText(nbv_text)
            setKuadranText(kuadran_text)
            setboxText(box_text)
            setNbvValue(nbv_value)
    
        } catch(error){
            console.log('error contentRadialBar', error)
        }

    }

    useEffect(() => {
        fetchData()
    },[])

    return (
        <>
        <Grid 
          item 
          lg={6}
          sm={6}
          xl={6}
          xs={6}
          style={{marginLeft: '.5em', marginRight: '-.5em'}}
        >
            <Paper style={{borderRadius: '50px', width: '550px',}} >
                <Grid item md={12}>
                    {/* <Typography variant="h5" gutterBottom>Waktu Response</Typography> */}
                    
                    <Divider/>

                    

                    <div style={{height: '600px', width: '550px', marginTop: '0em'}}>
                        <Paper className={classes.radialBar} style={{height: '100%', background: '#5D95FF', borderRadius: '50px'}}> 
                            <Grid container>

                            <Grid item xs={6} xl={6} style={{textAlign: 'left', paddingLeft: '4em', paddingTop: '2em', paddingBottom: '1em' }}>
                                <div style={{marginRight: '-2em'}}>
                                    <Typography variant='h5' style={{fontWeight: 'bold'}}>Tingkat Tanggap SIKD (%)</Typography>    
                                    {/* <Typography variant='h2' style={{color: '#E21333'}}>{nbvValue} of 100</Typography>     */}
                                    <Typography variant='h2' style={{color: '#F14F87' , fontWeight: 'bold'}}>{tingkatTanggapSikd} of 100</Typography>    
                                </div>                 
                                <div style={{paddingTop: '.4em'}}>
                                    <Typography variant='h5' style={{fontWeight: 'bold'}}>Putusan (%)</Typography>    
                                    {/* <Typography variant='h2' style={{color: '#69E300'}}>{boxText} of 9</Typography>     */}
                                    <Typography variant='h2' style={{color: '#F2DF3A' , fontWeight: 'bold'}}> {putusanValue} of 100</Typography>    
                                </div>        
                                        
                            </Grid>

                            <div 
                                style={{
                                    marginLeft: value == 0 ? '10.5em' : 
                                                value == 1 ? '12.9em' :      
                                                value == 2 ? '13em' : 
                                                '22.2em',   
                                    position: 'absolute', 
                                    marginTop: value == 0 ? '16.5em' : 
                                                value == 1 ? '16.5em' :      
                                                value == 2 ? '13em' : 
                                                '22.2em',   
                                    fontSize: '18px', 
                                    fontWeight: 'bold'
                                }}
                            > 
                                <div >
                                    {title}
                                </div>
                                <div style={{ paddingTop: '.1em', fontSize: '38px', fontWeight: 'bold' }}>
                                {
                                    value == null ? '' : 
                                    value == 0 ? tingkatTanggapSikd : 
                                    value == 1 ? putusanValue  : ''
                                }
                                </div>
                            </div> 
                            {/* </Grid> */}
                            <Grid item xs={6} xl={6} >
                            {
                                    series == null || options == null 
                                    ? ''
                                    :  <div id="chart">
                                            <ReactApexChart 
                                                options={options} 
                                                series={series} 
                                                type="radialBar"
                                                height={550} 
                                                style={{marginBottom: '-2em', marginTop: '6.3em', marginLeft: '-16em'}}
                                                
                                            />
                                        </div>
                                }
                            </Grid>
                            </Grid>
                        </Paper>
                    </div>
                    
                </Grid>
            </Paper>

        

        </Grid>
        </>
    )
}