import React, { useEffect, useState } from 'react';
import {useLocation} from "react-router-dom";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { Avatar, Divider, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import axios from 'axios';
import moment from 'moment';



function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  textColor: {
    color: theme.palette.text.secondary,
  },
  contentNineBox: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    height: 'auto',
    boxShadow: theme.shadow.large,
    width: '100%'
    // borderRadius: 15
    },
    active : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EE9524',color: '#fff',
        // height: '120px', borderRadius: '50px', backgroundColor: 'rgb(241, 225, 91)',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    activeAdmin : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EDDB2D', 
        // height: '120px', borderRadius: '50px', backgroundColor: '#CCF6C8',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    notActive : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px',  borderRadius: '50px', backgroundColor: '#e6e6e6', 
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    btn: {
        marginTop: '1.5em', borderRadius: 25 
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(8, 5, 8),
        borderRadius: 25
    
    },
    rootList: {
        marginTop: '2em',
        width: '100%',
        maxWidth: 600,
        backgroundColor: theme.palette.background.paper,
        overflow: 'auto',
        maxHeight: '80%'
    },
    card: {
        maxWidth: 600,
        margin: theme.spacing(2),
    },
    media: {
        height: 190,
    },
    fade_rule: {
        height: '5px',
        backgroundColor: '#E6E6E6',
        width: '120px',
        marginLeft:'auto', marginRight: 'auto',
        marginTop:'1em', marginBottom: '1em',
        backgroundImage: 'linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-o-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-moz-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-ms-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-gradient( linear, left bottom, right bottom, color-stop(0.02, white), color-stop(0.5, gray), color-stop(0.98, white) )'
    },
    table: {
        color: 'black'
    }
}));

export default function Recharging() {
  const classes = useStyles();
  let search  = useLocation().search;
  const select_id = new URLSearchParams(search).get('select_id');
  const tgl1_pre 	= new URLSearchParams(search).get('tgl1');
  const tgl2_pre 	= new URLSearchParams(search).get('tgl2');
  const tgl1		= moment(tgl1_pre).format('YYYY-MM-DD')
  const tgl2		= moment(tgl2_pre).format('YYYY-MM-DD')
  const tahun     = moment(tgl1).format('YYYY')

  const [value, setValue] = React.useState(0);
  const [struktural, setStruktural] = useState(null)
  const [fungsional, setFungsional] = useState(null)
  const [pelaksana, setPelaksana] = useState(null)
  const [ppnpn, setPpnpn] = useState(null)

  const [recharging, setRecharging] = useState([])

  const getData = async() => {
    try{
        const apiDev    = `http://localhost:3001/sikd/api/v1/ninebox/recharging/get-recharging/${tahun}`
        const apiProd   = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/recharging/get-recharging/${tahun}`
        const api       = process.env.NODE_ENV === 'development' ? apiDev : apiProd;
        const gett      = await axios({
            url: api,
            method: 'GET'
        })
        console.log('rechardging',gett.data)
        setRecharging(gett.data)
        
    } catch(e) {

    }
  }
  useEffect(() => {
    getData()
  }, [])  

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  // console.log(value)
  return (
    // <div className={classes.root}>
        <Grid item xs={12}>
            <Paper className={classes.contentNineBox}>
                <Grid item xs={12}>
                    {/* <Typography variant="h5" gutterBottom>Peserta Recharging Program Tahun {tahun}</Typography> */}
                    <Divider/>
                </Grid>

                <Grid item xs={12}>
                    <AppBar position="static" style={{ borderTopLeftRadius: '25px', borderTopRightRadius: '25px'}}>
                        <Tabs 
                          value={value} 
                          onChange={handleChange} 
                          aria-label="Pegawai Teladan" 
                          variant="fullWidth">
                        <Tab label={`Peserta Recharging Program Tahun ${tahun}`} {...a11yProps(0)} />
                        {/* <Tab label="Fungsional" {...a11yProps(1)} />
                        <Tab label="Pelaksana" {...a11yProps(2)} />
                        <Tab label="PPNPN" {...a11yProps(3)} /> */}
                        </Tabs> 
                    </AppBar>
                    <TabPanel value={value} index={0}>
                        <BasicTable data={recharging} />
                    </TabPanel>
                    {/* <TabPanel value={value} index={1}>
                        <BasicTable data={fungsional} />
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                        <BasicTable data={pelaksana} />
                    </TabPanel>
                    <TabPanel value={value} index={3}>
                        <BasicTable data={ppnpn} />
                    </TabPanel> */}
                </Grid>
            </Paper>
        </Grid>
   
    // </div>
  );
}





function BasicTable({ data }) {
    const classes = useStyles();
  
    return (
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell><Typography variant='h5' className={classes.textColor}>No</Typography></TableCell>
              <TableCell><Typography variant='h5' className={classes.textColor}>Nama</Typography></TableCell>
              {/* <TableCell><Typography variant='h5' className={classes.textColor}>Jabatan </Typography></TableCell> */}
              {/* <TableCell><Typography variant='h5' className={classes.textColor}>Box</Typography></TableCell>
              <TableCell><Typography variant='h5' className={classes.textColor}>NBV</Typography></TableCell> */}
            </TableRow>
          </TableHead>
          <TableBody>
            {data&&data.map((row, i) => (
              <TableRow key={i}>
                <TableCell component="th" scope="row">
                  <Typography variant='subtitle2' className={classes.textColor}>{i+1}</Typography>
                </TableCell>
                <TableCell align="left" style={{ alignItems: 'flex-start' , display: 'flex' }}>
                    <div>
                        <Avatar alt={row.nama} src={row.foto} />
                    </div>
                    <div style={{marginLeft: '1em'}}>
                        <div>
                            <Typography variant='subtitle2' color='primary'>{row.nama}</Typography>
                        </div>
                        <div>
                            <Typography variant='subtitle1' className={classes.textColor}>{row.jabatan}</Typography>
                        </div>
                    </div>
                    
                </TableCell>
                {/* <TableCell align="left">
                    <Typography variant='caption' className={classes.textColor}>{row.kuadran}</Typography>
                </TableCell>
                <TableCell align="left">
                    <Typography variant='caption' className={classes.textColor}>{row.box}</Typography>
                </TableCell>
                <TableCell align="left">
                    <Typography variant='caption' className={classes.textColor}>{row.nbv}</Typography>
                </TableCell> */}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }