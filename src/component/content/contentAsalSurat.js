import React,{useEffect,useState} from 'react';
import {useLocation} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Skeleton from '@material-ui/lab/Skeleton';
import Divider from '@material-ui/core/Divider';
import { ResponsivePie } from '@nivo/pie';

const useStyles = makeStyles((theme) => ({
	contentAsalSurat: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '350px',
		boxShadow: theme.shadow.large
		// borderRadius: 15
	},
	contentAsalSuratGrid: {
		paddingTop: theme.spacing(5),
	},


}));


const ContentAsalSurat = () => {
    const classes = useStyles();
    const [sumber, setSumber] = useState({external:[], internal:[]})
    const [grafik, setGrafik] = useState(null)
    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1 = new URLSearchParams(search).get('tgl1');
    const tgl2 = new URLSearchParams(search).get('tgl2');

	useEffect(() => {
		const fetchAsalSurat = async () => {
			try{
				await Promise.all([
                    fetch(`${process.env.REACT_APP_API_ASAL_EXTERNAL}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_ASAL_INTERNAL}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                ])
                .then((responses) => {
                    return Promise.all(responses.map(function (response) {
                        return response.json();
                    }));
                })
                .then((data) => {
                    // debug API TTDE
                    // 1. Initial Asal Sumber
                    const sumber_external = Number(data[0].data[0].sumber_external);
                    const sumber_internal = Number(data[1].data[0].sumber_internal);
                    // console.log(sumber_external)
                    // console.log(sumber_internal)

                    let objGrafikTotal = [
                        {
                            "id"    : "external",
                            "label" : "External",
                            "value" : sumber_external,
                            "color" : "hsl(210, 70%, 50%)"
                        },
                        {
                            "id"    : "internal",
                            "label" : "Internal",
                            "value" : sumber_internal,
                            "color" : "hsl(160, 70%, 50%)"
                        }
    
                    ]
                    // 2. setState 
                    setSumber({external: sumber_external, internal: sumber_internal})
                    setGrafik(objGrafikTotal)
                    console.log('objGrafikTotal', objGrafikTotal)
                })
			} catch(err){
				console.log(err)
			}
		}
		fetchAsalSurat();
	}, [])
	console.log('Sumber', sumber)
    return (
        <Grid item xs={6}>
            <Paper className={classes.contentAsalSurat}>
                <Typography variant="h5" gutterBottom>Sumber Surat</Typography>
                <Divider/>
                <div style={{height: 300}}>
                    {grafik
                        ? <ResponsivePie
                            data={grafik&&grafik}
                            margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                            innerRadius={0.5}
                            padAngle={2}
                            cornerRadius={9}
                            activeInnerRadiusOffset={17}
                            activeOuterRadiusOffset={8}
                            borderWidth={1}
                            borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.2 ] ] }}
                            arcLinkLabelsSkipAngle={7}
                            arcLinkLabelsTextColor="#333333"
                            arcLinkLabelsThickness={2}
                            arcLinkLabelsColor={{ from: 'color' }}
                            arcLabelsSkipAngle={10}
                            arcLabelsTextColor={{ from: 'color', modifiers: [ [ 'darker', 2 ] ] }}
                            colors={{scheme: 'paired'}}
                            defs={[
                                {
                                    // id: 'dots',
                                    // type: 'patternDots',
                                    background: 'inherit',
                                    color: '#3f51b5',
                                    size: 4,
                                    padding: 1,
                                    stagger: true
                                },
                                {
                                    id: 'lines',
                                    type: 'patternLines',
                                    background: 'inherit',
                                    color: 'rgba(255, 255, 255, 0.3)',
                                    rotation: -45,
                                    lineWidth: 6,
                                    spacing: 10
                                }
                            ]}
                            legends={[
                                {
                                    anchor: 'bottom',
                                    direction: 'row',
                                    justify: false,
                                    translateX: 67,
                                    translateY: 56,
                                    itemsSpacing: 0,
                                    itemWidth: 163,
                                    itemHeight: 18,
                                    itemTextColor: '#999',
                                    itemDirection: 'left-to-right',
                                    itemOpacity: 1,
                                    symbolSize: 18,
                                    symbolShape: 'circle',
                                    effects: [
                                        {
                                            on: 'hover',
                                            style: {
                                                itemTextColor: '#000'
                                            }
                                        }
                                    ]
                                }
                            ]}
                        />
                    : ''
                }
                </div>
                {/* <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={1}
                    className={classes.contentAsalSuratGrid}
                >
                   
                     
                       

                    </Grid> */}
                    {/* <Grid item md={12} >
                        <Typography variant="h5" gutterBottom>External</Typography>
                        <Typography variant="h2">
                            {sumber.external == undefined 
                                ? <Skeleton animation="wave" height={55}  variant="text" style={{ width: '100px', margin: 'auto',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                                : sumber.external
                            }
                            
                        </Typography>
                    </Grid>
                    <Divider orientation="vertical"  />
                    <Grid item md={12} style={{paddingTop: '1.5em'}} >
                        <Typography variant="h5" gutterBottom>Internal</Typography>
                        <Typography variant="h2">{sumber.internal}</Typography>
                    </Grid> */}
                {/* </Grid> */}
            </Paper>
        </Grid>
    )
}

export default ContentAsalSurat;