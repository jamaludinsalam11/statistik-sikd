import React, { useEffect, useState} from "react"
import {useLocation} from "react-router-dom";
import {
  Avatar,
  Button,
  Box,
  Card,
  CardContent,
  CardHeader ,
  CardMedia,
  Grid,
  Paper,
  Typography,
  Divider,
  Modal,
  Backdrop,
  Fade,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Skeleton from '@material-ui/lab/Skeleton';
import {Chart,  Scatter } from 'react-chartjs-2';
import annotationPlugin from "chartjs-plugin-annotation";
import {
    Chart as ChartJS,
  defaults,
    LinearScale,
    PointElement,
    LineElement,
    Tooltip,
    Legend,
  } from 'chart.js';


Chart.register([annotationPlugin,
    // datalabelsPlugin,
    LinearScale, PointElement, LineElement, Tooltip, Legend
])
const useStyles = makeStyles((theme) => ({
	contentNineBox: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: 'auto',
		boxShadow: theme.shadow.large,
        width: '100%'
		// borderRadius: 15
	},
    active : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EE9524',color: '#fff',
        // height: '120px', borderRadius: '50px', backgroundColor: 'rgb(241, 225, 91)',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    activeAdmin : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EDDB2D', 
        // height: '120px', borderRadius: '50px', backgroundColor: '#CCF6C8',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    notActive : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px',  borderRadius: '50px', backgroundColor: '#e6e6e6', 
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    btn: {
        marginTop: '1.5em', borderRadius: 25 
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(5, 5, 3),
        borderRadius: 25,
        width: '85vw'
    },
    rootList: {
        // marginTop: '2em',
        // width: '100%',
        // maxWidth: 600,
        backgroundColor: theme.palette.background.paper,
        overflow: 'auto',
        height: '400px',
        width: '500px'
        // maxHeight: '80%'
    },
    card: {
        maxWidth: 600,
        margin: theme.spacing(2),
    },
    media: {
        height: 190,
    },
    fade_rule: {
        height: '5px',
        backgroundColor: '#E6E6E6',
        width: '120px',
        marginLeft:'auto', marginRight: 'auto',
        marginTop:'1em', marginBottom: '1em',
        backgroundImage: 'linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-o-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-moz-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-ms-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-gradient( linear, left bottom, right bottom, color-stop(0.02, white), color-stop(0.5, gray), color-stop(0.98, white) )'
}

}));

const SpesificKepaniteraanCutOff = ({ dataModalAllNbvUnitkerja, dataScatterPotSeparate, modalAllNbvUnitkerja, handleCloseAllNbvUnitkerja , tahun_now, select_id1, spesific, spesificQuery}) => {
    const classes = useStyles()
    let search  = useLocation().search
    const roleid = new URLSearchParams(search).get('select_id')
    const tgl1 = new URLSearchParams(search).get('tgl1')
    const tgl2 = new URLSearchParams(search).get('tgl2')
    const tahun = moment(tgl1).format('YYYY')
    const [data, setData] = useState(null)
    const [options, setOptions] = useState(null)
    const [paramsKuadran, setParamsKuadran] = useState(0)
    const [openModalKuadran, setOpenModalKuadran] = useState(false)
    const [dataModalKuadran, setDataModalKuadran] = useState([])
    const [userTarget, setUserTarget] = useState(null)
    const [openDialog, setOpenDialog] = useState(false)
    useEffect(() => {
       const optionss = async() => {
            const apiProd = process.env.REACT_APP_API_APPS_FASTIFY2+'/sikd/api/v1/ninebox/scatterplot/v2/spesific/2022'+'/'+roleid+'/'+spesificQuery
            const apiDev = 'http://localhost:3001/sikd/api/v1/ninebox/scatterplot/v2/spesific/2022'+'/'+roleid+'/'+spesificQuery
            const api = process.env.NODE_ENV === "development" ? apiDev : apiProd

            const fetchScatterPlot2 = await fetch(api)
            const resScatterPlot2 = await fetchScatterPlot2.json()
            const ressScatterPlot2 = resScatterPlot2.spesific;
            // console.log('ressScatterPlot2', ressScatterPlot2)
            setData(ressScatterPlot2)
            const chartjs3Options = {
                maintainAspectRatio: false,
                // responsive: true,
                layout: {
                    padding: {
                        left: 0
                    }
                },
                scales: {
                    y: {
                    min: 0,
                    max: 100,
                    //   beginAtZero: true, 
                    grid: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Kinerja',
                        color: '#911',
                        font: {
                        family: 'Poppins, sans-serif',
                        size: 14,
                        weight: 'bold',
                        lineHeight: 1.2
                        }
                    }
                    },
                    x: {
                    min: 0,
                    max: 100,
                    grid: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Potensi Talenta',
                        color: '#911',
                        font: {
                        family: 'Poppins, sans-serif',
                        size: 14,
                        weight: 'bold',
                        lineHeight: 0.5
                        }
                    }
                    }
                },
                elements: {
                    point: {
                        borderWidth: 3,
                        radius: 6
                    }
                },
                interaction:{
                    mode: 'nearest'
                },
                plugins:{
                    htmlLegend: {
                        // ID of the container to put the legend in
                        containerID: 'legend-container',
                    },
                    legend:{
                        position: 'right',
                        display: false,
                        labels: {
                            usePointStyle: true,
                            
                        },
                    },
                    tooltip:{
                        // position: 'cursor',
                        callbacks: {
                            title: function(tooltipItems, data) {
                                const itterate = tooltipItems.map(function(x){ 
                                    const obj  = [
                                        x.raw.people_name,
                                        x.raw.people_desc
                                    ]
                                    return obj
                                })
                                // console.log('itterate',itterate)
                                const result = [
                                    tooltipItems[0].raw.people_name,
                                    
                                    tooltipItems[0].raw.people_desc,
                                    // "==============================",
                                    // tooltipItems[0].raw.unitkerja_full,
            
                                ]
                            return itterate
                            }, 
                            afterTitle: function(tooltipItems, data) {
                                // const itterate = tooltipItems.map((x,i )=> x.raw.people_name)
                                // console.log(tooltipItems[0])
                                // const result = [
                                //     tooltipItems[0].raw.people_name,
                                //     tooltipItems[0].raw.people_desc,
                                //     ,
            
                                // ]
                            //    return tooltipItems[0].raw.unitkerja_full
                            },
                            label: function(context) {
                                let label = context.dataset.label || '';
                                // console.log(context)
                                const result = [
                                    'Unitkerja: ' + ' ' + context.raw.unitkerja_full,
                                    'Jabatan: ' + ' ' + context.raw.jabatan,
                                    'Kinerja: ' + ' ' + context.raw.kinerja,
                                    'Potensial Talenta: ' + ' ' + context.raw.potensial_talenta,
                                    'Kuadran: ' + ' ' + context.raw.kuadran,
                                    'Box: ' + ' ' + context.raw.box,
                                    'NBV: ' + ' ' + context.raw.nbv,
                                ]
                            
                                return result;
                            }
                        }
                    },
                    annotation: {
                        annotations: {
                            
                            box1: {
                                drawTime: "beforeDatasetsDraw",
                                display: true,
                                type: "box",
                                xMin: 0,
                                xMax: 40,
                                yMin: 0,
                                yMax: 40,
                                backgroundColor: "rgba(133,28,118,0.8)",
                                borderColor: "rgba(133,28,118,0.8)",
                                
                            },
                            kuadran21: {
                                drawTime: "beforeDatasetsDraw",
                                display: true,
                                type: "box",
                                xMin: 40,
                                xMax: 80,
                                yMin: 40,
                                yMax: 80,
                                backgroundColor: "rgb(252,252,153)",
                                borderColor: "rgb(252,252,153)"
                            },
                            kuadran22: {
                                drawTime: "beforeDatasetsDraw",
                                display: true,
                                type: "box",
                                xMin: 0,
                                xMax: 40,
                                yMin: 40,
                                yMax: 80,
                                backgroundColor: "rgb(252,252,153)",
                                borderColor: "rgb(252,252,153)"
                            },
                            kuadran23: {
                                drawTime: "beforeDatasetsDraw",
                                display: true,
                                type: "box",
                                xMin: 40,
                                xMax: 80,
                                yMin: 0,
                                yMax: 40,
                                backgroundColor: "rgb(252,252,153)",
                                borderColor: "rgb(252,252,153)"
                            },
                            kuadran31: {
                                drawTime: "beforeDatasetsDraw",
                                display: true,
                                type: "box",
                                xMin: 80,
                                xMax: 100,
                                yMin: 0,
                                yMax: 100,
                                backgroundColor: "rgb(180,204,78, 1)",
                                borderColor: "rgb(180,204,78, 1)"
                            },
                            kuadran32: {
                                drawTime: "beforeDatasetsDraw",
                                display: true,
                                type: "box",
                                xMin: 0,
                                xMax: 100,
                                yMin: 80,
                                yMax: 100,
                                backgroundColor:"rgb(180,204,78, 1)",
                                borderColor:"rgb(180,204,78, 1)"
                            },
                            kuadran4: {
                                drawTime: "beforeDatasetsDraw",
                                display: true,
                                type: "box",
                                xMin: 80,
                                xMax: 100,
                                yMin: 80,
                                yMax: 100,
                                backgroundColor:"rgb(39,118,71, 1)",
                                borderColor:"rgb(39,118,71, 1)",
                                // click: function(context, event) {
                                //   console.log('im kuadran 4')
                                // },
                                // enter: function(context, event) {
                                //   console.log('im kuadran 4 with mouseover')
                                // },
                            },
                            label1: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 20,
                                yValue: 15,
                                backgroundColor: 'rgb(239,255,253,0)',
                                color: 'rgb(239,255,253)',
                                content: ['Kuadran I'],
                                font: {
                                size: 14,
                                },
                                click: function(context, event) {
                                // setParamsKuadran(1)
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran1
                                setParamsKuadran(1)
                                handleKuadran(1, datass)
                                // setDataModalKuadran(kuadran1 == null || kuadran1.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran1 : kuadran1) 
                                // setOpenModalKuadran(true)
                                }
                            },
                            // label12: {
                            //     type: 'label',
                            //     drawTime: 'beforeDatasetsDraw',
                            //     xValue: 20,
                            //     yValue: 10,
                            //     backgroundColor: 'rgb(239,255,253,0)',
                            //     color: 'rgb(239,255,253)',
                            //     content: [`(Pegawai: ${ressScatterPlot2.jumlah_pegawai_kuadran.kuadran1})`],
                            //     font: {
                            //     size: 12,
                            //     },
                            //     click: function(context, event) {
                            //     const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran1
                            //     setParamsKuadran(1)
                            //     handleKuadran(1, datass)
                            //     }
                            // },
                        
                            label31: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 35,
                                yValue: 92,
                                backgroundColor: 'rgba(245,245,245,0)',
                                content: ['Kuadran III.a'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran3a
                                setParamsKuadran(3)
                                handleKuadran(3, datass)
                                
                                // setDataModalKuadran(kuadran3a == null || kuadran3a.length == 0 ? resScatterPlot2.list_pegawai_kuadran.kuadran3a : kuadran3a) 
                                // setTimeout(function(){
                                //   setOpenModalKuadran(true)
                                // }, 1000)
                                // handleKuadran(3)
                                }
                            },
                            // label32: {
                            //     type: 'label',
                            //     drawTime: 'beforeDatasetsDraw',
                            //     xValue: 35,
                            //     yValue: 85,
                            //     backgroundColor: 'rgba(245,245,245,0)',
                            //     content: [`(Pegawai: ${ressScatterPlot2.jumlah_pegawai_kuadran.kuadran3a})`],
                            //     font: {
                            //     size: 12
                            //     },
                            //     click: function(context, event) {
                            //     const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran3a
                            //     setParamsKuadran(3)
                            //     handleKuadran(3, datass)
                            //     }
                            // },
                            label41: {
                                type: 'label', 
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 90,
                                yValue: 95,
                                backgroundColor: 'rgba(245,245,245,0)',
                                color: 'rgb(239,255,253)',
                                content: ['Kuadran'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran4
                                setParamsKuadran(5)
                                handleKuadran(5, datass)
                                }
                            },
                            label42: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 90,
                                yValue: 90,
                                backgroundColor: 'rgb(239,255,253,0)',
                                color: 'rgb(239,255,253)',
                                content: [' IV'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran4
                                setParamsKuadran(5)
                                handleKuadran(5, datass)
                                }
                            },
                            // label43: {
                            //     type: 'label',
                            //     drawTime: 'beforeDatasetsDraw',
                            //     xValue: 90,
                            //     yValue: 85,
                            //     backgroundColor: 'rgb(239,255,253,0)',
                            //     color: 'rgb(239,255,253)',
                            //     content: [`(Pegawai: ${ressScatterPlot2.jumlah_pegawai_kuadran.kuadran4})`],
                            //     font: {
                            //     size: 12
                            //     },
                            //     click: function(context, event) {
                            //     const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran4
                            //     setParamsKuadran(5)
                            //     handleKuadran(5, datass)
                            //     }
                            // },
                            label321: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 90,
                                yValue: 65,
                                backgroundColor: 'rgba(245,245,245,0)',
                                content: ['K'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran3b
                                setParamsKuadran(4)
                                handleKuadran(4, datass)
                                }
                            },
                            label322: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 90,
                                yValue: 60,
                                backgroundColor: 'rgba(245,245,245,0)',
                                content: ['u'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran3b
                                setParamsKuadran(4)
                                handleKuadran(4, datass)
                                }
                            },
                            label323: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 90,
                                yValue: 55,
                                backgroundColor: 'rgba(245,245,245,0)',
                                content: ['a'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran3b
                                setParamsKuadran(4)
                                handleKuadran(4, datass)
                                }
                            },
                            label324: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 90,
                                yValue: 50,
                                backgroundColor: 'rgba(245,245,245,0)',
                                content: ['d'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran3b
                                setParamsKuadran(4)
                                handleKuadran(4, datass)
                                }
                            },
                            label325: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 90,
                                yValue: 45,
                                backgroundColor: 'rgba(245,245,245,0)',
                                content: ['r'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran3b
                                setParamsKuadran(4)
                                handleKuadran(4, datass)
                                }
                            },
                            label326: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 90,
                                yValue: 40,
                                backgroundColor: 'rgba(245,245,245,0)',
                                content: ['a'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran3b
                                setParamsKuadran(4)
                                handleKuadran(4, datass)
                                }
                            },
                            label327: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 90,
                                yValue: 35,
                                backgroundColor: 'rgba(245,245,245,0)',
                                content: ['n'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran3b
                                setParamsKuadran(4)
                                handleKuadran(4, datass)
                                }
                            },
                            label328: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 90,
                                yValue: 25,
                                backgroundColor: 'rgba(245,245,245,0)',
                                content: ['III.b'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran3b
                                setParamsKuadran(4)
                                handleKuadran(4, datass)
                                }
                            },
                            // label329: {
                            //     type: 'label',
                            //     drawTime: 'beforeDatasetsDraw',
                            //     xValue: 90,
                            //     yValue: 10,
                            //     backgroundColor: 'rgba(245,245,245,0)',
                            //     content: [`(Pegawai: ${ressScatterPlot2.jumlah_pegawai_kuadran.kuadran3b})`],
                            //     font: {
                            //     size: 12
                            //     },
                            //     click: function(context, event) {
                            //     const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran3b
                            //     setParamsKuadran(4)
                            //     handleKuadran(4, datass)
                            //     }
                            // },
                            label2: {
                                type: 'label',
                                drawTime: 'beforeDatasetsDraw',
                                xValue: 60,
                                yValue: 15,
                                backgroundColor: 'rgba(245,245,245,0)',
                                content: ['Kuadran II'],
                                font: {
                                size: 14
                                },
                                click: function(context, event) {
                                const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran2
                                setParamsKuadran(2)
                                handleKuadran(2, datass)
                                }
                            },
                            // label21: {
                            //     type: 'label',
                            //     drawTime: 'beforeDatasetsDraw',
                            //     xValue: 60,
                            //     yValue: 10,
                            //     backgroundColor: 'rgba(245,245,245,0)',
                            //     content: [`(Pegawai: ${ressScatterPlot2.jumlah_pegawai_kuadran.kuadran2} )`],
                            //     font: {
                            //     size: 12
                            //     },
                            //     click: function(context, event) {
                            //     const datass = ressScatterPlot2.list_pegawai_kuadran.kuadran2
                            //     setParamsKuadran(2)
                            //     handleKuadran(2, datass)
                            //     }
                            // },
                        }
                    }
                }
            }
            setOptions(chartjs3Options)
       }
        
       optionss()
    },[dataScatterPotSeparate])


    // console.log('dataScatterPotSeparate.datasets_eselon1', dataModalAllNbvUnitkerja)
    const handleKuadran = async(params, data) => {
        if(params == 0){
          setDataModalKuadran([])
        } else if (params == 1) {
          setDataModalKuadran(data == null ? [] : data)
        } else if (params == 2) {
          setDataModalKuadran(data == null ? [] : data)
        } else if (params == 3) {
          setDataModalKuadran(data == null ? [] : data)
        } else if (params == 4) {
          setDataModalKuadran(data == null ? [] : data)
        } else if (params == 5) {
          setDataModalKuadran(data == null ? [] : data)
        }
  
        setOpenModalKuadran(true)
    }

    const getElementsAtEvent = (event) => {
        
        if(event.length !== 0){
            if(roleid === 'uk.1.1' || roleid === 'uk.1.1.18.26'){
                const userTarget = event[0].element.$context.raw
                setOpenDialog(true)
                setUserTarget(userTarget)
            }
        } else {
        }
    }
    return (
        <Grid 
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
        >
            <Grid item sm={5}>
                <List className={classes.rootList} style={{height: '350px', width: '250px'}}>
                    {
                        dataModalAllNbvUnitkerja.filter(x => 
                            x.unitkerja === 'Kepaniteraan'
                        ).length != 0  ? dataModalAllNbvUnitkerja.filter(x => 
                                x.unitkerja === 'Kepaniteraan'
                            ).map((box) => (
                            <>
                                <ListItem>
                                    {/* <Typography></Typography> */}
                                    <ListItemAvatar>
                                    <Avatar >
                                        {select_id1 === 'uk.1.1' ? box.urutan_global : box.urutan_unitkerja}
                                    </Avatar>
                                    {/* <Avatar >
                                        <AccountCircleIcon />
                                    </Avatar> */}
                                    </ListItemAvatar>
                                    <ListItemText primary={box.people_name} secondary={box.people_desc} />
                                    
                                </ListItem>
                                <div style={{marginLeft: '4.5em', marginTop: '-1em', color: 'rgba(0, 0, 0, 0.64)'}}>
                                    <Typography variant='subtitle2'>Urutan Global: {box.urutan_global}</Typography>
                                </div>
                                <div style={{marginLeft: '4.5em', marginTop: '0em', color: 'rgba(0, 0, 0, 0.64)'}}>
                                    <Typography variant='subtitle2'>Urutan Unitkerja: {box.urutan_unitkerja}</Typography>
                                </div>
                                <div style={{marginLeft: '4.5em', marginTop: '0em', color: 'rgba(0, 0, 0, 0.64)'}}>
                                    <Typography variant='subtitle2'>NBV: {box.nbv} || {box.box} || {box.kuadran}</Typography>
                                </div>
                                
                        </>
                        ))
                        : <div>
                            { [0,1,2,3,4,5].map(() => (
                                <Card className={classes.card}>
                                    <CardHeader
                                        avatar={ <Skeleton animation="wave" variant="circle" width={40} height={40} />}
                                        title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
                                        subheader={<Skeleton animation="wave" height={10} width="40%" />}
                                    />
                                </Card>
                            ))
                            }
                        </div>
                    }
                </List>
            </Grid>

            <Grid item sm={7}>
                <div style={{ minHeight: '400px'}}>
                    {
                        options == null ? 
                            ''
                            // JSON.stringify(options)
                            // JSON.stringify(dataScatterPotSeparate.datasets_eselon1)
                        : <Scatter
                            options={options}
                            data={data}
                            getElementAtEvent={getElementsAtEvent}
                            // style={{maxHeight: '480px', maxWidth: '450px'}}
                        />
                    }
                </div>
            </Grid>
        </Grid>
            

    )
}


export default SpesificKepaniteraanCutOff;