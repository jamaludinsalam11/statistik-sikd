import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Spesific from './Spesific';
import NonEselon from './NonEselon';
import SpesificKepaniteraan from './SpesificKepaniteraan';
import SpesificFungsional from './SpesificFungsional';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    // height: 'auto',
    height: 400, 
    // borderStyle:'solid'
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
}));

export default function VerticalTabs({ dataModalAllNbvUnitkerja,dataScatterPotSeparate, modalAllNbvUnitkerja, handleCloseAllNbvUnitkerja , tahun_now, select_id1, spesific}) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
    // const data_kepaniteraan = dataModalAllNbvUnitkerja.filter(
    //     x => 
    //     x.unitkerja ==='Kepaniteraan'
    // )
    //     console.log('data_kepanitersaan',data_kepaniteraan )
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        <Tab label="Eselon I" {...a11yProps(0)} />
        <Tab label="Eselon II" {...a11yProps(1)} />
        <Tab label="Eselon III" {...a11yProps(2)} />
        <Tab label="Eselon IV" {...a11yProps(3)} />
        <Tab label="Panitera" {...a11yProps(4)} />
        <Tab label="Panitera Muda" {...a11yProps(5)} />
        <Tab label="Panitera Pengganti - TK.I" {...a11yProps(6)} />
        <Tab label="Panitera Pengganti - TK.II" {...a11yProps(7)} />
        <Tab label="Fungsional Tertentu - Asisten Ahli Hakim Konstitusi" {...a11yProps(8)} />
        <Tab label="Fungsional Tertentu - Dokter" {...a11yProps(9)} />
        <Tab label="Fungsional Tertentu - Perawat" {...a11yProps(10)} />
        <Tab label="Fungsional Tertentu - Arsiparis" {...a11yProps(11)} />
        <Tab label="Fungsional Tertentu - Auditor" {...a11yProps(12)} />
        <Tab label="Fungsional Tertentu - Pustakawan" {...a11yProps(13)} />
        <Tab label="Fungsional Tertentu - Pranata Komputer" {...a11yProps(14)} />
        <Tab label="Fungsional Tertentu - Analis Hukum" {...a11yProps(15)} />
        <Tab label="Fungsional Tertentu - Analis SDM Aparatur" {...a11yProps(16)} />
        <Tab label="Fungsional Tertentu - Pengelola PBJ" {...a11yProps(17)} />
        <Tab label="Fungsional Umum" {...a11yProps(18)} />
        <Tab label="PPNPN" {...a11yProps(19)} />
      </Tabs>
      <TabPanel value={value} index={0}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === 'Eselon 1')}
            dataScatterPotSeparate ={dataScatterPotSeparate}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Eselon 1"
            spesificQuery="eselon1"
        />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === 'Eselon 2')}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Eselon 2"
            spesificQuery="eselon2"
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === 'Eselon 3')}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Eselon 3"
            spesificQuery="eselon3"
        />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === 'Eselon 4')}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Eselon 4"
            spesificQuery="eselon4"
        />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Panitera")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Panitera"
            spesificQuery="panitera"
        />
      </TabPanel>
      <TabPanel value={value} index={5}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Panitera Muda")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Panitera Muda"
            spesificQuery="panmud"
        />
      </TabPanel>
      <TabPanel value={value} index={6}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Panitera Pengganti TK.I")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Panitera Pengganti TK.I"
            spesificQuery="pptk1"
        />
      </TabPanel>
      <TabPanel value={value} index={7}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Panitera Pengganti TK.II")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Panitera Pengganti TK.II"
            spesificQuery="pptk2"
        />
      </TabPanel>
      <TabPanel value={value} index={8}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Fungsional Tertentu - Asisten Ahli Hakim Konstitusi")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Fungsional Tertentu - Asisten Ahli Hakim Konstitusi"
            spesificQuery="fungsional_tertentu_asli"
        />
      </TabPanel>
      <TabPanel value={value} index={9}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Fungsional Tertentu - Dokter")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Fungsional Tertentu - Dokter"
            spesificQuery="fungsional_tertentu_asli"
        />
      </TabPanel>
      <TabPanel value={value} index={10}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Fungsional Tertentu - Perawat")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Fungsional Tertentu - Perawat"
            spesificQuery="fungsional_tertentu_perawat"
        />
      </TabPanel>
      <TabPanel value={value} index={11}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Fungsional Tertentu - Arsiparis")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Fungsional Tertentu - Arsiparis"
            spesificQuery="fungsional_tertentu_arsiparis"
        />
      </TabPanel>
      <TabPanel value={value} index={12}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Fungsional Tertentu - Auditor")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Fungsional Tertentu - Auditor"
            spesificQuery="fungsional_tertentu_auditor"
        />
      </TabPanel>
      <TabPanel value={value} index={13}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Fungsional Tertentu - Pustakawan")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Fungsional Tertentu - Pustakawan"
            spesificQuery="fungsional_tertentu_pustakawan"
        />
      </TabPanel>
      <TabPanel value={value} index={14}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Fungsional Tertentu - Pranata Komputer")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Fungsional Tertentu - Pranata Komputer"
            spesificQuery="fungsional_tertentu_pranata_komputer"
        />
      </TabPanel>
      <TabPanel value={value} index={15}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Fungsional Tertentu - Analis Hukum")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Fungsional Tertentu - Analis Hukum"
            spesificQuery="fungsional_tertentu_analis_hukum"
        />
      </TabPanel>
      <TabPanel value={value} index={16}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Fungsional Tertentu - Analis SDM Aparatur")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Fungsional Tertentu - Analis SDM Aparatur"
            spesificQuery="fungsional_tertentu_analis_sdm"
        />
      </TabPanel>
      <TabPanel value={value} index={17}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Fungsional Tertentu - Pengelola PBJ")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Fungsional Tertentu - Pengelola PBJ"
            spesificQuery="fungsional_tertentu_pengelola_pbj"
        />
      </TabPanel>
      <TabPanel value={value} index={18}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "Fungsional Umum")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="Fungsional Umum"
            spesificQuery="fungsional_umum"
        />
      </TabPanel>
      <TabPanel value={value} index={19}>
        <Spesific
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja.filter(x=> x.jabatan === "PPNPN")}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
            spesific="PPNPN"
            spesificQuery="ppnpn"
        />
      </TabPanel>
    </div>
  );
}
