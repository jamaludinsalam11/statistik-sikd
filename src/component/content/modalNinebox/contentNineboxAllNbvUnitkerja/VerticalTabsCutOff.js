import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Spesific from './Spesific';
import NonEselon from './NonEselon';
import SpesificKepaniteraan from './SpesificKepaniteraan';
import SpesificFungsional from './SpesificFungsional';
import axios from 'axios';
import SpesificCutOff from './SpesificCutOff';
import SpesificKepaniteraanCutOff from './SpesificKepaniteraanCutOff';
import SpesificFungsionalCutOff from './SpesificFungsionalCutOff';
import { useLocation } from 'react-router-dom';
import moment from 'moment';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    // height: 'auto',
    height: 400, 
    // borderStyle:'solid'
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
}));

export default function VerticalTabsCutOff({ 
  dataModalAllNbvUnitkerja,
  dataModalAllNbvUnitkerjaCutOff,
  dataScatterPotSeparate, 
  modalAllNbvUnitkerja, 
  handleCloseAllNbvUnitkerja ,
  tahun_now, select_id1, spesific
}) {
  const classes = useStyles();
  let search  = useLocation().search
  const roleid = new URLSearchParams(search).get('select_id')
  const tgl1 = new URLSearchParams(search).get('tgl1')
  const tgl2 = new URLSearchParams(search).get('tgl2')
  const tahun = moment(tgl1).format('YYYY')

  const [value, setValue] = React.useState(0);
  const [dataCutoff, setDataCutoff] = useState([])
  const [status, setStatus ] = useState(0)

  // const getData = async() => {
  //   try{
  //     const apiProd = process.env.REACT_APP_API_APPS_NEST_SERVICE_32_NEST_2+'/scatterplot/v3-cutoff/'+tahun+'/'+roleid
  //     const apiDev = 'http://localhost:3025/scatterplot/v3-cutoff/'+tahun+'/'+roleid
  //     const api = process.env.NODE_ENV === "development" ? apiDev : apiProd

  //     const fetchdata   = await fetch(api)
  //     const res         = await fetchdata.json()
  //     setDataCutoff(res.all_unitkerja)
  //     setStatus(1)
  //   } catch(err) {
  //     setDataCutoff([])
  //     setStatus(2)
  //   }
  // }
  useEffect(() => {
    // getData()
    setStatus(1)
  },[dataModalAllNbvUnitkerjaCutOff])


    // const data_kepaniteraan = dataModalAllNbvUnitkerja.filter(
    //     x => 
    //     x.unitkerja ==='Kepaniteraan'
    // )
    //     console.log('data_kepanitersaan',data_kepaniteraan )
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    const getDataModalAllNbvUnitkerja = () => {
      // const get = await axios.get()
    }
  }, [])
  return (
    <>
      {
        status == 0 
        ? 'Loading'
        : status == 2 
        ? 'Data not found'
        :
        <div className={classes.root}>
          <Tabs
            orientation="vertical"
            variant="scrollable"
            value={value}
            onChange={handleChange}
            aria-label="Vertical tabs example"
            className={classes.tabs}
          >
            <Tab label="Eselon I" {...a11yProps(0)} />
            <Tab label="Eselon II" {...a11yProps(1)} />
            <Tab label="Eselon III" {...a11yProps(2)} />
            <Tab label="Eselon IV" {...a11yProps(3)} />
            <Tab label="Panitera" {...a11yProps(4)} />
            <Tab label="Panitera Muda" {...a11yProps(5)} />
            <Tab label="Panitera Pengganti - TK.I" {...a11yProps(6)} />
            <Tab label="Panitera Pengganti - TK.II" {...a11yProps(7)} />
            <Tab label="Fungsional Tertentu - Asisten Ahli Hakim Konstitusi" {...a11yProps(8)} />
            <Tab label="Fungsional Tertentu - Dokter" {...a11yProps(9)} />
            <Tab label="Fungsional Tertentu - Perawat" {...a11yProps(10)} />
            <Tab label="Fungsional Tertentu - Arsiparis" {...a11yProps(11)} />
            <Tab label="Fungsional Tertentu - Auditor" {...a11yProps(12)} />
            <Tab label="Fungsional Tertentu - Pustakawan" {...a11yProps(13)} />
            <Tab label="Fungsional Tertentu - Pranata Komputer" {...a11yProps(14)} />
            <Tab label="Fungsional Tertentu - Analis Hukum" {...a11yProps(15)} />
            <Tab label="Fungsional Tertentu - Analis SDM Aparatur" {...a11yProps(16)} />
            <Tab label="Fungsional Tertentu - Pengelola PBJ" {...a11yProps(17)} />
            <Tab label="Fungsional Umum" {...a11yProps(18)} />
            <Tab label="PPNPN" {...a11yProps(19)} />
          </Tabs>
          <TabPanel value={value} index={0}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === 'Eselon 1')}
                dataScatterPotSeparate ={dataScatterPotSeparate}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Eselon 1"
                spesificQuery="eselon1"
            />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === 'Eselon 2')}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Eselon 2"
                spesificQuery="eselon2"
            />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === 'Eselon 3')}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Eselon 3"
                spesificQuery="eselon3"
            />
          </TabPanel>
          <TabPanel value={value} index={3}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === 'Eselon 4')}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Eselon 4"
                spesificQuery="eselon4"
            />
          </TabPanel>
          <TabPanel value={value} index={4}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Panitera")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Panitera"
                spesificQuery="panitera"
            />
          </TabPanel>
          <TabPanel value={value} index={5}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Panitera Muda")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Panitera Muda"
                spesificQuery="panmud"
            />
          </TabPanel>
          <TabPanel value={value} index={6}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Panitera Pengganti TK.I")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Panitera Pengganti TK.I"
                spesificQuery="pptk1"
            />
          </TabPanel>
          <TabPanel value={value} index={7}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Panitera Pengganti TK.II")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Panitera Pengganti TK.II"
                spesificQuery="pptk2"
            />
          </TabPanel>
          <TabPanel value={value} index={8}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Fungsional Tertentu - Asisten Ahli Hakim Konstitusi")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Fungsional Tertentu - Asisten Ahli Hakim Konstitusi"
                spesificQuery="fungsional_tertentu_asli"
            />
          </TabPanel>
          <TabPanel value={value} index={9}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Fungsional Tertentu - Dokter")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Fungsional Tertentu - Dokter"
                spesificQuery="fungsional_tertentu_asli"
            />
          </TabPanel>
          <TabPanel value={value} index={10}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Fungsional Tertentu - Perawat")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Fungsional Tertentu - Perawat"
                spesificQuery="fungsional_tertentu_perawat"
            />
          </TabPanel>
          <TabPanel value={value} index={11}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Fungsional Tertentu - Arsiparis")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Fungsional Tertentu - Arsiparis"
                spesificQuery="fungsional_tertentu_arsiparis"
            />
          </TabPanel>
          <TabPanel value={value} index={12}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Fungsional Tertentu - Auditor")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Fungsional Tertentu - Auditor"
                spesificQuery="fungsional_tertentu_auditor"
            />
          </TabPanel>
          <TabPanel value={value} index={13}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Fungsional Tertentu - Pustakawan")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Fungsional Tertentu - Pustakawan"
                spesificQuery="fungsional_tertentu_pustakawan"
            />
          </TabPanel>
          <TabPanel value={value} index={14}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Fungsional Tertentu - Pranata Komputer")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Fungsional Tertentu - Pranata Komputer"
                spesificQuery="fungsional_tertentu_pranata_komputer"
            />
          </TabPanel>
          <TabPanel value={value} index={15}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Fungsional Tertentu - Analis Hukum")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Fungsional Tertentu - Analis Hukum"
                spesificQuery="fungsional_tertentu_analis_hukum"
            />
          </TabPanel>
          <TabPanel value={value} index={16}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Fungsional Tertentu - Analis SDM Aparatur")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Fungsional Tertentu - Analis SDM Aparatur"
                spesificQuery="fungsional_tertentu_analis_sdm"
            />
          </TabPanel>
          <TabPanel value={value} index={17}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Fungsional Tertentu - Pengelola PBJ")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Fungsional Tertentu - Pengelola PBJ"
                spesificQuery="fungsional_tertentu_pengelola_pbj"
            />
          </TabPanel>
          <TabPanel value={value} index={18}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "Fungsional Umum")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="Fungsional Umum"
                spesificQuery="fungsional_umum"
            />
          </TabPanel>
          <TabPanel value={value} index={19}>
            <SpesificCutOff
                dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerjaCutOff.filter(x=> x.jabatan === "PPNPN")}
                modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                tahun_now={tahun_now}
                select_id1={select_id1}
                spesific="PPNPN"
                spesificQuery="ppnpn"
            />
          </TabPanel>
        </div>
      }
    </>
    
  );
}
