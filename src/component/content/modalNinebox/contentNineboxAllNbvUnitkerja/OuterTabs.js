import React, { useEffect, useState} from "react"
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Umum from './Umum';
import InnerTabs from "./InnerTabs";
import VerticalTabs from "./VerticalTabs";
import moment from "moment";
import { useLocation } from "react-router-dom";
import VerticalTabsCutOff from "./VerticalTabsCutOff";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function OuterTabs({ 
  dataModalAllNbvUnitkerja,
  dataModalAllNbvUnitkerjaCutOff,
  dataScatterPotSeparate, 
  modalAllNbvUnitkerja, 
  handleCloseAllNbvUnitkerja , 
  tahun_now, select_id1
}) {
  const classes = useStyles();
  let search  = useLocation().search
  // const select_id1 = new URLSearchParams(search).get('select_id')
  const tgl1 = new URLSearchParams(search).get('tgl1')

  const [value, setValue] = React.useState(0);
  
  const tahun_real_now = moment().format('YYYY')
  const tahun_selected = tahun_now
  const isTahunNow = moment(tahun_real_now).isSame(tahun_selected)
  console.log(`isTahunNows tahun_real_now:${tahun_real_now}, tahun_selected: ${tahun_selected} `, isTahunNow)
  useEffect(() => {
       
    },[])
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example" centered>
          <Tab label="Umum" {...a11yProps(0)} />
          {/* <Tab label="Eselon" {...a11yProps(1)} /> */}
          {
          isTahunNow == true 
            ? <Tab label="Layer Jabatan Cut Off (31 Desember 2022)" {...a11yProps(1)} />
            : ''
          }
          <Tab label="Layer Jabatan" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0} >
        
        <Umum
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
        />
      </TabPanel>
      <TabPanel value={value} index={1}>
        {/* <InnerTabs/> */}
        <VerticalTabsCutOff
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja}
            dataModalAllNbvUnitkerjaCutOff ={dataModalAllNbvUnitkerjaCutOff}
            dataScatterPotSeparate={dataScatterPotSeparate}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={'2022'}
            select_id1={select_id1}
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        {/* <InnerTabs/> */}
        <VerticalTabs
            dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja}
            dataScatterPotSeparate={dataScatterPotSeparate}
            modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
            handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
            tahun_now={tahun_now}
            select_id1={select_id1}
        />
      </TabPanel>
    </div>
  );
}
