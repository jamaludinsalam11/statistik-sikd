import React, { useEffect, useState} from "react"
import {useLocation} from "react-router-dom";
import {
  Avatar,
  Button,
  Box,
  Card,
  CardContent,
  CardHeader ,
  CardMedia,
  Grid,
  Paper,
  Typography,
  Divider,
  Modal,
  Backdrop,
  Fade,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Skeleton from '@material-ui/lab/Skeleton';
import OuterTabs from "./contentNineboxAllNbvUnitkerja/OuterTabs";



const useStyles = makeStyles((theme) => ({
	contentNineBox: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: 'auto',
		boxShadow: theme.shadow.large,
        width: '100%'
		// borderRadius: 15
	},
    active : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EE9524',color: '#fff',
        // height: '120px', borderRadius: '50px', backgroundColor: 'rgb(241, 225, 91)',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    activeAdmin : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EDDB2D', 
        // height: '120px', borderRadius: '50px', backgroundColor: '#CCF6C8',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    notActive : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px',  borderRadius: '50px', backgroundColor: '#e6e6e6', 
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    btn: {
        marginTop: '1.5em', borderRadius: 25 
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(5, 5, 3),
        borderRadius: 25,
        width: '90vw'
    },
    rootList: {
        marginTop: '2em',
        width: '100%',
        maxWidth: 600,
        backgroundColor: theme.palette.background.paper,
        overflow: 'auto',
        maxHeight: '80%'
    },
    card: {
        maxWidth: 600,
        margin: theme.spacing(2),
    },
    media: {
        height: 190,
    },
    fade_rule: {
        height: '5px',
        backgroundColor: '#E6E6E6',
        width: '120px',
        marginLeft:'auto', marginRight: 'auto',
        marginTop:'1em', marginBottom: '1em',
        backgroundImage: 'linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-o-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-moz-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-ms-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-gradient( linear, left bottom, right bottom, color-stop(0.02, white), color-stop(0.5, gray), color-stop(0.98, white) )'
}

}));

const ModalAllNbvUnitkerja = ({ 
    dataModalAllNbvUnitkerja,
    dataModalAllNbvUnitkerjaCutOff, 
    dataScatterPotSeparate, 
    modalAllNbvUnitkerja, 
    handleCloseAllNbvUnitkerja , 
    tahun_now, select_id1
}) => {
    const classes = useStyles()

    useEffect(() => {
       
    },[])

    return (
        <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={modalAllNbvUnitkerja}
        onClose={handleCloseAllNbvUnitkerja}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={modalAllNbvUnitkerja}>
          <div className={classes.paper} style={{height: '95vh', minWidth: 'auto', 
           overflow: "hidden",
            overflowX: "scroll"
        }}>
            <div style={{textAlign: 'center'}}>
                <Typography variant="h2">KRS </Typography>
                <Typography variant="h5">Kelompok Rencana Suksesi</Typography>
                <Typography variant="h5">{dataModalAllNbvUnitkerja.length != 0  ? dataModalAllNbvUnitkerja[0].unitkerja_full : ''}</Typography>
                <Typography variant="subtitle2">Mahkamah Konstitusi Republik Indonesia</Typography>
            </div>

            <div style={{ paddingTop: '2em'}}>
                <OuterTabs
                    dataModalAllNbvUnitkerja ={dataModalAllNbvUnitkerja}
                    dataModalAllNbvUnitkerjaCutOff={dataModalAllNbvUnitkerjaCutOff}
                    dataScatterPotSeparate={dataScatterPotSeparate}
                    modalAllNbvUnitkerja={modalAllNbvUnitkerja} 
                    handleCloseAllNbvUnitkerja={handleCloseAllNbvUnitkerja}
                    tahun_now={tahun_now}
                    select_id1={select_id1}
                />
            </div>

            {/* <div style={{textAlign: 'center', marginTop: '2em'}}>
                {total9 ? <Typography variant="h5">{total9} Pegawai</Typography> : ''}
                {percentage9 ? <Typography variant="caption">{`${percentage9}% dari seluruh pegawai`}</Typography> : ''}
            </div> */}
          </div>
        </Fade>
      </Modal>
    )
}


export default ModalAllNbvUnitkerja;