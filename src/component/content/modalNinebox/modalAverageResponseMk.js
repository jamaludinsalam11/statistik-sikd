import React, { useEffect, useState} from "react"
import {useLocation} from "react-router-dom";
import {
  Avatar,
  Button,
  Box,
  Card,
  CardContent,
  CardHeader ,
  CardMedia,
  Grid,
  Paper,
  Typography,
  Divider,
  Modal,
  Backdrop,
  Fade,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  IconButton
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import CloseIcon from '@material-ui/icons/Close';
import "chartjs-plugin-datalabels";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import ChartDataLabelsPlugin from "chartjs-plugin-datalabels";
import { Chart,Line } from 'react-chartjs-2';
import annotationPlugin from "chartjs-plugin-annotation";
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Skeleton from '@material-ui/lab/Skeleton';
import axios from "axios";



const useStyles = makeStyles((theme) => ({
	contentNineBox: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: 'auto',
		boxShadow: theme.shadow.large,
        width: '1000px'
		// borderRadius: 15
	},
    active : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EE9524',color: '#fff',
        // height: '120px', borderRadius: '50px', backgroundColor: 'rgb(241, 225, 91)',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    activeAdmin : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EDDB2D', 
        // height: '120px', borderRadius: '50px', backgroundColor: '#CCF6C8',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    notActive : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px',  borderRadius: '50px', backgroundColor: '#e6e6e6', 
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    btn: {
        marginTop: '1.5em', borderRadius: 25 
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        // width: '900px'
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(5, 5, 3),
        borderRadius: 25,
        height: '95vh',
        width: '85vw'
    },
    rootList: {
        marginTop: '2em',
        width: '100%',
        maxWidth: 600,
        backgroundColor: theme.palette.background.paper,
        overflow: 'auto',
        maxHeight: '80%'
    },
    card: {
        maxWidth: 600,
        margin: theme.spacing(2),
    },
    media: {
        height: 190,
    },
    fade_rule: {
        height: '5px',
        backgroundColor: '#E6E6E6',
        width: '120px',
        marginLeft:'auto', marginRight: 'auto',
        marginTop:'1em', marginBottom: '1em',
        backgroundImage: 'linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-o-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-moz-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-ms-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-gradient( linear, left bottom, right bottom, color-stop(0.02, white), color-stop(0.5, gray), color-stop(0.98, white) )'
}

}));


const options = {
  responsive: true,
  scales: {
    y: {
      ticks: {
        display: false,
      }
    }
  },
  plugins: {
    ChartDataLabelsPlugin,
    datalabels: {
      display: true,
      color: "black",
      align: "end",
      padding: {
        right: 2
      },
      labels: {
        padding: { top: 10 },
        title: {
          font: {
            weight: "bold"
          }
        },
        value: {
          color: "green"
        }
      },
      formatter: function (value) {
        console.log('formatter: function ', value)
        return "\n" + value;
      }
    },
    legend: {
      display: false,
    },
    title: {
      display: false,
      text: [
        'Rata-rata responsetime pegawai MK',
        '2022',
      ],
    },
    tooltip: {
      callbacks: {
          label: function(context) {
            // console.log(context)
            return context.raw.text;
          }
      }
    }
  },
 
};

const data = {
  datasets: [
    {
      label: 'Dataset 1',
      data: [
        {x: 'Januari', y: 50},
        {x: 'Februari', y: 80},
        {x: 'Maret', y: 20},
        {x: 'April', y: 60, text: '0 Hari 7 Jam 34 Menit'},
        {x: 'Mei', y: 10},
        {x: 'Juni', y: 25},
        {x: 'Juli', y: 45},
        {x: 'Agustus', y: 65},
        {x: 'September', y: 86},
        {x: 'Oktober', y: 12},
        {x: 'November', y: 67},
        {x: 'Desember', y: 78}
      ], 
      borderColor: 'rgb(255, 99, 132)',
      backgroundColor: 'rgba(255, 99, 132, 0.5)',
    },
  ],
};

Chart.register([
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  // ChartDataLabelsPlugin
]);

// Chart.defaults.global.plugins.datalabels.display = false;
// ChartJS.register([
//   CategoryScale,
//   LinearScale,
//   PointElement,
//   LineElement,
//   Title,
//   Tooltip,
//   Legend,
//   // ChartDataLabelsPlugin
// ]);

const ModalAverageResponseMk= ({ dataAverageResponseMk, modalAverageResponseMk, handleCloseAverageResponseMk , tahun_now}) => {
    const classes = useStyles()
    let search  = useLocation().search
    const roleid = new URLSearchParams(search).get('select_id')
    const tgl1 = new URLSearchParams(search).get('tgl1')
    const tgl2 = new URLSearchParams(search).get('tgl2')
    const tahun = moment(tgl1).format('YYYY')
    const tahun_current = moment().format('YYYY')
    const bulan = moment(tahun_current).isSame(tahun) ? moment().format('M') : 12

    const [ dataSets, setDataSets ] = useState([])


    useEffect(async() => {
        const apiDev = `http://localhost:3025/average-response-mk/get-year`;
        const apiProd = `https://apiservice.mkri.id/service-32-nest-2/average-response-mk/get-year`
        const api    = process.env.NODE_ENV === 'development' ? apiDev : apiProd;
        
        const get =  await axios({
            url: api,
            method: "POST",
            data: {
                tahun: tahun,
                bulan: bulan
            }
        })
        setDataSets(get.data.datasetsLine)
        console.log('get.data.datasesstss',get.data.datasetsLine)
    },[])

    

    return (
        <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={modalAverageResponseMk}
        onClose={handleCloseAverageResponseMk}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={modalAverageResponseMk}>
          <div className={classes.paper} >
            <div style={{textAlign: 'right', marginBottom: '-50px'}}>
              <IconButton aria-label="delete" size="large" onClick={handleCloseAverageResponseMk}>
                <CloseIcon />
              </IconButton>
            </div>
            <div style={{textAlign: 'center', marginBottom: '1em'}}>
                <Typography variant="h2">Rata-rata Responsetime  </Typography>
                <Typography variant="subtitle2">Mahkamah Konstitusi Republik Indonesia</Typography>
                <Typography variant="h5">Tahun {tahun}</Typography>
            </div>
            
            <div style={{ height: '50vh'}}>
            {/* <Line 
                options={options}
                data={data}
            /> */}
                {
                    dataSets.length == 0 ?                    
                    ''
                    : 
                    <Line 
                        options={options}
                        data={dataSets}
                    />
                    // ''
                }
               
            </div>
            {/* <div style={{textAlign: 'center', marginTop: '2em'}}>
                {total9 ? <Typography variant="h5">{total9} Pegawai</Typography> : ''}
                {percentage9 ? <Typography variant="caption">{`${percentage9}% dari seluruh pegawai`}</Typography> : ''}
            </div> */}
          </div>
        </Fade>
      </Modal>
    )
}


export default ModalAverageResponseMk;