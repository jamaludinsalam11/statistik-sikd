import React,{useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import moment from 'moment';
import Moment from 'moment'
import { extendMoment } from 'moment-range';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { Bar } from 'react-chartjs-2';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';



const useStyles = makeStyles((theme) => ({
	contentTotalAsalSurat: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: 'auto',
		boxShadow: theme.shadow.large,
		// borderRadius: 15
	},

}));

function scaleValue(value, from, to) {
    var scale = (to[1] - to[0]) / (from[1] - from[0]);
    var capped = Math.min(from[1], Math.max(from[0], value)) - from[0];
    return ~~(capped * scale + to[0]);
  }
function convertToBulan(value){
	const hasilBulan = 
		value == 1 
		? 'Januari' 
		: value == 2 
		? 'Februari' 
		: value == 3
		? 'Maret'
		: value == 4  
		? 'April'
		: value == 5
		? 'Mei'
		: value == 6
		? 'Juni'
		: value == 7
		? 'Juli'
		: value == 8
		? 'Agustus'
		: value == 9
		? 'September'
		: value == 10
		? 'Oktober'
		: value == 11
		? 'November'
		: 'Desember'
	return hasilBulan
}
const Sorting = (data) =>{
  
	// Standar Sort Javascript - stable
	const sort = data.sort((a,b) => a.id_calendar - b.id_calendar)
	return sort
}


const ContentInstrumentNineBox = () => {
    const classes = useStyles();
    const [data, setData] = useState(null)
    const [options, setOptions] = useState(null)
    let search  = useLocation().search
    const select_id1 = new URLSearchParams(search).get('select_id')
    const tgl1 = new URLSearchParams(search).get('tgl1')
    const tgl2 = new URLSearchParams(search).get('tgl2')
    const tgl1_rapat = moment(tgl1).format('YYYY-MM-DD')
    const tgl2_rapat = moment(tgl2).format('YYYY-MM-DD')

    /**
	 * Declare Date for absensi
	 */
	const moment2 			= extendMoment(Moment)
	const bulanMulaiArray 	= moment(tgl1).format('YYYY-MM-DD')
	const bulanAkhirArray 	= moment(tgl2).format('YYYY-MM-DD')
	const range 			= moment2.range(bulanMulaiArray, bulanAkhirArray)
	const r2 				= range.snapTo('YYYY')
	const month_number 		= Array.from(r2.by('months')).map(m => Number(m.format('M')))
	const month_name 		= Array.from(r2.by('months')).map(m => convertToBulan(Number(m.format('MM'))))
	const tahun_now 		= moment().format('YYYY')

    useEffect(() => {
        const fetchAll = async () => {
            try{
                const apiUser   = `${process.env.REACT_APP_API_USER}?select_id=${select_id1}`
                const fetchUser = await fetch(apiUser)
                const resUser   = await fetchUser.json()
                const user      = resUser.data[0]
                const no_nip	= resUser.data[0].no_nip

                /**
                 * Tambahan initilaizz untuk absensi
                 */
                 const apiFetchUserApps = `${process.env.REACT_APP_API_APPS_PEGAWAI}/${no_nip}`
                 const fetchUserApps 	= await fetch(apiFetchUserApps)
                 const resUserApps 	    = await fetchUserApps.json()
                 const pgw_id		    = resUserApps[0].pgw_id

                const role_id       = user.RoleId
                const select_unit   = user.RoleAtasan === 'uk.1.1' ? user.RoleId : user.RoleAtasan
                const select_id     = user.PeopleId
                console.log('pgw_id', pgw_id)
                console.log('select_unit', select_unit)
                console.log('select_id', select_id)
                
                // SIKD, Rapat, Surat, responseTime
                const apiStatistikUnit      = `${process.env.REACT_APP_API_STATISTIK_UNIT}?select_id=${select_unit}&&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiStatistikPerson    = `${process.env.REACT_APP_API_STATISTIK_PERSON}?select_roleid=${role_id}&select_id=${select_unit}&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiRapat              = `${process.env.REACT_APP_API_RAPAT}?select_id=${select_id}&tgl1=${tgl1_rapat}&tgl2=${tgl2_rapat}`
                const apiWaktuResponse      = `${process.env.REACT_APP_API_WAKTURESPONSE}?select_id=${select_id1}&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiAverageResponse    = `${process.env.REACT_APP_API_AVERAGERESPONSE}?select_id=${select_id1}&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiBelumDibaca        = `${process.env.REACT_APP_API_BELUMDIBACA}?select_id=${select_id1}&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiAbsensiApps1Tahun  = `${process.env.REACT_APP_API_APPS_ABSENSIV2}/${pgw_id}/2021`
                // ApiAbsensi
                const apiJan	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[0]}/${tahun_now}`
				const apiFeb	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[1]}/${tahun_now}`
				const apiMar	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[2]}/${tahun_now}`
				const apiApr	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[3]}/${tahun_now}`
				const apiMei	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[4]}/${tahun_now}`
				const apiJun	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[5]}/${tahun_now}`
				const apiJul	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[6]}/${tahun_now}`
				const apiAgt	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[7]}/${tahun_now}`
				const apiSep	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[8]}/${tahun_now}`
				const apiOkt	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[9]}/${tahun_now}`
				const apiNov	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[10]}/${tahun_now}`
				const apiDes	= `${process.env.REACT_APP_API_APPS_ABSENSI}/${pgw_id}/${month_name[11]}/${tahun_now}`
                /**
                 * Fetch
                 */
                const [fetchStatistikUnit, fetchStatistikPerson, fetchRapat, 
                    fetchWaktuResponse, fetchAverageResponse, fetchBelumDibaca,
                    fetchAbsensiApps1Tahun,
                    fetchJanuari,fetchFebruari,fetchMaret,fetchApril,fetchMei,
                    fetchJuni,fetchJuli, fetchAgustus,fetchSeptember,fetchOktober,
					fetchNovember,fetchDesember
                ] = await Promise.all([
                    fetch(apiStatistikUnit),
                    fetch(apiStatistikPerson),
                    fetch(apiRapat),
                    fetch(apiWaktuResponse),
                    fetch(apiAverageResponse),
                    fetch(apiBelumDibaca),
                    fetch(apiAbsensiApps1Tahun),
                    fetch(apiJan),
                    fetch(apiFeb),
                    fetch(apiMar),
                    fetch(apiApr),
                    fetch(apiMei),
                    fetch(apiJun),
                    fetch(apiJul),
                    fetch(apiAgt),
                    fetch(apiSep),
                    fetch(apiOkt),
                    fetch(apiNov),
                    fetch(apiDes),
                ])

                /**
                 * Response json
                 */
                const resStatistikUnit      = await fetchStatistikUnit.json()
                const resStatistikPerson    = await fetchStatistikPerson.json()
                const resRapat              = await fetchRapat.json()
                const resWaktuResponse      = await fetchWaktuResponse.json()
                const resAverageResponse    = await fetchAverageResponse.json()
                const resBelumDibaca        = await fetchBelumDibaca.json()
                const resAbsensiApps1Tahun  = await fetchAbsensiApps1Tahun.json()
                const resJan = await fetchJanuari.json()
				const resFeb = await fetchFebruari.json()
				const resMar = await fetchMaret.json()
				const resApr = await fetchApril.json()
				const resMei = await fetchMei.json()
				const resJun = await fetchJuni.json()
				const resJul = await fetchJuli.json()
				const resAgt = await fetchAgustus.json()
				const resSep = await fetchSeptember.json()
				const resOkt = await fetchOktober.json()
				const resNov = await fetchNovember.json()
				const resDes = await fetchDesember.json()


                console.log('resWaktuResponse', resWaktuResponse)
                console.log('resAverageResponse', resAverageResponse)
                console.log('resBelumDibaca', resBelumDibaca)

                /**
                 * Processing
                 * ----------------
                 * SIKD, Rapat, Response Time, Absensi, SKP
                 */

                //Rapat
                const rapat_percentage = resRapat.data[0].percentage
                const rapat_percentage10 = resRapat.data[0].percentage / 10

                //SIKD
                const total_pesan_person    = resStatistikPerson.data[0].jmlSurat
                const total_pesan_unit      = resStatistikPerson.data[0].RoleId_To === 'uk.1.1' ? resStatistikPerson.data[0].jmlSurat : resStatistikUnit.total_surat
                const rumus     = (total_pesan_person / total_pesan_unit ) * 95 / 100
                const hasil     = Math.ceil(rumus * 10)

                    // Set For Hakim
                    const hakimSKPtoPPK = role_id === 'uk.1' || role_id === 'uk.2' || role_id === 'uk.3' || role_id === 'uk.5' || role_id === 'uk.6' || role_id === 'uk.7' || role_id === 'uk.8' || role_id === 'uk.9' || role_id === 'uk.12'   ? 'PPK' : 'SKP'
                    const valSKPorPPK   = role_id === 'uk.1' || role_id === 'uk.2' || role_id === 'uk.3' || role_id === 'uk.5' || role_id === 'uk.6' || role_id === 'uk.7' || role_id === 'uk.8' || role_id === 'uk.9' || role_id === 'uk.12'   ? 100 : 0
                    const valRapatHakim = role_id === 'uk.1' || role_id === 'uk.2' || role_id === 'uk.3' || role_id === 'uk.5' || role_id === 'uk.6' || role_id === 'uk.7' || role_id === 'uk.8' || role_id === 'uk.9' || role_id === 'uk.12'   ? 100 : rapat_percentage

                // Absensi
                    // 1Tahun
                    const res_absensi 		= resAbsensiApps1Tahun
                    const total_jam_kerja 	= Number(parseFloat(res_absensi.totaljamkerja).toFixed(1))
                    const per_minggu 		= Number(parseFloat(res_absensi.rata2perMinggu).toFixed(1))
                    const per_hari 			= Number(parseFloat(res_absensi.rata2perHari).toFixed(1))

                let combineArrayBulan		= []
				combineArrayBulan.push(
					...resJan, ...resFeb, ...resMar, ...resApr,
					...resMei, ...resJun, ...resJul, ...resAgt,
					...resSep, ...resOkt, ...resNov, ...resDes
				)
				const cobineArrayBulanSorted = Sorting(combineArrayBulan)

                let tgl_today = moment().format('YYYY-MM-DD') 

                let rata2Absen = []
				let jmlWfh = 0
				let jmlWfo = 0
				let sumJam = 0
				let jumlahHari = 0
				let arrJamKerja = []
				cobineArrayBulanSorted.forEach((el) => {
					if(!(el.bool_hari_libur || el.bool_weekend || el.id == null || tgl_today < el.tanggal)){
						rata2Absen.push(el)
						if(el.bool_wfh == true){
							jmlWfh = jmlWfh + 1
							arrJamKerja.push(7.5)
							sumJam = sumJam + 7.5
							
						} else {
							jmlWfo = jmlWfo + 1
							arrJamKerja.push(8)
							sumJam = sumJam + 8
						} 
						jumlahHari = jumlahHari + 1
						
					}
				})
                const ratarataSeharusnya = sumJam / jumlahHari 
				const compareRatarata = Number(parseFloat(per_hari - ratarataSeharusnya).toFixed(1)) 
				const rataPercentage = scaleValue(compareRatarata, [0,ratarataSeharusnya], [0,100])
                const rataPercentage10 = rataPercentage/5
                console.log('rataPercentage', rataPercentage)

                //ResponseTime
                    // 1. Initial Asal Sumber
                    const waktu_response    = resWaktuResponse.data[0].waktu_response;
                    const respon_menit      = resWaktuResponse.data[0].wt;
                    const average_response  = resAverageResponse.data[0].average_response;
                    const average_menit     = resAverageResponse.data[0].wt;
                    // Remove commas
                    const rm_average_wt = Number(resAverageResponse.data[0].wt2);
                    const rm_respon_wt2 = Number(resWaktuResponse.data[0].wt2);

                    const after_average_wt = Math.trunc(rm_average_wt);
                    const after_respon_wt2 = Math.trunc(rm_respon_wt2);
                    const fix_average_wt = Number(after_average_wt);
                    const fix_respon_wt2 = Number(after_respon_wt2);
                    const pMax = fix_average_wt * 3;
                    const pMin = fix_average_wt * (-3);
                    const avg = fix_average_wt;

                    // Choose Deviation
                    const oneday          = 86400;
                    const satujam         = 3600;
                    const duajam          = 7200;
                    const tigajam         = 10800;
                    const empatjam        = 14400;
                    const limajam         = 18000;
                    const enamjam         = 21600;
                    const tujuhjam        = 25200;
                    const delapanjam      = 28800;
                    const sembilanjam     = 32400;
                    const sepuluhjam      = 36000;
                    const sebelasjam      = 39600;
                    const duabelasjam     = 43200;
                    const tigabelasjam    = 46800;
                    const empatbelasjam   = 50400;
                    const limabelasjam    = 54000;
                    const enambelasjam    = 57600;
                    const tujuhbelasjam   = 61200;
                    const delapanbelasjam = 64800;
                    const sembilasjam     = 68400;
                    const duapuluhjam     = 72000;
                    const duapuluhsatujam = 75560;
                    const duapuluhduajam  = 79200;
                    const duapuluhtigajam = 82800;
                    const halfday         = oneday * 1 + 1;
                    const twoday          = oneday * 2 + 1;
                    const thirdday        = oneday * 3 + 1;
                    const fouthday        = oneday * 4 + 1;
                    const fifthday        = oneday * 5 + 1;
                    const sixday          = oneday * 6 + 1;
                    const sevenday        = oneday * 7 + 1;
                    const eightday        = oneday * 8 + 1;
                    const nineday         = oneday * 9 + 1;
                    const tenday          = oneday * 10 + 1;
                    const elevenday       = oneday * 11 + 1;
                    const twelveday       = oneday * 12 + 1;
                    const thirdteenday    = oneday * 13 + 1;
                    const fourteenday     = oneday * 14 + 1;
                    const fiftheenday     = oneday * 15 + 1;
                    const sixteenday      = oneday * 16 + 1;
                    const seventeenday    = oneday * 17 + 1;
                    const eightteenday    = oneday * 18 + 1;
                    const nineteenday     = oneday * 19 + 1;
                    const twentyday       = oneday * 20 + 1;
                    const twentyoneday    = oneday * 21 + 1;
                    const twentytwoday    = oneday * 22 + 1;
                    const twentythreeday  = oneday * 23 + 1;
                    const twentyfourday   = oneday * 24 + 1;
                    const twentyfiveday   = oneday * 25 + 1;
                    const twentysixday    = oneday * 26 + 1;
                    const twentysevenday  = oneday * 27 + 1;
                    const twentyeightday  = oneday * 28 + 1;
                    const twentynineday   = oneday * 29 + 1;
                    const thirdtyday      = oneday * 30 + 1;
                    const thirdtoneyday   = oneday * 31 + 1;
                    const thirdttwoyday   = oneday * 32 + 1;
                    const thirdtythreeday = oneday * 33 + 1;
                    const thirdtyfourday  = oneday * 34 + 1;
                    const thirdtyfiveday  = oneday * 35 + 1;
                    const constantaDay = 
                                        fix_average_wt < satujam ? satujam * 1:
                                        fix_average_wt < duajam ? duajam * 1:
                                        fix_average_wt < tigajam ? tigajam  * 1:
                                        fix_average_wt < empatjam ? empatjam * 1 :
                                        fix_average_wt < limajam ? limajam * 1 :
                                        fix_average_wt < enamjam ? enamjam * 1 :
                                        fix_average_wt < tujuhjam ? tujuhjam * 1  :
                                        fix_average_wt < delapanjam ? delapanjam * 1 :
                                        fix_average_wt < sembilanjam ? sembilanjam * 1 :
                                        fix_average_wt < sepuluhjam ? sepuluhjam * 1 :
                                        fix_average_wt < sebelasjam ? sebelasjam * 1:
                                        fix_average_wt < duabelasjam ? duabelasjam * 1:
                                        fix_average_wt < tigabelasjam ? tigabelasjam * 1:
                                        fix_average_wt < empatbelasjam ? empatbelasjam * 1:
                                        fix_average_wt < limabelasjam ? limabelasjam * 1:
                                        fix_average_wt < enambelasjam ? enambelasjam * 1:
                                        fix_average_wt < tujuhbelasjam ? tujuhbelasjam * 1:
                                        fix_average_wt < delapanbelasjam ? delapanbelasjam * 1:
                                        fix_average_wt < sembilasjam ? sembilasjam * 1:
                                        fix_average_wt < duapuluhjam ? duapuluhjam * 1:
                                        fix_average_wt < duapuluhsatujam ? duapuluhsatujam * 1:
                                        fix_average_wt < duapuluhduajam ? duapuluhduajam * 1:
                                        fix_average_wt < duapuluhtigajam ? duapuluhtigajam * 1:
                                        fix_average_wt < halfday ? halfday :
                                        fix_average_wt < oneday ? oneday :
                                        fix_average_wt < twoday ? twoday :
                                        fix_average_wt < thirdday ? thirdday :
                                        fix_average_wt < fouthday ? fouthday :
                                        fix_average_wt < fifthday ? fifthday :
                                        fix_average_wt < sixday ? sixday :
                                        fix_average_wt < sevenday ? sevenday :
                                        fix_average_wt < eightday ? eightday :
                                        fix_average_wt < nineday ? nineday :
                                        fix_average_wt < tenday ? tenday :
                                        fix_average_wt < elevenday ? elevenday :
                                        fix_average_wt < twelveday ? twelveday :
                                        fix_average_wt < thirdteenday ? thirdteenday :
                                        fix_average_wt < fourteenday ? fourteenday :
                                        fix_average_wt < fiftheenday ? fiftheenday :
                                        fix_average_wt < sixteenday ? sixteenday :
                                        fix_average_wt < seventeenday ? seventeenday :
                                        fix_average_wt < eightteenday ? eightteenday :
                                        fix_average_wt < nineteenday ? nineteenday :
                                        fix_average_wt < twentyday ? twentyday :
                                        fix_average_wt < twentyoneday ? twentyoneday:
                                        fix_average_wt < twentytwoday ? twentytwoday :
                                        fix_average_wt < twentythreeday ? twentythreeday:
                                        fix_average_wt < twentyfourday ? twentyfourday :
                                        fix_average_wt < twentyfiveday ? twentyfiveday :
                                        fix_average_wt < twentysixday ? twentysixday :
                                        fix_average_wt < twentysevenday ? twentysevenday :
                                        fix_average_wt < twentyeightday ? twentyeightday :
                                        fix_average_wt < twentynineday ? twentynineday :
                                        fix_average_wt < thirdtyday ? thirdtyday :
                                        fix_average_wt < thirdtoneyday ? thirdtoneyday :
                                        fix_average_wt < thirdttwoyday ? thirdttwoyday :
                                        fix_average_wt < thirdtythreeday ? thirdtythreeday :
                                        fix_average_wt < thirdtyfourday ? thirdtyfourday :
                                        thirdtyfiveday
                     ;
                    
                    const vRespon = fix_respon_wt2 
                    // Rumus 
                    const maxOld  = fix_average_wt + constantaDay ;
                    const minOld  = fix_average_wt - constantaDay  ;
                    const checkMinPositiveNegative = minOld > 0 ? minOld - minOld : minOld - minOld;
                    const checkMaxPositiveNegative = minOld > 0 ? maxOld - minOld : maxOld - minOld;
                    const checlValPositive = minOld > 0 ? vRespon - minOld : vRespon - minOld;
                    const FIXmin = scaleValue(checkMinPositiveNegative, [checkMinPositiveNegative,checkMaxPositiveNegative] , [0,10])
                    const FIXmax = scaleValue(checkMaxPositiveNegative, [checkMinPositiveNegative,checkMaxPositiveNegative] , [0,10])
                    const FIXval = scaleValue(checlValPositive, [checkMinPositiveNegative,checkMaxPositiveNegative] , [0,10])
                    const unread_pre = resBelumDibaca.data === "Not Found" ? 0 : Number(resBelumDibaca.total)
                    const unread = unread_pre

                    const valResponTemp = unread > 30 ? FIXmax : Number(resWaktuResponse.data[0].wt2) == 0 ? 0 : FIXval
                    const valll = 10 - FIXval
                    const percentage_responsetime = scaleValue(valll, [FIXmin, FIXmax], [0,10])
                    console.log('percentage_responsetime', percentage_responsetime)





                
                const dataa = {
                    labels: [
                        'SKP (1.1)', 'E-Kinerja (1.2)', 'Kehadiran Presensi (1.3)', 'Kehadiran Rapat (1.4)', 'Beban Kerja (1.5)', 'Keterlambatan (1.6)',
                        'Kompetensi (2.1)',
                        'Integritas (2.2.1)', 'Kerjasama (2.2.2)', 'Komunikasi (2.2.3)', 'Orientasi pada hasil (2.2.4)', 'Pelayanan Publik (2.2.5)', 'Pengembangan diri dan orang lain (2.2.6)', 'Mengelola Perubahan (2.2.7)',  'Pengambilan Keputusan (2.2.8)', 'Perekat Bangsa (2.2.9)', 
                        'Kualifikasi / Pendidikan (2.3.1)', 'Pangkat / Golongan (2.3.2)', 'Usia (2.3.3)', 'Pengalaman dalam jabatan (2.3.4)',  'Pelatihan Kepemimpinan (2.3.5)',  'Diklat Fungsional / Teknis (2.3.6)', 'Pengembangan kompetensi (2.3.7)', 'Hukuman Disiplin (2.3.8)', 'Penghargaan (2.3.9)', 
                        'Kemampuan Bahasa Asing (2.4)'
                    ],
                    datasets: [
                      {
                        label: 'Value',
                        data: [
                            5, 5,
                            9,
                            8, 8, percentage_responsetime, 8, 8, 8, 8, 8, 8,
                            10, 10, 10, 10, 10, 10, 10 ,10, 10,
                            rataPercentage10, rapat_percentage10 , hasil ,8, 8
                        
                        ],
                        // data: [
                        //     hasil, valSKPorPPK, 0, valRapatHakim, percentage_responsetime,
                        //     rataPercentage, 
                        //     80, 80,80,
                        //     100, 100, 100, 100, 100, 100, 100 ,100, 100,
                        //     80,80,80,80, 80
                        
                        // ],
                        backgroundColor: [
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(255, 99, 132, 0.2)',

                          'rgba(83, 184, 187, 0.2)',

                          'rgba(153, 102, 255, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(153, 102, 255, 0.2)',

                          'rgba(75, 192, 192, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          
                          'rgba(255, 159, 64, 0.2)',
                        ],
                        borderColor: [
                          'rgba(255, 99, 132, 1)',
                          'rgba(255, 99, 132, 1)',
                          'rgba(255, 99, 132, 1)',
                          'rgba(255, 99, 132, 1)',
                          'rgba(255, 99, 132, 1)',

                          'rgba(0, 54, 56, 1)',

                          'rgba(153, 102, 255, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(153, 102, 255, 1)',

                          'rgba(75, 192, 192, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(75, 192, 192, 1)',
                         
                          'rgba(255, 159, 64, 1)',
                        ],
                        borderWidth: 1,
                      },
                    ],
                }

                const optionss = {
                    scales: {
                      y: {
                        beginAtZero: true,
                        max: 10
                      },  
                     
                    },
                    plugins: {
                        legend: {
                          position: 'right',
                          display: false
                        },
                        title: {
                          display: true,
                          text: '(*) Disclaimer, data dalam tahap pengembangan dan belum sepenuhnya benar / lengkap',
                        },
                      },
                    responsive: true,
                    maintainAspectRatio: false
                  };
                setData(dataa)
                setOptions(optionss)
            } catch (err) {
                console.log('Error in contentGrafikTotal: ',err)
            }
        }
        fetchAll();
    }, [])

    // useEffect(() => {
    //     const initilize = async()=> {

    //     }
    //     return initilize
    // }, [data])
    // console.log('total',total)
    return(
            <Grid item xs={12}>
                <Paper className={classes.contentTotalAsalSurat}>
                    <Grid item xs={12}>
                        <Typography variant="h5" gutterBottom>Instrument Penilaian Nine Box</Typography>
                        <Divider/>
                        {data 
                        ? <div style={{height: '450px', paddingTop: '2em', paddingRight: '1.5em', paddingLeft: '1em'}}>
                            <Bar data={data} options={options} />
                            </div>
                        : ''
                    }
                    </Grid>

                    <Grid item xs={12} style={{paddingTop: '2em'}}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Typography variant="h4">Skema Penilaian</Typography>
                            </Grid>
                            <Grid item xs={3}>
                                <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                width: '100%', backgroundColor: 'rgba(255, 99, 132, 0.2)', 
                                borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                borderRadius: '50px'
                                }}>
                                    <Typography variant="h4">Kinerja</Typography>
                                    <Typography>30%</Typography>
                                </div>
                            </Grid>
                            <Grid item xs={3}>
                                <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                width: '100%', backgroundColor: 'rgba(153, 102, 255, 0.2)', 
                                borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                borderRadius: '50px'
                                }}>
                                    <Typography variant="h4">Potensi</Typography>
                                    <Typography>30%</Typography>
                                </div>
                            </Grid>
                            <Grid item xs={3}>
                            
                                <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                width: '100%', backgroundColor: 'rgba(75, 192, 192, 0.2)', 
                                borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                borderRadius: '50px'
                                }}>
                                    <Typography variant="h4">Rekam Jejak</Typography>
                                    <Typography>15%</Typography>
                                </div>
                            </Grid>
                            <Grid item xs={3}>
                                <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                width: '100%', backgroundColor: 'rgba(255, 159, 64, 0.2)', 
                                borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                borderRadius: '50px'
                                }}>
                                    <Typography variant="h4">Lain-lain</Typography>
                                    <Typography>25%</Typography>
                                </div>
                            </Grid>
                            
                            
                            
                            {/* <Grid item xs={12}>
                                <Typography variant="h4">Skema Penilaian 2</Typography>
                            </Grid>
                            <Grid item xs={3}>
                                <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                width: '100%', backgroundColor: 'rgba(255, 99, 132, 0.2)', 
                                borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                borderRadius: '50px'
                                }}>
                                    <Typography variant="h4">Kinerja</Typography>
                                    <Typography>50%</Typography>
                                </div>
                            </Grid>
                            <Grid item xs={9} style={{backgroundColor: 'rgba(153, 102, 255, 0.2)', borderRadius: '50px', }}>
                                <Grid container spacing={1}>
                                    <Grid item xs={12}>
                                        <Typography variant="h4">Potensial Talenta</Typography>
                                        <Typography>50%</Typography>
                                    </Grid>
                                       
                                    <Grid item xs={4}>
                                        <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                        width: '100%', backgroundColor: 'rgba(153, 102, 255, 0.2)', 
                                        borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                        borderRadius: '50px'
                                        }}>
                                            <Typography variant="h4">Potensi</Typography>
                                            <Typography>?%</Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={4}>
                                    
                                        <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                        width: '100%', backgroundColor: 'rgba(75, 192, 192, 0.2)', 
                                        borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                        borderRadius: '50px'
                                        }}>
                                            <Typography variant="h4">Rekam Jejak</Typography>
                                            <Typography>?%</Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <div style={{paddingTop: '2em', paddingBottom: '2em', 
                                        width: '100%', backgroundColor: 'rgba(255, 159, 64, 0.2)', 
                                        borderColor: 'rgba(255, 99, 132, 1)', borderWidth: '10px',
                                        borderRadius: '50px'
                                        }}>
                                            <Typography variant="h4">Lain-lain</Typography>
                                            <Typography>?%</Typography>
                                        </div>
                                    </Grid>
                                </Grid>
                            </Grid> */}




                        </Grid>
                        
                    </Grid>
                    
                    
                    
				</Paper>
			</Grid>
    )
}

export default ContentInstrumentNineBox;