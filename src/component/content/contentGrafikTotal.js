import React,{useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { ResponsivePie } from '@nivo/pie';

// const data = [
// 	{
// 	  "id": "notadinas",
// 	  "label": "Nota Dinas",
// 	  "value": 204,
// 	  "color": "hsl(210, 70%, 50%)"
// 	},
// 	{
// 	  "id": "disposisi",
// 	  "label": "Disposisi",
// 	  "value": 791,
// 	  "color": "hsl(160, 70%, 50%)"
// 	},
// 	{
// 	  "id": "tembusan",
// 	  "label": "Tembusan",
// 	  "value": 274,
// 	  "color": "hsl(88, 70%, 50%)"
// 	}
//  ]

const useStyles = makeStyles((theme) => ({
	contentTotalAsalSurat: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: '350px',
		boxShadow: theme.shadow.large,
		// borderRadius: 15
	},

}));

const ContentGrafikTotal = () => {
    const classes = useStyles();
    const [state, setState] = useState({total:[], detail: []});
    const [total, setTotal] = useState(null)
    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1 = new URLSearchParams(search).get('tgl1');
    const tgl2 = new URLSearchParams(search).get('tgl2');
    useEffect(() => {
        const fetchAll = async () => {
            try{
                const apiAllDisposisi = `${process.env.REACT_APP_API_ALL_DISPOSISI}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiAllNotadinas = `${process.env.REACT_APP_API_ALL_NOTADINAS}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`
                const apiAllTembusan = `${process.env.REACT_APP_API_ALL_TEMBUSAN}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`
                const [fetchAllDisposisi, fetchAllNotadinas, fetchAllTembusan] = await Promise.all([
                    fetch(apiAllDisposisi),
                    fetch(apiAllNotadinas),
                    fetch(apiAllTembusan),
                ])

                const resAllDisposisi   = await fetchAllDisposisi.json()
                const resAllNotadinas   = await fetchAllNotadinas.json()
                const resAllTembusan    = await fetchAllTembusan.json()

                const allDisposisi      = resAllDisposisi.data[0].read_disposisi
                const allAllNotadinas   = resAllNotadinas.data[0].read_notadinas
                const allAllTembusan    = resAllTembusan.data[0].read_tembusan

                const objGrafikTotal = [
                    {
                        "id"    : "notadinas",
                        "label" : "Nota Dinas",
                        "value" : allAllNotadinas,
                        "color" : "hsl(210, 70%, 50%)"
                    },
                    {
                        "id"    : "disposisi",
                        "label" : "Disposisi",
                        "value" : allDisposisi,
                        "color" : "hsl(160, 70%, 50%)"
                    },
                    {
                        "id"    : "tembusan",
                        "label" : "Tembusan",
                        "value" : allAllTembusan,
                        "color" : "hsl(88, 70%, 50%)"
                    }, 

                ]
                setTotal(objGrafikTotal)
               
            } catch (err) {
                console.log('Error in contentGrafikTotal: ',err)
            }
        }
        fetchAll();
    }, [])
    // console.log('total',total)
    return(
            <Grid item xs={6}>
                <Paper className={classes.contentTotalAsalSurat}>
                    <Grid item xs={12}>
                        <Typography variant="h5" gutterBottom>Jenis Surat Masuk</Typography>
                        <Divider/>
                    </Grid>
                    {total ?
                    <ResponsivePie
                        data={total}
                        margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                        innerRadius={0.5}
                        padAngle={2}
                        cornerRadius={9}
                        activeInnerRadiusOffset={17}
                        activeOuterRadiusOffset={8}
                        borderWidth={1}
                        borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.2 ] ] }}
                        arcLinkLabelsSkipAngle={7}
                        arcLinkLabelsTextColor="#333333"
                        arcLinkLabelsThickness={2}
                        arcLinkLabelsColor={{ from: 'color' }}
                        arcLabelsSkipAngle={10}
                        arcLabelsTextColor={{ from: 'color', modifiers: [ [ 'darker', 2 ] ] }}
                        colors={{scheme: 'paired'}}
                        defs={[
                            {
                                // id: 'dots',
                                // type: 'patternDots',
                                background: 'inherit',
                                color: '#3f51b5',
                                size: 4,
                                padding: 1,
                                stagger: true
                            },
                            {
                                id: 'lines',
                                type: 'patternLines',
                                background: 'inherit',
                                color: 'rgba(255, 255, 255, 0.3)',
                                rotation: -45,
                                lineWidth: 6,
                                spacing: 10
                            }
                        ]}
                        // fill={[
                        //     {
                        //         match: {
                        //             id: 'notadinas'
                        //         },
                        //         id: 'dots'
                        //     },
                        //     {
                        //         match: {
                        //             id: 'disposisi'
                        //         },
                        //         id: 'lines'
                        //     },
                        // ]}
                        legends={[
                            {
                                anchor: 'bottom',
                                direction: 'row',
                                justify: false,
                                translateX: 67,
                                translateY: 56,
                                itemsSpacing: 0,
                                itemWidth: 163,
                                itemHeight: 18,
                                itemTextColor: '#999',
                                itemDirection: 'left-to-right',
                                itemOpacity: 1,
                                symbolSize: 18,
                                symbolShape: 'circle',
                                effects: [
                                    {
                                        on: 'hover',
                                        style: {
                                            itemTextColor: '#000'
                                        }
                                    }
                                ]
                            }
                        ]}
                    />
                    : ''
                }
				</Paper>
			</Grid>
    )
}

export default ContentGrafikTotal;