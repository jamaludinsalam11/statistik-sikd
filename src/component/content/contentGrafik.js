import React,{useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import ContenGrafikTotal from './contentGrafikTotal';
import ContenGrafikDetail from './contentGrafikDetail';


const Grafik = () => {
    const [state, setState] = useState({total:[], detail: [], totalSurat: []});
    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const tgl1 = new URLSearchParams(search).get('tgl1');
    const tgl2 = new URLSearchParams(search).get('tgl2');
    useEffect(() => {
        const fetchAll = async () => {
            try{
                await Promise.all([
                    fetch(`${process.env.REACT_APP_API_NODIN_MASUK}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_NODIN_KELUAR}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_DISPOSISI_MASUK}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_DISPOSISI_KELUAR}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_TEMBUSAN_MASUK}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                    fetch(`${process.env.REACT_APP_API_TEMBUSAN_KELUAR}?select_id=${select_id}&tgl1=${tgl1}&tgl2=${tgl2}`),
                ])
                .then((responses) => {
                    return Promise.all(responses.map(function (response) {
                        return response.json();
                    }));
                })
                .then((data) => {
                    // console.log(data[0].data[0]);
                    // 1. Initial 
                    const nodin_masuk = data[0].data[0].notadinas_masuk;
                    const nodin_keluar = data[1].data[0].notadinas_keluar;
                    const disposisi_masuk = data[2].data[0].disposisi_masuk;
                    const disposisi_keluar = data[3].data[0].disposisi_keluar;
                    const tembusan_masuk = data[4].data[0].tembusan_masuk;
                    const tembusan_keluar = data[5].data[0].tembusan_keluar;

                    //2. Make object for TOTAL Statistic
                    const total_nodin = Number(nodin_masuk)  + Number(nodin_keluar) ;
                    const total_disposisi = Number(disposisi_masuk) + Number(disposisi_keluar);
                    const total_tembusan = Number(tembusan_masuk) + Number(tembusan_keluar);

                    // 3. Obj Total
                    const objGrafikTotal = [
                        {
                            "id"    : "notadinas",
                            "label" : "Nota Dinas",
                            "value" : total_nodin,
                            "color" : "hsl(210, 70%, 50%)"
                        },
                        {
                            "id"    : "disposisi",
                            "label" : "Disposisi",
                            "value" : total_disposisi,
                            "color" : "hsl(160, 70%, 50%)"
                        },
                        {
                            "id"    : "tembusan",
                            "label" : "Tembusan",
                            "value" : total_tembusan,
                            "color" : "hsl(88, 70%, 50%)"
                        },

                    ]
                
      
                    // 4. make Object for Detail Statistic
                    const total_surat = {
                        suratmasuk: Number(nodin_masuk) + Number(disposisi_masuk)+ Number(tembusan_masuk),
                        suratkeluar: Number(nodin_keluar) + Number(disposisi_keluar) + Number(tembusan_keluar)
                    }
                    const objGrafikDetail = [
                        {
                            "jenissurat": "Nota Dinas",
                            "surat masuk": Number(nodin_masuk),
                            "surat_masuk_count": Number(nodin_masuk),
                            "surat masukColor": "hsl(48, 70%, 50%)",
                            "surat keluar": Number(nodin_keluar),
                            "surat_keluar_count": Number(nodin_keluar),
                            "surat keluarColor": "hsl(188, 70%, 50%)",
                        },
                        {
                            "jenissurat": "Disposisi",
                            "surat masuk": Number(disposisi_masuk),
                            "surat_masuk_count": Number(disposisi_masuk),
                            "surat masukColor": "hsl(345, 70%, 50%)",
                            "surat keluar": Number(disposisi_keluar),
                            "surat_keluar_count": Number(disposisi_keluar),
                            "surat keluarColor": "hsl(253, 70%, 50%)",
                        },
                        {
                            "jenissurat": "Tembusan",
                            "surat masuk": Number(tembusan_masuk),
                            "surat_masuk_count": Number(tembusan_masuk),
                            "surat masukColor": "hsl(214, 70%, 50%)",
                            "surat keluar": Number(tembusan_keluar),
                            "surat_keluar_count": Number(tembusan_keluar),
                            "surat keluarColor": "hsl(284, 70%, 50%)",
                        }


                    ]
                    // Check 
                    // console.log('Check Fix objGrafikTotal: ',objGrafikTotal)
                    // console.log('Check Fix objGrafikDetail: ',objGrafikDetail)
                    
                    setState({total: objGrafikTotal, detail: objGrafikDetail, totalSurat: total_surat})
                })
                .then((data) => {
                    // console.log('resp 3', data)
                })
            } catch (err) {
                console.log(err)
            }
        }
        fetchAll();
    }, [])
    // console.log('state:',state)
    return(
        <React.Fragment>
            {/* <ContenGrafikTotal total={state.total}/> */}
            <ContenGrafikDetail detail={state.detail} totalSurat={state.totalSurat}/>
        </React.Fragment>
    )
}

export default Grafik;