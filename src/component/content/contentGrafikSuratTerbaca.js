import React ,{ useEffect , useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';


const useStyles = makeStyles((theme) => ({
    tanggap : {
        fill: 'transparent',
        stroke: 'rgb(106, 215, 45)',
        strokeWidth: 26,
        strokeDasharray: '0 10000',
        transition: 'stroke-dasharray .3s ease'
    },
    kurangTanggap: {
        fill: 'transparent',
        stroke: 'rgb(246, 150, 30)',
        strokeWidth: 26,
        strokeDasharray: '0 10000',
        transition: 'stroke-dasharray .3s ease'
    },
    tidakTanggap: {
        fill: 'transparent',
        stroke: '#D54C4C',
        strokeWidth: 26,
        strokeDasharray: '0 10000',
        transition: 'stroke-dasharray .3s ease'
    }
}))

const GrafikSuratTerbaca = (props) => {
    const classes  = useStyles()
    const [donutval, setDonutval] = useState(null)
    const [percentage, setPercentage] = useState(props.percentage);
    const [comment, setComment] = useState(null)
    const [initial,setInitial] = useState({
        value:props.percentage,
        valuelabel:'Tanggap',
        size:116,
        strokewidth:26
    });
    const [data, setData] = useState({
        halfsize:null,
        radius:null,
        rotateval:null,
        trackstyle: null,
        indicatorstyle: {
            fill: 'red',
            stroke: 'red'
        },



    })
    // console.log('iam here props.percentage',props.percentage)

    useEffect(() => {
        const fetchGrafik = async() => {
            try{
                const fromProps = await props.percentage
                const tmp = fromProps
                const olahComment = tmp > 0 && tmp <= 50 ? 'Tidak Tanggap' : tmp >= 51 && tmp <= 80 ? 'Kurang Tanggap' : tmp >= 81 && tmp <= 100 ? 'Tanggap' : ''
                // console.log('fromProps',olahComment)
                const halfsize = (initial.size * 0.5);
                const radius = halfsize - (initial.strokewidth * 0.5);
                const circumference = 2 * Math.PI * radius;
                const strokeval = ((initial.value * circumference) / 100);
                const dashval = (strokeval + ' ' + circumference);

                const trackstyle = {strokeWidth: initial.strokewidth};
                const indicatorstyle = {strokeWidth: initial.strokewidth, strokeDasharray: dashval}
                const rotateval = 'rotate(-90 '+halfsize+','+halfsize+')';

                setComment(olahComment)
                setData({
                    halfsize:halfsize,
                    radius:radius,
                    rotateval:rotateval,
                    trackstyle: trackstyle,
                    indicatorstyle: indicatorstyle,
                })
                setInitial({
                    value:fromProps,
                    valuelabel:'Completed',
                    size:116,
                    strokewidth:26
                })
                setPercentage(fromProps)
            } catch(err){
                console.log(err)
            }
        }
        fetchGrafik();
    },[])

   
    return (
        <>
        <svg width={initial.size} height={initial.size} className="donutchart">
            <circle fill="red" r={data.radius} cx={data.halfsize} cy={data.halfsize} transform={data.rotateval} style={data.trackstyle} className="donutchart-track"/>
            <circle fill="red" stroke="red" r={data.radius} cx={data.halfsize} cy={data.halfsize} transform={data.rotateval} 
                style={data.indicatorstyle} className={comment === 'Tanggap' ? classes.tanggap : comment === 'Kurang Tanggap' ? classes.kurangTanggap : classes.tidakTanggap}
            />
            <text className="donutchart-text" x={data.halfsize} y={data.halfsize} style={{textAnchor:'middle'}} >
            <tspan className="donutchart-text-val">{initial.value}</tspan>
            <tspan className="donutchart-text-percent">%</tspan>
            <tspan className="donutchart-text-label" x={data.halfsize} y={data.halfsize+10}>{comment}</tspan>
            </text>
        </svg>
        <div>

     
        </div>
        </>
    )
  }

  GrafikSuratTerbaca.propTypes= {
    value: PropTypes.number,        // value the chart should show
    valuelabel: PropTypes.string,   // label for the chart
    size: PropTypes.number,         // diameter of chart
    strokewidth: PropTypes.number   // width of chart line
  }

  export default GrafikSuratTerbaca;