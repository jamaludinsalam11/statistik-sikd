import React,{useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import {
  Avatar,
  Button,
  Box,
  Card,
  CardContent,
  CardHeader ,
  CardMedia,
  Grid,
  Paper,
  Typography,
  Divider,
  Modal,
  Backdrop,
  Fade,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import ReactFC from 'react-fusioncharts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import Powercharts from 'fusioncharts/fusioncharts.powercharts';

import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import Chart from 'react-apexcharts'
import Skeleton from '@material-ui/lab/Skeleton';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ContentNineBoxModals from './contentNineBoxModals';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import ModalAllNbv from './modalNinebox/modalNineboxAllNbv';
import ContentNineBoxNbvUnitkerja from './contentNineBoxNbvUnitkerja';


const options = {
    chart: {
      height: 350,
      width: '600px',
      type: 'radialBar',
      toolbar: {
        show: false
      }
    },
    plotOptions: {
      radialBar: {
        startAngle: -135,
        endAngle: 225,
         hollow: {
          margin: 0,
          size: '70%',
          background: '#fff',
          image: undefined,
          imageOffsetX: 0,
          imageOffsetY: 0,
          position: 'front',
          dropShadow: {
            enabled: true,
            top: 3,
            left: 0,
            blur: 4,
            opacity: 0.24
          }
        },
        track: {
          background: '#fff',
          strokeWidth: '67%',
          margin: 0, // margin is in pixels
          dropShadow: {
            enabled: true,
            top: -3,
            left: 0,
            blur: 4,
            opacity: 0.35
          }
        },
    
        dataLabels: {
          show: false,
          name: {
            offsetY: -80,
            show: true,
            color: '#888',
            fontSize: '28px'
          },
          value: {
            formatter: function(val) {
              return parseFloat(`${val}%\n${parseFloat(val).toFixed(2)}`);
            },
            color: '#111',
            fontSize: '65px',
            show: true,
          }
        }
      }
    },
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: ['#ABE5A1'],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      lineCap: 'round'
    },
    labels: ['Nilai'],
    responsive: [{
        breakpoint: 480,
        options: {
            legend: {
                show: false
            }
        }
    }]
}

const useStyles = makeStyles((theme) => ({
	contentNineBox: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
		height: 'auto',
		boxShadow: theme.shadow.large,
        width: '100%'
		// borderRadius: 15
	},
    active : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EE9524',color: '#fff',
        // height: '120px', borderRadius: '50px', backgroundColor: 'rgb(241, 225, 91)',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    activeAdmin : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px', borderRadius: '50px', backgroundColor: '#EDDB2D', 
        // height: '120px', borderRadius: '50px', backgroundColor: '#CCF6C8',
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    notActive : {
        paddingTop: '2em', marginLeft: '.5em',marginRight: '.5em', marginTop: '3em',
        height: '120px',  borderRadius: '50px', backgroundColor: '#e6e6e6', 
        boxShadow: '6px 6px 14px 0 rgba(0, 0, 0, 0.2),-8px -8px 18px 0 rgba(255, 255, 255, 0.55)',
    },
    btn: {
        marginTop: '1.5em', borderRadius: 25 
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(8, 5, 8),
        borderRadius: 25
       
    },
    rootList: {
        marginTop: '2em',
        width: '100%',
        maxWidth: 600,
        backgroundColor: theme.palette.background.paper,
        overflow: 'auto',
        maxHeight: '80%'
    },
    card: {
        maxWidth: 600,
        margin: theme.spacing(2),
    },
    media: {
        height: 190,
    },
    fade_rule: {
        height: '5px',
        backgroundColor: '#E6E6E6',
        width: '120px',
        marginLeft:'auto', marginRight: 'auto',
        marginTop:'1em', marginBottom: '1em',
        backgroundImage: 'linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-o-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-moz-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-ms-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%)',
        backgroundImage: '-webkit-gradient( linear, left bottom, right bottom, color-stop(0.02, white), color-stop(0.5, gray), color-stop(0.98, white) )'
}

}));


const series = (nilai) => ([nilai])

const checkRoleAtasan = (data1, data2) => {
    
    const roleAtasan = data1 === 'uk.1.1.2' ? 'uk.1.1.2' : data1 === 'uk.1.1.3' ? 'uk.1.1.3' : data1 === 'uk.1.1.4' ? 'uk.1.1.4' : data1 === 'uk.1.1.5' ? 'uk.1.1.5' : data1 === 'uk.1.1.6'  ? 'uk.1.1.6' : data1 === 'uk.1.1.7' ? 'uk.1.1.7'  : data1 === 'uk.1.1.8' ? 'uk.1.1.8'  
                        : data1 === 'uk.1.1.16' ? 'uk.1.1.16' : data1 === 'uk.1.1.18' ? 'uk.1.1.18' : data1 === 'uk.1.1.19' ? 'uk.1.1.19' : data1 === 'uk.1.1.20' ? 'uk.1.1.20' : data1 === 'uk.1.1.21' ? 'uk.1.1.21' : data1 === 'uk.1.1.22' ? 'uk.1.1.22' 
                        : data1 === 'uk.1.1.23' ? 'uk.1.1.23' : data1 === 'uk.1.1.24' ? 'uk.1.1.24' : data1 === 'uk.1.1.25' ? 'uk.1.1.25' : data2
    console.log('kepalaUnit',roleAtasan)
return roleAtasan 
}

export const ContentNineBox = () => {
    const classes = useStyles()
    let search  = useLocation().search
    const select_id1 = new URLSearchParams(search).get('select_id')
    const tgl1 = new URLSearchParams(search).get('tgl1')
    const tgl2 = new URLSearchParams(search).get('tgl2')
    const tahun_now 		= moment(tgl2).format('YYYY')
    const [roleAtasan, setRoleAtasan] = useState(null)
    const [nilai, setNilai] = useState(null)
    const [nilaiExact, setNilaiExact] = useState(null)
    const [nilaiExactPrevious, setNilaiExactPrevious] = useState(null)
    const [boxTalent, setBoxTalent] = useState(null)
    const [modal1, setModal1] = useState(false)
    const [modal2, setModal2] = useState(false)
    const [modal3, setModal3] = useState(false)
    const [modal4, setModal4] = useState(false)
    const [modal5, setModal5] = useState(false)
    const [modal6, setModal6] = useState(false)
    const [modal7, setModal7] = useState(false)
    const [modal8, setModal8] = useState(false)
    const [modal9, setModal9] = useState(false)
    const [modalAllNbv, setModalAllNbv] = useState(false)
    const [box1, setBox1] = useState(0)
    const [box2, setBox2] = useState(0)
    const [box3, setBox3] = useState(0)
    const [box4, setBox4] = useState(0)
    const [box5, setBox5] = useState(0)
    const [box6, setBox6] = useState(0)
    const [box7, setBox7] = useState(0)
    const [box8, setBox8] = useState(0)
    const [box9, setBox9] = useState(0)
    const [availBox, setAvailBox] = useState({
        box1: 0,
        box2: 0,
        box3: 0,
        box4: 0,
        box5: 0,
        box6: 0,
        box7: 0,
        box8: 0,
        box9: 0,
    })
    const [avgNbv, setAvgNbv] = useState(0);
    // const [allNbv, setAllNbv] = useState(null)
    
    useEffect(() => {
        const nineBox = async() => {
            try{
                // console.log('roleatasan', role_atasan1)
                const tahun = moment().format('YYYY')
                const apiNineBoxProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/pegawai/${select_id1}/${tahun_now}`
                const apiNineBoxDev = `http://localhost:3001/sikd/api/v1/ninebox/pegawai-krs-2023/${select_id1}/${tahun_now}`
                // const apiNineBoxDev = `http://localhost:3001/sikd/api/v1/ninebox/pegawai-eselon2-april-2023/${select_id1}/${tahun_now}`
                // const apiNineBoxDev = `http://localhost:3001/sikd/api/v1/ninebox/pegawai/${select_id1}/${tahun_now}`
                const apiNineBox = process.env.NODE_ENV === 'development' ? apiNineBoxDev : apiNineBoxProd;
                const apiCheckBoxTalentProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/check-box-talent/${tahun_now}/${select_id1}`
                const apiCheckBoxTalentDev = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/check-box-talent-krs-2023/${tahun_now}/${select_id1}`
                // const apiCheckBoxTalentDev = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/check-box-talent/${tahun_now}/${select_id1}`
                const apiCheckBoxTalent = process.env.NODE_ENV === 'development' ? apiCheckBoxTalentDev : apiCheckBoxTalentProd;
 

                const [ fetchNineBox, fetchCheckBoxTalent] = await Promise.all([
                    fetch(apiNineBox),
                    fetch(apiCheckBoxTalent)

                ])
                const resNineBox = await fetchNineBox.json()
                const resCheckBoxTalent = await fetchCheckBoxTalent.json()
                console.log('resChecassakBoxTalent', resNineBox)
                const { 
                    role_atasan,
                    nilai_total,nilai_total_exact,
                    one_skp, one_ekinerja, one_kehadiran_absensi, one_kehadiran_rapat, one_beban_kerja, one_kelebihan_jamkerja, one_keterlambatan,
                    two_kompetensi,
                    three_integritas, three_kerjasama, three_komunikasi, three_orientasi_pada_hasil, three_pelayanan_publik, three_pengembangan_diri_dan_oranglain, three_mengelola_perubahan, three_pengambilan_keputusan, three_perekat_bangsa,
                    four_kualifikasi_pendidikan, four_pangkat_golongan, four_usia, four_pengalaman_dlm_jabatan, four_pelatihan_kepemimpinan, four_diklat_fungsional_teknis, four_pengembangan_kompetensi, four_hukuman_disiplin, four_penghargaan,
                    five_kemampuan_bhs_asing,
                    nilai_total_exact_new,
                } = resNineBox.result[0]
                // console.log('ROELATASAN', )
                setRoleAtasan(checkRoleAtasan(select_id1 , role_atasan))
                setNilai(nilai_total_exact_new)
                setNilaiExact(nilai_total_exact_new)
                setNilaiExactPrevious(resNineBox.result_previous.length == 0 ? 0 : resNineBox.result_previous[0].nilai_total_exact)
                setBoxTalent(resCheckBoxTalent)
                setBox1(resCheckBoxTalent.box1)
                setBox2(resCheckBoxTalent.box2)
                setBox3(resCheckBoxTalent.box3)
                setBox4(resCheckBoxTalent.box4)
                setBox5(resCheckBoxTalent.box5)
                setBox6(resCheckBoxTalent.box6)
                setBox7(resCheckBoxTalent.box7)
                setBox8(resCheckBoxTalent.box8)
                setBox9(resCheckBoxTalent.box9)



                /** Avail Box */
                const apiAvailBoxDev = `http://localhost:3001/sikd/api/v1/ninebox/scatterplot/v2-box/${tahun_now}/${select_id1}`;
                const apiAvailBoxProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/scatterplot/v2-box/${tahun_now}/${select_id1}`;
                const apiAvail = process.env.NODE_ENV === 'development' 
                                    ? apiAvailBoxDev
                                    : apiAvailBoxProd
                const fetchAvail    = await fetch(apiAvail)
                const resAvail      = await fetchAvail.json()
                setAvailBox({
                    box1: resAvail.box1,
                    box2: resAvail.box2,
                    box3: resAvail.box3,
                    box4: resAvail.box4,
                    box5: resAvail.box5,
                    box6: resAvail.box6,
                    box7: resAvail.box7,
                    box8: resAvail.box8,
                    box9: resAvail.box9
                })

                const apiAvgNbvDev = `http://localhost:3001/sikd/api/v1/ninebox/user/avg-nbv/${tahun}`;
                const apiAvgNbvProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/user/avg-nbv/${tahun}`;
                const apiAvgNbv = process.env.NODE_ENV === 'development' 
                                    ? apiAvgNbvDev
                                    : apiAvgNbvProd
                const fetchAvgNbv    = await fetch(apiAvgNbv)
                const resAvgNbv      = await fetchAvgNbv.json()
                setAvgNbv(resAvgNbv)

                /** All NBV */
                // const apiAllNbvDev  = `http://localhost:3001/sikd/api/v1/ninebox/nbv/v1/${tahun_now}/${select_id1}`;
                // const apiAllNbvProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/nbv/v1/${tahun_now}/${select_id1}`;
                // const apiAllNbv     = process.env.NODE_ENV === 'development' 
                //                         ? apiAllNbvDev
                //                         : apiAllNbvProd
                // const fetchAllNbv   = await fetch(apiAllNbv)
                // const resAllNbv     = await fetchAllNbv.json()
                // setAllNbv(resAllNbv.arrResult)
                


            } catch(err) {
                console.log('error ninwebox', err)
            }
        }
        nineBox()
    }, [])
    // console.log('box7',box7)
    // console.log('box8',availBox)
    const handleOpenModal1 = () => {
        setModal1(true)
    }
    const handleOpenModal2 = () => {
        setModal2(true)
    }
    const handleOpenModal3 = () => {
        setModal3(true)
    }
    const handleOpenModal4 = () => {
        setModal4(true)
    }
    const handleOpenModal5 = () => {
        setModal5(true)
    }
    const handleOpenModal6 = () => {
        setModal6(true)
    }
    const handleOpenModal7 = () => {
        setModal7(true)
    }
    const handleOpenModal8 = () => {
        setModal8(true)
    }
    const handleOpenModal9 = () => {
        setModal9(true)
    }
    const handleOpenModalAllNbv = () => {
        setModalAllNbv(true)
    }
    
    const ModalBox1 = (props) => {
        const classes = useStyles()
        const [open, setOpen] = useState(true)
        const [box1, setBox1] = useState(null)
        const [total, setTotal] = useState(null)
        const [percentage, setPercentage] = useState(null)
        useEffect(() => {
            const box1 = async() => {
                try{
                    const devprod = process.env.NODE_ENV === 'development' 
                        ? 'http://localhost:3001/sikd/api/v1/ninebox/list-of-talent' 
                        : `${process.env.REACT_APP_API_APPS_LISTOFTALENT2}`
                    const apiListOfTalent   = `${devprod}/${tahun_now}/0/19/${select_id1}/${roleAtasan}`
                    // const apiListOfTalent   = `http://localhost:3001/sikd/api/v1/ninebox/list-of-talent/2021/10/19/${select_id1}/${roleAtasan}`
                    const fetchListOfTalent = await fetch(apiListOfTalent)
                    const resListOfTalent   = await fetchListOfTalent.json()
                    const { total,percentage, result }        = resListOfTalent
                    const sort = await sortDESCandNullishValue(result)
                    setBox1(sort)
                    setTotal(total)
                    setPercentage(percentage)
                } catch{
     
                }
            }
            box1()
        },[roleAtasan])
        useEffect(() => {
            const modal1 = async() => {
                try{
                    setOpen(props.open)
                } catch{
    
                }
            }
            modal1()
        }, [props])
    
        const handleClose = () => {
            setModal1(false)
        }
        return (
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper} style={{height: '80vh'}}>
                <div style={{textAlign: 'center'}}>
                    <Typography variant="h3">List of Talent 1</Typography>
                    <Typography variant="subtitle2">Kumpulan pegawai yang masuk dalam kategori Talent 1</Typography>
                </div>
                
                <List className={classes.rootList}>
                    {
                        box1  ? box1.map((box) => (
                            <ListItem>
                                <ListItemAvatar>
                                <Avatar>
                                    <AccountCircleIcon />
                                </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={box.people_name} secondary={box.people_desc} />
                            </ListItem>
                           
                        ))
                        : <div>
                            { [0,1,2,3,4,5].map(() => (
                                <Card className={classes.card}>
                                    <CardHeader
                                        avatar={ <Skeleton animation="wave" variant="circle" width={40} height={40} />}
                                        title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
                                        subheader={<Skeleton animation="wave" height={10} width="40%" />}
                                    />
                                </Card>
                            ))
                            }
                        </div>
                    }
                </List>
                <div style={{textAlign: 'center', marginTop: '2em'}}>
                    {total ? <Typography variant="h5">{total} Pegawai</Typography> : ''}
                    {percentage ? <Typography variant="caption">{`${percentage}% dari seluruh pegawai`}</Typography> : ''}
                </div>
              </div>
            </Fade>
          </Modal>
        )
    }
    const ModalBox2 = (props) => {
        const classes = useStyles()
        const [open, setOpen] = useState(true)
        const [box2, setBox2] = useState(null)
        const [total2, setTotal2] = useState(null)
        const [percentage2, setPercentage2] = useState(null)
        useEffect(() => {
            const box2 = async() => {
                try{
                    const apiListOfTalent   = `${process.env.REACT_APP_API_APPS_LISTOFTALENT2}/${tahun_now}/20/29/${select_id1}/${roleAtasan}`
                    // const apiListOfTalent   = `http://localhost:3001/sikd/api/v1/ninebox/list-of-talent/2021/20/29/${select_id1}/${roleAtasan}`
                    const fetchListOfTalent = await fetch(apiListOfTalent)
                    const resListOfTalent   = await fetchListOfTalent.json()
                    const { total,percentage, result }        = resListOfTalent
                    const sort = await sortDESCandNullishValue(result)
                    setBox2(sort)
                    setTotal2(total)
                    setPercentage2(percentage)
                } catch{
    
                }
            }
            box2()
        },[roleAtasan])
        useEffect(() => {
            const modal2 = async() => {
                try{
                    setOpen(props.open)
                } catch{
    
                }
            }
            modal2()
        }, [props])
    
        const handleClose = () => {
            setModal2(false)
        }
        return (
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper} style={{height: '80vh'}}>
                <div style={{textAlign: 'center'}}>
                    <Typography variant="h3">List of Box 2</Typography>
                    <Typography variant="subtitle2">Kumpulan pegawai yang masuk dalam kategori Box 2</Typography>
                </div>
                
                <List className={classes.rootList}>
                    {
                        box2  ? box2.map((box) => (
                            <ListItem>
                                <ListItemAvatar>
                                <Avatar>
                                    <AccountCircleIcon />
                                </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={box.people_name} secondary={box.people_desc} />
                            </ListItem>
                           
                        ))
                        : <div>
                           { [0,1,2,3,4,5].map(() => (
                               <Card className={classes.card}>
                                    <CardHeader
                                        avatar={ <Skeleton animation="wave" variant="circle" width={40} height={40} />}
                                        title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
                                        subheader={<Skeleton animation="wave" height={10} width="40%" />}
                                    />
                                </Card>
                            ))
                           }
                        </div>
                    }
                </List>
                <div style={{textAlign: 'center', marginTop: '2em'}}>
                    {total2 ? <Typography variant="h5">{total2} Pegawai</Typography> : ''}
                    {percentage2 ? <Typography variant="caption">{`${percentage2}% dari seluruh pegawai`}</Typography> : ''}
                </div>
              </div>
            </Fade>
          </Modal>
        )
    }
    const ModalBox3 = (props) => {
        const classes = useStyles()
        const [open, setOpen] = useState(true)
        const [box3, setBox3] = useState(null)
        const [total3, setTotal3] = useState(null)
        const [percentage3, setPercentage3] = useState(null)
        useEffect(() => {
            const box3 = async() => {
                try{
                    const apiListOfTalent   = `${process.env.REACT_APP_API_APPS_LISTOFTALENT2}/${tahun_now}/30/39/${select_id1}/${roleAtasan}`
                    // const apiListOfTalent   = `http://localhost:3001/sikd/api/v1/ninebox/list-of-talent/2021/30/39/${select_id1}/${roleAtasan}`
                    const fetchListOfTalent = await fetch(apiListOfTalent)
                    const resListOfTalent   = await fetchListOfTalent.json()
                    const { total,percentage,  result } = resListOfTalent
                    const sort = await sortDESCandNullishValue(result)
                    setBox3(sort)
                    setTotal3(total)
                    setPercentage3(percentage)
                } catch{
    
                }
            }
            box3()
        },[])
        useEffect(() => {
            const modal3 = async() => {
                try{
                    setOpen(props.open)
                } catch{
    
                }
            }
            modal3()
        }, [props])
    
        const handleClose = () => {
            setModal3(false)
        }
        return (
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper} style={{height: '80vh'}}>
                <div style={{textAlign: 'center'}}>
                    <Typography variant="h3">List of Box 3</Typography>
                    <Typography variant="subtitle2">Kumpulan pegawai yang masuk dalam kategori Box 3</Typography>
                </div>
                
                <List className={classes.rootList}>
                    {
                        box3  ? box3.map((box) => (
                            <ListItem>
                                <ListItemAvatar>
                                <Avatar>
                                    <AccountCircleIcon />
                                </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={box.people_name} secondary={box.people_desc} />
                            </ListItem>
                           
                        ))
                        : <div>
                            { [0,1,2,3,4,5].map(() => (
                                <Card className={classes.card}>
                                    <CardHeader
                                        avatar={ <Skeleton animation="wave" variant="circle" width={40} height={40} />}
                                        title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
                                        subheader={<Skeleton animation="wave" height={10} width="40%" />}
                                    />
                                </Card>
                            ))
                            }
                        </div>
                    }
                </List>
                <div style={{textAlign: 'center', marginTop: '2em'}}>
                    {total3 ? <Typography variant="h5">{total3} Pegawai</Typography> : ''}
                    {percentage3 ? <Typography variant="caption">{`${percentage3}% dari seluruh pegawai`}</Typography> : ''}
                </div>
              </div>
            </Fade>
          </Modal>
        )
    }
    const ModalBox4 = (props) => {
        const classes = useStyles()
        const [open, setOpen] = useState(true)
        const [box4, setBox4] = useState(null)
        const [total4, setTotal4] = useState(null)
        const [percentage4, setPercentage4] = useState(null)
        useEffect(() => {
            const box4 = async() => {
                try{
                    const apiListOfTalentProd   = `${process.env.REACT_APP_API_APPS_LISTOFTALENT2}/${tahun_now}/40/50/${select_id1}/${roleAtasan}`
                    const apiListOfTalentDev   = `http://localhost:3001/sikd/api/v1/ninebox/list-of-talent/${tahun_now}/40/50/${select_id1}/${roleAtasan}`
                    const api = process.env.NODE_ENV === 'development' ? apiListOfTalentDev : apiListOfTalentProd
                    const fetchListOfTalent = await fetch(api)
                    const resListOfTalent   = await fetchListOfTalent.json()
                    const { total, percentage,  result } = resListOfTalent
                    const sort = await sortDESCandNullishValue(result)
                    setBox4(sort)
                    setTotal4(total)
                    setPercentage4(percentage)
                } catch{
    
                }
            }
            box4()
        },[])
        useEffect(() => {
            const modal4 = async() => {
                try{
                    setOpen(props.open)
                } catch{
    
                }
            }
            modal4()
        }, [props])
    
        const handleClose = () => {
            setModal4(false)
        }
        return (
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper} style={{height: '80vh'}}>
                <div style={{textAlign: 'center'}}>
                    <Typography variant="h3">List of Box 4</Typography>
                    <Typography variant="subtitle2">Kumpulan pegawai yang masuk dalam kategori Box 4</Typography>
                </div>
                
                <List className={classes.rootList}>
                    {
                        box4  ? box4.map((box) => (
                            <ListItem>
                                <ListItemAvatar>
                                <Avatar>
                                    <AccountCircleIcon />
                                </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={box.people_name} secondary={box.people_desc} />
                            </ListItem>
                           
                        ))
                        : <div>
                            { [0,1,2,3,4,5].map(() => (
                                <Card className={classes.card}>
                                    <CardHeader
                                        avatar={ <Skeleton animation="wave" variant="circle" width={40} height={40} />}
                                        title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
                                        subheader={<Skeleton animation="wave" height={10} width="40%" />}
                                    />
                                </Card>
                            ))
                            }
                        </div>
                    }
                </List>
                <div style={{textAlign: 'center', marginTop: '2em'}}>
                    {total4 ? <Typography variant="h5">{total4} Pegawai</Typography> : ''}
                    {percentage4 ? <Typography variant="caption">{`${percentage4}% dari seluruh pegawai`}</Typography> : ''}
                </div>
              </div>
            </Fade>
          </Modal>
        )
    }
    const ModalBox5 = (props) => {
        const classes = useStyles()
        const [open, setOpen] = useState(true)
        const [box5, setBox5] = useState(null)
        const [total5, setTotal5] = useState(null)
        const [percentage5, setPercentage5] = useState(null)
        useEffect(() => {
            const box5 = async() => {
                try{
                    const apiListOfTalent   = `${process.env.REACT_APP_API_APPS_LISTOFTALENT2}/${tahun_now}/50/60/${select_id1}/${roleAtasan}`
                    const fetchListOfTalent = await fetch(apiListOfTalent)
                    const resListOfTalent   = await fetchListOfTalent.json()
                    const { total, percentage, result } = resListOfTalent
                    const sort = await sortDESCandNullishValue(result)
                    setBox5(sort)
                    setTotal5(total)
                    setPercentage5(percentage)
                } catch{
    
                }
            }
            box5()
        },[])
        useEffect(() => {
            const modal5 = async() => {
                try{
                    setOpen(props.open)
                } catch{
    
                }
            }
            modal5()
        }, [props])
    
        const handleClose = () => {
            setModal5(false)
        }
        return (
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper} style={{height: '80vh'}}>
                <div style={{textAlign: 'center'}}>
                    <Typography variant="h3">List of Box 5</Typography>
                    <Typography variant="subtitle2">Kumpulan pegawai yang masuk dalam kategori Box 5</Typography>
                </div>
                
                <List className={classes.rootList}>
                    {
                        box5  ? box5.map((box) => (
                            <ListItem>
                                <ListItemAvatar>
                                <Avatar>
                                    <AccountCircleIcon />
                                </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={box.people_name} secondary={box.people_desc} />
                            </ListItem>
                           
                        ))
                        : <div>
                            { [0,1,2,3,4,5].map(() => (
                                <Card className={classes.card}>
                                    <CardHeader
                                        avatar={ <Skeleton animation="wave" variant="circle" width={40} height={40} />}
                                        title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
                                        subheader={<Skeleton animation="wave" height={10} width="40%" />}
                                    />
                                </Card>
                            ))
                            }
                        </div>
                    }
                </List>
                <div style={{textAlign: 'center', marginTop: '2em'}}>
                    {total5 ? <Typography variant="h5">{total5} Pegawai</Typography> : ''}
                    {percentage5 ? <Typography variant="caption">{`${percentage5}% dari seluruh pegawai`}</Typography> : ''}
                </div>
              </div>
            </Fade>
          </Modal>
        )
    }
    const ModalBox6 = (props) => {
        const classes = useStyles()
        const [open, setOpen] = useState(true)
        const [box6, setBox6] = useState(null)
        const [total6, setTotal6] = useState(null)
        const [percentage6, setPercentage6] = useState(null)
        useEffect(() => {
            const box6 = async() => {
                try{
                    const apiListOfTalent   = `${process.env.REACT_APP_API_APPS_LISTOFTALENT2}/${tahun_now}/60/69/${select_id1}/${roleAtasan}`
                    const fetchListOfTalent = await fetch(apiListOfTalent)
                    const resListOfTalent   = await fetchListOfTalent.json()
                    const { total, percentage, result } = resListOfTalent
                    const sort = await sortDESCandNullishValue(result)
                    setBox6(sort)
                    setTotal6(total)
                    setPercentage6(percentage)
                } catch{
    
                }
            }
            box6()
        },[])
        useEffect(() => {
            const modal6 = async() => {
                try{
                    setOpen(props.open)
                } catch{
    
                }
            }
            modal6()
        }, [props])
    
        const handleClose = () => {
            setModal6(false)
        }
        return (
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper} style={{height: '80vh'}}>
                <div style={{textAlign: 'center'}}>
                    <Typography variant="h3">List of Box 6</Typography>
                    <Typography variant="subtitle2">Kumpulan pegawai yang masuk dalam kategori Box 6</Typography>
                </div>
                
                <List className={classes.rootList}>
                    {
                        box6  ? box6.map((box) => (
                            <ListItem>
                                <ListItemAvatar>
                                <Avatar>
                                    <AccountCircleIcon />
                                </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={box.people_name} secondary={box.people_desc} />
                            </ListItem>
                           
                        ))
                        : <div>
                            { [0,1,2,3,4,5].map(() => (
                                <Card className={classes.card}>
                                    <CardHeader
                                        avatar={ <Skeleton animation="wave" variant="circle" width={40} height={40} />}
                                        title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
                                        subheader={<Skeleton animation="wave" height={10} width="40%" />}
                                    />
                                </Card>
                            ))
                            }
                        </div>
                    }
                </List>
                <div style={{textAlign: 'center', marginTop: '2em'}}>
                    {total6 ? <Typography variant="h5">{total6} Pegawai</Typography> : ''}
                    {percentage6 ? <Typography variant="caption">{`${percentage6}% dari seluruh pegawai`}</Typography> : ''}
                </div>
              </div>
            </Fade>
          </Modal>
        )
    }
    const ModalBox7 = (props) => {
        const classes = useStyles()
        const [open, setOpen] = useState(true)
        const [box7, setBox7] = useState(null)
        const [total7, setTotal7] = useState(null)
        const [percentage7, setPercentage7] = useState(null)
        useEffect(() => {
            const box7 = async() => {
                try{
                    const apiListOfTalent   = `${process.env.REACT_APP_API_APPS_LISTOFTALENT2}/${tahun_now}/70/79/${select_id1}/${roleAtasan}`
                    const fetchListOfTalent = await fetch(apiListOfTalent)
                    const resListOfTalent   = await fetchListOfTalent.json()
                    const { total,percentage, result } = resListOfTalent
                    const sort = await sortDESCandNullishValue(result)
                    setBox7(sort)
                    setTotal7(total)
                    setPercentage7(percentage)
                } catch{
    
                }
            }
            box7()
        },[])
        useEffect(() => {
            const modal7 = async() => {
                try{
                    setOpen(props.open)
                } catch{
    
                }
            }
            modal7()
        }, [props])
    
        const handleClose = () => {
            setModal7(false)
        }
        return (
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper} style={{height: '80vh'}}>
                <div style={{textAlign: 'center'}}>
                    <Typography variant="h3">List of Box 7</Typography>
                    <Typography variant="subtitle2">Kumpulan pegawai yang masuk dalam kategori Box 7</Typography>
                </div>
                
                <List className={classes.rootList}>
                    {
                        box7  ? box7.map((box) => (
                            <ListItem>
                                <ListItemAvatar>
                                <Avatar>
                                    <AccountCircleIcon />
                                </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={box.people_name} secondary={box.people_desc} />
                            </ListItem>
                           
                        ))
                        : <div>
                            { [0,1,2,3,4,5].map(() => (
                                <Card className={classes.card}>
                                    <CardHeader
                                        avatar={ <Skeleton animation="wave" variant="circle" width={40} height={40} />}
                                        title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
                                        subheader={<Skeleton animation="wave" height={10} width="40%" />}
                                    />
                                </Card>
                            ))
                            }
                        </div>
                    }
                </List>
                <div style={{textAlign: 'center', marginTop: '2em'}}>
                    {total7 ? <Typography variant="h5">{total7} Pegawai</Typography> : ''}
                    {percentage7 ? <Typography variant="caption">{`${percentage7}% dari seluruh pegawai`}</Typography> : ''}
                </div>
              </div>
            </Fade>
          </Modal>
        )
    }
    const ModalBox8 = (props) => {
        const classes = useStyles()
        const [open, setOpen] = useState(true)
        const [box8, setBox8] = useState(null)
        const [total8, setTotal8] = useState(null)
        const [percentage8, setPercentage8] = useState(null)
        useEffect(() => {
            const box8 = async() => {
                try{
                    const apiListOfTalent   = `${process.env.REACT_APP_API_APPS_LISTOFTALENT2}/${tahun_now}/80/89/${select_id1}/${roleAtasan}`
                    const fetchListOfTalent = await fetch(apiListOfTalent)
                    const resListOfTalent   = await fetchListOfTalent.json()
                    const { total,percentage, result } = resListOfTalent
                    const sort = await sortDESCandNullishValue(result)
                    setBox8(sort)
                    setTotal8(total)
                    setPercentage8(percentage)
                } catch{
    
                }
            }
            box8()
        },[])
        useEffect(() => {
            const modal8 = async() => {
                try{
                    setOpen(props.open)
                } catch{
    
                }
            }
            modal8()
        }, [props])
    
        const handleClose = () => {
            setModal8(false)
        }
        return (
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper} style={{height: '80vh'}}>
                <div style={{textAlign: 'center'}}>
                    <Typography variant="h3">List of Box 8</Typography>
                    <Typography variant="subtitle2">Kumpulan pegawai yang masuk dalam kategori Box 8</Typography>
                </div>
                
                <List className={classes.rootList}>
                    {
                        box8  ? box8.map((box) => (
                            <ListItem>
                                <ListItemAvatar>
                                <Avatar>
                                    <AccountCircleIcon />
                                </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={box.people_name} secondary={box.people_desc} />
                            </ListItem>
                           
                        ))
                        : <div>
                            { [0,1,2,3,4,5].map(() => (
                                <Card className={classes.card}>
                                    <CardHeader
                                        avatar={ <Skeleton animation="wave" variant="circle" width={40} height={40} />}
                                        title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
                                        subheader={<Skeleton animation="wave" height={10} width="40%" />}
                                    />
                                </Card>
                            ))
                            }
                        </div>
                    }
                </List>
                <div style={{textAlign: 'center', marginTop: '2em'}}>
                    {total8 ? <Typography variant="h5">{total8} Pegawai</Typography> : ''}
                    {percentage8 ? <Typography variant="caption">{`${percentage8}% dari seluruh pegawai`}</Typography> : ''}
                </div>
              </div>
            </Fade>
          </Modal>
        )
    }
    const ModalBox9 = (props) => {
        const classes = useStyles()
        const [open, setOpen] = useState(true)
        const [box9, setBox9] = useState(null)
        const [total9, setTotal9] = useState(null)
        const [percentage9, setPercentage9] = useState(null)
        useEffect(() => {
            const box9 = async() => {
                try{
                    const apiListOfTalent   = `${process.env.REACT_APP_API_APPS_LISTOFTALENT2}/${tahun_now}/90/100/${select_id1}/${roleAtasan}`
                    const fetchListOfTalent = await fetch(apiListOfTalent)
                    const resListOfTalent   = await fetchListOfTalent.json()
                    const { total, percentage, result } = resListOfTalent
                    const sort = await sortDESCandNullishValue(result)
                    setBox9(sort)
                    setTotal9(total)
                    setPercentage9(percentage)
                } catch{
    
                }
            }
            box9()
        },[])
        useEffect(() => {
            const modal9 = async() => {
                try{
                    setOpen(props.open)
                } catch{
    
                }
            }
            modal9()
        }, [props])
    
        const handleClose = () => {
            setModal9(false)
        }
        return (
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper} style={{height: '80vh'}}>
                <div style={{textAlign: 'center'}}>
                    <Typography variant="h3">List of Box 9</Typography>
                    <Typography variant="subtitle2">Kumpulan pegawai yang masuk dalam kategori Box 9</Typography>
                </div>
                
                <List className={classes.rootList}>
                    {
                        box9  ? box9.map((box) => (
                            <ListItem>
                                <ListItemAvatar>
                                <Avatar>
                                    <AccountCircleIcon />
                                </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={box.people_name} secondary={box.people_desc} />
                            </ListItem>
                           
                        ))
                        : <div>
                            { [0,1,2,3,4,5].map(() => (
                                <Card className={classes.card}>
                                    <CardHeader
                                        avatar={ <Skeleton animation="wave" variant="circle" width={40} height={40} />}
                                        title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
                                        subheader={<Skeleton animation="wave" height={10} width="40%" />}
                                    />
                                </Card>
                            ))
                            }
                        </div>
                    }
                </List>
                <div style={{textAlign: 'center', marginTop: '2em'}}>
                    {total9 ? <Typography variant="h5">{total9} Pegawai</Typography> : ''}
                    {percentage9 ? <Typography variant="caption">{`${percentage9}% dari seluruh pegawai`}</Typography> : ''}
                </div>
              </div>
            </Fade>
          </Modal>
        )
    }


    const handleCloseAllNbv = () => {
        setModalAllNbv(false)
    }

    return (
        
            <Grid item xs={12}>
                <Paper className={classes.contentNineBox}>
                    <Grid item xs={12}>
                        <Typography variant="h5" gutterBottom>Ninebox Value (NBV)</Typography>
                        <Divider/>
                    </Grid>

                    <Grid item xs={12} style={{paddingTop: '2em', paddingBottom: '6em'}}>
                        <Grid container
                            direction="row-reverse"
                            justifyContent="center"
                            alignItems="center"
						    spacing={3}
                        >
                            <Grid item lg={12} sm={12} xl={12} xs={12} style={{paddingTop: '.1em', marginBottom: '-28em', zIndex: 1}} >
                                {/* <Typography variant="h3">---------</Typography> */}
                                {/* <hr style={{width: '100px', textAlign: 'center' , marginLeft: 'auto', marginRight: 'auto', marginTop:'1em', marginBottom: '1em'}}/> */}
                                <AccountBalanceIcon style={{
                                    fontSize: 65, 
                                    // color: nilaiExact == 0 ? 'rgba(0, 0, 0, 0.54)' : 'rgba(0, 0, 0, 0.54)',
                                    // color: 'rgb(169,214,160)',
                                    // color: 'linear-gradient(90deg, rgba(169,214,160,1) 0%, rgba(88,188,204,1) 46%, rgba(75,140,202,1) 100%)'
                                }}
                                /> 
                                <Typography variant="h3" style={{fontSize: '46px', color: '#112031'}}>{nilaiExact}</Typography>
                                <div className={classes.fade_rule}></div>  
                                <Typography variant="h3" style={{fontSize: '13px', fontStyle: 'italic'}}>
                                    NBV Tahun Sebelumnya :
                                </Typography>
                                <Typography variant="h1" style={{fontSize: '16px', fontStyle: 'italic',fontWeight: 'bold', marginBottom: '-1em'}}>{nilaiExactPrevious ? nilaiExactPrevious : 0}</Typography>
                                <Typography variant="h3" style={{ paddingTop: '2em',fontSize: '13px', fontStyle: 'italic'}}>Rata-rata seluruh pegawai:</Typography>
                                <Button 
                                    variant="contained" 
                                    size="small"
                                    onClick={handleOpenModalAllNbv} 
                                    style={{
                                        borderRadius: 75, 
                                        width: '100px',
                                        marginTop: '0.5em',
                                        background: 'rgb(169,214,160)',
                                        background: 'linear-gradient(90deg, rgba(169,214,160,1) 0%, rgba(88,188,204,1) 46%, rgba(75,140,202,1) 100%)',
                                        color: 'white'
                                    }} 
                                >
                                    {avgNbv}
                                </Button>
                                <ModalAllNbv 
                                    modalAllNbv={modalAllNbv} 
                                    handleCloseAllNbv={() => handleCloseAllNbv()}
                                    tahun_now={tahun_now}
                                    select_id1={select_id1}
                                />
                                {/* <Typography variant="h3" style={{fontSize: '30px', color: '#112031'}}>{nilaiExact}</Typography> */}
                            </Grid>
                            {nilai ==null 
                                ? '' 
                            
                                :<Grid item lg={12} style={{textAlign:'center', padding: 'auto auto', marginBottom: '-5em', width: 'inherit'}}>
                                    <div style={{padding: 'auto auto'}}>
                                        <Chart
                                            type="radialBar" height={450} width="100%"
                                            options={options}
                                            series={series(nilai)}
                                        />

                                    </div>
                                </Grid>     
                            }
                            
                            
                            {
                                nilai >= 0 && nilai < 10 
                                ?   <Grid item lg={12} sm={12} xl={12} xs={12} style={{paddingTop: '3em'}} >
                                        <Typography variant="h3">Maaf, Nilai kamu belum mencukupi untuk masuk kedalam Ninebox</Typography>
                                    </Grid>
                                    :''
                            }



                            {/* <Grid item lg={12} sm={12} xl={12} xs={12} style={{paddingTop: '2em'}} >
                                <Typography variant="h3">*Dalam pengembangan</Typography>
                            </Grid> */}

                            <Grid item lg={3} sm={3} xl={3} xs={3} className={nilai >= 90 && nilai <= 100    ? classes.active : availBox.box9 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4')? classes.notActive : availBox.box9 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? classes.activeAdmin : classes.notActive }>
                                <div>
                                    <Typography variant="h3" style={{fontWeight: 600, paddingTop: '.7em'}}>Box 9</Typography>
                                    <Typography variant="subtitle" style={{fontWeight: 300}}>{nilai >= 90 && nilai <= 100   ? 'You are here': 'You are not here' }</Typography>

                                </div>
                                <Button 
                                    onClick={handleOpenModal9} 
                                    variant="contained" 
                                    color="secondary" 
                                    disabled={nilai >= 90 && nilai <= 100  ? false : 
                                             availBox.box9 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? true : 
                                             availBox.box9 > 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26' )? false : true } 
                                    className={classes.btn}  
                                    >
                                    Details
                                </Button>
                                <ModalBox9 open={modal9}/>
                            </Grid>


                            <Grid item lg={3} sm={3} xl={3} xs={3} className={nilai >= 80 && nilai < 90   ? classes.active :  availBox.box8 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? classes.notActive : availBox.box8 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' ||select_id1 === 'uk.1.1.18.26') ? classes.activeAdmin : classes.notActive}>
                                <div>
                                    <Typography variant="h3" style={{fontWeight: 600, paddingTop: '.7em'}}>Box 8</Typography>
                                    <Typography variant="subtitle" style={{fontWeight: 300}}>{nilai >= 80 && nilai < 90   ? 'You are here': 'You are not here' }</Typography>

                                </div>
                                <Button 
                                onClick={handleOpenModal8} 
                                variant="contained" 
                                color="secondary" 
                                disabled={nilai >= 80 && nilai < 90   ? false : 
                                          availBox.box8 == 0 && select_id1 === 'uk.1.1'  ? true : 
                                          availBox.box8 == 0 && select_id1 === 'uk.1.4'  ? true : 
                                          availBox.box8 > 0 &&  (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? false : 
                                          true } 
                                className={classes.btn} >
                                    Details
                                </Button>
                                <ModalBox8 open={modal8}/>
                            </Grid>

                            <Grid 
                                item lg={3} sm={3} xl={3} xs={3}  
                                className={nilai >= 70 && nilai < 80   ? classes.active : 
                                            availBox.box7 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? classes.notActive : 
                                            availBox.box7 > 0 &&  (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? classes.activeAdmin : 
                                            classes.notActive}
                            >
                                <div>
                                    <Typography variant="h3" style={{fontWeight: 600, paddingTop: '.7em'}}>Box 7</Typography>
                                    <Typography variant="subtitle" style={{fontWeight: 300}}>{nilai >= 70 && nilai < 80   ? 'You are here': 'You are not here' }</Typography>

                                </div>
                                <Button 
                                    onClick={handleOpenModal7} variant="contained"  
                                    color="secondary" 
                                    disabled={nilai >= 70 && nilai < 80   ? false : 
                                                availBox.box7 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? true : 
                                                availBox.box7 > 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? false 
                                                : true } 
                                    className={classes.btn} 
                                >
                                    Details
                                </Button>
                                <ModalBox7 open={modal7}/>
                            </Grid>


                            <Grid 
                                item lg={3} sm={3} xl={3} xs={3} 
                                className={nilai >= 60 && nilai < 70  ? classes.active : 
                                            availBox.box6 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? classes.notActive : 
                                            availBox.box6 > 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4'|| select_id1 === 'uk.1.1.18.26') ? classes.activeAdmin : 
                                            classes.notActive}
                            >
                                <div>
                                    <Typography variant="h3" style={{fontWeight: 600, paddingTop: '.7em'}}>Box 6</Typography>
                                    <Typography variant="subtitle" style={{fontWeight: 300}}>{nilai >= 60 && nilai < 70  ? 'You are here': 'You are not here' }</Typography>

                                </div>
                                <Button 
                                    onClick={handleOpenModal6} variant="contained" 
                                    color="secondary" 
                                    disabled={nilai >= 60 && nilai < 70  ? false : 
                                                availBox.box6 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? true : 
                                                availBox.box6 > 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? false : 
                                                true } 
                                    className={classes.btn} 
                                >
                                    Details
                                </Button>
                                <ModalBox6 open={modal6}/>
                            </Grid>

                            <Grid 
                                item lg={3} sm={3} xl={3} xs={3}  
                                className={nilai >= 50 && nilai < 60  ? classes.active : 
                                            availBox.box5 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? classes.notActive : 
                                            availBox.box5 > 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? classes.activeAdmin : 
                                            classes.notActive}
                            >
                                <div>
                                    <Typography variant="h3" style={{fontWeight: 600, paddingTop: '.7em'}}>Box 5</Typography>
                                    <Typography variant="subtitle" style={{fontWeight: 300}}>{nilai >= 50 && nilai < 60  ? 'You are here': 'You are not here' }</Typography>

                                </div>
                                <Button 
                                    onClick={handleOpenModal5} variant="contained" 
                                    color="secondary" 
                                    disabled={nilai >= 50 && nilai < 60  ? false : 
                                                availBox.box5 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? true : 
                                                availBox.box5 > 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? false : 
                                                true } 
                                    className={classes.btn} 
                                >
                                    Details
                                </Button>
                                <ModalBox5 open={modal5}/>
                            </Grid>

                            <Grid 
                                item lg={3} sm={3} xl={3} xs={3}  
                                className={nilai >= 40 && nilai < 50  ? classes.active : 
                                            availBox.box4 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? classes.notActive : 
                                            availBox.box4 > 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? classes.activeAdmin : 
                                            classes.notActive}
                            >
                                <div>
                                    <Typography variant="h3" style={{fontWeight: 600, paddingTop: '.7em'}}>Box 4</Typography>
                                    <Typography variant="subtitle" style={{fontWeight: 300}}>{nilai >= 40 && nilai < 50   ? 'You are here': 'You are not here' }</Typography>

                                </div>
                                <Button 
                                    onClick={handleOpenModal4} variant="contained" 
                                    color="secondary" 
                                    disabled={nilai >= 40 && nilai < 50   ? false : 
                                                availBox.box4 == 0 &&  (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? true : 
                                                availBox.box4 > 0 &&  (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? false : 
                                                true } 
                                    className={classes.btn} 
                                >
                                    Details
                                </Button>
                                <ModalBox4 open={modal4}/>
                            </Grid>

                            <Grid 
                                item lg={3} sm={3} xl={3} xs={3}  
                                className={nilai >= 30 && nilai < 40 ? classes.active : 
                                            availBox.box3 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? classes.notActive : 
                                            availBox.box3 > 0 &&  (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? classes.activeAdmin : 
                                            classes.notActive }
                            >
                                <div>
                                    <Typography variant="h3" style={{fontWeight: 600, paddingTop: '.7em'}}>Box 3</Typography>
                                    <Typography variant="subtitle" style={{fontWeight: 300}}>{nilai >= 30 && nilai < 40   ? 'You are here': 'You are not here' }</Typography>

                                </div>
                                <Button 
                                    onClick={handleOpenModal3} variant="contained"
                                    color="secondary" 
                                    disabled={nilai >= 30 && nilai < 40  ? false : 
                                                availBox.box3 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? true : 
                                                availBox.box3 > 0 &&  (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? false : 
                                                true } 
                                    className={classes.btn} 
                                >
                                    Details
                                </Button>
                                <ModalBox3 open={modal3}/>
                            </Grid>

                            <Grid 
                                item lg={3} sm={3} xl={3} xs={3}  
                                className={nilai >= 20 && nilai < 30 ? classes.active : 
                                            availBox.box2 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? classes.notActive : 
                                            availBox.box2 > 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? classes.activeAdmin : 
                                            classes.notActive } 
                            >
                                <div>
                                    <Typography variant="h3" style={{fontWeight: 600, paddingTop: '.7em'}}>Box 2</Typography>
                                    <Typography variant="subtitle" style={{fontWeight: 300}}>{nilai >= 20 && nilai < 30  ? 'You are here': 'You are not here' }</Typography>

                                </div>
                                <div>
                                    <Button 
                                        onClick={handleOpenModal2} variant="contained" 
                                        color="secondary"  
                                        disabled={nilai >= 20 && nilai < 30  ? false : 
                                                    availBox.box2 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? true : 
                                                    availBox.box2 > 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? false : 
                                                    true } 
                                        className={classes.btn} >
                                        Details
                                    </Button>
                                    <ModalBox2 open={modal2}/>
                                </div>
                                
                            </Grid>

                            <Grid 
                                item lg={3} sm={3} xl={3} xs={3} 
                                className={nilai > 10 && nilai < 20 ? classes.active : 
                                            availBox.box1 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? classes.notActive : 
                                            availBox.box1 > 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? classes.activeAdmin : 
                                            classes.notActive } 
                                >
                                <div>
                                    <Typography variant="h3" style={{fontWeight: 600, paddingTop: '.7em'}}>Box 1</Typography>
                                    <Typography variant="subtitle" style={{fontWeight: 300}}>{nilai >= 10 && nilai < 20 ? 'You are here': 'You are not here' }</Typography>

                                </div>
                                
                                <Button 
                                    onClick={handleOpenModal1} variant="contained" 
                                    color="secondary" 
                                    disabled={nilai >= 10 && nilai < 20 ? false : 
                                                availBox.box1 == 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4') ? true : 
                                                availBox.box1 > 0 && (select_id1 === 'uk.1.1' || select_id1 === 'uk.1.4' || select_id1 === 'uk.1.1.18.26') ? false : 
                                                true } 
                                    className={classes.btn}  
                                >
                                    Details
                                </Button>
                                <ModalBox1 open={modal1}/>
                                {/* <ModalAllNBV open={false}/> */}
                            </Grid>

                            <Grid item lg={12} sm={12} xl={12} xs={12} style={{ marginTop: '4em', textAlign: 'left', marginLeft: '8em'}} >
                                <div> <Typography variant='h1'>Keterangan :</Typography></div>
                                <div> 
                                    <Typography variant='h5'>- <b>Kelompok Rencana Suksesi</b> dilihat berdasarkan box terdepan pada masing-masing jabatan yang dinominasikan. </Typography>
                                </div>
                                <div> 
                                    <Typography variant='h5'>- <b>Nilai Ambang Batas</b> untuk masing-masing tingkat jabatan maksimal NBV 99 dan minimal NBV 60. </Typography>
                                </div>
                            </Grid>

                            <Grid item lg={9} sm={9} xl={9} xs={9} style={{paddingTop: '3em'}} >
                                <ContentNineBoxNbvUnitkerja/>
                            </Grid>
                            
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
            
        
    )
}


const sortDESCandNullishValue = async(payload) => {
    const assignValue = (val) => {
        if(val == null || val == 0 || val === '0'){
          return Infinity
        } else {
          return val
        }
      }
      const sorter = (a,b) => {
        return assignValue(Number(b.nilai_total_exact)) - assignValue(Number(a.nilai_total_exact))
      }
      return payload.sort(sorter)
}