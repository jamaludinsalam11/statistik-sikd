import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Avatar from '@material-ui/core/Avatar';
import FolderIcon from '@material-ui/icons/Folder';
import { Grid , Paper, Typography,Slide } from '@material-ui/core'



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="left" ref={ref} {...props} />;
});

export default function ScatterPlotDialog({openDialog,userTarget, handleCloseDialog, tgl1, tgl2, roleid}) {
//   console.log(userTarget)
  const classes = useStyles();
  const handleRedirectToTargetUrl = () => {
    const url = `?select_id=${userTarget.role_id}&tgl1=${tgl1}&tgl2=${tgl2}`
    const res = roleid === 'uk.1.1' ? window.open(url, '_blank').focus() : ''
    return res
  }
  return (
    <div>
      <Dialog
        open={openDialog}
        TransitionComponent={Transition}
        onClose={handleCloseDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        classes={{paper: classes.newPosOfDialog}}
        BackdropProps={{
          style: {
            backgroundColor: 'rgba(0,0,0,0.1)'
          },
        }}
      >
        {/* <DialogTitle id="alert-dialog-title" style={{color: 'black'}}>{"Use Google's location service?"}</DialogTitle> */}
        <Grid
            container
            direction="column"
            justifyContent="flex-end"
            alignItems="center"
            style={{textAlign: 'center'}}
        >
            <div>
                <Grid item xs={12} lg={12} xl={12} style={{ display: 'flex' ,paddingTop: '1em', paddingBottom: '1em', textAlign: 'center', justifyContent: 'center'}}>
                    <Avatar src={userTarget? userTarget.foto_url: ''} className={classes.large}/>
                </Grid>
                <Grid item xs={12} lg={12} xl={12}>
                    <Typography variant='h5' style={{color: '#212121'}}>{userTarget? userTarget.people_name: ''} </Typography>
                </Grid>
                <Grid item xs={12} lg={12} xl={12} style={{paddingLeft: '1.5em', paddingRight: '1.5em'}} >
                    <Typography variant='subtitle2' style={{color: '#424242'}}>{userTarget? userTarget.people_desc: ''} </Typography>
                </Grid>
                
            </div>
        </Grid>
        <DialogContent style={{minWidth: '350px'}}>
          {/* <DialogContentText id="alert-dialog-description">
          Anda ingin membuka profil {userTarget? userTarget.people_name: ''} ?
          </DialogContentText> */}
        </DialogContent>
        <DialogActions style={{justifyContent: 'center'}}>
          <Button onClick={handleCloseDialog} color="primary" style={{fontSize: 12}}>
            Batal
          </Button>
          <Button onClick={handleRedirectToTargetUrl} variant="contained" color="primary" style={{fontSize: 12}}>
            Buka Profil
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    small: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    large: {
      width: theme.spacing(10),
      height: theme.spacing(10),
    },
    newPosOfDialog: {
      marginRight: 0,
      marginTop: 0,
      marginBottom: 0,
      position: "absolute",
      width: '30vw',
      minHeight: '250px',
      // top: "30%",
      right: "0%",
      // transform: "translate(-50%, -50%)",
    }
  }));
