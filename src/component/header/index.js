import React, {useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Logo from './GARUDA_white.png';
import SvgIcon from './svgicon-statistik.png';
import Foto from './foto';
import FotoNotFound from './fotonotfound';

// Foto - foto hakim
import anwar from './fotohakim/anwar-usman-fix.png'
import aswanto from './fotohakim/hakim-aswanto.png'
import arief from './fotohakim/arif-hidayat-tanpa-dasi.png'
import suhartoyo from './fotohakim/hakim-suhartoyo.png'
import manahan from './fotohakim/hakim-manahan.png'
import wahiduddin from './fotohakim/hakim-wahiduddin.png'
import enny from './fotohakim/hakim-enny-p.png'
import saldi from './fotohakim/saldi-isra.png'
import daniel from './fotohakim/hakim_daniel.png'
import mgunturhamzah from './fotohakim/hakim-m-guntur-hamzah.png'
import RadialBar from './RadialBar';
import moment from 'moment';


const useStyles = makeStyles((theme) => ({
    rootContainer:{
        backgroundColor: '#2A0CBD',
        height: '60vh',
        
    },
    rootHeader: {
        // flexGrow: 1,
        maxWidth: '100%',
        marginBottom: '2em'
    },
    titleLogo: {
        height: '110px',
        width: 'auto'

    },
    logoContainer: {
        padding: theme.spacing(2),
        textAlign: 'center',
        backgroundColor: 'transparent',
        boxShadow: 'none',
        height: '100%'
        // color: theme.palette.text.secondary,
    },
    user: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
        textAlign: 'center',
        background: 'rgba(68, 79, 224, 0.3)',
        // background: 'rgba(30, 30, 33, 0.3)',
        borderRadius: 15,
        // minHeight: '202px'
    },
    radialBar: {
        // paddingLeft: theme.spacing(1),
        // paddingRight: theme.spacing(1),
        // paddingTop: theme.spacing(1),
        // paddingBottom: theme.spacing(1),

        textAlign: 'center',
        // background: 'rgba(68, 79, 224, 0.1)',
        background: 'rgba(30, 30, 33, 0.3)',
        // background: '#1E1E21',
        marginBottom: '1em',
        borderRadius: 15,
        borderTopRightRadius: 100,
        borderBottomRightRadius: 100
    },
    userJabatan2: {
        fontWeight: 300
    },
    svgicon: {
        marginTop: '-3em',
        // marginBottom: '2em'
        textAlign: 'center',
        background: 'transparent',
        borderRadius: 15,
        boxShadow: 'none'
    },
    svgiconLogo: {
        marginTop: '3em',
        marginBottom: '-3em',
        height: '250px',
        width: 'auto',
        opacity: .8

    },
    imgComponent:{
        height: '90px',
        width: 'auto'
    }
}));


const Header = () => {
    const classes = useStyles();
    const [user, setUser] = useState({data:[]})
    const [foto, setFoto] = useState(null);
    const [nip, setNip] = useState(null)
    const [data, setData] = useState(null)
    let search  = useLocation().search;
    const select_id = new URLSearchParams(search).get('select_id');
    const pre_tgl1 = new URLSearchParams(search).get('tgl1');
    const tahun     = moment(pre_tgl1).format('YYYY');

    useEffect(() => {
        fetchUser()
    }, [])
    const fetchUser = async () => {
        try{
            // const apiDev = `http://localhost:3001/sikd/api/v1/ninebox/user/profile-eselon2-april-2023/${select_id}/${tahun}`;
            // const apiDev = `http://localhost:3001/sikd/api/v1/ninebox/user/profile-krs-2023/${select_id}/${tahun}`;
            const apiDev = `http://localhost:3001/sikd/api/v1/ninebox/user/profile/${select_id}/${tahun}`;
            const apiProd = `${process.env.REACT_APP_API_APPS_FASTIFY2}/sikd/api/v1/ninebox/user/profile/${select_id}/${tahun}`
            
            const api = process.env.NODE_ENV === 'development' ? apiDev : apiProd;
            const fetching = await fetch(api)
            const fetched   = await fetching.json()

            setFoto(fetched.foto)
            setData(fetched)
        
        } catch(error) {

        }     
    }


    return(
    <div className={classes.rootHeader}>
        <Grid container spacing={2}>
            <Grid item xs={12} md={12}  >
                <Paper className={classes.logoContainer}>
                    <img className={classes.titleLogo} src={Logo} />
                    <Typography variant="subtitle2">SIMANTAP</Typography>
                    <Typography variant="subtitle1">Sistem Informasi Manajemen Talenta Pegawai</Typography>
                    <Typography variant="h1">MANAJEMEN TALENTA</Typography>
                    <Typography variant="subtitle2">Kepaniteraan dan Sekretariat Jenderal</Typography>
                    <Typography variant="subtitle2" gutterBottom>Mahkamah Konstitusi</Typography>
                </Paper>
            </Grid>
            <Grid item xs={8} md={8}  >
                <div style={{height: '200px'}}>
                    <Paper className={classes.user} style={{height: '100%'}}>
                        <Grid container >
                            <Grid item xs={4}>
                                    {data&&data.PrimaryRoleId === 'uk.1'
                                        ? <Foto foto={anwar}/>
                                        : data&&data.PrimaryRoleId === 'uk.2'
                                        ? <Foto foto={aswanto}/>
                                        : data&&data.PrimaryRoleId === 'uk.3'
                                        ? <Foto foto={arief}/>
                                        : data&&data.PrimaryRoleId === 'uk.5'
                                        ? <Foto foto={suhartoyo}/>
                                        : data&&data.PrimaryRoleId === 'uk.6'
                                        ? <Foto foto={manahan}/>
                                        : data&&data.PrimaryRoleId === 'uk.7'
                                        ? <Foto foto={enny}/>
                                        : data&&data.PrimaryRoleId === 'uk.8'
                                        ? <Foto foto={wahiduddin}/>
                                        : data&&data.PrimaryRoleId === 'uk.9'
                                        ? <Foto foto={saldi}/>
                                        : data&&data.PrimaryRoleId === 'uk.12'
                                        ? <Foto foto={daniel}/>
                                        : data&&data.PrimaryRoleId === 'uk.1.1'
                                        ? <Foto foto={mgunturhamzah}/>
                                        : <Foto foto={foto}/>
                                        
                                    }
                                    {/* {pegawai === null 
                                        ? <FotoNotFound />
                                        : user.data.RoleId === 'uk.1'
                                        ? <Foto foto={anwar}/>
                                        : ''
                                    } */}
                                    
                                
                                
                            </Grid>
                            <Grid item xs={8} style={{textAlign: 'left', paddingTop: '.3em', marginLeft: '-2em', marginRight: '-2em'}}>
                            {/* {user.data.RoleId } */}
                            {
                            data&&data != null 
                            ?   <div>
                                    <Typography variant="h3" gutterBottom>{data&&data.PeopleName}</Typography>
                                    <Typography variant="h5" gutterBottom>{data&&data.PeoplePosition}</Typography>
                                    <hr style={{color: '#fff', opacity: 0.1.toExponential, borderRadius: '20px'}}></hr>
                                    <Typography variant="subtitle2" className={classes.userJabatan2}>{data.unitkerja}</Typography>
                                    <Typography variant="subtitle2" style={{fontSize: '14px', fontWeight: 'bold'}} gutterBottom>NIP: {data.PrimaryRoleId == null ? 'NIP kosong didalam SIKD' : data.nip }</Typography>
                                </div>
                            : ''
                            }
                                
                            </Grid>
                        </Grid>
                    
                    </Paper>
                </div>
            </Grid>

            {
                select_id === 'uk.1' || select_id === 'uk.2' || select_id === 'uk.3' ||
                select_id === 'uk.4' || select_id === 'uk.5' || select_id === 'uk.6' ||
                select_id === 'uk.7' || select_id === 'uk.8' || select_id === 'uk.9' ||
                select_id === 'uk.10' || select_id === 'uk.11' || select_id === 'uk.12' || 
                select_id === 'uk.1.1'
                ?   <Grid item xs={4}>
                        <Paper className={classes.svgicon}>
                            <img className={classes.svgiconLogo} src={SvgIcon}/>
                        </Paper>
                    </Grid>
                :   <Grid item xs={4}>
                            <div style={{height: '200px', width: '380px'}}>
                                <Paper className={classes.radialBar} style={{height: '100%'}}> 
                                    <Grid container>
                                        { 
                                            
                                            data != null 
                                            ? <RadialBar data={data} />
                                            : ''
                                        }
                                        
                                    </Grid>
                                
                                </Paper>
                            </div>
                        </Grid>
            }
            

        </Grid>
    </div>
    )
}

export default Header;