import { Grid, Typography } from '@material-ui/core';
import { SettingsOverscanOutlined } from '@material-ui/icons';
import React, { useEffect, useState } from 'react';
import ReactApexChart from 'react-apexcharts';


export default function RadialBar({ data }) {
    const [series, setSeries]   = useState(null)
    const [options, setOptions] = useState(null)
    const [title, setTitle]     = useState('')
    const [value, setValue]     = useState(null)
    const nbv_value         = data.value.nbv
    const kuadran_value     = data.value.kuadran
    const box_value         = data.value.box

    const nbv_text          = data.ninebox.nbv;
    const kuadran_text      = data.ninebox.kuadran2;
    const box_text          = data.ninebox.box2;
    const fetchData = () => {
        const data = {
            // [nbv, box, kuadran]
            series: [nbv_value, box_value, kuadran_value],
            options: {
                
                states:{
                    hover: {
                        filter: {
                            type: 'lighten',
                            value: 0.1,
                        }
                    },
                },
                stroke: {
                    curve: "smooth",
                    lineCap: "round"
                },
                chart: {
                    height: 50,
                    type: 'radialBar',
                    events: {
                        click: function(event, chartContext, config) {
                           console.log(';clidker') // The last parameter config contains additional information like `seriesIndex` and `dataPointIndex` for cartesian charts
                        },
                        dataPointMouseEnter: function(event, chartContext, config) {
                            console.log(';dataPointMouseEnter', config.dataPointIndex) // ...
                            const kode = config.dataPointIndex;
                            if(kode == 0) {
                                setTitle('NBV')
                                setValue(kode)
                            } else if (kode == 1) {
                                setTitle('Box')
                                setValue(kode)
                            } else if (kode == 2) {
                                setTitle('Kuadran')
                                setValue(kode)
                            } else {
                                setTitle('')
                                setValue(null)
                            }
                        },
                        dataPointMouseLeave: function(event, chartContext, config) {
                            setTitle('')  // ...
                            setValue(null)
                        }
                    },
                },
                plotOptions: {
                    radialBar: {
                        hollow: {
                            margin: 10,
                            size: "30%"
                        },
                        dataLabels: {
                            show: false,
                            name: {
                                fontSize: '16px',
                            },
                            value: {
                                fontSize: '12px',
                                color: 'white',
                                marginTop: '-5em'
                            },
                            total: {
                                show: false,
                                label: 'Total',
                                formatter: function (w) {
                                    // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                                    return 249
                                }
                            }
                        },
                        tooltip: {
                            enabled: false,
                            enabledOnSeries: undefined
                        }
                    }
                },
                fill: {
                    type: 'solid',
                    colors: ['#E21333', '#69E300', '#40CAE5']
                },
              labels: ['NBV', 'Box', 'Kuadran'],
            }
        }


        setSeries(data.series)
        setOptions(data.options)


    }

    useEffect(() => {
        fetchData()
    },[])

    return (
        <>
            <Grid item xs={6} xl={6} style={{textAlign: 'left', paddingLeft: '1em', paddingTop: '1em', paddingBottom: '1em'}}>
                <div>
                    <Typography variant='h5'>NBV</Typography>    
                    <Typography variant='h3' style={{color: '#E21333'}}>{nbv_value} of 100</Typography>    
                </div>                 
                <div style={{paddingTop: '.4em'}}>
                    <Typography variant='h5'>Box</Typography>    
                    <Typography variant='h3' style={{color: '#69E300'}}>{box_text}</Typography>    
                </div>                 
                <div style={{paddingTop: '.4em'}}>
                    <Typography variant='h5'>Kuadran</Typography>    
                    <Typography variant='h3' style={{color: '#40CAE5'}}> {kuadran_text} of IV</Typography>    
                </div>       
                         
            </Grid>
            {/* <Grid item> */}
            {/* <div style={{marginLeft: '23.2em', position: 'absolute', marginTop: '6.5em', fontSize: '12px', fontWeight: 'bold'}}> 
                {title} asdasdasd
            </div>  */}
            <div 
                style={{
                    marginLeft: value == 0 ? '21.9em' : 
                                value == 1 ? '22.9em' :      
                                value == 2 ? '22em' : 
                                '23.2em',   
                    position: 'absolute', 
                    marginTop: '6.7em', 
                    fontSize: '12px', 
                    fontWeight: 'bold'
                }}
            > 
                <div >
                    {title}
                </div>
                <div style={{ paddingTop: '.1em', fontSize: '16px', }}>
                {
                    value == null ? '' : 
                    value == 0 ? nbv_text : 
                    value == 1 ? box_text  : 
                    value == 2 ? kuadran_text : ''
                }
                </div>
            </div> 
            {/* </Grid> */}
            <Grid item xs={6} xl={6}>
            {
                    series == null || options == null 
                    ? ''
                    :  <div id="chart">
                            <ReactApexChart 
                                options={options} 
                                series={series} 
                                type="radialBar"
                                height={228} 
                                style={{marginBottom: '-2em', marginTop: '0em'}}
                                
                            />
                        </div>
                }
            </Grid>
            
           
        </>
    )
}