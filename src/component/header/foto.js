import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
    
    imgComponent:{
        height: '140px',
        width: 'auto',
        // backgroundImage: 'linear-gradient(to bottom, rgba(68, 79, 224, 0.3), rgba(68, 79, 224, 0.9)' ,
    }
}));

const Foto = (props) => {
    const classes = useStyles();
    console.log('dari foto', props)
    return (
        <div style={{borderRadius: '25px'}}>
            <img style={{borderRadius: '5px'}} className={classes.imgComponent} src={props.foto} />
        
        </div>
    )
}

export default Foto;