import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    
    imgComponent:{
        height: '100px',
        width: 'auto'
    }
}));

const FotoNotFound = (props) => {
    const classes = useStyles();
    console.log('dari foto', props)
    return (
        <div style={{borderRadius: '25px'}}>
            <img style={{borderRadius: '5px'}} className={classes.imgComponent} src="https://www.edmundsgovtech.com/wp-content/uploads/2020/01/default-picture_0_0.png" />
        
        </div>
    )
}

export default FotoNotFound;