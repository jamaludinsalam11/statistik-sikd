import 'bootstrap.css';
import '@grapecity/wijmo.styles/wijmo.css';
import './app.css';
//
import * as React from 'react';
import * as ReactDOM from 'react-dom';
//
import * as wjChart from '@grapecity/wijmo.react.chart';
import * as chart from '@grapecity/wijmo.chart';
import * as wjChartAnnotation from '@grapecity/wijmo.react.chart.annotation';
import { getData } from './data';
//
class App extends React.Component {
    constructor(props) {
        super(props);
        this.initChart = (sender) => {
            sender.dataLabel.content = this.customDataLabel;
            sender.tooltip.content = this.customTooltip;
        };
        this.customTooltip = (ht) => {
            let item = ht.item;
            return `<b>Name:</b> ${item.movie} </br><b>Kompetensi:</b> ${item.kompetensi}</br><b>Potensi:</b> ${item.potensi}`;
        };
        this.customDataLabel = (ht) => {
            return null
        };
        this.state = {
            data: getData(),
            palette: chart.Palettes.light
        };
        console.log(this.state)
    }
    render() {
        return <div className="container-fluid">
            <wjChart.FlexChart chartType="Scatter" palette={this.state.palette} initialized={this.initChart}>
                <wjChart.FlexChartAxis wjProperty="axisY" title="Kompetensi" min={0} max={100} axisLine={true} majorGrid={false} majorUnit={10}>
                </wjChart.FlexChartAxis>
                <wjChart.FlexChartAxis wjProperty="axisX" title="Potensi" min={0} max={100} majorUnit={10} axisLine={true}>
                </wjChart.FlexChartAxis>

                <wjChart.FlexChartLegend position="None"></wjChart.FlexChartLegend>
                <wjChart.FlexChartDataLabel connectingLine={false}  offset={10}></wjChart.FlexChartDataLabel>

                <wjChart.FlexChartSeries name="Data" itemsSource={this.state.data} bindingX="potensi" binding="kompetensi">
                </wjChart.FlexChartSeries>
                <wjChartAnnotation.FlexChartAnnotationLayer>
                    <wjChartAnnotation.FlexChartAnnotation type="Polygon" attachment="DataCoordinate" style={{ 'fill': 'red', 'opacity': 0.2 }}>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={0} y={0}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={0} y={68}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={68}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={0}></wjChart.FlexChartDataPoint>
                    </wjChartAnnotation.FlexChartAnnotation>
                    <wjChartAnnotation.FlexChartAnnotation type="Polygon" attachment="DataCoordinate" style={{ 'fill': 'orange', 'opacity': 0.2 }}>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={0}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={68}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={68}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={0}></wjChart.FlexChartDataPoint>
                    </wjChartAnnotation.FlexChartAnnotation>
                    <wjChartAnnotation.FlexChartAnnotation type="Polygon" attachment="DataCoordinate" style={{ 'fill': 'yellow', 'opacity': 0.3 }}>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={0}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={68}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={100} y={68}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={100} y={0}></wjChart.FlexChartDataPoint>
                    </wjChartAnnotation.FlexChartAnnotation>

                    <wjChartAnnotation.FlexChartAnnotation type="Polygon" attachment="DataCoordinate" style={{ 'fill': 'orange', 'opacity': 0.2 }}>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={0} y={68}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={0} y={80}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={80}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={68}></wjChart.FlexChartDataPoint>
                    </wjChartAnnotation.FlexChartAnnotation>
                    <wjChartAnnotation.FlexChartAnnotation type="Polygon" attachment="DataCoordinate" style={{ 'fill': 'yellow', 'opacity': 0.3 }}>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={68}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={80}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={80}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={68}></wjChart.FlexChartDataPoint>
                    </wjChartAnnotation.FlexChartAnnotation>
                    <wjChartAnnotation.FlexChartAnnotation type="Polygon" attachment="DataCoordinate" style={{ 'fill': 'green', 'opacity': 0.4 }}>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={68}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={80}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={100} y={80}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={100} y={68}></wjChart.FlexChartDataPoint>
                    </wjChartAnnotation.FlexChartAnnotation>

                    <wjChartAnnotation.FlexChartAnnotation type="Polygon" attachment="DataCoordinate" style={{ 'fill': 'yellow', 'opacity': 0.3 }}>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={0} y={80}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={0} y={100}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={100}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={80}></wjChart.FlexChartDataPoint>
                    </wjChartAnnotation.FlexChartAnnotation>
                    <wjChartAnnotation.FlexChartAnnotation type="Polygon" attachment="DataCoordinate" style={{ 'fill': 'green', 'opacity': 0.4 }}>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={80}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={68} y={100}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={100}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={80}></wjChart.FlexChartDataPoint>
                    </wjChartAnnotation.FlexChartAnnotation>
                    <wjChartAnnotation.FlexChartAnnotation type="Polygon" attachment="DataCoordinate" style={{ 'fill': 'green', 'opacity': 0.5 }}>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={80}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={80} y={100}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={100} y={100}></wjChart.FlexChartDataPoint>
                        <wjChart.FlexChartDataPoint wjProperty="points" x={100} y={80}></wjChart.FlexChartDataPoint>
                    </wjChartAnnotation.FlexChartAnnotation>



                </wjChartAnnotation.FlexChartAnnotationLayer>
            </wjChart.FlexChart>
        </div>;
    }
}
ReactDOM.render(<App />, document.getElementById('app'));
