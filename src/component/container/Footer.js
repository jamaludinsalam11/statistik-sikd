import React, { useState } from 'react';
import {useLocation} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import JabatanKritikal from '../footer/JabatanKritikal';
import moment from 'moment';
import BaganSuksesi from '../footer/bagan-suksesi/BaganSuksesi';
import RumpunJabatan from '../footer/rumpun-jabatan/RumpunJabatan';
import Penerapan from '../footer/penerapan/Penerapan';
const useStyles = makeStyles((theme) => ({
	rootContainer: {
		backgroundColor: '#2A0CBD',
		height: '100vh'
	},
	footer: {
		padding: theme.spacing(3, 2),
		textAlign: 'center',
		boxShadow: '0px 2px 1px -1px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 3px 0px rgba(0,0,0,0.12)',
		marginTop: '80px',
		backgroundColor: '#fff'
		// backgroundColor:
		//   theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[800],
	},

}));
function Copyright() {
	return (
	  <Typography variant="body2" color="textSecondary">
		{'Copyright © '}
		<Link color="inherit" href="https://sikd.mkri.id/">
		  SIKD Mahkamah Konstitusi
		</Link>{' '}
		{new Date().getFullYear()}
		{'.'}
	  </Typography>
	);
  }


function ProfilPemakaian() {
    const classes = useStyles();
	const [openJabatanKritikal, setOpenJabatanKritikal] = useState(false)
	const [openBaganSuksesi, setOpenBaganSuksesi] 		= useState(false)
	const [openRumpunJabatan, setOpenRumpunJabatan] 	= useState(false)
	const [openPenerapan, setOpenPenerapan] 	= useState(false)

	let search  	= useLocation().search;
    const tgl1 		= moment(new URLSearchParams(search).get('tgl1')).format('YYYY-MM-DD');
    const tgl2 		= moment(new URLSearchParams(search).get('tgl2')).format('YYYY-MM-DD');
	const select_id = new URLSearchParams(search).get('select_id')

	/** Jabatan Kritikal */
	const handleOpenJabatanKritikal = () => {
        setOpenJabatanKritikal(true);
    };
    
    const handleCloseJabatanKritikal = () => {
        setOpenJabatanKritikal(false);
		document.body.removeAttribute('style')
    };
	/** Bagan Suksesi */
	const handleOpenBaganSuksesi = () => {
        setOpenBaganSuksesi(true);
    };
    
	const handleCloseBaganSuksesi = () => {
        setOpenBaganSuksesi(false);
    };
	/** Rumpun Jabatan */
	const handleOpenRumpunJabatan = () => {
        setOpenRumpunJabatan(true);
    };
    
    const handleCloseRumpunJabatan = () => {
        setOpenRumpunJabatan(false);
		document.body.removeAttribute('style')
    };

	/** PENERAPAN */
	const handleOpenPenerapan = () => {
        setOpenPenerapan(true);
    };
    const handleClosePenerapan = () => {
        setOpenPenerapan(false);
		document.body.removeAttribute('style')
    };
	// console.log('openJabatanKritikal', openJabatanKritikal)
    return (
		<>
			<div style={{marginBottom: '2em', marginTop: '2em'}}>
				<Button color="primary" variant="contained" href="https://sikd.mkri.id/">Beranda</Button>{' '}
				<Button color="primary" variant="contained" href="https://sikd.mkri.id/index3.php?option=AdminReportRespontime">Manajemen Talenta</Button>{' '}
				<Button color="primary" variant="contained" href="https://sikd.mkri.id/index3.php?option=AdminReportRespontimePegawai">Respon Unit Kerja</Button>{' '}
				{/* <Button 
					color="primary" 
					variant="contained"
					// href="https://sikd.mkri.id/index3.php?option=AdminReportRespontimePegawai"
					onClick={handleOpenJabatanKritikal}
				>
					Jabatan Kritikal
				</Button>{' '}
				<Button 
					color="primary" 
					variant="contained" 
					// href="https://sikd.mkri.id/index3.php?option=AdminReportRespontimePegawai"
					onClick={handleOpenRumpunJabatan}
				>
					Rumpun Jabatan
				</Button>{' '}
				<Button 
					color="primary" 
					variant="contained" 
					// href="https://sikd.mkri.id/index3.php?option=AdminReportRespontimePegawai"
					onClick={handleOpenPenerapan}
				>
					Penerapan
				</Button>{' '} */}





				{/* <Button 
					color="primary" 
					variant="contained" 
					onClick={handleOpenBaganSuksesi}
					// href="https://sikd.mkri.id/index3.php?option=AdminReportRespontimePegawai"
				>
					Bagan Suksesi
				</Button>{' '} */}
			</div>

			{
				openJabatanKritikal == true
				? <JabatanKritikal 
					openJabatanKritikal={openJabatanKritikal}
					handleOpenJabatanKritikal={() => handleOpenJabatanKritikal()}
					handleCloseJabatanKritikal={() => handleCloseJabatanKritikal()}
					tgl1={tgl1}
					tgl2={tgl2}
					select_id={select_id}
				/>
				: ''
			}

			{
				openBaganSuksesi == true 
				? 	<BaganSuksesi 
						openBaganSuksesi={openBaganSuksesi}
						handleCloseBaganSuksesi={() => handleCloseBaganSuksesi()}
					/>
				: ''
			}

			{
				openRumpunJabatan == true 
				? 	<RumpunJabatan
						openRumpunJabatan={openRumpunJabatan}
						handleCloseRumpunJabatan={() => handleCloseRumpunJabatan()}
						handleOpenJabatanKritikal={() => handleOpenJabatanKritikal()}
						tgl1={tgl1}
						tgl2={tgl2}
						select_id={select_id}
					/>
				: ''
			}
			{
				openPenerapan == true 
				? 	<Penerapan
						openPenerapan={openPenerapan}
						handleClosePenerapan={() => handleClosePenerapan()}
						handleOpenPenerapan={() => handleOpenPenerapan()}
						tgl1={tgl1}
						tgl2={tgl2}
						select_id={select_id}
					/>
				: ''
			}
			
		</>


    );
}
const Footer = () => {
    const classes = useStyles();
	

    return (
        <footer className={classes.footer}>
            <Container>
                <Container maxWidth="xl">
                    <ProfilPemakaian /> 
                    <Divider style={{marginBottom: '2em'}}/>
                    <Typography variant="body1">Statistik Manajemen Talenta</Typography>
                    <Copyright />
                </Container>
            </Container>
        </footer>
    )
}

export default Footer