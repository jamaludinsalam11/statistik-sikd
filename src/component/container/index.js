import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Header from '../header/index';
import Content from '../content';
import Footer from './Footer';


const useStyles = makeStyles((theme) => ({
	rootContainer: {
		backgroundColor: '#2A0CBD',
		height: '90vh',
		minWidth: '1200px'
	},
	footer: {
		padding: theme.spacing(3, 2),
		textAlign: 'center',
		boxShadow: '0px 2px 1px -1px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 3px 0px rgba(0,0,0,0.12)',
		marginTop: '80px',
		backgroundColor: '#fff'
		// backgroundColor:
		//   theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[800],
	},

}));



const AppContainer = (theme) => {
	const classes = useStyles();
	return (
		<div className={classes.rootContainer}>
			<Container>
				<Header />
				<Content/> 
			</Container>
			<Footer/>

		</div>
	)
}


export default AppContainer;