import { PropTypes } from "prop-types";
import { Box, createTheme } from "@material-ui/core";
import { compose, palette, spacing, styleFunctionSx } from "@material-ui/system";
import styled from "styled-components";

const styleFunction = styleFunctionSx(compose(spacing, palette));

const Box = styled.div(styleFunction);

const theme = createTheme();

SvgIconStyle.propTypes = {
    src: PropTypes.string.isRequired,
    sx: PropTypes.object,
};

export default function SvgIconStyle({ src, sx }) {
    <Box 
        component="span"
        sx={{
            width: 24,
            height: 24,
            display: 'inline-block',
            bgcolor: 'currentColor',
            mask: `url(${src}) no-repeat center / contain`,
            WebkitMask: `url(${src}) no-repeat center / contain`,
            ...sx
        }}
    />
}