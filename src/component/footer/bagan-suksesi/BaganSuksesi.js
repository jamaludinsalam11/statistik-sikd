import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { Avatar, Button, Chip, ListItem, ListItemAvatar, ListItemText, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@material-ui/core';
import "./styles.css";
import datas from './data.json';
import Chart from './chart';
import MyComponent  from './fusioncharts'
import ComponentFS from './fusioncharts';


const useStyles = makeStyles((theme) => ({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      textAlign: 'center',
      // border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      width: '90vw',
      marginTop: '2em',
      // marginTop: '2em',
      height: 'auto'
    },
    
}));
export default function BaganSuksesi({
    openBaganSuksesi, 
    handleCloseBaganSuksesi, 
}) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [data, setData] = useState(datas)

    useEffect(() => {

    }, [openBaganSuksesi])

    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={openBaganSuksesi}
            onClose={handleCloseBaganSuksesi}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
            timeout: 500,
            }}
        >
            <Fade in={openBaganSuksesi}>
                <div className={classes.paper}>
                    <br></br>
                    <Typography variant='h2' >Bagan Suksesi</Typography>
                    <Typography variant='subtitle2' >Mahkamah Konstitusi Republik Indonesia</Typography>
                    {/* <h2 id="transition-modal-title">Transition modal</h2> */}
                    {/* <p id="transition-modal-description">Mahkamah Konstitusi Republik Indonesia</p> */}
                    {/* <br></br>
                    <br></br> */}
                    <div className="container">
                        {/* <ComponentFS/> */}
                        {/* <Chart /> */}
                        On Development
                    </div>
                </div>
            </Fade>
        </Modal>
    )
}