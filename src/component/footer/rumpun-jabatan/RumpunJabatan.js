import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { Avatar, Button, Chip, ListItem, ListItemAvatar, ListItemText, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@material-ui/core';
import FaceIcon from '@material-ui/icons/Face';
import { Table as BTable } from 'react-bootstrap';
import TableRumpunJabatan from './TableRumpunJabatan';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    textAlign: 'center',
    // border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width: '90vw',
    marginTop: '2em',
    // marginTop: '2em',
    // height: 'auto'


    maxHeight: '100%',
    overflow: 'auto'
  },
  table: {
    color: 'black'
  },
  titleColor: {
    color: 'rgba(0, 0, 0, 0.64)',
    // // borderRightStyle: "solid",
    // borderRightColor: "black",
    borderRight: '1px solid #bbb',
    border: '1.5px solid #bbb',
    display: "tableRowGroup"
  },
  textCell: {
    color: 'rgba(0, 0, 0, 0.64)',
    border: '1px solid #bbb',
  },
  cellJabatan: {
    color: 'rgba(0, 0, 0, 0.64)',
    border: '1px solid #bbb',
    width: 600,
    minWidth: 1,
  },
  container: {
    maxHeight: 640,
  },
  largeAvatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  modalStyle1:{
    position:'absolute',
    top:'10%',
    left:'10%',
    overflow:'scroll',
    height:'100%',
    display:'block',
    // display: 'flex',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  }
}));

export default function RumpunJabatan({ 
    openRumpunJabatan, 
    handleOpenRumpunJabatan,
    handleCloseRumpunJabatan, 
    tgl1, tgl2, select_id
}) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [scroll, setScroll] = useState('paper');
    const [result, setResult] = useState(null);
    const [jas, setJas] = useState(null);
    const [gas, setGas] = useState(null);

    useEffect(() => {       
        effect()
    }, [openRumpunJabatan, handleCloseRumpunJabatan])

    const effect = useCallback(async() => {
      try{
        const apiDev = `http://localhost:3001/sikd/api/v1/ninebox/rumpun-jabatan/active`;
        const apiProd = `https://sikd_jamal.mkri.id/services-new/sikd/api/v1/ninebox/rumpun-jabatan/active`;
        const api = process.env.NODE_ENV === 'development' ? apiDev : apiProd;
        // console.log('jabatankritikal-running:JAM ', tgl1, tgl2)
        const fetchData = await fetch(api);
        const result = await fetchData.json();
        setResult(result)
        setJas(result[0])
        setGas(result[1])

      } catch(e) {
        console.log(e)
      }
    })

    
    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={openRumpunJabatan}
            onClose={handleCloseRumpunJabatan}
            closeAfterTransition
            BackdropComponent={Backdrop}
            keepMounted
            BackdropProps={{
            timeout: 500,
            }}
        >
            <Fade in={openRumpunJabatan}>
            <div className={classes.paper}>
                <br></br>
                <Typography variant='h2' >Rumpun Jabatan</Typography>
                <Typography variant='subtitle2' >Mahkamah Konstitusi Republik Indonesia</Typography>
                <br></br>
                <br></br>
                <div style={{ marginTop: '1em', marginBottom: '1em', textAlign: 'left' }}>
                    <Typography variant='h2' >JAS</Typography>
                    <Typography variant='subtitle2' >Judiciary Administration System </Typography>
                    <Typography variant='subtitle2' >Total: {jas&&jas.data.length} </Typography>
                </div>

                <div>
                    <TableRumpunJabatan result={jas}/>
                </div>

                <div style={{ marginTop: '2em', marginBottom: '1em', textAlign: 'left' }}>
                    <Typography variant='h2' >GAS</Typography>
                    <Typography variant='subtitle2' >General Administration System </Typography>
                    <Typography variant='subtitle2' >Total: {gas&&gas.data.length} </Typography>
                </div>

                <div>
                    <TableRumpunJabatan result={gas}/>
                </div>
                
            </div>
            </Fade>
        </Modal>
    )
}