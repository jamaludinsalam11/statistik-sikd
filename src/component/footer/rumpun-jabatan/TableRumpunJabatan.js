import Table from 'react-bootstrap/Table';
import 'bootstrap/dist/css/bootstrap.min.css';
function TableRumpunJabatan({ result }) {
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th rowSpan={2}>No</th>
          <th rowSpan={2}>Nama Jabatan</th>
          <th rowSpan={2}>Unitkerja</th>
          <th colSpan={3}>Peta Jabatan</th>
        </tr>
        <tr>
          <th >Terisi</th>
          <th >Kebutuhan</th>
          <th >MPP</th>
        </tr>
      </thead>
      <tbody>
        {result&&result.data.map((row, i) => (
            <tr>
                <td style={{width: '5%'}}>{i+1}</td>
                <td style={{textAlign: 'left', width: '40%'}}>{row.nama_jabatan}</td>
                <td style={{textAlign: 'center', width: '30%'}}>{row.unitkerja}</td>
                <td style={{width: '5%'}}>{row.terisi}</td>
                <td style={{width: '5%'}}>{row.kebutuhan}</td>
                <td style={{width: '5%'}}>{row.mpp}</td>
            </tr>
        ))}
      </tbody>
    </Table>
  );
}

export default TableRumpunJabatan;