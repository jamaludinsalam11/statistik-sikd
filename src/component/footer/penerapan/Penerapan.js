import React, { Fragment, useCallback, useEffect, useState } from 'react';
import {useLocation} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { Avatar, Button, Chip, List, ListItem, ListItemAvatar, ListItemText, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@material-ui/core';
import FaceIcon from '@material-ui/icons/Face';
import moment from 'moment';
import PegawaiTeladan from '../../content/contentTabsPegawaiTeladan';
import Recharging from '../../content/contentTabsRecharging';
import RechargingSingapore from '../../content/contentTabsRechargingSingapore';
import PenetapanSekjen from '../../content/contentTabsPenetapanSekjen';
import Recharging2023 from '../../content/contentTabsRecharging2023';


const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    textAlign: 'center',
    // border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width: '90vw',
    marginTop: '2em',
    // marginTop: '2em',
    height: 'auto'
  },
  table: {
    color: 'black'
  },
  titleColor: {
    color: 'rgba(0, 0, 0, 0.64)',
    // // borderRightStyle: "solid",
    // borderRightColor: "black",
    borderRight: '1px solid #bbb',
    border: '1.5px solid #bbb',
    display: "tableRowGroup"
  },
  textCell: {
    color: 'rgba(0, 0, 0, 0.64)',
    border: '1px solid #bbb',
  },
  cellJabatan: {
    color: 'rgba(0, 0, 0, 0.64)',
    border: '1px solid #bbb',
    width: 600,
    minWidth: 1,
  },
  container: {
    maxHeight: 640,
  },
  largeAvatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));

export default function Penerapan({ 
    openPenerapan, 
    handleOpenJabatanKritikal,
    handleClosePenerapan, 
    tgl1, tgl2, select_id
}) {
    const classes = useStyles();
    let search  = useLocation().search
    const roleid = new URLSearchParams(search).get('select_id')
    const tgl11 = new URLSearchParams(search).get('tgl1')
    const tgl22 = new URLSearchParams(search).get('tgl2')
    const tahunn = moment(tgl11).format('YYYY')
    const [open, setOpen] = React.useState(false);
    const [data, setData] = useState(null)

    useEffect(() => {       
        effect()
    }, [openPenerapan, handleClosePenerapan])

    const effect = useCallback(async() => {
      try{
        const apiDev = `http://localhost:3001/sikd/api/v1/ninebox/jabatan-kritikal/active`;
        const apiProd = `https://sikd_jamal.mkri.id/services-new/sikd/api/v1/ninebox/jabatan-kritikal/active`;
        const api = process.env.NODE_ENV === 'development' ? apiDev : apiProd;
        // console.log('jabatankritikal-running:JAM ', tgl1, tgl2)
        const fetchData = await fetch(api);
        const result = await fetchData.json();
        setData(result)

      } catch(e) {
        console.log(e)
      }
    })

    
    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={openPenerapan}
            onClose={handleClosePenerapan}
            closeAfterTransition
            BackdropComponent={Backdrop}
            keepMounted
            BackdropProps={{
            timeout: 500,
            }}
        >
            <Fade in={openPenerapan}>
            <div className={classes.paper}>
                <br></br>
                <Typography variant='h2' >Penerapan Manajemen Talenta</Typography>
                <Typography variant='subtitle2' >Mahkamah Konstitusi Republik Indonesia</Typography>
                <Typography variant='subtitle2' >{tahunn}</Typography>
                {/* <h2 id="transition-modal-title">Transition modal</h2> */}
                {/* <p id="transition-modal-description">Mahkamah Konstitusi Republik Indonesia</p> */}
                <br></br>
                <br></br>
                <Paper style={{maxHeight: 600, overflow: 'auto'}}>
                <List>
                <div>
                    {
                        tahunn === '2022' 
                        ? <PenetapanSekjen/>
                        : ''
                    }

                </div>
                <div>
                    {
                        tahunn === '2022' 
                        ? <RechargingSingapore/>
                        : ''
                    }

                </div>
                <div>
                    {
                        tahunn === '2022' 
                        ? <PegawaiTeladan/>
                        : ''
                    }

                </div>
                <div>
                    {
                        tahunn === '2022' 
                        ? <Recharging/>
                        : tahunn === '2023'
                        ? <Recharging2023/>
                        : '' 
                    }

                </div>
                </List>
                </Paper>
            </div>
            </Fade>
        </Modal>
    )
}