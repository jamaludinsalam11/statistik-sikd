import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { Avatar, Button, Chip, ListItem, ListItemAvatar, ListItemText, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@material-ui/core';
import FaceIcon from '@material-ui/icons/Face';


const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    textAlign: 'center',
    // border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width: '90vw',
    marginTop: '2em',
    // marginTop: '2em',
    height: 'auto'
  },
  table: {
    color: 'black'
  },
  titleColor: {
    color: 'rgba(0, 0, 0, 0.64)',
    // // borderRightStyle: "solid",
    // borderRightColor: "black",
    borderRight: '1px solid #bbb',
    border: '1.5px solid #bbb',
    display: "tableRowGroup"
  },
  textCell: {
    color: 'rgba(0, 0, 0, 0.64)',
    border: '1px solid #bbb',
  },
  cellJabatan: {
    color: 'rgba(0, 0, 0, 0.64)',
    border: '1px solid #bbb',
    width: 600,
    minWidth: 1,
  },
  container: {
    maxHeight: 640,
  },
  largeAvatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));

export default function JabatanKritikal({ 
    openJabatanKritikal, 
    handleOpenJabatanKritikal,
    handleCloseJabatanKritikal, 
    tgl1, tgl2, select_id
}) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [data, setData] = useState(null)

    useEffect(() => {       
        effect()
    }, [openJabatanKritikal, handleCloseJabatanKritikal])

    const effect = useCallback(async() => {
      try{
        const apiDev = `http://localhost:3001/sikd/api/v1/ninebox/jabatan-kritikal/active`;
        const apiProd = `https://sikd_jamal.mkri.id/services-new/sikd/api/v1/ninebox/jabatan-kritikal/active`;
        const api = process.env.NODE_ENV === 'development' ? apiDev : apiProd;
        // console.log('jabatankritikal-running:JAM ', tgl1, tgl2)
        const fetchData = await fetch(api);
        const result = await fetchData.json();
        setData(result)

      } catch(e) {
        console.log(e)
      }
    })

    
    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={openJabatanKritikal}
            onClose={handleCloseJabatanKritikal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            keepMounted
            BackdropProps={{
            timeout: 500,
            }}
        >
            <Fade in={openJabatanKritikal}>
            <div className={classes.paper}>
                <br></br>
                <Typography variant='h2' >Jabatan Kritikal</Typography>
                <Typography variant='subtitle2' >Mahkamah Konstitusi Republik Indonesia</Typography>
                {/* <h2 id="transition-modal-title">Transition modal</h2> */}
                {/* <p id="transition-modal-description">Mahkamah Konstitusi Republik Indonesia</p> */}
                <br></br>
                <br></br>
           
                <div>
                    <TableContainer component={Paper} className={classes.container}>
                        <Table className={classes.table}   aria-label="simple table"  style={{ tableLayout: "auto" }}>
                            <TableHead style={{ backgroundColor: '#c5f2fa'}}>
                                <TableRow >
                                    <TableCell rowSpan={2} className={classes.titleColor} style={{width: '5%', borderBottom: 'none'}}>
                                        No
                                    </TableCell>
                                    <TableCell rowSpan={2}  align="left" className={classes.titleColor} style={{ minWidth: '350px' }}>
                                        <Typography>Jabatan</Typography> 
                                    </TableCell>
                                    <TableCell colSpan={4}   align="center" className={classes.titleColor} >
                                        Pegawai
                                    </TableCell>
                                    <TableCell colSpan={3}  align="left" className={classes.titleColor} >
                                        Jabatan mempunyai sifat tugas perumusan kebijakan, pelaksanaan kebijakan dan evaluasi kebijakan
                                    </TableCell>
                                    <TableCell colSpan={3}  align="left" className={classes.titleColor} >
                                        Jabatan yang bersifat strategis dan berkaitan langsung dengan strategi organisasi dan perkembangan lingkungan
                                    </TableCell>
                                    <TableCell colSpan={3}  align="left" className={classes.titleColor} >
                                        Jabatan yang memerlukan kompetensi yang sesuai dengan core bussines
                                    </TableCell>
                                    <TableCell colSpan={3}  align="left" className={classes.titleColor} >
                                        Jabatan yang membutuhkan kinerja yang tinggi dengan mempertimbangkan waktu penyelesaian pekerjaan yang dilakukan dalam satu tahun melebihi standar jam kerja dalam satu tahun 
                                    </TableCell>
                                    <TableCell colSpan={3}  align="left" className={classes.titleColor} >
                                        Jabatan yang memberi peluang pembelajaran yang tinggi
                                    </TableCell>
                                    <TableCell colSpan={3}  align="left" className={classes.titleColor} >
                                        Jabatan yang mendorong perubahan dan percepatan pembangunan dan pelayanan publik
                                    </TableCell>
                                    <TableCell colSpan={3}  align="left" className={classes.titleColor} >
                                        Jabatan yang sesuai kebutuhan prioritas organisasi
                                    </TableCell>
                                    <TableCell colSpan={3}  align="left" className={classes.titleColor} >
                                    Karakteristik lainnya (ditambahkan sesuai karakteristik teknis masing-masing UKE I
                                    </TableCell>
                                    <TableCell rowSpan={2} className={classes.titleColor} style={{width: '5%'}}>
                                        Nilai Total
                                    </TableCell>
                            

                                </TableRow>
                                <TableRow>
                                    {/* <TableCell rowSpan={2} align="left" className={classes.titleColor} style={{width: '5%'}}></TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor} style={{width: '20%'}}></TableCell> */}
                                    {/* <TableCell rowSpan=align="left" {2} className={classes.titleColor}>No</TableCell> */}
                                    {/* <TableCell rowSpan={2} className={classes.titleColor} style={{width: '5%'}}>
                                        No
                                    </TableCell>
                                    <TableCell rowSpan={2}  align="left" className={classes.titleColor} style={{width: '30%'}}>
                                        <Typography>Jabatan</Typography> 
                                    </TableCell> */}
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}  style={{ minWidth: '300px'}} >Nama</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor} style={{ minWidth: '5px'}}>NBV</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Kuadran</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Box</TableCell>

                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Klasifikasi</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Nilai</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Persentase (10%) dan Nilai Terbobot</TableCell>
                                    
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Klasifikasi</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Nilai</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Persentase (10%) dan Nilai Terbobot</TableCell>
                                    
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Klasifikasi</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Nilai</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Persentase (10%) dan Nilai Terbobot</TableCell>

                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Klasifikasi</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Nilai</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Persentase (10%) dan Nilai Terbobot</TableCell>
                                    
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Klasifikasi</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Nilai</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Persentase (10%) dan Nilai Terbobot</TableCell>
                                    
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Klasifikasi</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Nilai</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Persentase (10%) dan Nilai Terbobot</TableCell>
                                    
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Klasifikasi</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Nilai</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Persentase (10%) dan Nilai Terbobot</TableCell>
                                    
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Klasifikasi</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Nilai</TableCell>
                                    <TableCell rowSpan={2} align="left" className={classes.titleColor}>Persentase (10%) dan Nilai Terbobot</TableCell>
                                
                                    {/* <TableCell rowSpan={2} className={classes.titleColor} style={{width: '5%'}}>
                                        Nilai Total
                                    </TableCell> */}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                            {data&&data.map((row, i) => (
                                <Fragment>
                                    <TableRow key={i} >
                                        <TableCell 
                                            className={classes.textCell}
                                            rowSpan={row.data.length + 1}
                                        >
                                            {i+1}
                                        </TableCell>
                                        <TableCell 
                                            className={classes.textCell}
                                            rowSpan={row.data.length + 1}
                                            size='small'
                                            style={{ minWidth: '350px' }}
                                        >
                                           
                                            <Typography variant='h4'>{row.jabatan}</Typography>
                                            <div><Typography variant='caption'>Jenjang Jabatan: </Typography></div> 
                                            <div><Typography variant='subtitle1'>{row.jenjang_jabatan} </Typography></div> 
                                            {/* <Chip
                                                icon={<FaceIcon />}
                                                label= {row.jenjang_jabatan}
                                                clickable
                                                color="primary"
                                                size='small'
                                            /> */}
                                            <div> <Typography variant='caption'>Kelompok Jabatan: </Typography></div>
                                            <div><Typography variant='subtitle1'>{row.kelompok_jabatan} </Typography></div> 
                                            {/* <Chip
                                                icon={<FaceIcon />}
                                                label= {row.kelompok_jabatan}
                                                clickable
                                                color="primary"
                                                size='small'
                                            /> */}
                                        </TableCell>
                                        {/* {/* <TableCell align="right" className={classes.titleColor} >{row.calories}</TableCell> */}
                                        {/* <TableCell align="right" className={classes.titleColor}>{row.kelompok_jabatan}</TableCell>
                                        <TableCell align="right" className={classes.titleColor}>{row.jenjang_jabatan}</TableCell> */}
                                    </TableRow>
                                    {row&&row.data.map(pegawai => (
                                        <TableRow>
                                            <TableCell 
                                                className={classes.textCell}
                                                // rowSpan={row.data.length + 1}
                                            >
                                                {/* <div> </div> */}
                                                <ListItem>
                                                    <ListItemAvatar>
                                                    <Avatar alt={pegawai.people_name} src={pegawai.foto}/>
                                                        {/* <AccountCircleIcon /> */}
                                                    {/* </Avatar> */}
                                                    </ListItemAvatar>
                                                    <ListItemText primary={pegawai.people_name} secondary={pegawai.jabatan}/>
                                                </ListItem>
                                                {
                                                    pegawai.role_id === 'uk.1.1' || pegawai.id == 0
                                                    ? '' 
                                                    :  <div style={{textAlign: 'left', marginLeft: '5em'}}>
                                                            <Button 
                                                                variant="contained" 
                                                                size="small" 
                                                                color="primary" 
                                                                onClick={() => window.open(
                                                                   ( process.env.NODE_ENV === 'development' 
                                                                    ? `statistik?select_id=${pegawai.role_id}&tgl1=${tgl1} 00:00:00&tgl2=${tgl2} 23:59:59`
                                                                    : `?select_id=${pegawai.role_id}&tgl1=${tgl1} 00:00:00&tgl2=${tgl2} 23:59:59`), '_blank'
                                                                )}
                                                            >
                                                            View Profile
                                                            </Button>
                                                        </div>
                                                }
                                               
                                                
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.nilai}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.kuadran}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.box}
                                            </TableCell>     

                                            {/* T1 */}
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t1_klasifikasi}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t1_nilai}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t1_presentase}
                                            </TableCell>  

                                             {/* =============== T2 ====================== */}
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t2_klasifikasi}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t2_nilai}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t2_presentase}
                                            </TableCell>      

                                             {/* =============== T3 ====================== */}
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t3_klasifikasi}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t3_nilai}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t3_presentase}
                                            </TableCell>      

                                              {/* =============== T4 ====================== */}
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t4_klasifikasi}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t4_nilai}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t4_presentase}
                                            </TableCell>     

                                              {/* =============== T5 ====================== */}
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t5_klasifikasi}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t5_nilai}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t5_presentase}
                                            </TableCell>  

                                            {/* =============== T6 ====================== */}
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t6_klasifikasi}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t6_nilai}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t6_presentase}
                                            </TableCell>      

                                            {/* =============== T7 ====================== */}
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t7_klasifikasi}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t7_nilai}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t7_presentase}
                                            </TableCell>          

                                            {/* =============== T8 ====================== */}
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t8_klasifikasi}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t8_nilai}
                                            </TableCell>                                          
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.t8_presentase}
                                            </TableCell>         

                                            {/* =============== NILAI KLASIFIKASI JABATAN KRITIKAL ====================== */}
                                            <TableCell 
                                                className={classes.textCell}
                                            >
                                                {pegawai.nilai_kjk}
                                            </TableCell>                               
                                        </TableRow>
                                        
                                    ))}
                                    
                                </Fragment>
                            ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            </div>
            </Fade>
        </Modal>
    )
}