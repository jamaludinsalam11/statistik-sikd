import React,{useEffect,useState} from 'react'
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import { green } from '@material-ui/core/colors';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import PeopleIcon from '@material-ui/icons/PeopleOutlined';
import AlarmOnIcon from '@material-ui/icons/AlarmOn';
import Message from './Message';
import Icon1 from './Icon1';

const JamKerjaPerMinggu = (props) => {
  const [data, setData] = useState(null)

  useEffect(() => {
    const fetchData = () => {
      setData(props.data)
    }
    fetchData()
  }, [props.data])
  // console.log('data', data)

  return (
    <>
      <Card {...props}
        style={{height: '150px'}}
      >
        <CardContent>
          <Grid
            container
            spacing={3}
            style={{justifyContent: 'space-between'}}
          >
            <Grid item>
              <Typography
                color="textSecondary"
                gutterBottom
                variant="h6"
                // style={{fontSize: '11px'}}
              >
                Rata-rata Jam Kerja per Minggu
              </Typography>
              <Typography
                color="textPrimary"
                variant="h3"
                style={{color: 'black'}}
              >
                { data == null
                  ? <Skeleton animation="wave" height={40} variant="text" style={{backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                  : `${data && data.jamkerja_minggu} Jam`
                }
                
              </Typography>
            </Grid>
            { data == null 
              ? <Skeleton animation="wave" variant="circle" width={55} height={55} style={{marginRight: '1em', marginTop: '.8em', backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
              : <Icon1 minusPositive={data&&data.min_positive_mingguan}/>
            }
            
          </Grid>
          {
            data == null 
            ? <Skeleton animation="wave"  variant="text" style={{ marginTop: '1em',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
            : <Message data={data&&data.percentage_mingguan} minusPositive={data&&data.min_positive_mingguan}/>
          }
         
        </CardContent>
      </Card>
    </>
  )
};

export default JamKerjaPerMinggu;
