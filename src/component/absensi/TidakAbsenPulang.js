import React,{useEffect,useState} from 'react'
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';
import { red, green } from '@material-ui/core/colors';
import ErrorIcon from '@material-ui/icons/Error';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';

const TidakAbsenPulang = (props) => {
  const [data, setData] = useState(null)

  useEffect(() => {
    const fetchData = () => {
      setData(props.data)
    }
    fetchData()
  }, [props.data])
  return (
    <Card
      sx={{ height: '100%' }}
      {...props}
      style={{height: '150px'}}
    >
      <CardContent>
        <Grid
          container
          spacing={3}
          sx={{ justifyContent: 'space-between' }}
          style={{justifyContent: 'space-between' }}
        >
          <Grid item>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="h6"
            >
              Tidak Absen Pulang
            </Typography>
            <Typography
              color="textPrimary"
              variant="h3"
              style={{color: 'black'}}
            >
              {
                data == null
                ? <Skeleton animation="wave" height={40} variant="text" style={{backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                : `${data && data.tidakabsen_pulang} kali `
              }
              {/* {data && data.tidakabsen_pulang} kali  */}
            </Typography>
            {/* <Typography
              color="textSecondary"
              variant="caption"
              // style={{color: 'black'}}
            >
              0 jam 4 menit
            </Typography> */}
          </Grid>
          {
            data == null 
            ? <Skeleton animation="wave" variant="circle" width={55} height={55} style={{marginRight: '1em', marginTop: '.8em', backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
            : <Grid item>
                <Avatar
                  sx={{
                    backgroundColor: red[600],
                    height: 56,
                    width: 56
                  }}
                  style={{
                    backgroundColor: data && data.tidakabsen_pulang == 0 ? green[600] : red[600],
                    height: 56,
                    width: 56
                  }}
                >
                  
                  {data && data.tidakabsen_pulang == 0 ? <AssignmentTurnedInIcon/> :<ErrorIcon /> }
                </Avatar>
              </Grid>
          }
          
        </Grid>
        <Box
          sx={{
            pt: 2,
            display: 'flex',
            alignItems: 'center'
          }}
          style={{
            paddingTop: '1em',
            display: 'flex',
            alignItems: 'center'
          }}
        >
          <ArrowDownwardIcon sx={{ color: green[900] }} />
          {/* <Typography
            sx={{
              color: red[900],
              mr: 1
            }}
            variant="body2"
            style={{
              color: red[900],
              mr: '1em'
            }}
          >
            12%
          </Typography> */}
          {/* <Typography
            color="textSecondary"
            variant="caption"
            // style={{color: 'black'}}
          >
            0 jam 4 menit
          </Typography> */}
        </Box>
      </CardContent>
    </Card>
  )
};

export default TidakAbsenPulang;
