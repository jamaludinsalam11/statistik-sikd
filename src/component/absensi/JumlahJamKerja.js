import React,{useEffect,useState} from 'react'
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Chip,
  Typography
} from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';
import { red } from '@material-ui/core/colors';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import Message from './Message';
import Icon1 from './Icon1';

const JumlahJamKerja = (props) => {
  const [data, setData] = useState(null)

  useEffect(() => {
    const fetchData = () => {
      // console.log('received props from props.data:', props.data)
      setData(props.data)
    }
    fetchData()
  }, [props.data])
  // console.log('loading', props.loading)
  // console.log('data', data)
  return (
    <Card
      sx={{ height: '100%' }}
      style={{height: '150px'}}
      {...props}
    >
      <CardContent>
        <Grid
          container
          spacing={3}
          style={{justifyContent: 'space-between'}}
        >
          <Grid item>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="h6"
            >
              Jumlah Jam Kerja
            </Typography>
            
            <Typography
               color="textPrimary"
               variant="h3"
               style={{color: 'black', paddingLeft: '0em'}}
            >
              
              {data == null 
                ? <Skeleton animation="wave" height={40} variant="text" style={{backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                : `${data && data.jamkerja_total} Jam`
              }
              
            </Typography>
          </Grid>
          {/* ? <Skeleton animation="wave" variant="circle" width={60} height={60} style={{paddingRight: '1em', marginTop: '.8em', backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/> */}
          {data == null
            ? <Skeleton animation="wave" variant="circle" width={55} height={55} style={{marginRight: '1em', marginTop: '.8em', backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
            : <Icon1 minusPositive={data&&data.min_positive_total}/>
          }
          
        </Grid>
        {
          data == null 
            ? <Skeleton animation="wave"  variant="text" style={{ marginTop: '1em',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
            : <Message data={data&&data.percentage_total} minusPositive={data&&data.min_positive_total} />
        }
        
      </CardContent>
    </Card>
  )
};

export default JumlahJamKerja;
