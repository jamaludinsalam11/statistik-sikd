import React,{useEffect,useState} from 'react'
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Chip,
  Typography
} from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';
import { red, green } from '@material-ui/core/colors';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import AlarmOnIcon from '@material-ui/icons/AlarmOn';
import Message from './Message';

const Icon1 = (props) => {
    const [data, setData] = useState(null)
    const [minusPositive, setMinusPositive] = useState(null)

    useEffect(() => {
        const fetchMessage = () => {
            setData(props.data)
            setMinusPositive(props.minusPositive === 'minus' ? 'minus' : props.minusPositive === 'positive' ? 'positive' : 'equals')
        }
        fetchMessage()
    },[props.data, props.minusPositive])
    return (
        <Grid item>
            
            <Avatar
              sx={{
                backgroundColor: red[600],
                height: 56,
                width: 56
              }}
              style={{backgroundColor: minusPositive && minusPositive === 'minus' ? red[600]: minusPositive === 'positive' ? green[600] : green[600] , height: 56, 
              width: 56}}
            >
                {minusPositive && minusPositive === 'minus'
                    ? <ErrorOutlineIcon />
                    : minusPositive === 'positive'
                    ? <AlarmOnIcon />
                    : <AlarmOnIcon />
                }
                
            </Avatar>
        </Grid>
    )
}

export default Icon1