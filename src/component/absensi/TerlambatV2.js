import React,{useEffect,useState} from 'react'
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';
import { red, green } from '@material-ui/core/colors';
import ErrorIcon from '@material-ui/icons/Error';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';

const TerlambatV2 = (props) => {
  const [data, setData] = useState(null)

  useEffect(() => {
    const fetchData = () => {
      setData(props.data)
    }
    fetchData()
  }, [props.data])
  return (
    <Card
      sx={{ height: '100%' }}
      {...props}
      style={{height: '120px'}}
    >
      <CardContent>
        <Grid
          container
          spacing={3}
          sx={{ justifyContent: 'space-between' }}
          style={{justifyContent: 'space-between' }}
        >
          <Grid item>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="h6"
            >
              Terlambat
            </Typography>
            <Typography
              color="textPrimary"
              variant="h3"
              style={{color: 'rgba(0, 0, 0, 0.64)', fontWeight: '600',  paddingLeft: '1em'}}
            >
              --,-
              {/* {
                data == null
                ? <Skeleton animation="wave" height={40} variant="text" style={{backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                : `${data && data.telat} kali `
              } */}
            </Typography>
            {/* <Typography
              color="textSecondary"
              variant="caption"
              // style={{color: 'black'}}
            >
              -- jam -- menit
            </Typography> */}
          </Grid>


          {/* {
            data == null 
            ? <Skeleton animation="wave" variant="circle" width={55} height={55} style={{marginRight: '1em', marginTop: '.8em', backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
            : <Grid item>
                <Avatar
                  sx={{
                    backgroundColor: red[600],
                    height: 56,
                    width: 56
                  }}
                  style={{
                    backgroundColor: data && data.telat == 0 ? green[600] : red[600],
                    height: 56,
                    width: 56
                  }}
                >
                  
                  {data && data.telat == 0 ? <AssignmentTurnedInIcon/> :<ErrorIcon /> }
                </Avatar>
              </Grid>
          } */}

              <Grid item>
                <Avatar
                  sx={{
                    backgroundColor: red[600],
                    height: 56,
                    width: 56
                  }}
                  style={{
                    backgroundColor: green[600] ,
                    height: 56,
                    width: 56
                  }}
                >
                  
                  <AssignmentTurnedInIcon/>
                </Avatar>
              </Grid>
          
        </Grid>
        <Box
          sx={{
            pt: 2,
            display: 'flex',
            alignItems: 'center'
          }}
          style={{
            paddingTop: '1em',
            display: 'flex',
            alignItems: 'center',
            marginLeft: '-1.5em'
          }}
        >
          <ArrowDownwardIcon sx={{ color: red[900] }} />
          {/* <Typography
            sx={{
              color: red[900],
              mr: 1
            }}
            variant="body2"
            style={{
              color: red[900],
              mr: '1em'
            }}
          >
            12%
          </Typography> */}
          {/* {
            data == null 
            ? <Skeleton animation="wave"  variant="text" style={{ width: '100%' ,marginTop: '.1em',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
            : <Typography
                color="textSecondary"
                variant="caption"
                // style={{color: 'black'}}
              >
                
                {data && data.telat == 0 ? 'Tidak pernah Terlambat' : `${data && data.telat_jam} Jam ${data && data.telat_minute}  Menit` }
              </Typography>
          } */}

            <div style={{marginTop: '-1.2em'}}>
              <Typography
                color="textSecondary"
                variant="caption"
                // style={{color: 'black'}}
              >
                
               *Dalam pengembangan
              </Typography>
            </div>
        </Box>
      </CardContent>
    </Card>
  )
};

export default TerlambatV2;
