import React,{useEffect,useState} from 'react'
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Chip,
  Typography
} from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import { red, green } from '@material-ui/core/colors';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import AlarmOnIcon from '@material-ui/icons/AlarmOn';
import MoneyIcon from '@material-ui/icons/Money';
import Message from './Message';
import Icon1 from './Icon1';
import Message2 from './Message2';

const JamKerjaPerHariV2 = (props) => {
  const [data, setData] = useState(null)
  const [ratarata, setRataRata] = useState(null)

  useEffect(() => {
    const fetchData = () => {
      // console.log('received props from props.data:', props.data)
      setData(props.data)
      setRataRata(props.ratarata)
    }
    fetchData()
  }, [props.data])
  // console.log('loading', props.loading)
  // console.log('data', data)
  return (
    <Card
      sx={{ height: '100%' }}
      style={{height: '120px'}}
      {...props}
    >
      <CardContent>
        <Grid
          container
          spacing={3}
          style={{justifyContent: 'space-between'}}
        >
          <Grid item style={{textAlign: 'left'}}> 
            <Typography
              color="textSecondary"
              gutterBottom
              variant="h6"
            >
              Rata-rata Jam Kerja per Hari
            </Typography>
            
            <Typography
               color="textPrimary"
               variant="h3"
               style={{color: 'rgba(0, 0, 0, 0.64)', fontWeight: '600',  paddingLeft: '1em'}}
            >
              
              {data == null 
                ? <Skeleton animation="wave" height={40} variant="text" style={{backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
                : `${data && data.per_hari} Jam`
              }
              
            </Typography>
          </Grid>
         
          {data == null
            ? <Skeleton animation="wave" variant="circle" width={55} height={55} style={{marginRight: '1em', marginTop: '.8em', backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/>
            : <Grid item>
            
                <Avatar
                  sx={{
                    backgroundColor: red[600],
                    height: 56,
                    width: 56
                  }}
                  style={{backgroundColor:  green[600] , height: 56, 
                  width: 56}}
                >
                  <AlarmOnIcon />
                </Avatar>
            </Grid>
          }
        </Grid>
        {
          ratarata == null 
            ? <div style={{marginTop: '-1.8em'}}><Skeleton animation="wave"  variant="text" style={{ marginTop: '1em',backgroundColor: 'rgba(221, 221, 221, 0.5)'}}/></div>
            : <div style={{marginTop: '-1.2em'}}><Message2 data={data&&data.percentageRatarata} minusPositive={data&&data.minusPositive} /></div>
        }
        
      </CardContent>
    </Card>
  )
};

export default JamKerjaPerHariV2;
