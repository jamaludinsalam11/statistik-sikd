import React,{useState, useEffect} from 'react'
import {
    Avatar,
    Box,
    Card,
    CardContent,
    Grid,
    Typography
  } from '@material-ui/core';
import { green, red } from '@material-ui/core/colors';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';


const Message = (props) => {
    const [data, setData] = useState(null)
    const [minusPositive, setMinusPositive] = useState(null)

    useEffect(() => {
        const fetchMessage = () => {
            setData(props.data)
            setMinusPositive(props.minusPositive === 'minus' ? 'minus' : props.minusPositive === 'positive' ? 'positive' : 'equals')
        }
        fetchMessage()
    },[props.data, props.minusPositive])
    // console.log(minusPositive)
    return (
        <>
        <Box
          sx={{
            pt: 2,
            display: 'flex',
            alignItems: 'center'
          }}
          style={{paddingTop: '1em', 
            display: 'flex',
            alignItems: 'center'}}
        >
            {minusPositive && minusPositive === 'minus' 
                ? <ArrowDownwardIcon sx={{ color: red[900] }} style={{color: 'red'}} />
                : minusPositive === 'positive' 
                ? <ArrowUpwardIcon sx={{ color: green[900] }} style={{color: green[900]}} />
                : <PlaylistAddCheckIcon sx={{ color: green[900] }} style={{color: green[500]}} />
            }
            <Typography
                sx={{
                color: red[900],
                mr: 1
                }}
                variant="body2"
                style={{color: 'black', opacity: .6,mr: 1}}
            >
                {minusPositive && minusPositive === 'minus'
                    ? data
                    : minusPositive === 'positive'
                    ? data
                    : ''
                }
            </Typography>
            <Typography
                color="textSecondary"
                variant="caption"
                style={{textAlign: 'left', paddingLeft: '1em',}}
            >
                
                {minusPositive && minusPositive === 'minus' 
                    ? 'Lebih rendah dari total yang seharusnya'
                    : minusPositive === 'positive' 
                    ? 'Lebih tinggi dari total yang seharusnya'
                    : 'Sama dengan total yang seharusnya'
                }
            </Typography>
        </Box>
        </>
    )
}
export default Message;