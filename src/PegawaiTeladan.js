import React from 'react';
import { ThemeProvider } from '@material-ui/core';
import theme from './theme';
import GlobalStyles from './theme/GlobalStyles';

import AppContainer from './component/container';

function PegawaiTeladan() {
  // console.log('roleatasann', window.ra)
  return (
    <ThemeProvider theme={theme}>
      <h1>{process.env.TITLE_WEB}</h1>
      <h3>Pegawai Teladan</h3>
    </ThemeProvider>
  );
}

export default PegawaiTeladan;
